package com.yj.sentence;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.Calendar;

import com.yj.sentence.common.Define;
import com.yj.sentence.database.UserDBManager;
import com.yj.sentence.structs.UserData;
import com.yj.sentence.ui.setting.EsSettingActivity;
import com.yj.sentence.ui.studding.EsStuddingActivity;
import com.yj.sentence.ui.studding.SekWordPopup;
import com.yj.sentence.ui.studding.SekWordPopupOneButton;
import com.yj.sentence.ui.studding.study.StudyPronunciationActivity;
import com.yj.sentence.ui.studding.util.EsStuddingRecognizer;
import com.yj.sentence.ui.studyresult.EsStudyResultActivity;
import com.yj.sentence.ui.usereg.EsUserListActivity;
import com.yj.sentence.ui.words.EsRegistryActivity;
import com.yj.sentence.utils.ESUtils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * 
 * @author LJG
 * 2015.6.26
 * 프로그람이 기동하여 보여지는 첫 화면
 */
public class MainActivity extends Activity implements View.OnClickListener
{
   
	public final static String FONT_PATH = "fonts/KPCHOLIM.ttf";
	public final static String MAIN_APP_ID = "com.sforeignpad.sekmain";
	private static String DICT_APP_ID = "com.app.dic.samhung1";

	private TextView m_tvOrder = null;
	
	private UserDBManager mDbMana = null;
	private UserData mUserData = null;
	
	private UserOrderReceiver m_receiver = null;

	static {
		System.loadLibrary("native-lib");
	}

	public native boolean isVerified();

	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

		//CreateDialogs();

		String[] perms = {"android.permission.READ_EXTERNAL_STORAGE", "android.permission.WRITE_EXTERNAL_STORAGE"
				,"android.permission.RECORD_AUDIO" ,"android.permission.CAMERA", "android.permission.WRITE_SETTINGS"};

		int permsRequestCode = 200;
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
			requestPermissions(perms, permsRequestCode);
		}

		/*if(!ESUtils.isVerified()){
			finish();
		}*/

		if(!isVerified())
		{
	//		finish();
		}

//		initDateViews();
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int width=dm.widthPixels;
        int height=dm.heightPixels;

        float density = getResources().getDisplayMetrics().density;

        
        Log.e("TEST", String.format("width = %d, height = %d, density = %f",  width, height, density));

       
        if(!createDB(this))
        	this.finish();
        
        initViews();
        registerReceiver();

		ESUtils.initializeSounds(this);
		CreateTemp();

		new AsyncTask<Void, Void, Exception>() {

			@Override
			protected Exception doInBackground(Void... params) {
				EsStuddingRecognizer recognizer = EsStuddingRecognizer.getInstance();
				recognizer.setContext(MainActivity.this);
				recognizer.initialize();
				return null;
			}

			@Override
			protected void onPostExecute(Exception result) {
			}
		}.execute();

		//HashSet<String> strTmp = getExternalMounts();
	}


	private void CreateDialogs(){
		SekWordPopup popup;
		popup = new SekWordPopup(MainActivity.this);
		popup.setMessage(this.getString(R.string.stop_study_pronunciation));
		popup.setListener(new SekWordPopup.OnSEKLaunchListener() {

			@Override
			public void onOK() {
			}
			@Override
			public void onCancel() {
				// TODO Auto-generated method stub
			}
		});
		popup.show();

		SekWordPopup popup1;
		popup1 = new SekWordPopup(MainActivity.this);
		popup1.setMessage(this.getString(R.string.app_back));
		popup1.setListener(new SekWordPopup.OnSEKLaunchListener() {

			@Override
			public void onOK() {
			}
			@Override
			public void onCancel() {
				// TODO Auto-generated method stub
			}
		});
		popup1.show();

		SekWordPopup popup2;
		popup2 = new SekWordPopup(MainActivity.this);
		popup2.setMessage(this.getString(R.string.app_test_count));
		popup2.setListener(new SekWordPopup.OnSEKLaunchListener() {

			@Override
			public void onOK() {
			}
			@Override
			public void onCancel() {
				// TODO Auto-generated method stub
			}
		});
		popup2.show();

		SekWordPopup popup3;
		popup3 = new SekWordPopup(MainActivity.this);
		popup3.setMessage("2" + this.getString(R.string.start_study_order));
		popup3.setListener(new SekWordPopup.OnSEKLaunchListener() {

			@Override
			public void onOK() {
			}
			@Override
			public void onCancel() {
				// TODO Auto-generated method stub
			}
		});
		popup3.show();

		SekWordPopup popup4;
		popup4 = new SekWordPopup(MainActivity.this);
		popup4.setMessage(this.getString(R.string.noscheduledata));
		popup4.setListener(new SekWordPopup.OnSEKLaunchListener() {

			@Override
			public void onOK() {
			}
			@Override
			public void onCancel() {
				// TODO Auto-generated method stub
			}
		});
		popup4.show();

		SekWordPopup popup5;
		popup5 = new SekWordPopup(MainActivity.this);
		popup5.setMessage(this.getString(R.string.settingMsg));
		popup5.setListener(new SekWordPopup.OnSEKLaunchListener() {

			@Override
			public void onOK() {
			}
			@Override
			public void onCancel() {
				// TODO Auto-generated method stub
			}
		});
		popup5.show();


		SekWordPopup popup6;
		popup6 = new SekWordPopup(MainActivity.this);
		popup6.setMessage(this.getString(R.string.user_del_msg));
		popup6.setListener(new SekWordPopup.OnSEKLaunchListener() {

			@Override
			public void onOK() {
			}
			@Override
			public void onCancel() {
				// TODO Auto-generated method stub
			}
		});
		popup6.show();

		SekWordPopup popup7;
		popup7 = new SekWordPopup(MainActivity.this);
		popup7.setMessage(this.getString(R.string.study_back));
		popup7.setListener(new SekWordPopup.OnSEKLaunchListener() {

			@Override
			public void onOK() {
			}
			@Override
			public void onCancel() {
				// TODO Auto-generated method stub
			}
		});
		popup7.show();

		SekWordPopup popup8;
		popup8 = new SekWordPopup(MainActivity.this);
		popup8.setMessage(this.getString(R.string.study_back));
		popup8.setListener(new SekWordPopup.OnSEKLaunchListener() {

			@Override
			public void onOK() {
			}
			@Override
			public void onCancel() {
				// TODO Auto-generated method stub
			}
		});
		popup8.show();

	}

	private void CreateTemp(){
		File dir = new File(Define.TMP_PATH);

		if(dir.exists()){
			if (dir.isDirectory())
			{
				String[] children = dir.list();
				for (int i = 0; i < children.length; i++)
				{
					new File(dir, children[i]).delete();
				}
			}
		}else{
			dir.mkdirs();
		}
	}

    //모든 뷰 초기화c
    private void initViews() {
    	initUserInfoTextView();
    	initMenuButtons();
    	initDateViews();
    }

	//사용자이름 및 급수 초기화
    @SuppressLint("NewApi")
	private void initUserInfoTextView() {
    	TextView tvUserName = (TextView)findViewById(R.id.tvUserName);
    	m_tvOrder = (TextView)findViewById(R.id.tvOrder);
    	
    	ImageView ivPhoto = (ImageView)findViewById(R.id.ivUserPhoto);
    	ImageView ivPhotoMask = (ImageView)findViewById(R.id.ivUserPhotoMask);
    	
    	//ESUtils.setTextViewTypeFaceByRes(this, tvUserName, FONT_PATH);

		ESUtils.setTextViewTypeFaceByRes(this, tvUserName, Define.getMainFont());
		ESUtils.setTextViewTypeFaceByRes(this, m_tvOrder, Define.getMainFont());

    	if (mDbMana == null) 
			mDbMana = new UserDBManager(this);
		
		if (mDbMana == null) return;
		
		mUserData = mDbMana.getActiveUser();
		
		if (mUserData == null) {
			tvUserName.setText(Html.fromHtml(String.format("<u>%s</u>", getString(R.string.user_reg))));
			m_tvOrder.setText("0");
			
			ivPhoto.setVisibility(View.INVISIBLE);
			ivPhotoMask.setVisibility(View.INVISIBLE);
		} else {
			tvUserName.setText(Html.fromHtml(String.format("<u>%s</u>", mUserData.getName())));
			m_tvOrder.setText(String.valueOf(mUserData.getLevel()));
			
			if (mUserData.getPhoto() == null || mUserData.getPhoto().isEmpty()) {
				ivPhoto.setVisibility(View.INVISIBLE);
				ivPhotoMask.setVisibility(View.INVISIBLE);
			} else {
				Bitmap bmpPhoto = BitmapFactory.decodeFile(mUserData.getPhoto());
				if (bmpPhoto == null)
				{
					ivPhoto.setVisibility(View.INVISIBLE);
					ivPhotoMask.setVisibility(View.INVISIBLE);
				}
				else
				{
					ivPhoto.setImageBitmap(bmpPhoto);
					ivPhoto.setVisibility(View.VISIBLE);
					ivPhotoMask.setVisibility(View.VISIBLE);
				}
			}
		}
    }
    
	//기본메뉴단추 초기화
    private void initMenuButtons() {
    	findViewById(R.id.tvUserName).setOnClickListener(this);
    	findViewById(R.id.btnPhoto).setOnClickListener(this);
    	findViewById(R.id.btnHelp).setOnClickListener(this);
    	findViewById(R.id.btnSetting).setOnClickListener(this);
    	findViewById(R.id.btnStudding).setOnClickListener(this);
    	findViewById(R.id.btnStudyResult).setOnClickListener(this);
    	findViewById(R.id.btnWord).setOnClickListener(this);
    	findViewById(R.id.btnDict).setOnClickListener(this);
    }
    
	@Override
	public void onClick(View v) {
		ESUtils.playBtnSound(this);
		switch(v.getId()) {
			case R.id.tvUserName:		goUserRegActivity();		break;
			case R.id.btnPhoto:			goPhotoActivity();			break;
			case R.id.btnHelp:			goHelpActivity();			break;
			case R.id.btnSetting:		goSettingActivity();		break;
			case R.id.btnStudding:		goStuddingActivity();		break;
			case R.id.btnStudyResult:	goStudyResultActivity();	break;
			case R.id.btnWord:			goWordActivity();			break;
			case R.id.btnDict:			goDictActivity();			break;
		}
	} 

	//날자 설정
    private void initDateViews() {
    	/*TextView tvMonth = (TextView)findViewById(R.id.tvMonth);
    	ESUtils.setTextViewTypeFaceByRes(this, tvMonth, FONT_PATH);
    	
    	ImageView ivDay1 = (ImageView)findViewById(R.id.ivDayNum1);
    	ImageView ivDay2 = (ImageView)findViewById(R.id.ivDayNum2);
    	
    	Calendar calendar = Calendar.getInstance();
    	
    	String strMonth = Integer.toString(calendar.get(Calendar.MONTH)+1);
    	tvMonth.setText(strMonth + getResources().getString(R.string.month));
    	
    	int date = calendar.get(Calendar.DATE);
    	if(date > 9) {
    		ivDay2.setVisibility(View.VISIBLE);
    		
    		String strDate = Integer.toString(date);
    		String strNum1 = strDate.substring(0, 1);
    		String strNum2 = strDate.substring(1, 2);
    		
    		ivDay1.setImageResource(R.drawable.main_num0 + Integer.valueOf(strNum1));
    		ivDay2.setImageResource(R.drawable.main_num0 + Integer.valueOf(strNum2));
    	}
    	else {
    		ivDay1.setImageResource(R.drawable.main_num0 + date);
    		ivDay2.setVisibility(View.GONE);
    	}*/

		TextView tvYear = (TextView)findViewById(R.id.textYear);
		TextView tvDate = (TextView)findViewById(R.id.textDate);
		TextView tvDay = (TextView)findViewById(R.id.textDay);

		ESUtils.setTextViewTypeFaceByRes(this, tvYear, Define.getMainFont());
		ESUtils.setTextViewTypeFaceByRes(this, tvDate, Define.getMainFont());
		ESUtils.setTextViewTypeFaceByRes(this, tvDay, Define.getMainFont());

		Calendar calendar = Calendar.getInstance();

		String strYear = Integer.toString(calendar.get(Calendar.YEAR));
		String strMonth = Integer.toString(calendar.get(Calendar.MONTH) + 1);
		String strDate = Integer.toString(calendar.get(Calendar.DATE));
		int nDay = calendar.get(Calendar.DAY_OF_WEEK);

		tvYear.setText(strYear);

		String strMDate = String.format("%s.%s", strMonth, strDate);
		tvDate.setText(strMDate);

		String strDay = "";
		switch (nDay){
			case 2:
				strDay = getString(R.string.string_monday);
				break;
			case 3:
				strDay = getString(R.string.string_tuesday);
				break;
			case 4:
				strDay = getString(R.string.string_wednesday);
				break;
			case 5:
				strDay = getString(R.string.string_thursday);
				break;
			case 6:
				strDay = getString(R.string.string_friday);
				break;
			case 7:
				strDay = getString(R.string.string_saturday);
				break;
			case 1:
				strDay = getString(R.string.string_sunday);
				break;
		}

		tvDay.setText(strDay);
	}



	//현재 사용자가 선택되였는가 판단
    private boolean isUserSelected() {
    	if(mUserData == null)
    		return false;
    	
    	return true;
    }
    
    //사용자등록화면으로 이행
    private void goUserRegActivity() {
    	Intent intent = new Intent(MainActivity.this, EsUserListActivity.class);
    	MainActivity.this.startActivity(intent);
    }
    
    private void goPhotoActivity() {
    	Intent intent = new Intent(MainActivity.this, EsUserListActivity.class);
    	MainActivity.this.startActivity(intent);
    }

    //도움말화면으로 이행
    private void goHelpActivity() {
		SekWordPopup popup;

		popup = new SekWordPopup(MainActivity.this);
		popup.setMessage(this.getString(R.string.app_back));
		popup.setListener(new SekWordPopup.OnSEKLaunchListener() {

			@Override
			public void onOK() {
				//gotoMainApp();
				MainActivity.this.finish();
				///System.exit(0);
			}

			@Override
			public void onCancel() {
				// TODO Auto-generated method stub
			}

		});

		popup.show();
    	//finish();
    }
    
    //설정화면으로 이행
    private void goSettingActivity() {
		if( !isUserSelected() )
		{
			goUserRegActivity();
			return;
		}
		
    	Intent intent = new Intent(MainActivity.this, EsSettingActivity.class);
    	intent.putExtra(Define.USER_ID, mUserData.getId());
    	MainActivity.this.startActivity(intent);
    }
    
    //학습진행화면으로 이행
    private void goStuddingActivity() {
		if( !isUserSelected() )
		{
			goUserRegActivity();
			return;
		}

		Intent intent = new Intent(this, EsStuddingActivity.class);
    	intent.putExtra(Define.USER_ID, mUserData.getId());
    	startActivity(intent);
    }
    
    //학습결과화면으로 이행
    private void goStudyResultActivity() {
		if( !isUserSelected() )
		{
			goUserRegActivity();
			return;
		}

		Intent intent = new Intent(MainActivity.this, EsStudyResultActivity.class);
    	intent.putExtra(Define.USER_ID, mUserData.getId());
    	MainActivity.this.startActivity(intent);
    }
    
    //단어장화면으로 이행
    private void goWordActivity() {
		if( !isUserSelected() )
		{
			goUserRegActivity();
			return;
		}

		Intent intent = new Intent(MainActivity.this, EsRegistryActivity.class);
    	intent.putExtra(Define.USER_ID, mUserData.getId());
    	MainActivity.this.startActivity(intent);
    }
    
    //사전화면으로 이행
    private void goDictActivity() {
		Intent launchIntent = getPackageManager().getLaunchIntentForPackage(DICT_APP_ID);
		if (launchIntent != null) {
			startActivity(launchIntent);//null pointer check in case package name was not found
		}
    }
    
    //자료기지창조
    private boolean createDB(Context context) {
    	try{
			File databaseFile = new File(Define.DB_PATH);

			if (!databaseFile.isDirectory()) {
				if (!databaseFile.mkdir()) 
					return false;
			}
			
			if (databaseFile.isDirectory()) {
				File dbFile = new File(Define.DB_PATH + "/" + Define.DBFileName);
				if(dbFile.exists())
					return true;
				
				FileOutputStream out = null;
				try {
					out = new FileOutputStream(dbFile);
				} catch (FileNotFoundException e) {
					e.printStackTrace();
					return false;
				}
				
				int nRead = 0;	
				InputStream in = null;
				
				String strFile = "db/" + Define.DBFileName;
				in = context.getAssets().open(strFile);
					
				try {
					int len = in.available();
					byte[] buf = new byte[len];
					nRead = in.read(buf);
						
					out.write(buf, 0, nRead);						
				} catch(Exception e) {
					e.printStackTrace();
					in.close();
					out.close();
					return false;
				}
				in.close();				
				out.close();
			}
		} catch(Exception e) {
			e.printStackTrace();
			return false;
		}

		try{
			File databaseFile = new File(Define.DB_PATH);

			if (!databaseFile.isDirectory()) {
				if (!databaseFile.mkdir())
					return false;
			}

			if (databaseFile.isDirectory()) {
				File dbFile = new File(Define.DB_PATH + "/" + Define.PAKDBFileName);
				if(dbFile.exists())
					return true;

				FileOutputStream out = null;
				try {
					out = new FileOutputStream(dbFile);
				} catch (FileNotFoundException e) {
					e.printStackTrace();
					return false;
				}

				int nRead = 0;
				InputStream in = null;

				String strFile = "db/" + Define.PAKDBFileName;
				in = context.getAssets().open(strFile);

				try {
					int len = in.available();
					byte[] buf = new byte[len];
					nRead = in.read(buf);

					out.write(buf, 0, nRead);
				} catch(Exception e) {
					e.printStackTrace();
					in.close();
					out.close();
					return false;
				}
				in.close();
				out.close();
			}
		} catch(Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
    }

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		initUserInfoTextView();
		
		super.onResume();
	}
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		unregisterReceiver();
		if(mDbMana != null) {
			mDbMana.close();
			mDbMana = null;
		}
		
		super.onDestroy();
	}

	//리시버 등록 및 해제
	private void registerReceiver() {
		IntentFilter filter = new IntentFilter(Define.SCHEDULE_VIEW_ACTION);
		
		m_receiver = new UserOrderReceiver();
		registerReceiver(m_receiver, filter);
	}
	
	private void unregisterReceiver() {
		if(m_receiver != null)
			unregisterReceiver(m_receiver);
	}
	
	private class UserOrderReceiver extends BroadcastReceiver {
	   public void onReceive(Context context, Intent intent) {
	    	if (intent.getAction().equals(Define.USER_ORDER_ACTION)) {
	    		m_tvOrder.setText(String.valueOf(mUserData.getLevel()));
	    	}
	    }
	}

	private void gotoMainApp(){
		Intent launchIntent = getPackageManager().getLaunchIntentForPackage(MAIN_APP_ID);
		if (launchIntent != null) {
			startActivity(launchIntent);//null pointer check in case package name was not found
		}
	}
}
