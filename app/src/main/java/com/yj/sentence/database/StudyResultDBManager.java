package com.yj.sentence.database;

import java.util.ArrayList;

import com.yj.sentence.common.Define;
import com.yj.sentence.structs.DateData;
import com.yj.sentence.utils.ESUtils;

import android.content.Context;
import android.database.Cursor;

public class StudyResultDBManager extends DBManager {

	public StudyResultDBManager(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	public StudyResultDBManager(Context context, int uid) {
		super(context, uid);
		// TODO Auto-generated constructor stub
	}

	public ArrayList<DateData> getDateList(long startDate, long endDate) {
		ArrayList<DateData> result = new ArrayList<DateData>();

		if (startDate == 0 || endDate == 0)
			return result;

		int[] mEndDay = {31,28,31,30,31,30,31,31,30,31,30,31};
		int sYear = ESUtils.getDateOfYear(startDate);
		int sMonth = ESUtils.getDateOfMonth(startDate);
		int eYear = ESUtils.getDateOfYear(endDate);
		int eMonth = ESUtils.getDateOfMonth(endDate);
		int mYear = sYear;
		int mMonth = sMonth;
		boolean isExit = false;

		while(!isExit) {

			DateData item = new DateData();
			item.setDate(String.format("%s.%2s.1 - %2s.%2s", mYear, mMonth, mMonth, mEndDay[mMonth-1]));
			item.setStartValue(ESUtils.getDateMilliseconds(mYear, mMonth-1, 1));
			item.setEndValue(ESUtils.getDateMilliseconds(mYear, mMonth-1, mEndDay[mMonth-1]));
			item.setDays(mEndDay[mMonth-1]);
			result.add(item);

			if (mYear<eYear) {
				if (mMonth<12){
					mMonth++;
				} else {
					mMonth = 1;
					mYear++;
				}
			} else if (mYear==eYear) {
				if (mMonth<eMonth) {
					mMonth++;
				} else {
					isExit = true;
				}
			} else {
				isExit = true;
			}
		}

		return result;
	}

	public ArrayList<Integer> getTimeDataList(int nUserId, long startDate, long endDate){
		ArrayList<Integer> result = new ArrayList<Integer>();
		String query = String.format("SELECT IFNULL(times,0), sdate FROM %s WHERE userid=%s AND sdate>=%s AND sdate<=%s ORDER BY sdate", Define.TB_PROGRESS_TIMES , nUserId, startDate, endDate);

		int index = 0;
		for (long date=startDate; date<=endDate; date+=Define.ONE_DAY_TIME) {
			try{
				result.add(index, 0);
				Cursor cursor = mDbConnection.query(query);
				if(cursor != null){
					int nCount = cursor.getCount();
					if(nCount > 0) {
						for(int i=0; i<nCount; i++) {
							int year = ESUtils.getDateOfYear(date);
							int month = ESUtils.getDateOfMonth(date);
							int day = ESUtils.getDateOfDay(date);

							int myear = ESUtils.getDateOfYear(cursor.getLong(1));
							int mmonth = ESUtils.getDateOfMonth(cursor.getLong(1));
							int mday = ESUtils.getDateOfDay(cursor.getLong(1));

							if (year==myear && month==mmonth && day==mday)
								result.set(index, result.get(index)+(int)(cursor.getLong(0)/(60)));

							cursor.moveToNext();
						}
					}
					cursor.close();
				}
				index++;
			} catch(Exception e) {
				e.printStackTrace();
				close();
			}
		}

		return result;
	}

	public ArrayList<Integer> getWordDataList(int nUserId, long startDate, long endDate) {
		ArrayList<Integer> result = new ArrayList<Integer>();


		int index = 0;
		for (long date=startDate; date<=endDate; date+=Define.ONE_DAY_TIME) {
			try{
				result.add(index, 0);

				String query = String.format("SELECT id FROM %s WHERE userid=%s AND sdate=%s", Define.TB_PROGRESS_WORDS , nUserId, date);
				Cursor cursor = mDbConnection.query(query);
				if(cursor != null) {
					int nCount = cursor.getCount();
					result.set(index, nCount);
					cursor.close();
				}
				index++;
			} catch(Exception e) {
				e.printStackTrace();
				close();
			}
		}


		return result;

	}

	public ArrayList<Integer> getMarkDataList(int nUserId, long startDate, long endDate) {
		ArrayList<Integer> result = new ArrayList<Integer>();
		String query = String.format("SELECT IFNULL(marks,0),sDate FROM %s WHERE userid=%s AND sdate>=%s AND sdate<=%s ORDER BY sdate",
				Define.TB_PROGRESS_MARKS, nUserId, startDate, endDate);
		int index = 0;
		for (long date=startDate; date<=endDate; date+=Define.ONE_DAY_TIME) {
			try{
				result.add(index, 0);

				int sum = 0;
				int sumCount = 0;

				Cursor cursor = mDbConnection.query(query);
				if(cursor != null){
					int nCount = cursor.getCount();
					if(nCount > 0) {
						for(int i=0; i<nCount; i++) {

							int year = ESUtils.getDateOfYear(date);
							int month = ESUtils.getDateOfMonth(date);
							int day = ESUtils.getDateOfDay(date);

							int myear = ESUtils.getDateOfYear(cursor.getLong(1));
							int mmonth = ESUtils.getDateOfMonth(cursor.getLong(1));
							int mday = ESUtils.getDateOfDay(cursor.getLong(1));

							if (year==myear && month==mmonth && day==mday) {
								sum = sum + cursor.getInt(0);
								sumCount++;
							}

							cursor.moveToNext();
						}

						if(sumCount > 0)
						{
							result.set(index, sum / sumCount);
						}
					}
					cursor.close();
				}
				index++;
			} catch(Exception e) {
				e.printStackTrace();
				close();
			}
		}


		return result;
	}

	/*
	public ArrayList<Integer> getTimeDataList(ScheduleData data, long startDate, long endDate) {
		ArrayList<Integer> result = new ArrayList<Integer>();

		String query = String.format("SELECT IFNULL(times,0), sDate FROM schedule_date WHERE sid=%s AND sDate>=%s AND sDate<=%s ORDER BY sDate", data.getId(), startDate, endDate);

		int index = 0;
		for (long date=startDate; date<=endDate; date+=Define.ONE_DAY_TIME) {
			try{
				result.add(index, 0);
				Cursor cursor = mDbConnection.query(query);
				if(cursor != null){
					int nCount = cursor.getCount();
					if(nCount > 0) {
						for(int i=0; i<nCount; i++) {
							int year = ESUtils.getDateOfYear(date);
							int month = ESUtils.getDateOfMonth(date);
							int day = ESUtils.getDateOfDay(date);

							int myear = ESUtils.getDateOfYear(cursor.getLong(1));
							int mmonth = ESUtils.getDateOfMonth(cursor.getLong(1));
							int mday = ESUtils.getDateOfDay(cursor.getLong(1));

							if (year==myear && month==mmonth && day==mday)
								result.set(index, result.get(index)+(int)(cursor.getLong(0)/(60)));

							cursor.moveToNext();
						}
					}
					cursor.close();
				}
				index++;
			} catch(Exception e) {
				e.printStackTrace();
				close();
			}
		}

		return result;
	}

	public ArrayList<Integer> getWordDataList(ScheduleData data, long startDate, long endDate) {
		ArrayList<Integer> result = new ArrayList<Integer>();

		String query = String.format("SELECT (SELECT COUNT(id) FROM schedule_word WHERE did=a.id) AS words, a.sDate FROM schedule_date AS a WHERE a.sid=%s  AND a.sDate>=%s AND a.sDate<=%s AND a.marks>69 ORDER BY a.sDate",
				data.getId(), startDate, endDate);

		int index = 0;
		for (long date=startDate; date<=endDate; date+=Define.ONE_DAY_TIME) {
			try{
				result.add(index, 0);
				Cursor cursor = mDbConnection.query(query);
				if(cursor != null) {
					int nCount = cursor.getCount();
					if(nCount > 0) {
						for(int i=0; i<nCount; i++) {

							int year = ESUtils.getDateOfYear(date);
							int month = ESUtils.getDateOfMonth(date);
							int day = ESUtils.getDateOfDay(date);

							int myear = ESUtils.getDateOfYear(cursor.getLong(1));
							int mmonth = ESUtils.getDateOfMonth(cursor.getLong(1));
							int mday = ESUtils.getDateOfDay(cursor.getLong(1));


							if (year==myear && month==mmonth && day==mday) {
								result.set(index, result.get(index)+cursor.getInt(0));
							}


							cursor.moveToNext();
						}
					}
					cursor.close();
				}
				index++;
			} catch(Exception e) {
				e.printStackTrace();
				close();
			}
		}


		return result;

	}

	public ArrayList<Integer> getMarkDataList(ScheduleData data, long startDate, long endDate) {
		ArrayList<Integer> result = new ArrayList<Integer>();
		String query = String.format("SELECT IFNULL(marks,0),sDate FROM schedule_date WHERE sid=%s AND sDate>=%s AND sDate<=%s ORDER BY sDate",
				data.getId(), startDate, endDate);
		int index = 0;
		for (long date=startDate; date<=endDate; date+=Define.ONE_DAY_TIME) {
			try{
				result.add(index, 0);

				int sum = 0;
				int sumCount = 0;

				Cursor cursor = mDbConnection.query(query);
				if(cursor != null){
					int nCount = cursor.getCount();
					if(nCount > 0) {
						for(int i=0; i<nCount; i++) {

							int year = ESUtils.getDateOfYear(date);
							int month = ESUtils.getDateOfMonth(date);
							int day = ESUtils.getDateOfDay(date);

							int myear = ESUtils.getDateOfYear(cursor.getLong(1));
							int mmonth = ESUtils.getDateOfMonth(cursor.getLong(1));
							int mday = ESUtils.getDateOfDay(cursor.getLong(1));

							if (year==myear && month==mmonth && day==mday) {
								sum = sum + cursor.getInt(0);
								sumCount++;
							}

							cursor.moveToNext();
						}

						if(sumCount > 0)
						{
							result.set(index, sum / sumCount);
						}
					}
					cursor.close();
				}
				index++;
			} catch(Exception e) {
				e.printStackTrace();
				close();
			}
		}


		return result;
	}
	*/

}
