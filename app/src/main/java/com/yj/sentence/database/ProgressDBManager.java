package com.yj.sentence.database;

import android.content.Context;
import android.database.Cursor;

import com.yj.sentence.common.Define;
import com.yj.sentence.structs.EpisodeData;
import com.yj.sentence.structs.ProgressData;
import com.yj.sentence.structs.StageData;

import java.util.ArrayList;

public class ProgressDBManager extends DBManager {

	public ProgressDBManager(Context context, int uid) {
		super(context, uid);
	}

	public void updateLevel(int nOrder){
		String query = String.format("UPDATE user SET level=%s WHERE id=%s", nOrder, mUid);
		excuteQuery(query);
	}

	public int getLevel(){
		int result = getValueInt(String.format("SELECT level FROM %s WHERE id=%s", Define.TB_USER, mUid));
		return result;
	}

	public void createProgress(ProgressData data) {
		String query = "";
		query = String.format("INSERT INTO %s (userId, level, episode, stage, marks) VALUES ('%s', '%s', '%s', '%s', '%s')",
				  			   Define.TB_PROGRESS, mUid, data.getLevel(), data.getEpisode(), data.getStage(), data.getMarks());
		excuteQuery(query);
	}

	public void updateProgress(ProgressData data) {
		String query = "";

		int nCount = getValueInt(String.format("SELECT COUNT(id) FROM %s WHERE userid=%s and level=%s and episode=%s and stage=%s", Define.TB_PROGRESS, mUid, data.getLevel(), data.getEpisode(), data.getStage()));
		if(nCount == 0){
			createProgress(data);
		}else{
			query = String.format("UPDATE %s SET marks='%s' WHERE userid=%s AND level=%s AND episode=%s AND stage=%s", Define.TB_PROGRESS, data.getMarks(), mUid, data.getLevel() , data.getEpisode(), data.getStage());
			excuteQuery(query);
		}
	}

	public boolean isAvailableData(ProgressData data){
		String query = "";

		int nCount = getValueInt(String.format("SELECT COUNT(id) FROM %s WHERE userid=%s and level=%s and episode=%s and stage=%s and marks>=1", Define.TB_PROGRESS, mUid, data.getLevel(), data.getEpisode(), data.getStage()));
		if(nCount == 0) {
			return  false;
		}else {
			return true;
		}
	}
	public ProgressData getNextProgress(ProgressData data){
		String query = "";
		query = String.format("SELECT * FROM %s WHERE userid=%s AND level=%s ORDER BY episode, stage", Define.TB_PROGRESS, mUid, data.getLevel());

		ProgressData returnData = new ProgressData(mUid, data.getLevel(), data.getEpisode(), data.getStage());
		ArrayList<ProgressData> dataList = new ArrayList<ProgressData>();
		try {
			Cursor cursor = mDbConnection.query(query);
			if (cursor != null) {
				int nCount = cursor.getCount();
				if(nCount > 0) {
					for(int i=0; i<nCount; i++) {

						ProgressData item = new ProgressData(mUid,
								cursor.getInt(2),
								cursor.getInt(3),
								cursor.getInt(4),
								cursor.getInt(5));

						dataList.add(item);
						cursor.moveToNext();
					}
				}
				cursor.close();
			}
		}catch (Exception e){
			int t = 1;
		}

		for(int i =0; i < dataList.size(); i++){

			ProgressData item =dataList.get(i);
			if(item.getMarks() >= 70) continue;
			if(item.getEpisode() == data.getEpisode() && item.getStage() > data.getStage()){
				returnData = item;
				return returnData;
			}

			if(item.getEpisode() > data.getEpisode()){
				returnData = item;
				return returnData;
			}
		}

		if(dataList.size() > 0  && !(data.getEpisode() == 1 && data.getStage() == 0)){
			ProgressData firstData = new ProgressData(mUid, data.getLevel(), 1, 0);
			returnData = firstData;
			return firstData;
		}

		return  returnData;
	}

	public int getMarks(ProgressData data)
	{
		String query = "";
		int nMarks = getValueInt(String.format("SELECT marks FROM %s WHERE userid=%s AND level=%s AND episode=%s AND stage=%s", Define.TB_PROGRESS, mUid, data.getLevel(), data.getEpisode(), data.getStage()));
		return nMarks;
	}

	public void updateProgressTime(long sDate, int time){
		String query = "";

		int nCount = getValueInt(String.format("SELECT COUNT(id) FROM %s WHERE userid=%s and sdate=%s", Define.TB_PROGRESS_TIMES, mUid, sDate));

		if(nCount == 0){
			query = String.format("INSERT INTO %s (sdate, times, userid) VALUES ('%s', '%s', '%s')", Define.TB_PROGRESS_TIMES, sDate, time, mUid);
			excuteQuery(query);
		}else {
			int nOriginTime = getValueInt(String.format("SELECT times FROM %s WHERE userid=%s and sdate=%s ", Define.TB_PROGRESS_TIMES, mUid, sDate));
			query = String.format("UPDATE %s SET times=%s WHERE userid=%s and sdate=%s", Define.TB_PROGRESS_TIMES, time + nOriginTime, mUid, sDate);
			excuteQuery(query);
		}
	}

	public void updateProgressMarks(long sDate, int marks){
		String query = "";
		query = String.format("INSERT INTO %s (sdate, marks, userid) VALUES ('%s', '%s', '%s')", Define.TB_PROGRESS_MARKS, sDate, marks, mUid);
		excuteQuery(query);
	}

	public void updateProgressWords(long sDate, int wid){
		String query = "";
		int nCount = getValueInt(String.format("SELECT COUNT(id) FROM %s WHERE userid=%s and wid=%s", Define.TB_PROGRESS_WORDS, mUid, wid));

		if(nCount == 0){
			query = String.format("INSERT INTO %s (sdate, wid, userid) VALUES ('%s', '%s', '%s')", Define.TB_PROGRESS_WORDS, sDate, wid, mUid);
			excuteQuery(query);
		}
	}



	public StageData getStageData(int nLevel, int nEpisode, int nStage){
		StageData stgData = null;

		String query = String.format("SELECT * FROM %s WHERE lvlnum=%s AND epinum=%s AND stgnum=%s", Define.TB_STAGE, nLevel, nEpisode, nStage);
		ArrayList<Integer[]> result1 = new ArrayList<Integer[]>();
		try{
			Cursor cursor = mDbConnection.query(query);
			if(cursor != null){
				stgData = new StageData(cursor.getInt(0), cursor.getInt(1), cursor.getInt(2), cursor.getInt(3),
						cursor.getString(4), cursor.getString(4), cursor.getString(4), cursor.getInt(5));
				cursor.close();
			}
		} catch(Exception e) {
			e.printStackTrace();
			close();
		}

		return stgData;
	}


	public EpisodeData getEpisodeData(int nLevel, int nEpisode){
		EpisodeData epidata = null;

		String query = String.format("SELECT * FROM %s WHERE lvlnum=%s AND epinum=%s", Define.TB_EPISODE, nLevel, nEpisode);
		ArrayList<Integer[]> result1 = new ArrayList<Integer[]>();
		try{
			Cursor cursor = mDbConnection.query(query);
			if(cursor != null){
				epidata = new EpisodeData(cursor.getInt(0), cursor.getInt(1), cursor.getString(2), cursor.getString(3),
						cursor.getString(4));
				cursor.close();
			}
		} catch(Exception e) {
			e.printStackTrace();
			close();
		}

		return epidata;
	}
	
}
