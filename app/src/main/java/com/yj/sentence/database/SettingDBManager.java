package com.yj.sentence.database;


import android.annotation.SuppressLint;
import android.content.Context;
import android.database.Cursor;

@SuppressLint("DefaultLocale")
public class SettingDBManager {
	
	private String[][] mStudy1 = {{"1회반복","1"}, {"2회반복","2"}, {"3회반복","3"}, {"4회반복","4"}, {"5회반복","5"}};
	private String[][] mStudy2 = {{"1회반복","1"}, {"2회반복","2"}, {"3회반복","3"}, {"4회반복","4"}, {"5회반복","5"}};
	private String[][] mWordColor = {{"없음","0"}, {"노란색","16771584"}, {"풀색","1953792"}, {"빨간색","16711680"}, {"파란색","2206207"}};
	private String[][] mMeanColor = {{"없음","0"}, {"노란색","16771584"}, {"풀색","1953792"}, {"빨간색","16711680"}, {"파란색","2206207"}};
	private String[][] mTestTime = {{"5초","5"}, {"7초","7"}, {"10초","10"},  {"14초","14"}};
	private String[][] mStudySpeed = {{"매우느림","1"}, {"느림","2"}, {"보통","3"}, {"빠름","4"}, {"매우빠름","5"}};

	private DBConnection mDbConnection = null;
	private Context mContext;
	
	public SettingDBManager(Context context) {
		mDbConnection = DBConnection.open(context);
		mContext = context;
	}
	
	
	// 학습반복1
	public int getStudy1Index() {
		return getValueInt(String.format("SELECT value FROM base WHERE id=1"));
	}
	
	public String getStudy1Name() {
		return mStudy1[getStudy1Index()][0];
	}
	//학습반복 1 단계 반복 회수 얻기
	public int getStudy1Value() {
		return Integer.parseInt(mStudy1[getStudy1Index()][1]);
	}
	//학습반복 1 단계 반복 회수 설정
	public void setStudy1Value(int value) {
		excuteQuery(String.format("UPDATE base SET value='%s' WHERE id=1", value));
	}	
	//학습반복 2 단계 반복 회수 얻기
	public int getRecognitionIndex() {
		return getValueInt(String.format("SELECT value FROM base WHERE id=2"));
	}

	public String getRecognitionName() {
		return mStudy2[getRecognitionIndex()][0];
	}
	
	public int getRecognitioinValue() {
		return Integer.parseInt(mStudy2[getRecognitionIndex()][1]);
	}
	//학습반복 2 단계 반복 회수 설정
	public void setRecognitionValue(int value) {
		excuteQuery(String.format("UPDATE base SET value='%s' WHERE id=2", value));
	}
	
	// 단어색상
	public int getWordColorIndex() {
		return getValueInt(String.format("SELECT value FROM base WHERE id=3"));
	}
	
	public String getWordColorName() {
		return mWordColor[getWordColorIndex()][0];
	}
	
	public int getWordColorValue() {
		return Integer.parseInt(mWordColor[getWordColorIndex()][1]);
	}
	
	public void setWordColorValue(int value) {
		excuteQuery(String.format("UPDATE base SET value='%s' WHERE id=3", value));
	}
	
	// 뜻색상
	public int getFontSizeIndex() {
		return getValueInt(String.format("SELECT value FROM base WHERE id=4"));
	}
	
	public String getFontSizeName() {
		return mMeanColor[getFontSizeIndex()][0];
	}
	
	public int getFontSizeValue() {
		return Integer.parseInt(mMeanColor[getFontSizeIndex()][1]);
	}
	
	public void setFontSizeValue(int value) {
		excuteQuery(String.format("UPDATE base SET value='%s' WHERE id=4", value));
	}
	
	// 시험시간
	public int getTestTimeIndex() {
		return getValueInt(String.format("SELECT value FROM base WHERE id=5"));
	}
	
	public String getTestTimeName() {
		return mTestTime[getTestTimeIndex()][0];
	}
	
	public int getTestTimeValue() {
		return Integer.parseInt(mTestTime[getTestTimeIndex()][1]);
	}
	
	public void setTestTimeValue(int value) {
		excuteQuery(String.format("UPDATE base SET value='%s' WHERE id=5", value));
	}
	
	// 시험방법
	public String getTestModeValue() {
		return getValueString(String.format("SELECT value FROM base WHERE id=6"));
	}
	
	public void setTestModeValue(String value) {
		excuteQuery(String.format("UPDATE base SET value='%s' WHERE id=6", value));
	}
	
	// 학습진행 방식
	public int getStudySpeedIndex() {
		return getValueInt(String.format("SELECT value FROM base WHERE id=7"));
	}
	
	public String getStudySpeedName() {
		return mStudySpeed[getStudySpeedIndex()][0];
	}
	
	public int getStudySpeedValue() {
		return Integer.parseInt(mStudySpeed[getStudySpeedIndex()][1]);
	}
	
	public void setStudySpeedValue(int value) {
		excuteQuery(String.format("UPDATE base SET value='%s' WHERE id=7", value));
	}
	
	
	private int getValueInt(String query) {
		String result = String.valueOf(getValue(query, 0));	
		return (result != null) ? Integer.parseInt(result) : 0;
	}
	
	private String getValueString(String query) {
		String result = String.valueOf(getValue(query, 2));	
		return (result!=null) ? result : "";
	}
	
	private Object getValue(String query, int type) {
		Object result = null;
		
		try{
			Cursor cursor = mDbConnection.query(query);
			if(cursor != null){
				int nCount = cursor.getCount();
				if(nCount > 0) {
					for(int i=0; i<nCount; i++) {
						if (type == 0) {
							result = cursor.getInt(0);
						} else if (type == 1) {
							result = cursor.getFloat(0);
						} else if (type == 2) {
							result = cursor.getString(0);
						}
						cursor.moveToNext();
					}
				}
				cursor.close();
			}
		} catch(Exception e) {
			e.printStackTrace();
			close();
		}
		
		return result;
	}
	
	private void excuteQuery(String query) {		
		DBConnection db = DBConnection.open(mContext);

		if (db == null)
			return;

		db.updateTableData(query);
		db.close();
	}
	
	
	public void close(){
		if (mDbConnection != null) {
			mDbConnection.close();
			mDbConnection = null;
		}
	}
}
