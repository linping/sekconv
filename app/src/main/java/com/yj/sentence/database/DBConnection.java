package com.yj.sentence.database;

import com.yj.sentence.common.Define;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBConnection {
	
	private DBHelper mDbHelper = null;
	private SQLiteDatabase mDb = null;
	
	public DBConnection(Context context) {
		mDbHelper = new DBHelper(context);
		mDb = mDbHelper.getWritableDatabase();
	}
	
	public static DBConnection open(Context context){
		DBConnection connection = new DBConnection(context);
		return connection;
	}
	
	public void close(){
		mDbHelper.close();
		mDb.close();
	}
	
	public Cursor query(String strQuery){
		Cursor cursor = mDb.rawQuery(strQuery, null);
		if(cursor != null)
			cursor.moveToFirst();
		
		return cursor;
	}
	
	public void updateTableData(String sql) {
		mDb.execSQL(sql);
	}

	public void queryExec(String strQuery) {
		mDb.execSQL(strQuery);
	}
	
	class DBHelper extends SQLiteOpenHelper{
		public DBHelper(Context context){			
			super(context, Define.DBFileName, null, 1);			
		}
		
		public void onCreate(SQLiteDatabase db){
		
		}
		
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){			
			onCreate(db);
		}
	}
}
