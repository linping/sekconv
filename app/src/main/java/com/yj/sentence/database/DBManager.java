package com.yj.sentence.database;

import java.util.ArrayList;

import android.content.Context;
import android.database.Cursor;

public class DBManager {
	public DBConnection mDbConnection;
	private Context mContext;
	public int mUid;
	
	public DBManager(Context context) {
		this(context, -1);
	}
	
	public DBManager(Context context, int uid) {
		mContext = context;
		mDbConnection = DBConnection.open(mContext);
		mUid = uid;
	}
	
	public int getValueInt(String query) {
		String result = String.valueOf(getValue(query, 0));	
		return (result!=null && !result.equals("null")) ? Integer.parseInt(result) : 0;
	}
	
	public long getValueLong(String query) {
		String result = String.valueOf(getValue(query, 2));	
		return (result!=null && !result.equals("null")) ? Long.parseLong(result) : 0;
	}
	
	public String getValueString(String query) {
		String result = String.valueOf(getValue(query, 2));	
		return (result!=null) ? result : "";
	}
	
	private Object getValue(String query, int type) {
		Object result = null;
		
		try{
			Cursor cursor = mDbConnection.query(query);
			if(cursor != null){
				int nCount = cursor.getCount();
				if(nCount > 0) {
					for(int i=0; i<nCount; i++) {
						if (type == 0) {
							result = cursor.getInt(0);
						} else if (type == 1) {
							result = cursor.getFloat(0);
						} else if (type == 2) {
							result = cursor.getString(0);
						}
						cursor.moveToNext();
					}
				}
				cursor.close();
			}
		} catch(Exception e) {
			e.printStackTrace();
			close();
		}
		
		return result;
	}
	
	public ArrayList<Integer> getListValueInt(String query) {
		ArrayList<Integer> result = new ArrayList<Integer>();
		
		try{
			Cursor cursor = mDbConnection.query(query);
			if(cursor != null){
				int nCount = cursor.getCount();
				if(nCount > 0) {
					for(int i=0; i<nCount; i++) {
						result.add(cursor.getInt(0));
						cursor.moveToNext();
					}
				}
				cursor.close();
			}
		} catch(Exception e) {
			e.printStackTrace();
			close();
		}
		
		return result;
	}
	
	public void excuteQuery(String query) {		
		DBConnection db = DBConnection.open(mContext);

		if (db == null)
			return;

		db.updateTableData(query);		
		db.close();
	}
	
	public void close(){
		if (mDbConnection != null) {
			mDbConnection.close();
			mDbConnection = null;
		}
	}
}
