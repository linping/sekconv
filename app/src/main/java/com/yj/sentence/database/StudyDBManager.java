package com.yj.sentence.database;

import java.util.ArrayList;

import com.yj.sentence.common.Define;
import com.yj.sentence.structs.ProgressData;
import com.yj.sentence.structs.WordData;
import com.yj.sentence.utils.ESUtils;

import android.content.Context;
import android.database.Cursor;

public class StudyDBManager extends DBManager {
	
	public StudyDBManager(Context context, int uid) {
		super(context, uid);
	}

	public ArrayList<WordData> getWordList(ProgressData data) {
		ArrayList<WordData> result = new ArrayList<WordData>();

		String query = String.format("SELECT a.* FROM %s as a LEFT JOIN %s as b ON a.id=b.word WHERE b.lvlnum=%s AND b.epinum=%s AND b.stgnum=%s AND b.typnum=1",
				Define.TB_WORD_DATA, Define.TB_WORD_SET, data.getLevel(), data.getEpisode(), data.getStage());
		
		try{
			Cursor cursor = mDbConnection.query(query);
			if(cursor != null){
				int nCount = cursor.getCount();
				if(nCount > 0) {
					for(int i=0; i<nCount; i++) {
						
						WordData item = new WordData(cursor.getInt(0), 
													 cursor.getString(3),
													 cursor.getString(2),
													 cursor.getString(0), 
													 cursor.getString(0), 
													 0,
													 0);
						result.add(item);
						cursor.moveToNext();
					}
				}
				cursor.close();
			}
		} catch(Exception e) {
			e.printStackTrace();
			close();
		}

		return result;
	}

	public ArrayList<WordData> getPronunciationList(ProgressData data) {
		ArrayList<WordData> result = new ArrayList<WordData>();

		String query = String.format("SELECT a.* FROM %s as a LEFT JOIN %s as b ON a.id=b.id WHERE b.level=%s AND b.episode=%s AND b.type=6",
				Define.TB_WORD_DATA_PRO, Define.TB_WORD_SET_PRO, data.getLevel(), data.getEpisode());

		try{
			Cursor cursor = mDbConnection.query(query);
			if(cursor != null){
				int nCount = cursor.getCount();
				if(nCount > 0) {
					for(int i=0; i<nCount; i++) {

						WordData item = new WordData(cursor.getInt(0),
								cursor.getString(3),
								cursor.getString(1),
								cursor.getString(0),
								cursor.getString(0),
								0,
								0);
						result.add(item);
						cursor.moveToNext();
					}
				}
				cursor.close();
			}
		} catch(Exception e) {
			e.printStackTrace();
			close();
		}

		return result;
	}

	public ArrayList<WordData> getPronunciationResultList(ProgressData data) {
		ArrayList<WordData> result = new ArrayList<WordData>();

		String query = String.format("SELECT a.* FROM %s as a LEFT JOIN %s as b ON a.id=b.id WHERE b.level=%s AND b.episode=%s AND b.type=7",
				Define.TB_WORD_DATA_PRO, Define.TB_WORD_SET_PRO, data.getLevel(), data.getEpisode());

		try{
			Cursor cursor = mDbConnection.query(query);
			if(cursor != null){
				int nCount = cursor.getCount();
				if(nCount > 0) {
					for(int i=0; i<nCount; i++) {

						WordData item = new WordData(cursor.getInt(0),
								cursor.getString(3),
								cursor.getString(1),
								cursor.getString(0),
								cursor.getString(0),
								0,
								0);
						result.add(item);
						cursor.moveToNext();
					}
				}
				cursor.close();
			}
		} catch(Exception e) {
			e.printStackTrace();
			close();
		}

		return result;
	}


	public WordData getWordData(int nWordID){
		WordData data = null;
		String query = String.format("SELECT * FROM %s WHERE id=%s", Define.TB_WORD_DATA, nWordID);
		Cursor cursor = mDbConnection.query(query);
		if(cursor != null){
			data = new WordData(cursor.getInt(0),
					cursor.getString(3),
					cursor.getString(2),
					cursor.getString(0),
					cursor.getString(0),
					0,
					0);
			cursor.close();
		}

		return data;
	}

	public ArrayList<WordData> getSentenceList(ProgressData data) {
		ArrayList<WordData> result = new ArrayList<WordData>();

		String query = String.format("SELECT a.* FROM %s as a LEFT JOIN %s as b ON a.id=b.word WHERE b.lvlnum=%s AND b.epinum=%s AND b.stgnum=%s AND b.typnum=3",
				Define.TB_WORD_DATA, Define.TB_WORD_SET, data.getLevel(), data.getEpisode(), data.getStage());

		try{
			Cursor cursor = mDbConnection.query(query);
			if(cursor != null){
				int nCount = cursor.getCount();
				if(nCount > 0) {
					for(int i=0; i<nCount; i++) {

						WordData item = new WordData(cursor.getInt(0),
								cursor.getString(3),
								cursor.getString(2),
								cursor.getString(0),
								cursor.getString(0),
								0,
								0);
						result.add(item);
						cursor.moveToNext();
					}
				}
				cursor.close();
			}
		} catch(Exception e) {
			e.printStackTrace();
			close();
		}

		return result;
	}

	public ArrayList<WordData> getPracticeList(ProgressData data) {
		ArrayList<WordData> result = new ArrayList<WordData>();

		String query = String.format("SELECT a.* FROM %s as a LEFT JOIN %s as b ON a.id=b.word WHERE b.lvlnum=%s AND b.epinum=%s AND b.stgnum=%s AND b.typnum=4",
				Define.TB_WORD_DATA, Define.TB_WORD_SET, data.getLevel(), data.getEpisode(), data.getStage());

		try{
			Cursor cursor = mDbConnection.query(query);
			if(cursor != null){
				int nCount = cursor.getCount();
				if(nCount > 0) {
					for(int i=0; i<nCount; i++) {

						WordData item = new WordData(cursor.getInt(0),
								cursor.getString(3),
								cursor.getString(2),
								cursor.getString(0),
								cursor.getString(0),
								0,
								0);
						result.add(item);
						cursor.moveToNext();
					}
				}
				cursor.close();
			}
		} catch(Exception e) {
			e.printStackTrace();
			close();
		}

		return result;
	}
	public ArrayList<WordData> getTestList(ProgressData data) {
		ArrayList<WordData> result = new ArrayList<WordData>();

		String query = String.format("SELECT a.* FROM %s as a LEFT JOIN %s as b ON a.id=b.word WHERE b.lvlnum=%s AND b.epinum=%s AND b.stgnum=%s AND b.typnum=3 ORDER BY RANDOM() LIMIT 4",
				Define.TB_WORD_DATA, Define.TB_WORD_SET, data.getLevel(), data.getEpisode(), data.getStage());

		try{
			Cursor cursor = mDbConnection.query(query);
			if(cursor != null){
				int nCount = cursor.getCount();
				if(nCount > 0) {
					for(int i=0; i<nCount; i++) {

						WordData item = new WordData(cursor.getInt(0),
								cursor.getString(3),
								cursor.getString(2),
								cursor.getString(0),
								cursor.getString(0),
								0,
								0);
						result.add(item);
						cursor.moveToNext();
					}
				}
				cursor.close();
			}
		} catch(Exception e) {
			e.printStackTrace();
			close();
		}

		return result;
	}

	public ArrayList<WordData> getEachStageList(ProgressData data, int nStage, int nLimit) {
		ArrayList<WordData> result = new ArrayList<WordData>();

		String query = String.format("SELECT a.* FROM %s as a LEFT JOIN %s as b ON a.id=b.word WHERE b.lvlnum=%s AND b.epinum=%s AND b.stgnum=%s AND b.typnum=3 ORDER BY RANDOM() LIMIT %s",
				Define.TB_WORD_DATA, Define.TB_WORD_SET, data.getLevel(), data.getEpisode(), nStage, nLimit);

		try{
			Cursor cursor = mDbConnection.query(query);
			if(cursor != null){
				int nCount = cursor.getCount();
				if(nCount > 0) {
					for(int i=0; i<nCount; i++) {

						WordData item = new WordData(cursor.getInt(0),
								cursor.getString(3),
								cursor.getString(2),
								cursor.getString(0),
								cursor.getString(0),
								0,
								0);
						result.add(item);
						cursor.moveToNext();
					}
				}
				cursor.close();
			}
		} catch(Exception e) {
			e.printStackTrace();
			close();
		}

		return result;
	}

	public ArrayList<WordData> getExerciseList(ProgressData data) {
		/*ArrayList<WordData> result = new ArrayList<WordData>();

		for (int i = 0; i < 4 ; i++){
			ArrayList<WordData> result_tmp = new ArrayList<WordData>();
			result_tmp = getEachStageList(data, i+1, 4);
			for(int j = 0; j < result_tmp.size(); j++){
				result.add(result_tmp.get(j));
			}
		}
		return result;*/

		ArrayList<WordData> result = new ArrayList<WordData>();

		String query = String.format("SELECT a.* FROM %s as a LEFT JOIN %s as b ON a.id=b.word WHERE b.lvlnum=%s AND b.epinum=%s AND b.typnum=3 ORDER BY RANDOM()",
				Define.TB_WORD_DATA, Define.TB_WORD_SET, data.getLevel(), data.getEpisode());

		try{
			Cursor cursor = mDbConnection.query(query);
			if(cursor != null){
				int nCount = cursor.getCount();
				if(nCount > 0) {
					for(int i=0; i<nCount; i++) {

						WordData item = new WordData(cursor.getInt(0),
								cursor.getString(3),
								cursor.getString(2),
								cursor.getString(0),
								cursor.getString(0),
								0,
								0);
						result.add(item);
						cursor.moveToNext();
					}
				}
				cursor.close();
			}
		} catch(Exception e) {
			e.printStackTrace();
			close();
		}

		return result;

	}

	public ArrayList<WordData> getAllTextList() {
		ArrayList<WordData> result = new ArrayList<WordData>();

		for (int i = 0; i < 9; i++) {
			ArrayList<WordData> result_tmp = new ArrayList<WordData>();
			result_tmp = getEachLevelTestList(i+1);

			for (int j = 0; j < result_tmp.size(); j++) {
				result.add(result_tmp.get(j));
			}
		}

		return result;
	}

	public ArrayList<WordData> getEachLevelTestList(int level) {
		ArrayList<WordData> result = new ArrayList<WordData>();

		int levelTestWordCounts = 0;

		switch (level) {
			case 1:
				levelTestWordCounts = Define.LEVEL1TESTWORDCOUNT;
				break;
			case 2:
				levelTestWordCounts = Define.LEVEL2TESTWORDCOUNT;
				break;
			case 3:
				levelTestWordCounts = Define.LEVEL3TESTWORDCOUNT;
				break;
			case 4:
				levelTestWordCounts = Define.LEVEL4TESTWORDCOUNT;
				break;
			case 5:
				levelTestWordCounts = Define.LEVEL5TESTWORDCOUNT;
				break;

			default:
				break;
		}

		String query = String.format("SELECT a.* FROM %s as a LEFT JOIN %s as b ON a.id=b.word WHERE b.lvlnum=%s AND b.typnum=3 ORDER BY RANDOM() LIMIT %s",
				Define.TB_WORD_DATA, Define.TB_WORD_SET, level, levelTestWordCounts);

		/*String query = String.format("SELECT * FROM word_dic WHERE level=%s ORDER BY RANDOM() LIMIT %s",level,levelTestWordCounts);*/
		try{
			Cursor cursor = mDbConnection.query(query);
			if(cursor != null) {
				int nCount = cursor.getCount();
				if(nCount > 0) {
					for(int i=0; i<nCount; i++) {
						WordData item = new WordData(cursor.getInt(0),
								cursor.getString(3),
								cursor.getString(2),
								cursor.getString(0),
								cursor.getString(0),
								level,
								0);
						result.add(item);
						cursor.moveToNext();
					}
				}
				cursor.close();
			}
		} catch(Exception e) {
			e.printStackTrace();
			close();
		}
		return result;
	}
}
