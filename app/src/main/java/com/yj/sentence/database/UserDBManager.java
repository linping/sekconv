package com.yj.sentence.database;

import java.util.ArrayList;
import com.yj.sentence.common.Define;
import com.yj.sentence.structs.UserData;
import android.content.Context;
import android.database.Cursor;

public class UserDBManager extends DBManager {
	
	public UserDBManager(Context context) {
		super(context);
	}
	
	public UserData getActiveUser() {
		UserData result = null;
		
		String query = String.format("SELECT * FROM %s WHERE active=1", Define.TB_USER);
		
		try{
			Cursor cursor = mDbConnection.query(query);
			if(cursor != null){
				int nCount = cursor.getCount();
				if(nCount > 0) {
					for(int i=0; i<nCount; i++) {
						result = new UserData(cursor.getInt(0), cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getInt(4), cursor.getInt(5));
						cursor.moveToNext();
					}
				}
				cursor.close();
			}
		} catch(Exception e) {
			e.printStackTrace();
			close();
		}
		
		return result;
	}
	
	public ArrayList<UserData> getUserList() {
		ArrayList<UserData> result = new ArrayList<UserData>();
		
		String query = String.format("SELECT * FROM %s", Define.TB_USER);
		
		try{
			Cursor cursor = mDbConnection.query(query);
			if(cursor != null){
				int nCount = cursor.getCount();
				if(nCount > 0) {
					for(int i=0; i<nCount; i++) {
						UserData item = new UserData(cursor.getInt(0), cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getInt(4), cursor.getInt(5));
						result.add(item);	
						cursor.moveToNext();
					}
				}
				cursor.close();
			}
		} catch(Exception e) {
			e.printStackTrace();
			close();
		}		
		
		return result;
	}
	
	public void Insert(UserData data) {
		String query = "";
		
		if (data == null) return;
		
		query = String.format("INSERT INTO %s (name, job, photo) VALUES ('%s', '%s', '%s')", 
							  Define.TB_USER, data.getName(), data.getJob(), data.getPhoto());
		
		excuteQuery(query);
	}
	
	public void Update(UserData data) {
		String query = "";
		
		if (data == null) return;
		
		query = String.format("UPDATE %s SET name='%s', job='%s', photo='%s' WHERE id=%s", 
							  Define.TB_USER,
							  data.getName(),
							  data.getJob(),
							  data.getPhoto(),
							  data.getId());
		
		excuteQuery(query);
	}
	
	public void Delete(UserData data) {
		String query = "";
		
		if (data == null) return;
		
		query = String.format("DELETE FROM %s WHERE id=%s", Define.TB_USER, data.getId());
		
		excuteQuery(query);
	}	
	
	public void setActive(UserData data) {
		String query = "";
		
		if (data == null) return;
		
		query = String.format("UPDATE %s SET active=0", Define.TB_USER);
		excuteQuery(query);
		
		query = String.format("UPDATE %s SET active=1 WHERE id=%s", Define.TB_USER, data.getId());
		excuteQuery(query);
	}
	
	public void setLevel(UserData data) {
		String query = "";
		
		if (data == null) return;
		
		query = String.format("UPDATE %s SET level=%s WHERE id=%s", Define.TB_USER, data.getLevel(), data.getId());
		excuteQuery(query);
	}
	
	public String getUserName(int id) {
		return getValueString(String.format("SELECT name FROM %s WHERE id=%s", Define.TB_USER, id));
	}
	
	// help 관련
	
	public int getIsUseWordHelp(int id) {
		return getIsHelp(String.format("SELECT wordHelp FROM %s WHERE id=%s", Define.TB_USER, id));
	}
	
	public int getIsUseStudyHelp(int id) {
		return getIsHelp(String.format("SELECT studyHelp FROM %s WHERE id=%s", Define.TB_USER, id));
	}
	
	public int getIsUseResultHelp(int id) {
		return getIsHelp(String.format("SELECT resultHelp FROM %s WHERE id=%s", Define.TB_USER, id));
	}
	
	public int getIsUseSettingHelp(int id) {
		return getIsHelp(String.format("SELECT settingHelp FROM %s WHERE id=%s", Define.TB_USER, id));
	}
	
	public void setNoUseWordHelp(int id) {
		setNoUseHelp(String.format("UPDATE %s SET wordHelp=0 WHERE id=%s", Define.TB_USER, id));
	}
	
	public void setNoUseStudyHelp(int id) {
		setNoUseHelp(String.format("UPDATE %s SET studyHelp=0 WHERE id=%s", Define.TB_USER, id));
	}
	
	public void setNoUseResultHelp(int id) {
		setNoUseHelp(String.format("UPDATE %s SET resultHelp=0 WHERE id=%s", Define.TB_USER, id));
	}
	
	public void setNoUseSettingHelp(int id) {
		setNoUseHelp(String.format("UPDATE %s SET settingHelp=0 WHERE id=%s", Define.TB_USER, id));
	}
	
	private int getIsHelp(String query) {
		return getValueInt(query);
	}
	
	private void setNoUseHelp(String query) {
		excuteQuery(query);
	}
	
}
