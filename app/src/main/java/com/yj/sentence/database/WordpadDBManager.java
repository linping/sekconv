package com.yj.sentence.database;

import java.util.ArrayList;

import com.yj.sentence.common.Define;
import com.yj.sentence.structs.WordData;
import com.yj.sentence.structs.WordpadData;

import android.content.Context;
import android.database.Cursor;

public class WordpadDBManager extends DBManager {
	
	public WordpadDBManager(Context context) {
		super(context);
	}
	
	public WordpadDBManager(Context context, int uid) {
		super(context, uid);
	}
	
	public ArrayList<WordpadData> getWordListBySort1(int uid, int order, int kind) {
		String query = String.format("SELECT a.id, a.wid, b.mean_en, b.mean_kr, b.image, b.sound, b.level ");
		query += String.format("FROM %s AS a LEFT JOIN %s AS b ON a.wid=b.id WHERE a.uid=%s ", Define.TB_WORD_PAD, Define.TB_WORD_DIC, uid);
		query += (kind>0) ? String.format("AND a.kind=%s ", kind-1) : "";
		
		if (order == 0 || order == 1)
			query += String.format("ORDER BY b.mean_en");
		else
			query += String.format("ORDER BY b.mean_kr");
		
		
		return getWordList(query);
	}
	
	public ArrayList<WordpadData> getWordListBySort2(int uid, int kind) { 
		String query = String.format("SELECT a.id, a.wid, b.mean_en, b.mean_kr ");
		query += String.format("FROM %s AS a LEFT JOIN %s AS b ON a.wid=b.id ", Define.TB_WORD_PAD, Define.TB_WORD_DATA);
		query += String.format("WHERE a.uid=%s ", uid);
		query += String.format("AND a.kind=%s ", kind);
		query += String.format("ORDER BY a.id DESC");
		
		return getWordList(query);
	}

	public ArrayList<WordpadData> getWordListBySort3(int uid, int kind, int nEpisode) {
		String query = String.format("SELECT a.id, a.wid, b.mean_en, b.mean_kr ");
		query += String.format("FROM %s AS a LEFT JOIN %s AS b ON a.wid=b.id ", Define.TB_WORD_PAD, Define.TB_WORD_DATA);
		query += String.format("WHERE a.uid=%s ", uid);
		query += String.format("AND a.kind=%s ", kind);
		query += String.format("ORDER BY a.id DESC");

		return getWordList(query, nEpisode);
	}

	public WordpadData getWordByWordId(int wid) {
		String query = String.format("SELECT id, mean_en, mean_kr, image, sound, level FROM %s WHERE id=%s", Define.TB_WORD_DIC, wid);

		try{
			Cursor cursor = mDbConnection.query(query);
			if(cursor != null){
				int nCount = cursor.getCount();
				if(nCount > 0) {
					for(int i=0; i<nCount; i++) {
						WordpadData item = new WordpadData();

						WordData word = new WordData(cursor.getInt(0), 
													 cursor.getString(2), 
													 cursor.getString(1), 
													 cursor.getString(0), 
													 cursor.getString(0), 
													 cursor.getInt(5), 
													 0);

						item.setWord(word);
						return item;
					}
				}
				cursor.close();
			}
		} catch(Exception e) {
			e.printStackTrace();
			close();
		}
		return new WordpadData();

	}
	
	public ArrayList<WordpadData> getWordListByWordId(int wid) {
		String query = String.format("SELECT id, mean_en, mean_kr, image, sound, level FROM %s WHERE id=%s", Define.TB_WORD_DIC, wid);
		
		ArrayList<WordpadData> result = new ArrayList<WordpadData>();
		
		try{
			Cursor cursor = mDbConnection.query(query);
			if(cursor != null){
				int nCount = cursor.getCount();
				if(nCount > 0) {
					for(int i=0; i<nCount; i++) {
						WordpadData item = new WordpadData();
						
						WordData word = new WordData(cursor.getInt(0),
													 cursor.getString(2),
													 cursor.getString(1),
													 cursor.getString(0),
													 cursor.getString(0), 
													 cursor.getInt(5),
													 0);

						item.setWord(word);
						result.add(item);
						cursor.moveToNext();
					}
				}
				cursor.close();
			}
		} catch(Exception e) {
			e.printStackTrace();
			close();
		}
		
		return result;
	}
		
	private ArrayList<WordpadData> getWordList(String query) {
		ArrayList<WordpadData> result = new ArrayList<WordpadData>();
		
		try{
			Cursor cursor = mDbConnection.query(query);
			if(cursor != null){
				int nCount = cursor.getCount();
				if(nCount > 0) {
					for(int i=0; i<nCount; i++) {
						WordpadData item = new WordpadData(cursor.getInt(0), cursor.getInt(1));
						
						WordData word = new WordData(cursor.getInt(1),
													 cursor.getString(3),
													 cursor.getString(2), 
													 cursor.getString(1), 
													 cursor.getString(1), 
													 0,
													 0);

						item.setWord(word);
						result.add(item);
						cursor.moveToNext();
					}
				}
				cursor.close();
			}
		} catch(Exception e) {
			e.printStackTrace();
			close();
		}		
		
		return result;
	}

	private ArrayList<WordpadData> getWordList(String query, int nEpisode) {
		ArrayList<WordpadData> result = new ArrayList<WordpadData>();
		ArrayList<WordpadData> epiResult = new ArrayList<WordpadData>();

		try{
			Cursor cursor = mDbConnection.query(query);
			if(cursor != null){
				int nCount = cursor.getCount();
				if(nCount > 0) {
					for(int i=0; i<nCount; i++) {
						WordpadData item = new WordpadData(cursor.getInt(0), cursor.getInt(1));

						WordData word = new WordData(cursor.getInt(1),
								cursor.getString(3),
								cursor.getString(2),
								cursor.getString(1),
								cursor.getString(1),
								0,
								0);

						item.setWord(word);
						result.add(item);
						cursor.moveToNext();
					}
				}
				cursor.close();
			}
		} catch(Exception e) {
			e.printStackTrace();
			close();
		}

		for(int i = 0; i < result.size(); i++){
			WordpadData item = result.get(i);
			int nWordId = item.getWid();

			String epiQuery = String.format("Select epinum from %s where word=%s", Define.TB_WORD_SET, nWordId );
			Cursor cursor = mDbConnection.query(epiQuery);

			int nCount = cursor.getCount();
			if(nCount > 0){
				int nNumber = cursor.getInt(0);
				if(nNumber == nEpisode){
					epiResult.add(item);
				}
			}
		}

		return epiResult;
	}

	public void Delete(WordpadData data) {
		String query = "";
		
		if (data == null) return;
		
		query = String.format("DELETE FROM %s WHERE id=%s", Define.TB_WORD_PAD, data.getId());
		
		excuteQuery(query);
	}

	public void Delete(int mWPID) {
		String query = "";

		query = String.format("DELETE FROM %s WHERE id=%s", Define.TB_WORD_PAD, mWPID);

		excuteQuery(query);
	}

	public void InsertWordpad(int kind, int wid) {
		String query = "";
		
		if (isExistWord(wid)) {
			query = String.format("UPDATE word_pad SET kind=%s WHERE uid=%s AND wid=%s", kind, mUid, wid);
		} else {
			query = String.format("INSERT INTO word_pad (kind, uid, wid) VALUES (%s, %s, %s)", kind, mUid, wid);
		}
		
		excuteQuery(query);
	}
	
	private boolean isExistWord(int wid) {
		int result = getValueInt(String.format("SELECT id FROM word_pad WHERE uid=%s AND wid=%s", mUid, wid));
		return (result>0) ? true : false;
	}
	
}
