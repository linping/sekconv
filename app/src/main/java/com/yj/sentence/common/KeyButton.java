package com.yj.sentence.common;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;

public class KeyButton extends Button {

    public KeyButton(Context context) {
        super(context);
    }

    public KeyButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        ArialFontHelper.setCustomFont(this, context, attrs);
    }

    public KeyButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        ArialFontHelper.setCustomFont(this, context, attrs);
    }
}
