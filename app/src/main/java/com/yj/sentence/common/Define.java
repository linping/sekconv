package com.yj.sentence.common;

import android.os.Environment;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashSet;
import java.util.Locale;

public class Define {

	//public final static String PREFIX_PATH = Environment.getExternalStorageDirectory().getPath();
	//public final static String PREFIX_PATH = "/storage/0000-006F";
	//public final static String PREFIX_PATH = getExternalMounts() + "/EnglishConv_Domestic";
	//public final static String PREFIX_PATH = getExternalMounts() + "/EnglishConv";
	//public final static String FONT_PATH = "fonts/pwpugobo.ttf";
	//public final static String SOUND_TYPE = ".mp3";
	//public final static String SOUND_PATH = PREFIX_PATH + "/mp3/";
	//public final static String IMAGE_PATH = PREFIX_PATH + "/png/";
	//public final static String VIDEO_PATH = PREFIX_PATH + "/mp4/";

	public final static String PREFIX_PATH = getExternalMounts();

	public final static boolean bDomestic = true;
/*
	public final static String FONT_PATH = "fonts/WKLCHBO.TTF";
	public final static String SUBFONT_PATH = "fonts/PWPUCHBK.TTF";
	public final static String ENGLISH_FONT_PATH = "fonts/arial.ttf";
*/
	public final static String DOMESTIC_FONT_PATH = "fonts/WKLCHBO.TTF";
	public final static String DOMESTIC_SUBFONT_PATH = "fonts/PWPUCHBK.TTF";
	public final static String DOMESTIC_ENGLISH_FONT_PATH = "fonts/arial.ttf";

	public final static String ABROAD_FONT_PATH = "fonts/arial.ttf";
	public final static String ABROAD_SUBFONT_PATH = "fonts/arial.ttf";
	public final static String ABROAD_ENGLISH_FONT_PATH = "fonts/arial.ttf";


	public static String getMainFont(){
		if(bDomestic)
			return DOMESTIC_FONT_PATH;
		else
			return ABROAD_FONT_PATH;
	}

	public static String getSubFont(){
		if(bDomestic)
			return DOMESTIC_SUBFONT_PATH;
		else
			return ABROAD_SUBFONT_PATH;
	}

	public static String getEnglishFont(){
		if(bDomestic)
			return DOMESTIC_ENGLISH_FONT_PATH;
		else
			return ABROAD_ENGLISH_FONT_PATH;
	}

	public final static String RECORD_PATH_RAW =  Environment.getExternalStorageDirectory().getPath() + "/SEKStudy/ECONV/tmp/record.raw";
	public final static String RECORD_PATH =  Environment.getExternalStorageDirectory().getPath() + "/SEKStudy/ECONV/tmp/record.wav";
	public final static String TMP_PATH = Environment.getExternalStorageDirectory().getPath()+ "/SEKStudy/ECONV/tmp/";
	public final static String DATA_PATH = PREFIX_PATH + "/ECONV/";


	public final static int SCHEDULE_MEDAL = 1;
	public final static int SCHEDULE_STUDY = 2;
	public final static int SCHEDULE_NO = 3;
	
	public final static int MEDAL_BAD = 2;
	public final static int MEDAL_NORMAL = 3;
	public final static int MEDAL_GOOD = 4;
	public final static int MEDAL_EXCELLENT = 5;

	//step
	public final static int STUDY_STEP_WORD = 0;
	public final static int STUDY_STEP_SENTENCE = 1;
	public final static int STUDY_STEP_TEST = 2;
	public final static int STUDY_STEP_PRACTICE = 3;

	public final static long ONE_DAY_TIME = 1000*60*60*24;
	
	public final static String DBFileName = "conv_db.db";
	public final static String PAKDBFileName = "data.db";
	public final static String DB_PATH = "/data/data/com.sforeignpad.sekconv/databases";
	
	public final static String TB_USER = "user";
	public final static String TB_WORD_DIC = "word_dic";
	public final static String TB_TEST_DIC = "test_dic";
	public final static String TB_WORD_PAD = "word_pad";
	public final static String TB_SCHEDULE= "schedule";

	//added
	public final static String TB_PROGRESS = "progress";
	public final static String TB_PROGRESS_MARKS = "progress_marks";
	public final static String TB_PROGRESS_TIMES = "progress_times";
	public final static String TB_PROGRESS_WORDS = "progress_words";

	public final static String TB_COMPLETE = "complete";
	public final static String TB_STAGE = "stage_info";
	public final static String TB_EPISODE = "episode_info";
	public final static String TB_WORD_SET = "word_set";
	public final static String TB_WORD_DATA = "word_data";
	public final static String TB_WORD_DATA_PRO = "word_data_pro";
	public final static String TB_WORD_SET_PRO = "word_set_pro";

	public final static String USER_ID = "UserId";
	public final static String SCHEDULE_ID = "ScheduleId";
	
	public final static int KIND_IMPORT_WORD = 0;
	public final static int KIND_WRONG_WORD = 1;
	
	public final static int KIND_TEST_VTT = 0;
	public final static int KIND_TEST_TTV = 1;
	public final static int KIND_TEST_VTS = 2;
	public final static int KIND_TEST_LTV = 3;
	
	public final static String IS_ORDERTEST = "IsOrderTest";
	public final static String IS_EXERCISE = "exercise";
	public final static String IS_VTT = "vtt";
	public final static String IS_TTV = "ttv";
	public final static String IS_VTS = "vts";
	public final static String IS_LTV = "ltv";

	public final static String STUDY_ORDER = "StudyOrder";
	public final static String STUDY_EPISODE = "StudyEpisode";
	public final static String STUDY_STAGE = "StudyStage";

	public final static String SCHEDULE_VIEW_ACTION = "com.yj.sentence.scheduleview";
	public final static String USER_ORDER_ACTION = "com.yj.sentence.userorder";

	public final static String TEXTFONT_GOTHICSTD_BOLD_PATH = "fonts/AdobeGothicStd-Bold.otf";
	public final static String TEXTFONT_GULIM_PATH = "fonts/ufonts.com_gulim.ttf";
	public final static String TEXTFONT_GULIM_PATH1 = "fonts/GULIM.TTC";
	
	
	//급수시험 수준별 단어개수
	public final static int LEVEL1TESTWORDCOUNT = 5;
	public final static int LEVEL2TESTWORDCOUNT = 8;
	public final static int LEVEL3TESTWORDCOUNT = 14;
	public final static int LEVEL4TESTWORDCOUNT = 8;
	public final static int LEVEL5TESTWORDCOUNT = 5;
	public final static int LEVELALLTESTWORDCOUNT = 40;

	public final static int LevelProblemCounts = 10;
	
	//급수시험 총 문제수
	public final static int LevelTestProblemCounts = 40;

	public static String getExternalMounts() {
		final HashSet<String> out = new HashSet<String>();
		String reg = "(?i).*vold.*(vfat|ntfs|exfat|fat32|ext3|ext4).*rw.*";
		String s = "";
		try {
			final Process process = new ProcessBuilder().command("mount")
					.redirectErrorStream(true).start();
			process.waitFor();
			final InputStream is = process.getInputStream();
			final byte[] buffer = new byte[1024];
			while (is.read(buffer) != -1) {
				s = s + new String(buffer);
			}
			is.close();
		} catch (final Exception e) {
			e.printStackTrace();
		}

		// parse output
		final String[] lines = s.split("\n");
		for (String line : lines) {
			if (!line.toLowerCase(Locale.US).contains("asec")) {
				if (line.matches(reg)) {
					String[] parts = line.split(" ");
					for (String part : parts) {
						if (part.startsWith("/"))
							if (!part.toLowerCase(Locale.US).contains("vold")){
								out.add(part);

								String strSplit[] = part.split("/");
								String strPath = "/storage/" + strSplit[strSplit.length - 1];

								{
									File file = new File("/sdcard/log_sekconv.tmp");
									try {
										file.createNewFile();
										FileOutputStream fout = new FileOutputStream(file);
										fout.write(strPath.getBytes());
										fout.close();
									} catch (IOException e) {
										e.printStackTrace();
									}

								}
								return strPath;
							}
					}
				}
			}
		}
		return null;

	}
}