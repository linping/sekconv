package com.yj.sentence.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Random;

import com.yj.sentence.R;
import com.yj.sentence.common.Define;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.media.AudioManager;
import android.media.SoundPool;
import android.widget.Button;
import android.widget.TextView;

public class ESUtils {

	@SuppressLint("NewApi")
	/*public static void setTextViewTypeFaceByRes(Context context, TextView tv, String fontPath) {
		if(!fontPath.isEmpty()) {
			Typeface tf = Typeface.createFromAsset(context.getAssets(), fontPath);
			tv.setTypeface(tf);
		}
	}

	public static void setButtonTypeFaceByRes(Context context, Button tv, String fontPath) {
		if(!fontPath.isEmpty()) {
			Typeface tf = Typeface.createFromAsset(context.getAssets(), fontPath);
			tv.setTypeface(tf);
		}
	}*/

	private static HashMap<String, Typeface> m_TypefaceMap = new HashMap<String, Typeface>();


	public static boolean isVerified() {

		ProcessBuilder cmd;
		String result="";

		try{
			String[] args = {"/system/bin/cat", "/proc/cpuinfo"};
			cmd = new ProcessBuilder(args);

			Process process = cmd.start();
			InputStream in = process.getInputStream();
			byte[] re = new byte[1024];
			while(in.read(re) != -1){
				System.out.println(new String(re));
				result = result + new String(re);
			}
			in.close();
		} catch(IOException ex){
			ex.printStackTrace();
		}

		String CPUName = "";

		String[] lines = result.split("\n");

		for (int i = 0; i < lines.length; i++) {

			String temp = lines[i];

			if (lines[i].contains("Hardware\t:")) {

				CPUName = lines[i].replace("Hardware\t: ", "");
				break;
			}
		}

		if(CPUName.contains("sun8i"))
			return true;
		return false;
	}

	public static void setTextViewTypeFaceByRes(Context context, TextView tv, String sfontPath) {
		String fontPath = sfontPath;
		if(sfontPath.contains(Define.getMainFont()) && tv.getTextSize() < 20)
			fontPath = Define.getSubFont();

		Typeface tf = m_TypefaceMap.get(fontPath);
		if(tf == null){

			if(fontPath.contains(Define.getMainFont())){
				tf = Typeface.createFromAsset(context.getAssets(), fontPath);
			}else if(fontPath.contains(Define.getSubFont())){
				tf = Typeface.createFromAsset(context.getAssets(), fontPath);
			}else{
				tf = Typeface.create(Typeface.SANS_SERIF, Typeface.NORMAL);
			}

			m_TypefaceMap.put(fontPath, tf);

		}

		/*if(fontPath.contains(Define.getMainFont()))
			tv.setLetterSpacing(-0.1f);
*/
		tv.setTypeface(tf);
	}

	public static void setButtonTypeFaceByRes(Context context, Button tv, String sfontPath) {
		String fontPath = sfontPath;
		if(sfontPath.contains(Define.getMainFont()) && tv.getTextSize() < 20)
			fontPath = Define.getSubFont();

		Typeface tf = m_TypefaceMap.get(fontPath);
		if(tf == null){

			if(fontPath.contains(Define.getMainFont())){
				tf = Typeface.createFromAsset(context.getAssets(), fontPath);
			}else if(fontPath.contains(Define.getSubFont())){
				tf = Typeface.createFromAsset(context.getAssets(), fontPath);
			}else{
				tf = Typeface.create(Typeface.SANS_SERIF, Typeface.NORMAL);
			}

			m_TypefaceMap.put(fontPath, tf);
		}

		tv.setTypeface(tf);
	}


	public static String getDateFormat(Context context, long milliseconds) {
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(milliseconds);

		return String.valueOf(cal.get(Calendar.YEAR)) + context.getString(R.string.year) +
				String.format("%02d", cal.get(Calendar.MONTH) + 1) + context.getString(R.string.month) +
				String.format("%02d", cal.get(Calendar.DATE)) + context.getString(R.string.day);
	}

	public static int getDay(Context context, int lastDays) {
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(cal.getTimeInMillis() + Define.ONE_DAY_TIME * lastDays);

		return cal.get(Calendar.DATE);
	}

	public static long getTimeMilliSeconds(Context context, int lastDays) {
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(cal.getTimeInMillis() + Define.ONE_DAY_TIME * lastDays);

		return cal.getTimeInMillis();
	}

	public static int getNowDay(Context context) {
		Calendar cal = Calendar.getInstance();
		return cal.get(Calendar.DATE);
	}

	public static String getNowDate(Context context) {
		Calendar cal = Calendar.getInstance();

		String strNowDate = "";

		strNowDate = Integer.toString(cal.get(Calendar.YEAR)).substring(2) +
				"." + String.format("%02d", cal.get(Calendar.MONTH) + 1) +
				"." + String.format("%02d", cal.get(Calendar.DATE));

		return strNowDate;
	}

	public static String getLastDate(Context context, int lastDays) {
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(cal.getTimeInMillis() + Define.ONE_DAY_TIME * lastDays);

		String strNowDate = "";

		strNowDate = Integer.toString(cal.get(Calendar.YEAR)).substring(2) +
				"." + String.format("%02d", cal.get(Calendar.MONTH) + 1) +
				"." + String.format("%02d", cal.get(Calendar.DATE));

		return strNowDate;
	}

	public static String getLastDateSlash(Context context, int lastDays) {
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(cal.getTimeInMillis() + Define.ONE_DAY_TIME * lastDays);

		String strNowDate = "";

		strNowDate = Integer.toString(cal.get(Calendar.YEAR)) +
					"/" + String.format("%02d", cal.get(Calendar.MONTH) + 1) +
					"/" + String.format("%02d", cal.get(Calendar.DATE));

		return strNowDate;
	}

	public static String getDateString(long milliseconds) {
		String strDate = "";
		
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(milliseconds);
		
		strDate = Integer.toString(cal.get(Calendar.YEAR)) +
				"/" + String.format("%02d", cal.get(Calendar.MONTH) + 1) +
				"/" + String.format("%02d", cal.get(Calendar.DATE));
		
		return strDate;
	}
	
	public static long getNowDateMilliseconds() {
		Calendar cal = Calendar.getInstance();
		return cal.getTimeInMillis();
	}
	
	public static long getDateMilliseconds(int year, int month, int day) {
		Calendar cal = Calendar.getInstance();
		Date date = new Date(year - 1900, month, day);
		cal.setTime(date);
		return cal.getTimeInMillis();
	}
	
	public static int getDateOfYear(long milliseconds) {
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(milliseconds);
		
		return cal.get(Calendar.YEAR);
	}
	
	public static int getDateOfMonth(long milliseconds) {
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(milliseconds);
		
		return cal.get(Calendar.MONTH)+1;
	}
	
	public static int getDateOfDay(long milliseconds) {
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(milliseconds);
		
		return cal.get(Calendar.DATE);
	}
	
	public static String getDateStringOtherType(String strDate) {
		String [] strContents = strDate.split("/");
		if(strContents.length < 3)
			return "";
		
		String strResDate = strContents[0].substring(2) +
					"." + strContents[1] +
					"." + strContents[2];
			
		return strResDate;
	}
	
	public static boolean isSameDate(long milliseconds1, long milliseconds2) {
		boolean bResult = false;
		
		Calendar cal1 = Calendar.getInstance();
		cal1.setTimeInMillis(milliseconds1);
		
		Calendar cal2 = Calendar.getInstance();
		cal2.setTimeInMillis(milliseconds2);
		
		if(cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) &&
				cal1.get(Calendar.MONTH) == cal2.get(Calendar.MONTH) &&
				cal1.get(Calendar.DATE) == cal2.get(Calendar.DATE))
			bResult = true;
		
		return bResult;
	}
	
	public static int getMedalType(float fScore) {
		int nType = Define.MEDAL_BAD;
		
		if(fScore >= 90.0f && fScore <= 100.0f)
			nType = Define.MEDAL_EXCELLENT;
		else if(fScore >= 80.0f && fScore < 90.0f)
			nType = Define.MEDAL_GOOD;
		else if(fScore >= 70.0f && fScore < 80.0f)
			nType = Define.MEDAL_NORMAL;
		
		return nType;
	}
	
	public static long getNowTimeSeconds() {
		Calendar cal = Calendar.getInstance();
		long nSecond = cal.getTimeInMillis()/1000;
		
		return nSecond;
	}
	
	public static int getRedColor(int nColor) {
		int nRed = 0;
		nRed = nColor/0xffff;
		//nRed = nColor<<16;
		return nRed;
	}
	
	public static int getGreenColor(int nColor) {
		int nGreen = 0;
		nGreen = (nColor%0xffff)/0xff;
		//nGreen = nColor<<8;
		return nGreen;
	}
	
	public static int getBlueColor(int nColor) {
		int nBlue = 0;
		//nBlue = ((nColor%0xffff)%0xff)/0xff;
		nBlue = nColor&0xff;
		return nBlue;
	}
	
	public static int getRandomValue(int nLimit) {
		int val = 0;
		Random rnd = new Random();
		
		while(true) {
			val = rnd.nextInt(nLimit);
			
			if (val != 0)
				break;
		}
		
		return val;
	}
	
	public static int getRandomValue(int nLimit, ArrayList<Integer> indexs) {
		int val = 0;
		Random rnd = new Random();
		
		while(true) {
			val = rnd.nextInt(nLimit);
			
			if (!indexs.contains(val))
				break;
		}
		
		return val;
	}
	
	public static float getTextWidth(Paint paint, String strText) {
		float width = 0.0f;
		float[] chrWidths = new float[strText.length()];
		
		paint.getTextWidths(strText, chrWidths);
		
		for(int i=0; i<strText.length(); i++)
			width += chrWidths[i];
		
		return width;
	}

	private static SoundPool m_soundPoolTest = null;
	private static HashMap<String, Integer> m_soundMapTest = new HashMap<String, Integer>();
	private static float m_fVolume = 0.0f;
	private static boolean bInitialized = false;

	public static void initializeSounds(Context context){
		m_soundPoolTest = new SoundPool(5, AudioManager.STREAM_MUSIC, 0);
		m_fVolume = 0.99f;

		m_soundMapTest = new HashMap<String, Integer>();
		m_soundMapTest.put("tock", Integer.valueOf(m_soundPoolTest.load(context, R.raw.tock, 1)));
		m_soundMapTest.put("success", Integer.valueOf(m_soundPoolTest.load(context, R.raw.success, 1)));
		m_soundMapTest.put("fail", Integer.valueOf(m_soundPoolTest.load(context, R.raw.fail, 1)));
		m_soundMapTest.put("page", Integer.valueOf(m_soundPoolTest.load(context, R.raw.page, 1)));

	}

	public static void playBtnSound(Context context){
		/*if(m_soundPoolTest == null){
			initializeSounds(context);
		}

		if(m_soundPoolTest != null) {
			m_soundPoolTest.play(m_soundMapTest.get("tock").intValue(), m_fVolume, m_fVolume, 1, 0, 1.0F);
		}*/

	}

	public static void playSuccessSound(Context context){

		if(m_soundPoolTest == null){
			initializeSounds(context);
		}

		if(m_soundPoolTest != null)
			m_soundPoolTest.play(m_soundMapTest.get("success").intValue(), m_fVolume, m_fVolume, 1, 0, 1.0F);
	}

	public static void playFailSound(Context context){

		if(m_soundPoolTest == null){
			initializeSounds(context);
		}

		if(m_soundPoolTest != null)
			m_soundPoolTest.play(m_soundMapTest.get("fail").intValue(), m_fVolume, m_fVolume, 1, 0, 1.0F);
	}

	public static void playPageSound(Context context){

		if(m_soundPoolTest == null){
			initializeSounds(context);
		}

		if(m_soundPoolTest != null)
			m_soundPoolTest.play(m_soundMapTest.get("page").intValue(), m_fVolume, m_fVolume, 1, 0, 1.0F);
	}
}
