package com.yj.sentence.structs;

public class PAKITEMData {

	public int nID;
	public int nWordID;
	public int nType;
	public int nFileID;
	public int nOffset;
	public int nSize;

	public PAKITEMData() {
	}

	public PAKITEMData(int id, int wid, int type, int fid, int offset, int size) {
		this.nID = id;
		this.nWordID = wid;
		this.nType = type;
		this.nFileID = fid;
		this.nOffset = offset;
		this.nSize = size;
	}

}
