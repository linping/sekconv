package com.yj.sentence.structs;

public class EpisodeData {

	private int lvlnum;
	private int epinum;
	private String epiname;
	private String strDesc1;
	private String strDesc2;

	public EpisodeData() {
		this(-1, 0, "", "", "");
	}

	public EpisodeData(int lvlnum, int epinum, String epiname, String strDesc1, String strDesc2) {
		this.lvlnum = lvlnum;
		this.epinum = epinum;
		this.epiname = epiname;
		this.strDesc1 = strDesc1;
		this.strDesc2 = strDesc2;

	}

	public String getStrDesc1() {
		return strDesc1;
	}

	public String getStrDesc2() {
		return strDesc2;
	}


}
