package com.yj.sentence.structs;

public class UserData {
	
	private int id;			//ID	
	private String name;	//이름
	private String job;		//직업
	private String photo;	//사진
	private int level;		//급수
	private int active;		//선택됬는가 판단 1:선택, 0:선택안됨
	
	public UserData() {
		this.id = -1;
		this.name = "";
		this.job = "";
		this.photo = "";
		this.level = -1;
		this.active = 0;
	}
	
	public UserData(int id, String name, String job, String photo, int level, int active) {
		this.id = id;
		this.name = name;
		this.job = job;
		this.photo = photo;
		this.level = level;
		this.active = active;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getJob() {
		return job;
	}
	
	public void setJob(String job) {
		this.job = job;
	}
	
	public String getPhoto() {
		return photo;
	}
	
	public void setPhoto(String photo) {
		this.photo = photo;
	}
	
	public int getLevel() {
		return level;
	}
	
	public void setLevel(int level) {
		this.level = level;
	}
	
	public int getActive() {
		return active;
	}
	
	public void setActive(int active) {
		this.active = active;
	}
	
}
