package com.yj.sentence.structs;

public class WordData {
	
	private int id;
	private String meanKr;
	private String meanEn;
	private String image;
	private String sound;
	private int level;
	private int complete;
	
	public WordData() {
		this(-1, "", "", "", "", 0, 0);		
	}
	
	public WordData(String meanKr, String meanEn, String image, String sound) {
		this(-1, meanKr, meanEn, image, sound, 0, 0);
	}
	
	public WordData(int id, String meanKr, String meanEn, String image, String sound, int level, int complete) {
		this.id = id;
		this.meanKr = meanKr;
		this.meanEn = meanEn;
		this.image = image;
		this.sound = sound;
		this.level = level;
		this.complete = complete;
	}
	
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getMeanKr() {
		return meanKr;
	}
	
	public void setMeanKr(String meanKr) {
		this.meanKr = meanKr;
	}
	
	public String getMeanEn() {
		return meanEn;
	}
	
	public void setMeanEn(String meanEn) {
		this.meanEn = meanEn;
	}
	
	public String getImage() {
		return image;
	}
	
	public void setImage(String image) {
		this.image = image;
	}
	
	public String getSound() {
		return sound;
	}
	
	public void setSound(String sound) {
		this.sound = sound;
	}
	
	public int getLevel() {
		return level;
	}
	
	public void setLevel(int level) {
		this.level = level;
	}

	public int getComplete() {
		return complete;
	}

	public void setComplete(int complete) {
		this.complete = complete;
	}
	
}
