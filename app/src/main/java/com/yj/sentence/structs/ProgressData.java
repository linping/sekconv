package com.yj.sentence.structs;

public class ProgressData {

	private int userId;
	private int level;
	private int episode;
	private int stage;
	private int marks;

	public ProgressData() {
		this(-1, 0, 0, 0);
	}

	public ProgressData(int userId, int level, int episode, int stage) {
		this.level = level;
		this.userId = userId;
		this.episode = episode;
		this.stage = stage;
		this.marks = 0;
	}

	public ProgressData(int userId, int level, int episode, int stage, int marks) {
		this.level = level;
		this.userId = userId;
		this.episode = episode;
		this.stage = stage;
		this.marks = marks;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public int getEpisode() {
		return episode;
	}

	public void setEpisode(int episode) {
		this.episode = episode;
	}

	public int getStage() {
		return stage;
	}

	public void setStage(int stage) {
		this.stage = stage;
	}

	public int getMarks() {return marks; }

	public void setMarks(int marks) {this.marks = marks;}


}
