package com.yj.sentence.structs;

public class StageData {

	private int lvlnum;
	private int epinum;
	private int stgnum;
	private int stgimg;
	private String stgname;
	private String strDesc1;
	private String strDesc2;
	private int mWord;

	public StageData() {
		this(-1, 0, 0, 0, "", "", "", 0);
	}

	public StageData(int lvlnum, int epinum, int stgnum, int stgimg, String stgname, String strDesc1, String strDesc2, int mWord) {
		this.lvlnum = lvlnum;
		this.epinum = epinum;
		this.stgnum = stgnum;
		this.stgimg = stgimg;
		this.stgname = stgname;
		this.strDesc1 = strDesc1;
		this.strDesc2 = strDesc2;
		this.mWord = mWord;

	}

	public int getStageImage() {
		return stgimg;
	}

	public String getStgname() {
		return stgname;
	}


	public String getStrDesc1() {
		return strDesc1;
	}

	public String getStrDesc2() {
		return strDesc2;
	}

	public int getmWord() {
		return mWord;
	}
}
