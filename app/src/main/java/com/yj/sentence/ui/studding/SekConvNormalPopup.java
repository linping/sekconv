package com.yj.sentence.ui.studding;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.yj.sentence.R;
import com.yj.sentence.common.Define;
import com.yj.sentence.utils.ESUtils;


public class SekConvNormalPopup extends Dialog implements View.OnClickListener {

	private Context mContext;
	private Button mBtnOK;

	private TextView textTitle;
	private TextView textDescription;

	private OnSEKLaunchListener mListener;

	public SekConvNormalPopup(Context context) {
		super(context, R.style.MyDialog);
		// TODO Auto-generated constructor stub
		this.mContext = context;
		this.setCancelable(false);
		initViews();
	}
	
	private void initViews() {
		View localView = LayoutInflater.from(this.mContext).inflate(R.layout.sekconv_normal_popup, null);
		this.setContentView(localView);
		
		mBtnOK = (Button)localView.findViewById(R.id.btnOk);
		mBtnOK.setOnClickListener(this);

		textDescription = (TextView) findViewById(R.id.textDescription);
		textTitle = (TextView) findViewById(R.id.textTitle);

		ESUtils.setTextViewTypeFaceByRes(mContext, textDescription, Define.getMainFont());
		ESUtils.setTextViewTypeFaceByRes(mContext, textTitle, Define.getMainFont());
	}
	
	public void setListener(OnSEKLaunchListener listener) {
		mListener = listener;
	}

	public void setInfo(String strTitle, String strDescription)
	{
		textTitle.setText(strTitle);
		textDescription.setText(strDescription);
	}

	@Override
	public void dismiss() {
	    super.dismiss();
	}


	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		ESUtils.playBtnSound(mContext);
		switch (arg0.getId()) {
			case R.id.btnOk:
				mListener.onOK();
				this.dismiss();
				break;
				
		}
	}
	
	public abstract interface OnSEKLaunchListener {
		public abstract void onOK();
	}

}
