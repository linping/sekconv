/*
 * Copyright (C) 2010 Neil Davies
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * This code is base on the Android Gallery widget and was Created 
 * by Neil Davies neild001 'at' gmail dot com to be a Coverflow widget
 * 
 * @author Neil Davies
 */
package com.yj.sentence.ui.studding.adapter;

import android.content.Context;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.yj.sentence.R;
import com.yj.sentence.ui.studding.EpisodeView;

import org.json.JSONArray;

public class ListAdapter extends BaseAdapter {

	private Context m_context;
	private JSONArray m_data;
	private LayoutInflater m_inflater;
	private Handler m_handler;
	private int m_nUserId = -1;

	private int nSelectedEpisode;
	private int nSelectedStage;

	public ListAdapter(Context ctx, Handler handler, int nUserId) {

		m_context = ctx;
		m_inflater = (LayoutInflater)m_context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		m_handler = handler;
		m_nUserId = nUserId;
	}

	public void setInfo(int epi, int stg){
		nSelectedEpisode = epi;
		nSelectedStage = stg;
	}

	@Override
	public int getCount() {
		return 9;
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;

		//if(row == null)
		{
			//row = m_inflater.inflate(R.layout.progress_item, parent, false);

			EpisodeView viewEpisode = new EpisodeView(m_context);
			viewEpisode.setEpiNumber(position + 1);
			viewEpisode.setHandler(m_handler);
			viewEpisode.setUserId(m_nUserId);

			if(nSelectedEpisode == position + 1){
				viewEpisode.setSelectedIndex(nSelectedStage);
			}else{
				viewEpisode.setSelectedIndex(-1);
			}

			row = viewEpisode;
		}


		return row;
	}
}