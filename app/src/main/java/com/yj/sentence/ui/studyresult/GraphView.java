package com.yj.sentence.ui.studyresult;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.View;

public class GraphView extends View {
	
	public final int DRAW = 0;
	
	public int value;
	public int days;
	public int colorIndex;
	public int index;
	public float offset;
	public int left;
	public int right;
	public int top;
	public int bottom;
	
	public boolean isExit;
	public ArrayList<Integer> data;
	public ArrayList<int[]> color;
	
	public Paint mPaint;
	public Handler mHandler;
	public Thread mThread;

	public GraphView(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		
		init();
	}

	public GraphView(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		
		init();
	}

	public GraphView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		// TODO Auto-generated constructor stub
		
		init();
	}
	
	private void init() {
		setFocusable(true);
		initVariables();
	}
	
	private void initVariables() {
		isExit = true;
		index = 0;
		days = 1;
		data = new ArrayList<Integer>();
		color = new ArrayList<int[]>();
		mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		
		color.add(new int[]{0xff198869, 0xff027554});
		color.add(new int[]{0xff91b800, 0xff779700});
		color.add(new int[]{0xfff93f67, 0xffd63255});
		color.add(new int[]{0xfffda031, 0xfffb5d2a});
		
		mPaint.setAntiAlias(true);
        mPaint.setTextSize(16);
        mPaint.setTypeface(Typeface.SERIF);
	}

	public void closeHandler() {
		if (this.mHandler != null) {
			this.mHandler.removeMessages(DRAW);
		}
		this.mHandler = null;
	}
	
	public void closeThread() {
		if(this.mThread!=null && this.mThread.isAlive()) {
			try {
				this.isExit = true;
				this.mThread.join();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public boolean sleepThread(int nMillSeconds) {
		int nCount = 0;
		boolean result = false;
		
		while(true) {
			if (isExit) 
				break;
			
			if((nCount*100) >= nMillSeconds) 
				break;
			
			try {
				Thread.sleep(100);
			} catch(Exception e) {
				e.printStackTrace();
			}
			
			nCount ++;
		}
		
		return result;
	}
	
	public int getGraphHeight() {
		return getHeight() - 20;
	}
	

}
