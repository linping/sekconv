package com.yj.sentence.ui.studding.study;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Html;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.BounceInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.easyvideoplayer.EasyVideoCallback;
import com.afollestad.easyvideoplayer.EasyVideoPlayer;
import com.yj.sentence.R;
import com.yj.sentence.common.Define;
import com.yj.sentence.database.PAKDBManager;
import com.yj.sentence.database.ProgressDBManager;
import com.yj.sentence.database.StudyDBManager;
import com.yj.sentence.structs.ProgressData;
import com.yj.sentence.structs.WordData;
import com.yj.sentence.ui.studding.SekWordPopup;
import com.yj.sentence.ui.studding.SekWordPopupOneButton;
import com.yj.sentence.ui.studding.audio.AudioDataReceivedListener;
import com.yj.sentence.ui.studding.audio.AudioRealDataReceivedListener;
import com.yj.sentence.ui.studding.audio.RecognizeResultListener;
import com.yj.sentence.ui.studding.audio.RecordWaveformView;
import com.yj.sentence.ui.studding.audio.RecordingThread;
import com.yj.sentence.ui.studding.util.EsStuddingRecognizer;
import com.yj.sentence.ui.studding.util.EsStuddingUtils;
import com.yj.sentence.ui.words.EsRegistryActivity;
import com.yj.sentence.utils.ESUtils;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;

public class StudyPronunciationActivity extends Activity implements View.OnClickListener {

    //ui objects
    private FrameLayout frameMask;
    private EasyVideoPlayer easyVideoPlayer;
    private FrameLayout framePlayerMask;
    private TextView textKorean;
    private TextView textEnglish;
    private ColorView viewColor;
    private ImageView imageStatus;
    private RecordWaveformView waveform;
    private ImageView imageTalkDesc;
    private ImageButton btnIndiator[] = new ImageButton[16];

    private ProgressDBManager mProgressManager;
    private StudyDBManager mStudyManager;
    private PAKDBManager m_pakManager = null;

    ArrayList<WordData> m_ProData;

    private boolean m_bStudyStop = false;
    private boolean m_bStudyPause = false;
    private boolean m_bArrowBtnPressed = false;
    private Thread  m_threadStudy = null;
    private RecordingThread mRecordingThread;

    private int nUserId;
    private int nParam;
    private int m_nCurrentWordCount = 0;
    private int nTotalCount = 0;

    String strAudioPath;
    private MediaPlayer mp;

    private RandomAccessFile mRecordRawFile = null;
    private EsStuddingRecognizer mRecognizer = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_study_pronunciation);

        Intent intent = this.getIntent();
        nUserId = intent.getIntExtra(Define.USER_ID, -1);
        nParam = intent.getIntExtra(Define.SCHEDULE_ID, -1);

        if (!openDB()) {
            finish();
            return;
        }

        mRecognizer = EsStuddingRecognizer.getInstance();
        mRecognizer.setListner(new AudioDataReceivedListener() {
            @Override
            public void onAudioDataReceived(short[] data) {
                processBuffer(data);
            }
        });

        mRecognizer.setResultListner(new RecognizeResultListener() {
            @Override
            public void onResult(String strResult) {
                int tt=1;

            }
        });
        initViews();
        initData();

        int userLevel = mProgressManager.getLevel();
        int epiNum = nParam / 9;
        int stgNum = nParam % 9;

        /*
        String strTitle = String.format("%d%s %d%s", userLevel, getString(R.string.order), epiNum, getString(R.string.episode));
        String strDescription = getString(R.string.start_study_pronunciation);
*/
        SekWordPopupOneButton popup;

        popup = new SekWordPopupOneButton(StudyPronunciationActivity.this);
        popup.setMessage(this.getString(R.string.start_study_pronunciation));
        popup.setListener(new SekWordPopupOneButton.OnSEKLaunchListener() {

            @Override
            public void onOK() {
                frameMask.setVisibility(View.INVISIBLE);
                startStudy();
            }


        });

        popup.show();
        /*
        SekConvNormalPopup popup;
        popup = new SekConvNormalPopup(this);
        popup.setInfo(strTitle, strDescription);
        popup.setListener(new SekConvNormalPopup.OnSEKLaunchListener() {
            @Override
            public void onOK() {
                frameMask.setVisibility(View.INVISIBLE);
                startStudy();
            }
        });
        popup.show();*/
    }

    private boolean openDB() {
        mProgressManager = new ProgressDBManager(this, nUserId);
        if (mProgressManager == null)
            return false;

        mStudyManager = new StudyDBManager(this, nUserId);
        if (mStudyManager == null)
            return false;

        m_pakManager = new PAKDBManager(this);
        if(m_pakManager == null)
            return false;

        return true;
    }

    private void initData() {
        int userLevel = mProgressManager.getLevel();
        int epiNum = nParam / 9;
        int stgNum = nParam % 9;

        ProgressData data = new ProgressData(nUserId, userLevel, epiNum, stgNum);

        m_ProData = mStudyManager.getPronunciationList(data);
        ArrayList<String> stringData = EsStuddingUtils.getWordList( m_ProData);
        String jsgfString = EsStuddingUtils.makeJSGFString(stringData);
        mRecognizer.setGrammarText(jsgfString);


        nTotalCount = m_ProData.size();
        for(int i = nTotalCount; i < 16; i++){
            btnIndiator[i].setVisibility(View.GONE);
        }

    }

    private void initViews() {
        ImageButton ibtn0 = (ImageButton)findViewById(R.id.imageIndicate1);
        ImageButton ibtn1 = (ImageButton)findViewById(R.id.imageIndicate2);
        ImageButton ibtn2 = (ImageButton)findViewById(R.id.imageIndicate3);
        ImageButton ibtn3 = (ImageButton)findViewById(R.id.imageIndicate4);
        ImageButton ibtn4 = (ImageButton)findViewById(R.id.imageIndicate5);
        ImageButton ibtn5 = (ImageButton)findViewById(R.id.imageIndicate6);
        ImageButton ibtn6 = (ImageButton)findViewById(R.id.imageIndicate7);
        ImageButton ibtn7 = (ImageButton)findViewById(R.id.imageIndicate8);
        ImageButton ibtn8 = (ImageButton)findViewById(R.id.imageIndicate9);
        ImageButton ibtn9 = (ImageButton)findViewById(R.id.imageIndicate10);
        ImageButton ibtn10 = (ImageButton)findViewById(R.id.imageIndicate11);
        ImageButton ibtn11 = (ImageButton)findViewById(R.id.imageIndicate12);
        ImageButton ibtn12 = (ImageButton)findViewById(R.id.imageIndicate13);
        ImageButton ibtn13 = (ImageButton)findViewById(R.id.imageIndicate14);
        ImageButton ibtn14 = (ImageButton)findViewById(R.id.imageIndicate15);
        ImageButton ibtn15 = (ImageButton)findViewById(R.id.imageIndicate16);

        btnIndiator[0] = ibtn0;
        btnIndiator[1] = ibtn1;
        btnIndiator[2] = ibtn2;
        btnIndiator[3] = ibtn3;
        btnIndiator[4] = ibtn4;
        btnIndiator[5] = ibtn5;
        btnIndiator[6] = ibtn6;
        btnIndiator[7] = ibtn7;
        btnIndiator[8] = ibtn8;
        btnIndiator[9] = ibtn9;
        btnIndiator[10] = ibtn10;
        btnIndiator[11] = ibtn11;
        btnIndiator[12] = ibtn12;
        btnIndiator[13] = ibtn13;
        btnIndiator[14] = ibtn14;
        btnIndiator[15] = ibtn15;

        frameMask = (FrameLayout) findViewById(R.id.frameMask);
        easyVideoPlayer = (EasyVideoPlayer) findViewById(R.id.easyPlayer);
        framePlayerMask = (FrameLayout) findViewById(R.id.framePlayerMask);
        textKorean = (TextView) findViewById(R.id.textKorean);
        textEnglish = (TextView) findViewById(R.id.textEnglish);
        viewColor = (ColorView) findViewById(R.id.viewColor);
        imageStatus = (ImageView) findViewById(R.id.imageStatus);
        waveform = (RecordWaveformView) findViewById(R.id.viewRecordForm);
        imageTalkDesc = (ImageView) findViewById(R.id.imgTalkDesc);

        ESUtils.setTextViewTypeFaceByRes(this, textKorean, Define.getMainFont());
        ESUtils.setTextViewTypeFaceByRes(this, textEnglish, Define.getEnglishFont());
        ESUtils.setTextViewTypeFaceByRes(this, (TextView) findViewById(R.id.textDescription), Define.getMainFont());

        waveform.setColor(0);
/*
        mRecordingThread = new RecordingThread(new AudioDataReceivedListener() {
            @Override
            public void onAudioDataReceived(short[] data) {
                waveform.setSamples(data);

            }
        });

        mRecordingThread.setRealDataListener(new AudioRealDataReceivedListener() {
            @Override
            public void onAudioDataReceived(byte[] data) {
                if(mRecordRawFile != null){
                    try {
                        mRecordRawFile.write(data);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });*/

    }

    private void setIndicator(int nIndex){
        for(int i =0; i<nTotalCount; i++){
            btnIndiator[i].setImageResource(R.drawable.st_study_circle_gray);
        }

        btnIndiator[nIndex].setImageResource(R.drawable.st_study_circle_red);
    }


    @Override
    public void onClick(View v) {
        ESUtils.playBtnSound(this);
        switch (v.getId()) {
            case R.id.btnBack:
                showBackDlg();
                //finish();
                break;
            case R.id.btnStart:
                doNext();
                break;
        }
    }


    private void doNext() {
        if(mp != null){
            mp.stop();
            mp = null;
        }
        m_bArrowBtnPressed = true;
        viewColor.setDraw(false);
        m_nCurrentWordCount ++;
    }

    private void pauseThread() {
        while (m_bStudyPause) {
            try {
                Thread.sleep(20);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    private void sleepThread(int nTime) {
        int nCount = 0;
        while (!m_bStudyStop) {
            pauseThread();

            if (m_bArrowBtnPressed)
                break;

            if (nCount * 20 > nTime)
                break;

            nCount++;

            try {
                Thread.sleep(20);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        finishPro();
        super.onDestroy();
    }

    private void clearDraw(){
        viewColor.setDraw(false);
        waveform.setVisibility(View.INVISIBLE);
    }

    private void finishPro(){
        viewColor.finishView();
        mRecognizer.stopSearch();

        m_bStudyPause = false;
        m_bStudyStop = true;
        m_bArrowBtnPressed = true;

        if(mp != null){
            mp.stop();
            mp = null;
        }


        if(m_threadStudy != null && m_threadStudy.isAlive()) {
            try{
                m_threadStudy.join();
            }
            catch(Exception e) {

            }
        }
    }

    private void setStudyData(){
        WordData data = m_ProData.get(m_nCurrentWordCount);
        String strEnglish = data.getMeanEn();
        String strKorean = data.getMeanKr();

        setIndicator(m_nCurrentWordCount);

        textEnglish.setText(strEnglish);
        textKorean.setText(Html.fromHtml(strKorean));
        imageStatus.setImageResource(R.drawable.st_study_listening);

        framePlayerMask.setVisibility(View.VISIBLE);
        waveform.setVisibility(View.INVISIBLE);
        textKorean.setVisibility(View.INVISIBLE);
        imageStatus.setVisibility(View.INVISIBLE);

        new Handler().postDelayed(new Runnable() {
            public void run() {
                viewColor.setColor(false);
                viewColor.setDraw(true);
                viewColor.setTime(1000);
                viewColor.startDraw();
            }
        }, 200);
    }

    int nVideoDuration = 3000;
    private void startVideo(){
        WordData data = m_ProData.get(m_nCurrentWordCount);

        String strVideoPath = getVideoFileName(data.getId());
        //String strVideoPath = Define.VIDEO_PATH + String.format("%d.mp4", data.getId() );

        framePlayerMask.setVisibility(View.VISIBLE);
        easyVideoPlayer.setCallback(callback);
        easyVideoPlayer.setSource(Uri.parse(strVideoPath));
        easyVideoPlayer.disableControls();
        easyVideoPlayer.setAutoPlay(true);
        /*
        nVideoDuration = easyVideoPlayer.getDuration();
        if(nVideoDuration > 7000) nVideoDuration = 3000;
        */
    }

    private void startExplanation(){
        textKorean.setVisibility(View.VISIBLE);
        imageStatus.setVisibility(View.VISIBLE);
        //playSound();
    }

    int nDuration = 0;
    private void playSound(){
        //strAudioPath = Define.SOUND_PATH + String.format("%d.mp3", m_ProData.get(m_nCurrentWordCount).getId() - 10000 );

        /*
        new Handler().postDelayed(new Runnable() {
            public void run() {
                try {
                    if(mp != null){
                        mp.stop();
                        mp = null;
                    }
                    mp = new MediaPlayer();
                    mp.setDataSource(StudyPronunciationActivity.this, Uri.parse(strAudioPath));
                    mp.prepare();
                    mp.start();

                    nDuration = mp.getDuration();

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }, 0);
        */
    }
    short[] returnBuffer;

    private void playVideoAgain(){
        //imageStatus.setVisibility(View.INVISIBLE);
        WordData data = m_ProData.get(m_nCurrentWordCount);
        String strVideoPath = getVideoFileName(data.getId());
        //String strVideoPath = Define.VIDEO_PATH + String.format("%d.mp4", data.getId() );

        framePlayerMask.setVisibility(View.VISIBLE);
        easyVideoPlayer.setCallback(callback);
        easyVideoPlayer.setSource(Uri.parse(strVideoPath));
        easyVideoPlayer.disableControls();
        easyVideoPlayer.setAutoPlay(true);
    }

    private void startSpeaking(){
        imageStatus.setImageResource(R.drawable.st_study_speakin_red);
        imageStatus.setVisibility(View.VISIBLE);
        waveform.setVisibility(View.VISIBLE);

        imageTalkDesc.setVisibility(View.VISIBLE);
        ObjectAnimator fadeAnim = ObjectAnimator.ofFloat(imageTalkDesc, "alpha", 1.0f).setDuration(1000);
        fadeAnim.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                ObjectAnimator closeAnim = ObjectAnimator.ofFloat(imageTalkDesc, "alpha", 0.0f).setDuration(500);
                closeAnim.setInterpolator(new AccelerateInterpolator());
                closeAnim.start();
            }
        });
        fadeAnim.setInterpolator(new BounceInterpolator());
        fadeAnim.start();


   /*     try {

            File fdelete = new File(Define.RECORD_PATH_RAW);
            if (fdelete.exists()) {
                if (fdelete.delete()) {

                } else {

                }
            }

            if (mRecordRawFile != null){
                mRecordRawFile.close();
                mRecordRawFile = null;
            }

            mRecordRawFile = new RandomAccessFile(Define.RECORD_PATH_RAW, "rw");

        } catch (IOException e) {
            e.printStackTrace();
        }
*/
        //mRecordingThread.startRecording();

        returnBuffer= new short[20];

        try {

            File fdelete = new File(Define.RECORD_PATH_RAW);
            if (fdelete.exists()) {
                if (fdelete.delete()) {

                } else {

                }
            }

            if (mRecordRawFile != null){
                mRecordRawFile.close();
                mRecordRawFile = null;
            }

            mRecordRawFile = new RandomAccessFile(Define.RECORD_PATH_RAW, "rw");

        } catch (IOException e) {
            e.printStackTrace();
        }


        mRecognizer.startSearch();

    }

    private void startHearing(){
/*
            try {
                if (mRecordRawFile != null){
                    mRecordRawFile.close();
                    mRecordRawFile = null;
                }

                File mRawFile = new File(Define.RECORD_PATH_RAW);
                File mWavFile = new File(Define.RECORD_PATH);

                EsStuddingUtils.rawToWave(mRawFile, mWavFile, 16000);

            } catch (IOException e) {
                e.printStackTrace();
            }
            */


        try {
            if (mRecordRawFile != null){
                mRecordRawFile.close();
                mRecordRawFile = null;
            }

            mRecognizer.stopSearch();
            String strResult = mRecognizer.getSearchResult();

            File mRawFile = new File(Define.RECORD_PATH_RAW);
            File mWavFile = new File(Define.RECORD_PATH);

            EsStuddingUtils.rawToWave(mRawFile, mWavFile, 16000);

        } catch (IOException e) {
            e.printStackTrace();
        }

        waveform.setSamples(new short[20]);

        imageTalkDesc.setAlpha(0.0f);

        waveform.setVisibility(View.INVISIBLE);
        imageStatus.setImageResource(R.drawable.st_study_listening);

        WordData data = m_ProData.get(m_nCurrentWordCount);
        String strVideoPath = getVideoFileName(data.getId());

        //String strVideoPath = Define.VIDEO_PATH + String.format("%d.mp4", data.getId() );

        MediaPlayer mp = MediaPlayer.create(this, Uri.parse(Define.RECORD_PATH));
        //AudioManager audio = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        //int nVolume = audio.getStreamVolume(AudioManager.STREAM_MUSIC);
        mp.setVolume(0.99f, 0.99f);
        mp.start();

/*
        easyVideoPlayer.setSource(Uri.parse(strVideoPath));
        easyVideoPlayer.disableControls();
        easyVideoPlayer.setAutoPlay(true);*/


    }

    float rms = 0f;
    float rms1 = 0f;
    float rms2 = 0f;
    float rms3 = 0f;
    float rms4 = 0f;

    float peak = 0f;

    private void processBuffer(short[] buffer){

        rms = 0f;
        rms1 = 0f;
        rms2 = 0f;
        rms3 = 0f;
        rms4 = 0f;

        peak = 0f;

        /*for(float sample : buffer) {

            float abs = Math.abs(sample);
            if(abs > peak) {
                peak = abs;
            }

            rms += sample * sample;
        }

        rms = (float)Math.sqrt(rms / buffer.length);

        */
        for(int i = 0; i < buffer.length / 4 ; i++){
            float sample = buffer[i];
            float abs = Math.abs(sample);
            if(abs > peak) {
                peak = abs;
            }

            rms1 += sample * sample;
        }

        for(int i = buffer.length / 4; i < buffer.length / 2; i++){
            float sample = buffer[i];
            float abs = Math.abs(sample);
            if(abs > peak) {
                peak = abs;
            }

            rms2 += sample * sample;
        }

        for(int i = buffer.length / 2; i < buffer.length / 4 * 3; i++){
            float sample = buffer[i];
            float abs = Math.abs(sample);
            if(abs > peak) {
                peak = abs;
            }

            rms3 += sample * sample;
        }

        for(int i = buffer.length / 4 * 3; i < buffer.length ; i++){
            float sample = buffer[i];
            float abs = Math.abs(sample);
            if(abs > peak) {
                peak = abs;
            }

            rms4 += sample * sample;
        }


        rms1 = (float)Math.sqrt(rms1 * 4 / buffer.length);
        rms2 = (float)Math.sqrt(rms2 * 4 / buffer.length);
        rms3 = (float)Math.sqrt(rms3 * 4 / buffer.length);
        rms4 = (float)Math.sqrt(rms4 * 4 / buffer.length);

        for(int i =0;i<19;i++){
            returnBuffer[i] = returnBuffer[i+1];
        }
        returnBuffer[19] = (short)rms1;

        waveform.setSamples(returnBuffer);


        new Handler().postDelayed(new Runnable() {
            public void run() {
                for(int i =0;i<19;i++){
                    returnBuffer[i] = returnBuffer[i+1];
                }
                returnBuffer[19] = (short)rms2;

                waveform.setSamples(returnBuffer);
            }
        }, 100);

        new Handler().postDelayed(new Runnable() {
            public void run() {
                for(int i =0;i<19;i++){
                    returnBuffer[i] = returnBuffer[i+1];
                }
                returnBuffer[19] = (short)rms3;

                waveform.setSamples(returnBuffer);
            }
        }, 200);

        new Handler().postDelayed(new Runnable() {
            public void run() {
                for(int i =0;i<19;i++){
                    returnBuffer[i] = returnBuffer[i+1];
                }
                returnBuffer[19] = (short)rms4;

                waveform.setSamples(returnBuffer);
            }
        }, 300);



        if(mRecordRawFile != null){
            try {
                mRecordRawFile.write(EsStuddingUtils.shortToBytes(buffer));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private Handler m_handlerStudy = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0:
                    setStudyData();
                    break;
                case 1:
                    startVideo();
                    break;
                case 2:
                    startExplanation();
                    break;
                case 3:
                    playVideoAgain();
                    break;
                case 4:
                    startSpeaking();
                    break;
                case 5:
                    startHearing();
                    break;
                case 6:
                    m_nCurrentWordCount ++;
                    clearDraw();
                    break;
                case 7:
                    finishStudy();
                    break;
                default:
                    break;
            }
        }
    };

    private void finishStudy(){

        frameMask.setVisibility(View.VISIBLE);

        int userLevel = mProgressManager.getLevel();
        int epiNum = nParam / 9;
        int stgNum = nParam % 9;

        int nMarks = 100;
        ProgressData m_progressData = new ProgressData(userLevel, userLevel, epiNum, stgNum, nMarks);
        mProgressManager.updateProgress(m_progressData);

        Intent intent = new Intent(StudyPronunciationActivity.this, StudyPronunciationResultActivity.class);
        intent.putExtra(Define.USER_ID, nUserId);
        intent.putExtra(Define.SCHEDULE_ID, nParam);
        startActivity(intent);
        finish();

        /*
        new Handler().postDelayed(new Runnable() {
            public void run() {

            }
        }, 50);*/


    }
    private void startStudy() {
        m_threadStudy = new Thread() {
            public void run() {
                while(!m_bStudyStop) {
                    if(m_nCurrentWordCount > m_ProData.size() - 1) {
                        m_handlerStudy.sendEmptyMessage(7);
                        m_bStudyStop = true;
                        continue;
                    }

                    sleepThread(0);
                    if(m_bArrowBtnPressed) {
                        m_bArrowBtnPressed = false;
                        continue;
                    }

                    m_handlerStudy.sendEmptyMessage(0);

                    sleepThread(1500);
                    if(m_bArrowBtnPressed) {
                        m_bArrowBtnPressed = false;
                        continue;
                    }
                    m_handlerStudy.sendEmptyMessage(1);

                    sleepThread(3000);

                    if(nVideoDuration > 3000){
                        sleepThread(nVideoDuration - 3000 + 300);
                    }
                    if(m_bArrowBtnPressed) {
                        m_bArrowBtnPressed = false;
                        continue;
                    }
                    m_handlerStudy.sendEmptyMessage(2);

                    sleepThread(5000);
                    if(nDuration - 5000 > 0){
                        sleepThread(nDuration - 5000 + 300);
                    }
                    if(m_bArrowBtnPressed) {
                        m_bArrowBtnPressed = false;
                        continue;
                    }

                    m_handlerStudy.sendEmptyMessage(3);

                    sleepThread(3000);
                        if(nVideoDuration > 3000){
                            sleepThread(nVideoDuration - 3000 + 3000);
                        }
                    if(m_bArrowBtnPressed) {
                        m_bArrowBtnPressed = false;
                        continue;
                    }

                    m_handlerStudy.sendEmptyMessage(4);
                    sleepThread(4000);
                    if(m_bArrowBtnPressed) {
                        m_bArrowBtnPressed = false;
                        continue;
                    }

                    m_handlerStudy.sendEmptyMessage(5);
                    sleepThread(3000);
                    if(nVideoDuration > 3000){
                        sleepThread(nVideoDuration - 3000 + 1000);
                    }
                    if(m_bArrowBtnPressed) {
                        m_bArrowBtnPressed = false;
                        continue;
                    }

                    {
                        m_handlerStudy.sendEmptyMessage(3);

                        sleepThread(3000);
                        if(nVideoDuration > 3000){
                            sleepThread(nVideoDuration - 3000 + 3000);
                        }
                        if(m_bArrowBtnPressed) {
                            m_bArrowBtnPressed = false;
                            continue;
                        }

                        m_handlerStudy.sendEmptyMessage(4);
                        sleepThread(4000);
                        if(m_bArrowBtnPressed) {
                            m_bArrowBtnPressed = false;
                            continue;
                        }

                        m_handlerStudy.sendEmptyMessage(5);
                        sleepThread(3000);
                        if(nVideoDuration > 3000){
                            sleepThread(nVideoDuration - 3000 + 300);
                        }
                        if(m_bArrowBtnPressed) {
                            m_bArrowBtnPressed = false;
                            continue;
                        }
                    }


                    m_handlerStudy.sendEmptyMessage(6);

                    try{
                        Thread.sleep(100);
                    }
                    catch(Exception e) {}

                }
            }
        };
        m_threadStudy.setDaemon(true);
        m_threadStudy.setName("StudyWordThread");
        m_threadStudy.start();
    }

    EasyVideoCallback callback = new EasyVideoCallback() {
        @Override
        public void onStarted(EasyVideoPlayer player) {
            //easyPlayer.setVisibility(View.VISIBLE);
            nVideoDuration = easyVideoPlayer.getDuration();

        }

        @Override
        public void onPaused(EasyVideoPlayer player) {

        }

        @Override
        public void onPreparing(EasyVideoPlayer player) {

        }

        @Override
        public void onPrepared(EasyVideoPlayer player) {
            //player.start();
            Handler mainHandler = new Handler(StudyPronunciationActivity.this.getMainLooper());

            Runnable myRunnable = new Runnable() {
                @Override
                public void run() {
                    framePlayerMask.setVisibility(View.INVISIBLE);
                } // This is your code
            };
            mainHandler.post(myRunnable);

        }

        @Override
        public void onBuffering(int percent) {

        }

        @Override
        public void onError(EasyVideoPlayer player, Exception e) {

        }

        @Override
        public void onCompletion(EasyVideoPlayer player) {
            //framePlayerMask.setVisibility(View.VISIBLE);

        }

        @Override
        public void onRetry(EasyVideoPlayer player, Uri source) {

        }

        @Override
        public void onSubmit(EasyVideoPlayer player, Uri source) {

        }
    };

    public String getImageFileName(int nWordID){
        return m_pakManager.getFileName(nWordID, 0);
    }

    public String getAudioFileName(int nWordID){
        return m_pakManager.getFileName(nWordID, 1);
    }

    public String getVideoFileName(int nWordID){
        return m_pakManager.getFileName(nWordID, 2);
    }

    private void showBackDlg(){

        m_bStudyPause = true;
        SekWordPopup popup;

        popup = new SekWordPopup(StudyPronunciationActivity.this);
        popup.setMessage(this.getString(R.string.study_back));
        popup.setListener(new SekWordPopup.OnSEKLaunchListener() {

            @Override
            public void onOK() {
                finish();
            }

            @Override
            public void onCancel() {
                m_bStudyPause = false;
                // TODO Auto-generated method stub
            }
        });

        popup.show();
    }


    @Override
    public void onBackPressed() {
        showBackDlg();
    }
}