package com.yj.sentence.ui.studding;

import com.yj.sentence.R;
import com.yj.sentence.common.Define;
import com.yj.sentence.utils.ESUtils;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class SekWordPopup extends Dialog implements View.OnClickListener {
	
	private Context mContext;
	private Button mBtnOK;
	private Button mBtnCancel;
	private TextView textMessage;
	
	private OnSEKLaunchListener mListener;

	public SekWordPopup(Context context) {
		super(context, R.style.MyDialog);
		// TODO Auto-generated constructor stub
		this.mContext = context;
		this.setCancelable(false);
		initViews();
	}
	
	private void initViews() {
		View localView = LayoutInflater.from(this.mContext).inflate(R.layout.sekword_popup, null);
		this.setContentView(localView);
		
		mBtnOK = (Button)localView.findViewById(R.id.btnOk);
		mBtnOK.setOnClickListener(this);
		
		mBtnCancel = (Button)localView.findViewById(R.id.btnCancel);
		mBtnCancel.setOnClickListener(this);
		
		textMessage = (TextView) localView.findViewById(R.id.textMessage);
		ESUtils.setTextViewTypeFaceByRes(mContext, textMessage, Define.getMainFont());

	}
	
	public void setListener(OnSEKLaunchListener listener) {
		mListener = listener;
	}
	
	public void setMessage(String text){
		textMessage.setText(text);
	}
	
	public void setMessageSize(int size){
		textMessage.setTextSize(size);
	}
	
	@Override
	public void dismiss() {
	    super.dismiss();
	}

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		ESUtils.playBtnSound(mContext);
		switch (arg0.getId()) {
			case R.id.btnOk:
				mListener.onOK();
				this.dismiss();
				break;
				
			case R.id.btnCancel:
				mListener.onCancel();
				this.dismiss();
				break;
		}
	}
	
	public abstract interface OnSEKLaunchListener {
		public abstract void onOK();
		public abstract void onCancel();

	}

}
