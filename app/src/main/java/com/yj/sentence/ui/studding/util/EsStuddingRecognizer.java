package com.yj.sentence.ui.studding.util;

import android.content.Context;
import android.os.AsyncTask;

import com.yj.sentence.ui.studding.audio.AudioDataReceivedListener;
import com.yj.sentence.ui.studding.audio.RecognizeResultListener;

import java.io.File;
import java.io.IOException;

import edu.cmu.pocketsphinx.Assets;
import edu.cmu.pocketsphinx.Hypothesis;
import edu.cmu.pocketsphinx.RecognitionListener;
import edu.cmu.pocketsphinx.SpeechRecognizer;
import edu.cmu.pocketsphinx.SpeechRecognizerSetup;

/**
 * Created by achilles on 3/16/17.
 */


public class EsStuddingRecognizer extends Object implements RecognitionListener {

    private static final String ES_SEARCH = "essearch";

    static EsStuddingRecognizer mThis;

    private Context m_Context;
    private File m_AssetDir;
    private SpeechRecognizer recognizer;
    private Boolean bInitialized = false;

    private String strResult;
    private AudioDataReceivedListener mListener;
    private RecognizeResultListener mResultListener;

    public static EsStuddingRecognizer getInstance(){
        if(mThis == null){
            mThis = new EsStuddingRecognizer();
        }
        return mThis;
    }

    public EsStuddingRecognizer(){
        //initialize();
    }

    public SpeechRecognizer getSpeechRecognizer(){
        return recognizer;
    }
    public void setContext(Context context){
        m_Context = context;
    }

    public void setGrammarText(String strGrammar){
        if(bInitialized == false) return;
        recognizer.addGrammarSearch(ES_SEARCH, strGrammar);
        //File digitsGrammar = new File(m_AssetDir, "digits_test.gram");
        //recognizer.addGrammarSearch(ES_SEARCH, digitsGrammar);
    }

    public void setListner(AudioDataReceivedListener listener){
        mListener = listener;
    }

    public void setResultListner(RecognizeResultListener listener){
        mResultListener = listener;
    }

    public boolean startSearch(){
        if(bInitialized == false) return false;

        strResult = null;
        recognizer.stop();
        recognizer.startListening(ES_SEARCH);

        return true;
    }

    public boolean startSearch(int nTimeOut){
        if(bInitialized == false) return false;

        recognizer.stop();
        recognizer.startListening(ES_SEARCH, nTimeOut);

        return true;
    }

    public void stopSearch(){
        if(bInitialized == false) return ;
        recognizer.stop();
    }

    public String getSearchResult(){
        return strResult;
    }
    public void initialize(){
        new AsyncTask<Void, Void, Exception>() {

            @Override
            protected Exception doInBackground(Void... params) {
                try {
                    Assets assets = new Assets(m_Context);
                    m_AssetDir  = assets.syncAssets();
                    setupRecognizer(m_AssetDir);
                } catch (IOException e) {
                    return e;
                }
                return null;
            }

            @Override
            protected void onPostExecute(Exception result) {
                bInitialized = true;
            }
        }.execute();
    }

    private void setupRecognizer(File assetsDir) throws IOException {
        recognizer = SpeechRecognizerSetup.defaultSetup()
                .setAcousticModel(new File(assetsDir, "en-us-ptm"))
                .setDictionary(new File(assetsDir, "cmudict-en-us.dict"))
                //.setDictionary(new File(assetsDir, "lm_giga_64k_nvp.sphinx.dic"))
                //.setDictionary(new File(assetsDir, "lm_giga_64k_nvp.hvite.dic"))
                .setRawLogDir(assetsDir) // To disable logging of raw audio comment out this call (takes a lot of space on the device)

                .getRecognizer();
        recognizer.addListener(this);

    }

    @Override
    public void onBeginningOfSpeech() {

    }

    @Override
    public void onEndOfSpeech() {

    }

    @Override
    public void onPartialResult(Hypothesis hypothesis) {
        if (hypothesis == null)
            return;

        strResult = hypothesis.getHypstr();
    }

    @Override
    public void onResult(Hypothesis hypothesis) {
        if(mResultListener == null) return;
        if (hypothesis == null) {
            mResultListener.onResult("");
            return;
        }

        strResult = hypothesis.getHypstr();
        mResultListener.onResult(strResult);
    }

    @Override
    public void onError(Exception e) {

    }

    @Override
    public void onTimeout() {

    }

    @Override
    public void onData(short[] buffer){
        mListener.onAudioDataReceived(buffer);
    }
}
