package com.yj.sentence.ui.studding.test;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import com.afollestad.easyvideoplayer.EasyVideoPlayer;
import com.yj.sentence.R;
import com.yj.sentence.common.Define;
import com.yj.sentence.database.PAKDBManager;
import com.yj.sentence.structs.WordData;
import com.yj.sentence.ui.studding.audio.AudioDataReceivedListener;
import com.yj.sentence.ui.studding.audio.RecognizeResultListener;
import com.yj.sentence.ui.studding.audio.RecordWaveformView;
import com.yj.sentence.ui.studding.util.EsStuddingRecognizer;
import com.yj.sentence.ui.studding.util.EsStuddingUtils;
import com.yj.sentence.utils.ESUtils;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.media.AudioManager;
import android.media.Image;
import android.media.SoundPool;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.BounceInterpolator;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

public class TestSpeakingLayout extends TestLayout {

	private Context m_context;

	private EasyVideoPlayer easyPlayer;
	private FrameLayout frameMask;
	private TextView textKorean;
	private TextView textEnglish;
	private ImageView imageStatus;
	private RecordWaveformView formView;
	private ImageView imageTalkDesc;
	private ImageView imageMask;

	boolean is_enable_thread = false;
	boolean is_message_enabled = false;

	SoundPool m_soundPool = null;
	HashMap<String, Integer> m_soundMap;
	float m_fVolume = 0.0f;
	private boolean m_bRecognitionResult = false;
	private boolean m_bArrowBtnPressed = false;

	private PAKDBManager m_pakManager = null;

	private OnEngVoiceTestFinishListener m_listener = null;

	public TestSpeakingLayout(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		m_context = context;
		m_pakManager = new PAKDBManager(context);

		mRecognizer = EsStuddingRecognizer.getInstance();
		mRecognizer.setListner(new AudioDataReceivedListener() {
			@Override
			public void onAudioDataReceived(short[] data) {
				processBuffer(data);
			}
		});

		mRecognizer.setResultListner(new RecognizeResultListener() {
			@Override
			public void onResult(String strResult) {
				processResult(strResult);
			}
		});

		initLayout();
		initKorEngTestLayout();
	}

	private void initLayout() {
		View v = LayoutInflater.from(m_context).inflate(R.layout.test_vts_layout, null);
		easyPlayer = (EasyVideoPlayer) v.findViewById(R.id.easyPlayer);
		frameMask = (FrameLayout) v.findViewById(R.id.frameMask);
		textKorean = (TextView) v.findViewById(R.id.textKorean);
		textEnglish = (TextView) v.findViewById(R.id.textEnglish);
		imageStatus = (ImageView) v.findViewById(R.id.imageStatus);
		formView = (RecordWaveformView) v.findViewById(R.id.viewRecordForm);
		formView.setColor(0);
		imageMask = (ImageView) v.findViewById(R.id.imageMask);
		imageMask.setVisibility(View.INVISIBLE);
		imageTalkDesc = (ImageView) v.findViewById(R.id.imgTalkDesc);
		imageTalkDesc.setVisibility(View.VISIBLE);
		imageTalkDesc.setAlpha(0.0f);
		ESUtils.setTextViewTypeFaceByRes(m_context, textKorean, Define.getMainFont());
		ESUtils.setTextViewTypeFaceByRes(m_context, (TextView) v.findViewById(R.id.textDescription), Define.getMainFont());

		this.addView(v);
	}

	private void initData(){

		ArrayList<String> stringData = EsStuddingUtils.getWordList( m_arrWordData);
		/*ArrayList<String> stringData = new ArrayList<String>();
		stringData.add("i");
		stringData.add("you");
		stringData.add("he");
		stringData.add("she");
		stringData.add("the");
		stringData.add("this");
		stringData.add("that");
		stringData.add("go");
		stringData.add("do");
		stringData.add("have");
		stringData.add("take");
		stringData.add("they");
		stringData.add("there");
		stringData.add("them");
		stringData.add("here");
		stringData.add("it");
		stringData.add("is");
		stringData.add("are");
		stringData.add("am");
		stringData.add("those");
		stringData.add("these");
		stringData.add("get");
		stringData.add("set");
		stringData.add("at");*/

		String jsgfString = EsStuddingUtils.makeJSGFString(stringData);

		mRecognizer.setGrammarText(jsgfString);
	}

	/*
	private void initSound() {
		m_soundPool = new SoundPool(1, AudioManager.STREAM_MUSIC, 0);		
		
		AudioManager audioManager = (AudioManager)m_context.getSystemService(Context.AUDIO_SERVICE);
		m_fVolume = audioManager.getStreamMaxVolume(3);
		
		m_soundMap = new HashMap<String, Integer>();
		
		for(int i=0; i < m_arrWordData.size(); i++)
			loadSound(m_arrWordData.get(i).getSound());
	}
	
	private void loadSound(String strWord) {
		try {
			String soundPath = Define.SOUND_PATH + strWord + Define.SOUND_TYPE;
			int nSoundId = m_soundPool.load(soundPath , 1);
			m_soundMap.put(strWord, nSoundId);
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}*/
	
	private void playWordVoice(String strWord) {
		if(m_soundPool != null)
			m_soundPool.play(m_soundMap.get(strWord).intValue(), m_fVolume, m_fVolume, 1, 0, 1.0F);
	}
	
	private void pauseWordVoice(String strWord) {
		if(m_soundPool != null)
			m_soundPool.pause(m_soundMap.get(strWord).intValue());
	}
	
	private void resumeWordVoice(String strWord) {
		if(m_soundPool != null)
			m_soundPool.resume(m_soundMap.get(strWord).intValue());
	}
	
	private void initKorEngTestLayout() {
		
		Typeface textTypeFace_gulim = Typeface.createFromAsset(getContext().getAssets(), Define.TEXTFONT_GULIM_PATH);

	}
	
	private View.OnClickListener m_clickListener = new View.OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			//ESUtils.playBtnSound(m_context);

		}
	};
	
	@SuppressLint("HandlerLeak")
	private void checkResult(int nIndex) {
		m_bResultPressed = true;	

		if(isTrueWord(nIndex-1)) {	//í•©ê²©
			is_successProblem[m_nCurrentTestProblemCount-1] = true;
			m_nTrueResCount ++;

			StudyTestActivity.playWordVoice("success");
		}
		else {
			is_successProblem[m_nCurrentTestProblemCount-1] = false;

			m_nFalseResCount ++;
			StudyTestActivity.playWordVoice("fail");

			if(m_nTestCount != 0)
				insertWrongWord(m_arrWordData.get(m_nTestCount-1).getId());
		}
		
		setTestResult();		
		
		new Handler() {
			public void handleMessage(Message msg) {
				m_nTestTime = 0;
				m_bResultPressed = false;
				m_bProblemView = false;
			}
		}.sendEmptyMessageDelayed(0, 1200);
		
	}

	private boolean isTrueWord(int nResIndex) {
		boolean bTrue = false;

		return bTrue;
	}
	
	private void setTestResult() {
		
		m_nTestCurrentProblemIndex++;
	}
	
	private void setTestInit() {
	}

	void startWord(){

		WordData data = m_arrWordData.get(m_nTestCount);

		String strVideoPath = getVideoFileName(Integer.parseInt(data.getImage()));
		//String strVideoPath = Define.VIDEO_PATH + String.format("%s.mp4", data.getImage() );

		//waveUser.setSoundFile(null);

		//easyPlayer.setCallback(callback);
		easyPlayer.setSource(Uri.parse(strVideoPath));
		easyPlayer.disableControls();
		easyPlayer.setAutoPlay(true);
		easyPlayer.setLoop(true);

		//textEnglish.setTextColor(0x000000);
		textKorean.setText(data.getMeanKr() );
		textKorean.setVisibility(View.VISIBLE);
		textEnglish.setVisibility(View.VISIBLE);
		setEnglishData();

		imageStatus.setImageResource(R.drawable.st_study_thinking);
		imageTalkDesc.setAlpha(0.0f);

		formView.setVisibility(View.INVISIBLE);
		imageMask.setVisibility(View.INVISIBLE);

	}
	private void setEnglishData(){
		WordData data = m_arrWordData.get(m_nTestCount);
		String strEn = data.getMeanEn();

		String[] strWords = strEn.split(" ");

		int nNormalBlanks = 0;

		if(strWords.length > 0 && strWords.length <= 3){
			nNormalBlanks = 1;
		}else if(strWords.length > 3 && strWords.length <= 7){
			nNormalBlanks = 2;
		}else if(strWords.length > 7){
			nNormalBlanks = 3;
		}


		String strResult = "";
		for(int i = 0; i < nNormalBlanks; i++){
			Random r = new Random();
			int i1 = r.nextInt(strWords.length);

			int nCount = strWords[i1].length();
			strWords[i1] ="";
			for(int j = 0; j < nCount; j++){
				strWords[i1] += "_";
			}
		}

		for(int i = 0; i <strWords.length; i++){
			strResult += (strWords[i] + " ");
		}

		textEnglish.setText(strResult);


	}
	short[] returnBuffer;
	private EsStuddingRecognizer mRecognizer = null;
	private RandomAccessFile mRecordRawFile = null;

	private void startAudioRecording() {

        /*
        if(mRecorder != null){
            mRecorder.release();
        }
        mRecorder = WavAudioRecorder.getInstanse();
        mRecorder.setOutputFile(Define.RECORD_PATH);
        mRecorder.setAudioDataReceiveListener(new AudioDataReceivedListener() {
            @Override
            public void onAudioDataReceived(short[] data) {
                //mFormView.setSamfples(data);
            }
        });

        mRecorder.prepare();
        mRecorder.start();
        */



		returnBuffer= new short[20];

		try {

			File fdelete = new File(Define.RECORD_PATH_RAW);
			if (fdelete.exists()) {
				if (fdelete.delete()) {

				} else {

				}
			}

			if (mRecordRawFile != null){
				mRecordRawFile.close();
				mRecordRawFile = null;
			}

			mRecordRawFile = new RandomAccessFile(Define.RECORD_PATH_RAW, "rw");

		} catch (IOException e) {
			e.printStackTrace();
		}


		mRecognizer.startSearch();

	}

	private void goRecording()
	{

		startAudioRecording();


		new Handler().postDelayed(new Runnable() {
			public void run() {

				imageStatus.setImageResource(R.drawable.st_study_speakin_red);
				imageMask.setVisibility(View.VISIBLE);

				imageTalkDesc.setVisibility(View.VISIBLE);
				formView.setVisibility(View.VISIBLE);

				textEnglish.setVisibility(View.VISIBLE);

				ObjectAnimator fadeAnim = ObjectAnimator.ofFloat(imageTalkDesc, "alpha", 1.0f).setDuration(1000);
				fadeAnim.addListener(new AnimatorListenerAdapter() {
					@Override
					public void onAnimationEnd(Animator animation) {
						super.onAnimationEnd(animation);
						ObjectAnimator closeAnim = ObjectAnimator.ofFloat(imageTalkDesc, "alpha", 0.0f).setDuration(500);
						closeAnim.setInterpolator(new AccelerateInterpolator());
						closeAnim.start();
					}
				});
				fadeAnim.setInterpolator(new BounceInterpolator());
				fadeAnim.start();
			}
		}, 400);
	}

	private void stopAudioRecording() {
        /*
        mRecorder.stop();
        mRecorder.release();
        mRecorder = null;
        */


		imageTalkDesc.setAlpha(0.0f);
		formView.setVisibility(View.INVISIBLE);

		try {
			if (mRecordRawFile != null){
				mRecordRawFile.close();
				mRecordRawFile = null;
			}

			mRecognizer.stopSearch();
			String strResult = mRecognizer.getSearchResult();

			File mRawFile = new File(Define.RECORD_PATH_RAW);
			File mWavFile = new File(Define.RECORD_PATH);

			EsStuddingUtils.rawToWave(mRawFile, mWavFile, 16000);

		} catch (IOException e) {
			e.printStackTrace();
		}

		formView.setSamples(new short[20]);

	}


	private Handler m_handlerStudy = new Handler() {
		public void handleMessage(Message msg) {
			switch (msg.what) {
				case 0:
					startWord();
					break;
				case 2:
					goRecording();
					break;
				case 4:
					m_bRecognitionResult = true;
					stopAudioRecording();
					break;
				default:
					break;
			}
		}
	};
	private long m_nStartStudyTime = 0;

	@SuppressLint("HandlerLeak")
	public void performEngVoiceTest() {
		initData();
		m_nStartStudyTime = ESUtils.getNowTimeSeconds();

		m_threadTest = new Thread() {
			private Handler m_handler = new Handler() {
				public void handleMessage(Message msg) {
					if(m_arrWordData == null) return;
					
					switch(msg.what) {
					case 0:
						new Handler() {
							public void handleMessage(Message msg) {
								m_bResultPressed = false;
								long nEndStudyTime = ESUtils.getNowTimeSeconds();
								int nDiff = (int)(nEndStudyTime - m_nStartStudyTime);
								long nCurTime = ESUtils.getNowDateMilliseconds();
								int nYear = ESUtils.getDateOfYear(nCurTime);
								int nMonth = ESUtils.getDateOfMonth(nCurTime);
								int nDate = ESUtils.getDateOfDay(nCurTime);
								long nRegTime = ESUtils.getDateMilliseconds(nYear, nMonth - 1, nDate);
								m_progressDbMana.updateProgressTime(nRegTime, nDiff);

								m_listener.onAfter(m_nCurrentTestProblemCount, m_nTrueResCount, m_nFalseResCount,m_nTestCurrentProblemIndex,is_successProblem,m_arrAllWordData);
							}
						}.sendEmptyMessageDelayed(0, 500);

						break;
					case 1:
						if (!is_enable_thread) {
							is_enable_thread = true;
							
							m_nFalseResCount ++;
							is_successProblem[m_nCurrentTestProblemCount-1] = false;

							StudyTestActivity.playWordVoice("fail");
							if(m_nTestCount != 0)
								insertWrongWord(m_arrWordData.get(m_nTestCount-1).getId());

							m_bResultPressed = true;
							
							new Handler() {
								public void handleMessage(Message msg) {
									m_nTestTime = 0;
									m_bResultPressed = false;
									m_bProblemView = false;
								}
							}.sendEmptyMessageDelayed(0, 1200);
						}
						break;
					}
				}
			};
			
			public void run() {
				while(!m_bTestStop) {
					sleepThread(0);

					if(m_nTestCount > m_tvTotalProblems-1) {	//모든 시험을 다 쳤다면

						m_handler.sendEmptyMessage(0);

						return;
					}

					if(m_bArrowBtnPressed) {
						m_bArrowBtnPressed = false;
						continue;
					}
					m_handlerStudy.sendEmptyMessage(0);

					// start - go first speaking
					sleepThread(7000);

					if(m_bArrowBtnPressed) {
						m_bArrowBtnPressed = false;
						continue;
					}

					m_handlerStudy.sendEmptyMessage(2);
					// end

					sleepThread(5000);
					if(m_bArrowBtnPressed) {
						m_bArrowBtnPressed = false;
						continue;
					}

					m_handlerStudy.sendEmptyMessage(4);
					sleepThread(1500);
					if(m_bArrowBtnPressed) {
						m_bArrowBtnPressed = false;
						continue;
					}

					m_nTestCount ++;
					m_nCurrentTestProblemCount++;
					/*pauseThread();
					sleepResultPressed();
					if(m_nTestTime == 0) m_handler.sendEmptyMessageDelayed(0, 300);	//ì‹œí—˜ë¬¸ì œ ì œì‹œ
					
					m_nTestTime ++;
									
					m_nTestTime += 20;
					
					if(m_nTestTime >= m_nTestLimitTime*1000) {
						m_nTestTime = m_nTestLimitTime*1000;
						is_message_enabled = true;
						m_handler.sendEmptyMessage(1);
					}
					
					if (is_message_enabled) {
						
					}else{

					}
					try {
						Thread.sleep(20);
					}
					catch(Exception e) {
						e.printStackTrace();
					}
					*/
				}
			}
		};

		getTestProblems(Define.KIND_TEST_VTS);
		//initSound();
		
		m_threadTest.setName("EngVoiceTestThread");
		m_threadTest.setDaemon(true);
		m_threadTest.start();
	}


	private void waitRecognitionResult(){
		while(m_bRecognitionResult){
			try{
				Thread.sleep(20);
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
	}

	private void sleepThread(int nTime) {
		int nCount = 0;
		while(!m_bTestStop) {
			pauseThread();

            if(m_bArrowBtnPressed)
                break;

			if(nCount * 20 > nTime) {
				waitRecognitionResult();
				break;
			}

			nCount ++;

			try{
				Thread.sleep(20);
			}
			catch(Exception e) {
				e.printStackTrace();
			}
		}
	}
	//ë‹µì�„ ì„ íƒ�í•œí›„ 1ì´ˆìžˆë‹¤ê°€ ë‹¤ì�Œ ë¬¸ì œë¥¼ ì œì‹œ
	private void sleepResultPressed() {
		while(m_bResultPressed) {
			try {
				Thread.sleep(20);
			}
			catch(Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public void setTestPause(boolean bPause) {
		super.setTestPause(bPause);
		
		if(bPause)
			pauseWordVoice(m_arrWordData.get(m_nTestCount).getSound());
		else
			resumeWordVoice(m_arrWordData.get(m_nTestCount).getSound());
	}	
		
	public void finishTest() {

		if(mRecognizer != null) {
			mRecognizer.stopSearch();

		}
		if(m_soundPool != null) {
			m_soundPool.release();
			m_soundPool = null;
		}
		
		if(m_soundMap != null) {
			m_soundMap.clear();
			m_soundMap = null;
		}

		if(m_threadTest != null && m_threadTest.isAlive()) {
			m_bTestStop = true;
			try {
				m_threadTest.join();
			}
			catch(Exception e) {} 
		}
		
		super.finishTest();
	}

	public void doNext(){
		m_nFalseResCount++;
		is_successProblem[m_nCurrentTestProblemCount] = false;
		/*
		is_successProblem[m_nCurrentTestProblemCount - 1] = false;
		m_nFalseResCount++;*/

		StudyTestActivity.playWordVoice("fail");

		setTestResult();

		new Handler() {
			public void handleMessage(Message msg) {
				m_bArrowBtnPressed  = true;
				m_nTestCount ++;
				m_nCurrentTestProblemCount++;
				m_nTestTime = 0;
				m_bResultPressed = false;
				m_bProblemView = false;
				m_bRecognitionResult = false;
			}
		}.sendEmptyMessageDelayed(0, 100);
	}

	public void setListener(OnEngVoiceTestFinishListener listener) {
		m_listener = listener;
	}
	
	public abstract interface OnEngVoiceTestFinishListener {
		public abstract void onAfter(int nTestTotalCount, int nTrueCount, int nFalseCount,int m_nTestCurrentProblemIndex_,Boolean[] is_successProblem_,ArrayList<WordData> m_arrAllWordData_);

	}
	float rms = 0f;
	float rms1 = 0f;
	float rms2 = 0f;
	float rms3 = 0f;
	float rms4 = 0f;

	float peak = 0f;

	private void processBuffer(short[] buffer){

		rms = 0f;
		rms1 = 0f;
		rms2 = 0f;
		rms3 = 0f;
		rms4 = 0f;

		peak = 0f;

        /*for(float sample : buffer) {

            float abs = Math.abs(sample);
            if(abs > peak) {
                peak = abs;
            }

            rms += sample * sample;
        }

        rms = (float)Math.sqrt(rms / buffer.length);

        */
		for(int i = 0; i < buffer.length / 4 ; i++){
			float sample = buffer[i];
			float abs = Math.abs(sample);
			if(abs > peak) {
				peak = abs;
			}

			rms1 += sample * sample;
		}

		for(int i = buffer.length / 4; i < buffer.length / 2; i++){
			float sample = buffer[i];
			float abs = Math.abs(sample);
			if(abs > peak) {
				peak = abs;
			}

			rms2 += sample * sample;
		}

		for(int i = buffer.length / 2; i < buffer.length / 4 * 3; i++){
			float sample = buffer[i];
			float abs = Math.abs(sample);
			if(abs > peak) {
				peak = abs;
			}

			rms3 += sample * sample;
		}

		for(int i = buffer.length / 4 * 3; i < buffer.length ; i++){
			float sample = buffer[i];
			float abs = Math.abs(sample);
			if(abs > peak) {
				peak = abs;
			}

			rms4 += sample * sample;
		}


		rms1 = (float)Math.sqrt(rms1 * 4 / buffer.length);
		rms2 = (float)Math.sqrt(rms2 * 4 / buffer.length);
		rms3 = (float)Math.sqrt(rms3 * 4 / buffer.length);
		rms4 = (float)Math.sqrt(rms4 * 4 / buffer.length);

		for(int i =0;i<19;i++){
			returnBuffer[i] = returnBuffer[i+1];
		}
		returnBuffer[19] = (short)rms1;

		formView.setSamples(returnBuffer);


		new Handler().postDelayed(new Runnable() {
			public void run() {
				for(int i =0;i<19;i++){
					returnBuffer[i] = returnBuffer[i+1];
				}
				returnBuffer[19] = (short)rms2;

				formView.setSamples(returnBuffer);
			}
		}, 100);

		new Handler().postDelayed(new Runnable() {
			public void run() {
				for(int i =0;i<19;i++){
					returnBuffer[i] = returnBuffer[i+1];
				}
				returnBuffer[19] = (short)rms3;

				formView.setSamples(returnBuffer);
			}
		}, 200);

		new Handler().postDelayed(new Runnable() {
			public void run() {
				for(int i =0;i<19;i++){
					returnBuffer[i] = returnBuffer[i+1];
				}
				returnBuffer[19] = (short)rms4;

				formView.setSamples(returnBuffer);
			}
		}, 300);



		if(mRecordRawFile != null){
			try {
				mRecordRawFile.write(EsStuddingUtils.shortToBytes(buffer));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	private int controlPercent(int nPercent){

		int nResult = 0;
		if(nPercent >= 30){
			nResult = 80 + 20  * (nPercent - 70) / 70;
		}else if(nPercent >= 10){
			nResult = 50 + 30 * (nPercent - 10)/ 20;
		}else if(nPercent >=0){
			nResult = 0 + 50 * nPercent / 10;
		}

		return nResult;
	}
	private void processResult(String strResult){
		if(m_arrWordData == null) return;

		WordData data = m_arrWordData.get(m_nTestCount);
		String strEnglish = data.getMeanEn();

		int nPercent = (int) ( EsStuddingUtils.similarity(strEnglish.toLowerCase(), strResult.toLowerCase()) * 100);

		int nResultPercent = controlPercent(nPercent);

		if(nResultPercent >= 70) {
			m_nTrueResCount ++;
			is_successProblem[m_nCurrentTestProblemCount] = true;
			StudyTestActivity.playWordVoice("success");

		}else{
			m_nFalseResCount++;
			is_successProblem[m_nCurrentTestProblemCount] = false;
			StudyTestActivity.playWordVoice("fail");

		}

		m_bRecognitionResult = false;

		//Toast.makeText(this, String.format("%d", nResultPercent), Toast.LENGTH_LONG).show();

	}

	public String getImageFileName(int nWordID){
		return m_pakManager.getFileName(nWordID, 0);
	}

	public String getAudioFileName(int nWordID){
		return m_pakManager.getFileName(nWordID, 1);
	}

	public String getVideoFileName(int nWordID){
		return m_pakManager.getFileName(nWordID, 2);
	}
}
