package com.yj.sentence.ui.studding;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.yj.sentence.R;
import com.yj.sentence.common.Define;
import com.yj.sentence.ui.studding.study.StudyPrepareActivity;
import com.yj.sentence.ui.studding.test.TestSelectActivity;
import com.yj.sentence.utils.ESUtils;

import org.w3c.dom.Text;

/**
 * Created by achilles on 12/29/16.
 */

public class PracticeResultItemView extends FrameLayout implements View.OnClickListener {

    private Context m_Context;

    private ImageView imgUSLine;
    private ImageView imgSLine;
    private TextView  textKorean;
    private TextView  textEnglish;
    private ImageButton btnStart;
    private ImageView imgMark;

    private boolean m_bRunning;
    private Handler m_Handler;
    private int m_nIndex;
    private int m_nMarks;

    public PracticeResultItemView(Context context) {
        super(context);
        m_Context = context;
        initViews();
    }

    private void initViews(){
        View v = LayoutInflater.from(m_Context).inflate(R.layout.practice_result_item, null);
        imgUSLine = (ImageView) v.findViewById(R.id.imgUnSelectedLine);
        imgSLine = (ImageView) v.findViewById(R.id.imgSelectedLine);
        textKorean = (TextView) v.findViewById(R.id.textKorean);
        textEnglish = (TextView) v.findViewById(R.id.textEnglish);
        imgMark = (ImageView) v.findViewById(R.id.imageMark);
        imgMark.setVisibility(View.INVISIBLE);

        ESUtils.setTextViewTypeFaceByRes(m_Context, textKorean, Define.getMainFont());
        ESUtils.setTextViewTypeFaceByRes(m_Context, textEnglish, Define.getEnglishFont());

        btnStart = (ImageButton) v.findViewById(R.id.btnPlay);

        imgSLine.setVisibility(View.INVISIBLE);
        imgUSLine.setVisibility(View.VISIBLE);
        btnStart.setBackgroundResource(R.drawable.btn_presult_start);

        btnStart.setOnClickListener(this);
        this.addView(v);

    }

    public void setIndex(int pIndex){
        m_nIndex = pIndex;
    }

    public void setMarks(int nMarks){
        m_nMarks = nMarks;

        imgMark.setVisibility(View.INVISIBLE);
        if(nMarks >= 76) {
            imgMark.setImageResource(R.drawable.st_study_excellent);
            imgMark.setVisibility(View.VISIBLE);
        }
        else if(nMarks >= 70) {
            imgMark.setImageResource(R.drawable.st_study_good);
            imgMark.setVisibility(View.VISIBLE);

        }
    }

    public void setInfo(String strEnglish, String strKorean){
        textEnglish.setText(strEnglish);
        textKorean.setText(strKorean);
    }

    public void setHandler(Handler pHandler){
        m_Handler = pHandler;
    }

    public void setRunning( boolean bRunning){
        m_bRunning = bRunning;

        if(m_bRunning)
        {
            imgUSLine.setVisibility(View.INVISIBLE);
            imgSLine.setVisibility(View.VISIBLE);
            btnStart.setBackgroundResource(R.drawable.btn_presult_stop);
        }else
        {
            imgUSLine.setVisibility(View.VISIBLE);
            imgSLine.setVisibility(View.INVISIBLE);
            btnStart.setBackgroundResource(R.drawable.btn_presult_start);
        }
    }

    private void startPlay(){
        int param = 0;

        if(m_bRunning == true){
            param = 1000;
        }else{
            param = 2000;
        }

        param += m_nIndex;

        Message msg = m_Handler.obtainMessage(param);
        msg.sendToTarget();
    }

    @Override
    public void onClick(View v) {
        ESUtils.playBtnSound(m_Context);

        switch (v.getId()){
            case R.id.btnPlay:
                startPlay();
                break;
            default:
                break;
        }
    }
}
