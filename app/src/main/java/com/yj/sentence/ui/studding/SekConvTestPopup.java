package com.yj.sentence.ui.studding;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.yj.sentence.R;
import com.yj.sentence.common.Define;
import com.yj.sentence.utils.ESUtils;

public class SekConvTestPopup extends Dialog implements View.OnClickListener {

	private Context mContext;
	private Button mBtnOK;
	private TextView textAll;
	private TextView textRight;
	private TextView textWrong;
	private TextView textTitle;
	private OnSEKConvLaunchListener mListener;

	public SekConvTestPopup(Context context) {
		super(context, R.style.MyDialog);
		// TODO Auto-generated constructor stub
		this.mContext = context;
		this.setCancelable(false);
		initViews();
	}
	
	private void initViews() {
		View localView = LayoutInflater.from(this.mContext).inflate(R.layout.sekconv_test_dialog, null);
		this.setContentView(localView);
		
		mBtnOK = (Button)localView.findViewById(R.id.btnOk);
		mBtnOK.setOnClickListener(this);

		textAll = (TextView) findViewById(R.id.textAll);
		textRight = (TextView) findViewById(R.id.textRight);
		textWrong = (TextView) findViewById(R.id.textWrong);
		textTitle = (TextView) findViewById(R.id.textTitle);

		ESUtils.setTextViewTypeFaceByRes(mContext, textAll, Define.getMainFont());
		ESUtils.setTextViewTypeFaceByRes(mContext, textRight, Define.getMainFont());
		ESUtils.setTextViewTypeFaceByRes(mContext, textWrong, Define.getMainFont());
		ESUtils.setTextViewTypeFaceByRes(mContext, textTitle, Define.getMainFont());

		ESUtils.setTextViewTypeFaceByRes(mContext, (TextView)findViewById(R.id.textRightUnit), Define.getMainFont());
		ESUtils.setTextViewTypeFaceByRes(mContext, (TextView)findViewById(R.id.textWrongUnit), Define.getMainFont());
		ESUtils.setTextViewTypeFaceByRes(mContext, (TextView)findViewById(R.id.textAllUnit), Define.getMainFont());


	}
	
	public void setListener(OnSEKConvLaunchListener listener) {
		mListener = listener;
	}

	public void setInfo(int nALL, int nRight, int nWrong){
		textAll.setText(String.format("%d", nALL));
		textRight.setText(String.format("%d", nRight));
		textWrong.setText(String.format("%d", nWrong));
	}

	public void setTitle(String strTitle){
		textTitle.setText(strTitle);
	}
	@Override
	public void dismiss() {
	    super.dismiss();
	}

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		ESUtils.playBtnSound(mContext);
		switch (arg0.getId()) {
			case R.id.btnOk:
				mListener.onOK();
				this.dismiss();
				break;
		}
	}
	
	public abstract interface OnSEKConvLaunchListener {
		public abstract void onOK();

	}

}
