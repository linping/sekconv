package com.yj.sentence.ui.studding.test;

import java.util.ArrayList;

import com.yj.sentence.common.Define;
import com.yj.sentence.database.ProgressDBManager;
import com.yj.sentence.database.StudyDBManager;
import com.yj.sentence.database.WordpadDBManager;
import com.yj.sentence.structs.ProgressData;
import com.yj.sentence.structs.WordData;

import android.content.Context;
import android.widget.FrameLayout;

public class TestLayout extends FrameLayout {

	protected int m_nTestTime = 0;	//한개 문제에 대한 시간계수
	protected int m_nTestCount = 0;	//시험문제 개수
	protected int m_nTrueResCount = 0;	//합격한 개수
	protected int m_nFalseResCount = 0;	//틀린 개수
	
	protected int m_nCurrentTestProblemCount = 0;	//현재 문제제시수
	protected int m_nTotalTestProblemCount = 0;		//총 문제수
	protected int m_nTestLimitTime = 7;	//설정에서 설정된 시험시간
	
	protected int m_tvTotalProblems = 4;	//단계별 전체 문제개수

	protected Boolean[] is_successProblem = new Boolean[Define.LevelTestProblemCounts];
	protected int m_nTestCurrentProblemIndex = -1;	//단계별 전체 문제개수

	protected Thread m_threadTest;
	protected boolean m_bTestStop = false;
	protected boolean m_bTestPause = false;
	protected boolean m_bResultPressed = false;	//한개 문제에 대한 시험이 끝났다면 true
	protected boolean m_bProblemView = false;	//
	
	protected boolean m_bIsTestOrder = false;
	protected int m_nScheduleId = -1;
	protected int m_nOrder = 0;
	
	protected StudyDBManager m_studyDbMana = null;
	protected ProgressDBManager m_progressDbMana = null;
	protected WordpadDBManager m_wordpadDbMana = null;
	//protected ScheduleDateData m_scheduleData = null;
	protected ProgressData m_progressData = null;

	protected ArrayList<WordData> m_arrWordData = new ArrayList<WordData>();	//시험문제
	protected ArrayList<WordData> m_arrAllWordData = new ArrayList<WordData>();	//전체시험문제
	
	public TestLayout(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	protected void finishTest() {
		if(m_arrWordData != null && m_arrWordData != null) {
			m_arrWordData.clear();
			m_arrWordData = null;
		}
	}
	
	protected void makeResWords() {
		
	}
	
	public void setTestPause(boolean bPause) {
		m_bTestPause = bPause;
	}
	
	protected void pauseThread() {
		while(m_bTestPause) {
			try {
				Thread.sleep(20);
			}
			catch(Exception e) {
				
			}
		}
	}
	//자료기지 해당한 값 얻기
	public void setDbMana(StudyDBManager studyDbMana, ProgressDBManager scheduleDbMana,
						  WordpadDBManager wordpadDbMana) {
		m_studyDbMana = studyDbMana;
		m_progressDbMana = scheduleDbMana;
		m_wordpadDbMana = wordpadDbMana;
		
	}

	public ArrayList<WordData> initWordData() {
		//만일 1단계 시험이라면
		return m_studyDbMana.getAllTextList();
	}
	
	public void setWordData(ArrayList<WordData> m_arrAllWordData_) {
		
		ArrayList<WordData> m_arrAllWordData__ = new ArrayList<WordData>();
		m_arrAllWordData__ = m_arrAllWordData_;	
		m_arrAllWordData = m_studyDbMana.getAllTextList();  
	}
	
	public ArrayList<WordData> getWordData() {
		return m_arrAllWordData; 
	}
	
	/*public void setScheduleData(ScheduleDateData scheduleData) {
		m_scheduleData = scheduleData;
	}
*/
	public void setProgressData(ProgressData progressData){
		m_progressData = progressData;
	}
	
	public void setCurrentProblemCount(int nCount) {
		m_nCurrentTestProblemCount = nCount;
	}
	
	public void setTestTimeLimit(int nLimit) {
		m_nTestLimitTime = nLimit;
	}
	public void setCurrentProblemIndex(int m_nTestCurrentProblemIndex_) {
		m_nTestCurrentProblemIndex = m_nTestCurrentProblemIndex_;
	}
	public void setIs_successProblem(Boolean[] is_successProblem_) {
		is_successProblem = is_successProblem_;
	}

	
	public void setTrueFalseWordCount(int nTrueCount, int nFalseCount) {
		m_nTrueResCount = nTrueCount;
		m_nFalseResCount = nFalseCount;
	}
	
	public void setIsTestOrder(boolean bIsTestOrder) {
		m_bIsTestOrder = bIsTestOrder;
	}
	
	public void setScheduleId(int sid) {
		m_nScheduleId = sid;
		
		if(m_nScheduleId != -1){
			//m_nOrder = m_progressDbMana.getLevel(m_nScheduleId);
			m_nOrder = m_progressDbMana.getLevel();
		}

	}
	
	public void setAllTestWordData(ArrayList<WordData> m_arrAllWordData_) {
		m_arrAllWordData = m_arrAllWordData_;
		m_arrWordData = m_arrAllWordData_;
	}
	
	public void setTotalProblemCount(int nCount) {
		if(m_bIsTestOrder)	//급수시험인 경우
			m_nTotalTestProblemCount = 40;
		else	//학습후 시험이라면 총 40문제
			m_nTotalTestProblemCount = nCount;

	}
	
	protected void getTestProblems(int nType) {
		if(m_bIsTestOrder) {	//급수선택시험이라면
//			m_arrWordData = m_studyDbMana.getLevelTestList(nType);
//			m_arrWordData = getLevelTestList(nType);
			m_arrWordData = m_arrAllWordData;
		}
		else {	//학습후 시험이라면 학습급수에 따르는 시험문제 제시
			if(m_nOrder != 0) {

				if( m_progressData.getStage() == 6){
					m_arrWordData = m_studyDbMana.getExerciseList(m_progressData);
				}else {
					m_arrWordData = m_studyDbMana.getTestList(m_progressData);
				}

			}
		}
	}
	
	public ArrayList<WordData> getLevelTestList(int kind) { 
		ArrayList<WordData> result = new ArrayList<WordData>();

		for (int i = 0; i < 40; i++) {
			result.add(m_arrAllWordData.get(i));
		}
		return result;
	}
	
	//학습후 시험이라면 틀린 단어 단어장에 보관
	protected void insertWrongWord(int nWordId) {
		if(!m_bIsTestOrder)	m_wordpadDbMana.InsertWordpad(m_nOrder, nWordId);
	}

}
