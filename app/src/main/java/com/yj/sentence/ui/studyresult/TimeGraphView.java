package com.yj.sentence.ui.studyresult;

import java.text.DecimalFormat;
import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.GradientDrawable;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;

public class TimeGraphView extends GraphView {

	public TimeGraphView(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	public TimeGraphView(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
	}

	public TimeGraphView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		// TODO Auto-generated constructor stub
	}

	@SuppressLint("HandlerLeak")
	public void initHandler() {
		if (mHandler == null) {
			mHandler = new Handler() {
				public void handleMessage(Message msg) {
					switch(msg.what) {
						case 0 :
							invalidate();
							break;
					}
				}
			};
		}
	}

	public void setGraphData(ArrayList<Integer> graphData, int day) {
		data = graphData;
		days = day;

		initHandler();

//		data.clear();
//		for (int i=0; i<days-7; i++) {
//			data.add(i);
//		}

		if (data.size() > 0) {
			isExit = false;
			offset = getWidth() / days;

			mThread = new Thread() {
				public void run() {
					index = data.size()-1;
					colorIndex = (int)data.get(index);
					left = (int)Math.floor(index * offset);
					right = (int)Math.floor(left + offset);
					bottom = getGraphHeight();

						/*int num = 10;
						for (value=0; value<=(colorIndex*getGraphHeight()/4); value+=num) {
							top = getGraphHeight() - value;
							if (mHandler != null)
								mHandler.sendEmptyMessage(0);

							sleepThread(10);
						}*/

					if (((colorIndex*getGraphHeight()/4)-value)>0) {
						top = getGraphHeight() - (colorIndex*getGraphHeight()/4) - value;
						if (mHandler != null)
							mHandler.sendEmptyMessage(0);

						sleepThread(10);
					}

					index = data.size();
					if (mHandler != null)
						mHandler.sendEmptyMessage(0);
				}
			};

			mThread.setName("graphthread");
			mThread.setDaemon(true);
			mThread.start();
		}
	}

	private int[] getGraphColor() {
		return getGraphColor(colorIndex);
	}

	private int[] getGraphColor(int value) {
		int[] result = new int[2];
		if (value<1)
			result = color.get(0);

		if (value>=1 && value<2)
			result = color.get(1);

		if (value>=2 && value<3)
			result = color.get(2);

		if (value>=3)
			result = color.get(3);

		return result;
	}

	public void finish() {
		closeHandler();
		closeThread();
	}

	@SuppressLint("DrawAllocation")
	@Override
	protected void onDraw(Canvas canvas) {
		if (index > 0 && !data.isEmpty() && days>0) {

			offset = getWidth()/days;
			mPaint.setColor(0xff666666);

			for (int i=1; i<=31; i++) {
				float x = (i-1)*offset + offset/2;
				float y = getHeight();
				mPaint.setTextAlign(Paint.Align.CENTER);
				canvas.drawText(String.valueOf(i), x, y, mPaint);
			}


			for (int i=0; i<index; i++) {
				int mValue = (int)data.get(i)*getGraphHeight()/4/60;
				int mLeft = (int)Math.floor(i * offset);
				int mRight = (int)Math.floor(mLeft + offset);
				int mTop = getGraphHeight() - mValue;
				int mBottom = getGraphHeight();
				int[] mColor = getGraphColor((int)data.get(i)/60);

				GradientDrawable drwable = new GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, mColor);
				drwable.setShape(GradientDrawable.RECTANGLE);
				drwable.setGradientRadius((float)(Math.sqrt(2) * 60));
				Rect rc = new Rect(mLeft, mTop, mRight, mBottom);
				drwable.setBounds(rc);
				canvas.save();
				drwable.setGradientType(GradientDrawable.LINEAR_GRADIENT);
				drwable.draw(canvas);
				canvas.restore();

				if (data.get(i) > 0) {
					mPaint.setColor(mColor[1]);
					mPaint.setTextSize(12);
					String strText = String.format("%d:%02d", data.get(i)/60, data.get(i)%60);

					//canvas.drawText(String.valueOf(new DecimalFormat("#.#").format(data.get(i) / 60.0f)), i*offset + offset/2, mTop-10, mPaint);
					canvas.drawText(strText, i*offset + offset/2, mTop-8, mPaint);
				}
			}
		}

		/*GradientDrawable drwable = new GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, getGraphColor());
		drwable.setShape(GradientDrawable.RECTANGLE);
		drwable.setGradientRadius((float)(Math.sqrt(2) * 60));
		Rect rc = new Rect(left, top, right, bottom);
		drwable.setBounds(rc);
		canvas.save();
	    drwable.setGradientType(GradientDrawable.LINEAR_GRADIENT);
	    drwable.draw(canvas);*/
		//canvas.restore();
	}

}
