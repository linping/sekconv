package com.yj.sentence.ui.usereg;

import java.util.ArrayList;

import com.yj.sentence.R;
import com.yj.sentence.database.UserDBManager;
import com.yj.sentence.structs.UserData;
import com.yj.sentence.ui.studding.SekWordPopup;
import com.yj.sentence.ui.studding.SekWordPopup.OnSEKLaunchListener;

import com.yj.sentence.ui.usereg.UserListItem.OnUserItemListener;
import com.yj.sentence.utils.ESUtils;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;

/**
 * 
 * @author RWI
 * 2015.6.28
 * ì‚¬ìš©ìž�ëª©ë¡�í™”ë©´
 */

public class EsUserListActivity extends PhotoActivity implements View.OnTouchListener
{
	
	private int mPageNum = 0;
	private int mEndNum = 0;
	
	private ArrayList<UserData> mDataList = null;
	
	private UserDBManager mDbMana = null;
	private UserData mUserData = null;
	
	private FrameLayout mRegItemLayout;
	private FrameLayout mainLayout;
	private UserListItem[] mItemsLayout;
	
	private GestureDetector m_gd;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_user_main);
		
		if (!openDB()) {
			this.finish();
			return;
		}
		
		init();
	} 
	
	private void init() {
		initViews();
		initButtons();
		initData();
		
		m_gd = new GestureDetector(this,
				new GestureDetector.OnGestureListener()
				{
					public boolean onSingleTapUp(MotionEvent me)
					{
						return false;
					}
					public void onShowPress(MotionEvent me)
					{
					}
					public void onLongPress(MotionEvent me)
					{
					}
					public boolean onDown(MotionEvent me)
					{
						return false;
					}
					public boolean onScroll(MotionEvent me, MotionEvent me2, float vx, float vy)
					{
						return true;
					}
					public boolean onFling(MotionEvent me, MotionEvent me2, float vx, float vy)
					{
						if (vx < 0) {
							nextPage();
						} else {
							prePage();
						}
						return true;
					}
				});
	}
	
	private void initData() {
		if (mDbMana == null) 
			mDbMana = new UserDBManager(this);
		
		if (mDbMana == null) return;
		
		mDataList = mDbMana.getUserList();
		mUserData = mDbMana.getActiveUser();

		if(mUserData == null & mDataList.size() == 1){
			userActive(0);
		}

		setDataList();
	}
	
	private void initViews() {
		mainLayout = (FrameLayout)findViewById(R.id.userLayout);
		mainLayout.setOnTouchListener(this);
		
		mRegItemLayout = (FrameLayout)findViewById(R.id.user_reg_layout);
		
		mItemsLayout = new UserListItem[6];
		mItemsLayout[0] = (UserListItem)findViewById(R.id.user_item1_layout);
		mItemsLayout[1] = (UserListItem)findViewById(R.id.user_item2_layout);
		mItemsLayout[2] = (UserListItem)findViewById(R.id.user_item3_layout);
		mItemsLayout[3] = (UserListItem)findViewById(R.id.user_item4_layout);
		mItemsLayout[4] = (UserListItem)findViewById(R.id.user_item5_layout);
		mItemsLayout[5] = (UserListItem)findViewById(R.id.user_item6_layout);
	}
	
	private void initButtons() {
		Button btnUserReg = (Button)findViewById(R.id.btnUserReg);
    	btnUserReg.setOnClickListener(new View.OnClickListener() {			
			@Override
			public void onClick(View v) {
				ESUtils.playBtnSound(EsUserListActivity.this);
				goUserRegActivity(-1);
			}
		});
    	
    	Button btnExit = (Button)findViewById(R.id.btnExit);
    	btnExit.setOnClickListener(new View.OnClickListener() {			
			@Override
			public void onClick(View v) {
				ESUtils.playBtnSound(EsUserListActivity.this);
				EsUserListActivity.this.finish();
			}
		});
    	
    	for (int i=0; i<mItemsLayout.length; i++) {
    		mItemsLayout[i].setIndex(i);
    		mItemsLayout[i].setListener(new OnUserItemListener() {
				
				@Override
				public void onEdit(int index) {
					goUserRegActivity((mPageNum*6+5)-6+index);
				}
				
				@Override
				public void onDelete(int index) {
					userDelete((mPageNum*6+5)-6+index);
				}
				
				@Override
				public void onActive(int index) {
					userActive((mPageNum*6+5)-6+index);
				}
				
				@Override
				public void onPhoto(int index) {
					userPhoto((mPageNum*6+5)-6+index);
				}
			});
    	}
    	
	}
	
	private void goUserRegActivity(int index) {
		Intent localIntent = new Intent(EsUserListActivity.this, EsUserRegActivity.class);
		
		if (index > -1) {
			UserData data = (UserData)mDataList.get(index);
			localIntent.putExtra("uId", data.getId());
			localIntent.putExtra("uName", data.getName());
			localIntent.putExtra("uJob", data.getJob());
			localIntent.putExtra("uLevel", data.getLevel());
			localIntent.putExtra("uPhoto", data.getPhoto());
			localIntent.putExtra("uActive", data.getActive());
		}
		
		EsUserListActivity.this.startActivity(localIntent);
	}
	
	private void userDelete(int index) {
		final UserData data = (UserData)mDataList.get(index);
		
		/*new AlertDialog.Builder(this)
			.setTitle(R.string.user_del_title)
			.setMessage( data.getName()+this.getString(R.string.user_del_msg))
			.setCancelable(false)
			.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
			
				public void onClick(DialogInterface dialog, int which) {	
					mDbMana.Delete(data);
					
					initData();
				}
			})
			.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
				
				public void onClick(DialogInterface dialog, int which) {
					dialog.cancel();
				}
			})
			.show();*/
		SekWordPopup popup;

		popup = new SekWordPopup(EsUserListActivity.this);
		popup.setMessage(data.getName()+this.getString(R.string.user_del_msg));
		popup.setListener(new OnSEKLaunchListener() {

			@Override
			public void onOK() {
				mDbMana.Delete(data);
				initData();
			}

			@Override
			public void onCancel() {
				// TODO Auto-generated method stub
				
			}

		});
		
		popup.show();


	}
	
	private void userActive(int index) {
		UserData data = (UserData)mDataList.get(index);
		
		mDbMana.setActive(data);
		this.finish();
	}

	private int m_nSeletedUserIndex = -1;
	private void userPhoto(int index) {
		m_nSeletedUserIndex = index;
		getPhoto();
	}

	@Override
	protected Bitmap setPhotoImage(String strFile) {
		Bitmap bmpPhoto = super.setPhotoImage(strFile);
		if (bmpPhoto != null)
		{
			UserData data = (UserData)mDataList.get(m_nSeletedUserIndex);
			data.setPhoto(m_strPhotoPath);
			mDbMana.Update(data);

			mDataList = mDbMana.getUserList();
			mUserData = mDbMana.getActiveUser();
		}
		
		return bmpPhoto;
	}

	private boolean openDB() {
		if (mDbMana == null) 
			mDbMana = new UserDBManager(this);
		
		if (mDbMana == null) 
			return false;
		
		return true;
	}
	
	private void closeDB() {
		if (mDbMana != null) {
			mDbMana.close();
			mDbMana = null;
		}
	}
	
	private void setDataList() {
		for (int i=0; i<mItemsLayout.length; i++) {
			mItemsLayout[i].setVisibility(View.GONE);
		}
		
		if (mPageNum > 0 && ((mDataList.size()-5)%6)==0) {
			int maxPage = (mDataList.size() - 5) / 6 + 1;
			if (mPageNum >= maxPage) {
				mPageNum = maxPage - 1;
				mEndNum = (mPageNum-1)*6+5;
			}
		}
		
		if (mPageNum == 0) {
			mRegItemLayout.setVisibility(View.VISIBLE);
			mItemsLayout[0].setVisibility(View.GONE);
			
			for (int i=0; i<5; i++) {
				if (i >= mDataList.size())
					break;
				
				UserData		item = (UserData)mDataList.get(i);
				UserListItem	itemLayout = mItemsLayout[i+1];
				itemLayout.setName(item.getName());
				itemLayout.setJob(item.getJob());
				itemLayout.setLevel(item.getLevel());
				itemLayout.setPhoto(item.getPhoto());
				itemLayout.setVisibility(View.VISIBLE);
				
				if (mUserData != null && mUserData.getId() == item.getId())
					itemLayout.setSelect();
			}
			
			mEndNum = 5;
		} else {
			mRegItemLayout.setVisibility(View.GONE);
			mItemsLayout[0].setVisibility(View.VISIBLE);
			mEndNum = (mPageNum-1)*6+5;
			int i = 0;
			for (i=mEndNum; i<mEndNum+6; i++) {
				if (i >= mDataList.size())
					break;
				
				UserData		item = (UserData)mDataList.get(i);
				UserListItem	itemLayout = mItemsLayout[i-mEndNum];
				itemLayout.setName(item.getName());
				itemLayout.setJob(item.getJob());
				itemLayout.setLevel(item.getLevel());
				itemLayout.setPhoto(item.getPhoto());
				itemLayout.setVisibility(View.VISIBLE);
				
				if (mUserData != null && mUserData.getId() == item.getId())
					itemLayout.setSelect();
			}
			
			mEndNum = i;
		}	
	}
	
	private void nextPage() {
		if ((mPageNum*6+5) < mDataList.size()) {
			mPageNum ++;
			
			setDataList();
		}
	}
	
	private void prePage() {
		if (mPageNum > 0) {
			mPageNum--;
			mEndNum = (mPageNum-1)*6+5;
			
			setDataList();
		}
	}
	
	@Override
	protected void onResume() {
		initData();
		
		super.onResume();
	}

	@Override
	protected void onDestroy() {
		closeDB();
		
		super.onDestroy();
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		
		switch(v.getId()) {
			case R.id.userLayout:
				switch (event.getAction()) {
					case MotionEvent.ACTION_DOWN:
						break;
					case MotionEvent.ACTION_UP:
						break;
				}
				break;
		}
		
		m_gd.onTouchEvent(event);
		
		return true;
	}
}
