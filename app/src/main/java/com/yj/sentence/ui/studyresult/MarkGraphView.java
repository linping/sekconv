package com.yj.sentence.ui.studyresult;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;

public class MarkGraphView extends GraphView {

	public MarkGraphView(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	public MarkGraphView(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
	}

	public MarkGraphView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		// TODO Auto-generated constructor stub
	}

	@SuppressLint("HandlerLeak")
	public void initHandler() {
		if (mHandler == null) {
			mHandler = new Handler() {
				public void handleMessage(Message msg) {
					switch(msg.what) {
						case 0 :
							invalidate();
							break;
					}
				}
			};
		}
	}

	public void setGraphData(ArrayList<Integer> graphData, int day) {
		data = graphData;
		days = day;
		initHandler();

//		data.clear();
//		data.add(0);
//		for (int i=0; i<10; i++) {
//			data.add((int)(Math.random() * 90));
//		}

		if (data.size()>0) {
			mHandler.sendEmptyMessage(0);
			/*isExit = false;
			offset = getWidth() / days;
			mThread = new Thread() {
				public void run() {
						index = data.size()-1;
						colorIndex = (int)data.get(index);
						left = (int)Math.floor(index * offset);
						right = (int)Math.floor(left + offset);
						bottom = getGraphHeight();

						int num = 10;
						for (value=0; value<=(colorIndex*getGraphHeight()/100); value+=num) {
							top = getGraphHeight() - value;
							if (mHandler != null)
								mHandler.sendEmptyMessage(0);

							sleepThread(10);
						}

						if (((colorIndex*getGraphHeight()/100)-value)>0) {
							top = getGraphHeight() - (colorIndex*getGraphHeight()/100) - value;
							if (mHandler != null)
								mHandler.sendEmptyMessage(0);

							sleepThread(10);
						}

					index = data.size();
					if (mHandler != null)
						mHandler.sendEmptyMessage(0);
				}
			};

			mThread.setName("graphthread");
			mThread.setDaemon(true);
			mThread.start();*/
		}
	}

	public void finish() {
		closeHandler();
		closeThread();
	}

	@SuppressLint("DrawAllocation")
	@Override
	protected void onDraw(Canvas canvas) {
		if (!data.isEmpty() && days>0) {

			offset = getWidth()/days;
			mPaint.setColor(0xff666666);

			for (int i=1; i<=days; i++) {
				float x = (i-1)*offset + offset/2;
				float y = getHeight();
				mPaint.setTextAlign(Paint.Align.CENTER);
				canvas.drawText(String.valueOf(i), x, y, mPaint);
			}

			int lastIndex = 0;
			int firstX = 0;
			int firstY = 0;
			Path path = new Path();

			for (int i=0; i<data.size(); i++) {
				if (data.get(i)>0) {
					lastIndex = i;
					if (path.isEmpty()) {
						firstX = (int) (i*offset);
						firstY = getGraphHeight();
						path.moveTo(firstX, firstY);

						int mValue = (int)data.get(i)*getGraphHeight()/100;
						path.lineTo((i*offset + offset/2), getGraphHeight()-mValue);
					} else {
						int mValue = (int)data.get(i)*getGraphHeight()/100;
						path.lineTo((i*offset + offset/2), getGraphHeight()-mValue);
					}
				}
			}

			path.lineTo((lastIndex*offset + offset/2), getGraphHeight());
			path.lineTo(firstX, firstY);

			mPaint.setColor(0x63eaddd0);
			canvas.drawPath(path, mPaint);

			for (int i=0; i<data.size(); i++) {
				if (data.get(i)>0) {
					int cx = (int) (i*offset + offset/2);
					int cy = (int) (int)data.get(i)*getGraphHeight()/100;

					mPaint.setColor(0xffff7e00);
					canvas.drawCircle(cx, getGraphHeight()-cy + 3, 3, mPaint);

					//canvas.drawText(String.valueOf(data.get(i)), i*offset + offset/2, getGraphHeight()-cy-20, mPaint);
					canvas.save();
					canvas.restore();
				}
			}
		}

	}

}
