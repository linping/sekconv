package com.yj.sentence.ui.studding.test;


import java.util.ArrayList;
import java.util.HashMap;

import com.yj.sentence.R;
import com.yj.sentence.common.Define;
import com.yj.sentence.database.ProgressDBManager;
import com.yj.sentence.database.SettingDBManager;
import com.yj.sentence.database.StudyDBManager;
import com.yj.sentence.database.UserDBManager;
import com.yj.sentence.database.WordpadDBManager;
import com.yj.sentence.structs.ProgressData;
import com.yj.sentence.structs.WordData;
import com.yj.sentence.ui.studding.SekConvTestPopup;
import com.yj.sentence.ui.studding.SekWordPopup;
import com.yj.sentence.ui.studding.SekWordPopup.OnSEKLaunchListener;
import com.yj.sentence.ui.studding.study.StudyPracticeActivity;
import com.yj.sentence.utils.ESUtils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;

public class StudyTestActivity extends Activity {

	private FrameLayout m_layoutParent;
	
	private TestVideoToTextLayout m_layoutVideoToTextTest;
	private TestTextToVideoLayout m_layoutTextToVideoTest;
	private TestSpeakingLayout m_layoutSpeakingTest;
	private TestListeningLayout m_layoutListeningTest;
	public static TestResultMedalLayout m_layoutTestResultMedal;
	
	private int m_nTrueResCount = 0;
	private int m_nFalseResCount = 0;
	
	private int m_nTestLimitTime = 7;
	private int m_nTestLimitTime_typing = 14;
	private int m_nCurrentTestProblemCount = 0;
	private int m_nTotalTestProblemCount = 0;
	
	protected int m_nTestCurrentProblemIndex = -1;	//단계별 전체 문제개수
	protected Boolean[] is_successProblem = new Boolean[Define.LevelTestProblemCounts];
	
	private SettingDBManager mSettingDbMana = null;
	private StudyDBManager m_studyDbMana = null;
	//private ScheduleDBManager m_progressDbMana = null;
	private ProgressDBManager m_progressDbMana = null;

	private WordpadDBManager m_wordpadDbMana = null;
	private UserDBManager m_userDbMana = null;
	//private ScheduleDateData m_scheduleData = null;
	private ProgressData m_progressData = null;

	private int m_nUserId = -1;		//ì‚¬ìš©ìž�ì•„ì�´ë””
	private int m_nScheduleId = -1;	//ì�¼ì •ì•„ì�´ë””
	
	private boolean m_bIsTestOrder = false;			//ê¸‰ìˆ˜ì‹œí—˜ì�´ë©´ true, í•™ìŠµí›„ ì‹œí—˜ì�´ë©´ false
	private boolean m_bExercise = false;
	private int[] m_arrTestCase = {1, 1, 0, 1, 1};	//ë§¤ ì‹œí—˜ë‹¨ê³„ì�˜ ìˆœì„œ
	private int m_nTestPhase = -1;					//ì‹œí—˜ë‹¨ê³„

	protected ArrayList<WordData> m_arrAllWordData = new ArrayList<WordData>();	//전체시험문제
	protected WordData[] m_arrAllWordDataArray = new WordData[40];


	private ImageButton btnNext;
	private boolean bFinish = false;

	private static StudyTestActivity mThis;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_study_test);

		mThis = this;
		Intent intent = getIntent();
		m_nUserId = intent.getIntExtra(Define.USER_ID, -1);
		m_nScheduleId = intent.getIntExtra(Define.SCHEDULE_ID, -1);
		m_bExercise = intent.getBooleanExtra(Define.IS_EXERCISE,false);
		m_bIsTestOrder = intent.getBooleanExtra(Define.IS_ORDERTEST, false);

		m_arrTestCase[0] = intent.getBooleanExtra(Define.IS_VTT, false) ? 1 : 0;
		m_arrTestCase[1] = intent.getBooleanExtra(Define.IS_TTV, false) ? 1 : 0;
		m_arrTestCase[2] = intent.getBooleanExtra(Define.IS_VTS, false) ? 1 : 0;
		m_arrTestCase[3] = intent.getBooleanExtra(Define.IS_LTV, false) ? 1 : 0;

		if(!isConnectDB()){
			finish();
			return;
		}	
		
		if(m_bIsTestOrder)
		{
			m_nTotalTestProblemCount = Define.LevelTestProblemCounts;
		}else{
			m_nTotalTestProblemCount = getTestCaseSchedule();
		}
		if(m_nScheduleId != -1) getScheduleData();
		
		getTestTimeData();
		
		initTab();
		initLayouts();
		initStartButton();

		startTest();

	}
	
	private void finishActivity() {		
		disableAllTestLayout();
		closeDB();

	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		doBack();
		//super.onBackPressed();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		finishActivity();
		
		super.onDestroy();
		System.gc();
	}

	private void initLayouts() {
		m_layoutParent = (FrameLayout)findViewById(R.id.TestParentLayout);
	}
	
	private void initTab() {
		ImageButton btnBack = (ImageButton)findViewById(R.id.btnBack);
		btnBack.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				ESUtils.playBtnSound(StudyTestActivity.this);

				doBack();
			}
		});
	}
	
	private void initStartButton() {
		btnNext = (ImageButton) findViewById(R.id.btnNext);
		btnNext.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				ESUtils.playBtnSound(StudyTestActivity.this);

				if(bFinish){
					Intent intent = new Intent(StudyTestActivity.this, StudyPracticeActivity.class);
					intent.putExtra(Define.USER_ID, m_nUserId);
					intent.putExtra(Define.SCHEDULE_ID, m_nScheduleId);
					startActivity(intent);
					finish();
				} else {
					doNext();
				}
			}
		});

		/*Button btnStartTest = (Button)findViewById(R.id.btnTestStart);
		btnStartTest.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				m_nTestPhase = 0;
				if(!m_bIsTestOrder)
					m_studyDbMana.setFormatTestTmpTable();
				performTest();
			}
		});*/
	}

	private  void startTest(){
		m_nTestPhase = 0;
		/*if(!m_bIsTestOrder)
			m_studyDbMana.setFormatTestTmpTable();*/
		performTest();
	}

	private void doNext(){
		if(m_nTestPhase >= 0 && m_nTestPhase < 4) {
			switch(m_nTestPhase) {
				case 0:
					m_layoutVideoToTextTest.doNext();
					break;
				case 1:
					m_layoutTextToVideoTest.doNext();
					break;
				case 2:
					m_layoutSpeakingTest.doNext();
					break;
				case 3:
					m_layoutListeningTest.doNext();
					break;
			}
		}
	}
	private void doBack() {
		if(m_nTestPhase >= 0 && m_nTestPhase < 4) { //í˜„ìž¬ ì‹œí—˜ì¤‘ì�´ë�¼ë©´
			switch(m_nTestPhase) {
			case 0:
				m_layoutVideoToTextTest.setTestPause(true);
				break;
			case 1:
				m_layoutTextToVideoTest.setTestPause(true);
				break;
			case 2:
				m_layoutSpeakingTest.setTestPause(true);
				break;
			case 3:
				m_layoutListeningTest.setTestPause(true);
				break;
			}
			showTestStopDlg();
		}
		else {
			/*if(!m_bIsTestOrder)
				sendScheduleInfo(false, m_progressDbMana.getLevel(m_nScheduleId));
			
			StudyTestActivity.this.finish();*/

			StudyTestActivity.this.finish();
		}
	}

	
	public static void playWordVoice(String strVoice) {

		if(strVoice.contains("success")){
			ESUtils.playSuccessSound(mThis);
		}else if(strVoice.contains("fail")){
			ESUtils.playFailSound(mThis);
		}
	}
	
	//DBì ‘ì†�
	private boolean isConnectDB() {
		/*m_studyDbMana = new StudyDBManager(this, m_nUserId);
		if(m_studyDbMana == null)
			return false;*/
		
		/*m_progressDbMana = new ScheduleDBManager(this, m_nUserId);
		if(m_progressDbMana == null)
			return false;*/
		m_studyDbMana = new StudyDBManager(this, m_nUserId);
		if(m_studyDbMana == null)
			return  false;

		m_progressDbMana = new ProgressDBManager(this, m_nUserId);
		if(m_progressDbMana == null)
			return false;
		
		mSettingDbMana = new SettingDBManager(this);
		if(mSettingDbMana == null)
			return false;
		
		m_wordpadDbMana = new WordpadDBManager(this, m_nUserId);
		if(m_wordpadDbMana == null)
			return false;
		
		m_userDbMana = new UserDBManager(this);
		if(m_userDbMana == null)
			return false;
		
		return true;
	}
	
	private void closeDB() {
		if (m_studyDbMana != null) {
			m_studyDbMana.close();
			m_studyDbMana = null;
		}
		
		/*if (m_progressDbMana != null) {
			m_progressDbMana.close();
			m_progressDbMana = null;
		}*/
		if(m_progressDbMana != null){
			m_progressDbMana.close();
			m_progressDbMana = null;
		}
		
		if (mSettingDbMana != null) {
			mSettingDbMana.close();
			mSettingDbMana = null;
		}
		
		if(m_wordpadDbMana != null) {
			m_wordpadDbMana.close();
			m_wordpadDbMana = null;
		}
		
		if(m_userDbMana != null) {
			m_userDbMana.close();
			m_userDbMana = null;
		}
	}
	
	//ì„¤ì •ë�œ ì‹œí—˜ì‹œê°„ ì–»ê¸°
	private void getTestTimeData() {
		m_nTestLimitTime = mSettingDbMana.getTestTimeValue();
	}
	
	//ì�¼ì •ì•„ì�´ë””ì—� ë”°ë¥´ëŠ” ì�¼ì •ìž�ë£Œ ì–»ê¸°
	private void getScheduleData() {
		//m_scheduleData = m_progressDbMana.getScheduleDateData(m_nScheduleId);
		int userLevel = m_progressDbMana.getLevel();
		int epiNum = m_nScheduleId/9;
		int stgNum = m_nScheduleId%9;

		m_progressData = new ProgressData(m_nUserId, userLevel, epiNum, stgNum);

	}

	//ì„¤ì •ì—�ì„œ ì„ íƒ�í•œ í•™ìŠµí›„ ì‹œí—˜ì�¼ì • ì–»ê¸°
	private int getTestCaseSchedule() {
		String strMode = mSettingDbMana.getTestModeValue();
		String[] strTemp = strMode.split(",");
		String strPhase = "";
		
		for(int i=0; i<4; i++) { 
			//m_arrTestCase[i] = Integer.valueOf(strTemp[i]);
			if(m_arrTestCase[i] == 1)
				strPhase += "1";
		}
		
		return strPhase.length() * 4;	//ì´�ë¬¸ì œìˆ˜ ê·€í™˜ã„´
	}
	
	//ëª¨ë“  ì‹œí—˜ë ˆì�´ì•„ì›ƒ Disable
	private void disableAllTestLayout() {
		m_layoutParent.removeAllViews();
		
		if(m_layoutVideoToTextTest != null) {
			m_layoutVideoToTextTest.finishTest();
			m_layoutVideoToTextTest = null;
		}
		
		if(m_layoutTextToVideoTest != null) {
			m_layoutTextToVideoTest.finishTest();
			m_layoutTextToVideoTest = null;
		}
		
		if(m_layoutSpeakingTest != null) {
			m_layoutSpeakingTest.finishTest();
			m_layoutSpeakingTest = null;
		}
		
		if(m_layoutListeningTest != null) {
			m_layoutListeningTest.finishTest();
			m_layoutListeningTest = null;
		}

		/*if(m_layoutTestResultMedal != null) {
			m_layoutTestResultMedal = null;
		}*/
	}
	
	//ì‹œí—˜ì�¼ì •ì—� ë”°ë¥¸ ì‹œí—˜ì§„í–‰
	private void performTest() {
		disableAllTestLayout();
		
		for(int i=m_nTestPhase; i<m_arrTestCase.length; i++) 
			if(m_arrTestCase[m_nTestPhase] == 0) {
				m_nTestPhase ++;
				if(m_nTestPhase == m_arrTestCase.length-1)
					break;
			}
			else
				break;
		
		
		//debug purpose
//		displayChartResult();

//		performEngTypingTest();

		
		switch(m_nTestPhase) {
		case 0:			//ì¡°ì„ ì–´-ì˜�ì–´
			performKorEngTest();
			break;
		case 1:			//ì˜�ì–´-ì¡°ì„ ì–´
			performEngKorTest();
			break;
		case 2:			//ì˜�ì–´-ë°œì�Œ
			performEngVoiceTest();
			break;
		case 3:
			performEngTypingTest();
			break;
		case 4:
			if(m_bIsTestOrder)
				displayChartResult();
			else {
				//displayResultMedal();
				displayPopup();
			}
			break;
		}
	}
	
	//ì¡°ì„ ì–´-ì˜�ì–´ì‹œí—˜ ì§„í–‰
	private void performKorEngTest() {
		m_layoutVideoToTextTest = new TestVideoToTextLayout(this);
		m_layoutVideoToTextTest.setListener(new TestVideoToTextLayout.OnKorEngTestFinishListener() {
			
			@Override
			public void onAfter(int nTestTotalCount, int nTrueCount, int nFalseCount,int m_nTestCurrentProblemIndex_,Boolean[] is_successProblem_,ArrayList<WordData> m_arrAllWordData_) {
				// TODO Auto-generated method stub
				m_nCurrentTestProblemCount = nTestTotalCount;
				m_nTrueResCount = nTrueCount;
				m_nFalseResCount = nFalseCount;
				m_nTestCurrentProblemIndex = m_nTestCurrentProblemIndex_;
				is_successProblem = is_successProblem_;

				
//				ArrayList<WordData> m_arrAllWordData__ = new ArrayList<WordData>();
//				m_arrAllWordData__ = m_arrAllWordData_;
				
				m_arrAllWordData =m_arrAllWordData_;
				m_nTestPhase ++;
				performTest();
			};
		});		
		m_layoutParent.addView(m_layoutVideoToTextTest);

		//í•´ë‹¹í•œ ìž�ë£Œ ì„¤ì •
		initTestLayouts(m_layoutVideoToTextTest);
		initWordData(m_layoutVideoToTextTest);
		m_layoutVideoToTextTest.setAllTestWordData(m_arrAllWordData);

		//초기 40문자 얻기
		m_layoutVideoToTextTest.performKorEngTest();
	}
	
	//ì˜�ì–´-ì¡°ì„ ì–´ì‹œí—˜ ì§„í–‰
	private void performEngKorTest() {
		m_layoutTextToVideoTest = new TestTextToVideoLayout(this);
		m_layoutTextToVideoTest.setListener(new TestTextToVideoLayout.OnEngKorTestFinishListener() {
			
			@Override
			public void onAfter(int nTestTotalCount, int nTrueCount, int nFalseCount,int m_nTestCurrentProblemIndex_,Boolean[] is_successProblem_,ArrayList<WordData> m_arrAllWordData_) {
				// TODO Auto-generated method stub
				m_nCurrentTestProblemCount = nTestTotalCount;
				m_nTrueResCount = nTrueCount;
				m_nFalseResCount = nFalseCount;
				m_nTestCurrentProblemIndex = m_nTestCurrentProblemIndex_;
				is_successProblem = is_successProblem_;
				
//				ArrayList<WordData> m_arrAllWordData__ = new ArrayList<WordData>();
//				m_arrAllWordData__ = m_arrAllWordData_;
				m_arrAllWordData = m_arrAllWordData_;

				m_nTestPhase ++;
				performTest();
			}
		});

		
		m_layoutParent.addView(m_layoutTextToVideoTest);
	
		//í•´ë‹¹í•œ ìž�ë£Œ ì„¤ì •
		initTestLayouts(m_layoutTextToVideoTest);
		initWordData(m_layoutTextToVideoTest);
		m_layoutTextToVideoTest.setAllTestWordData(m_arrAllWordData);

		m_layoutTextToVideoTest.performEngKorTest();
	}
	
	//ì˜�ì–´ë°œì�Œ - ì¡°ì„ ì–´ ì‹œí—˜ ì§„í–‰
	private void performEngVoiceTest() {
		m_layoutSpeakingTest = new TestSpeakingLayout(this);
		m_layoutSpeakingTest.setListener(new TestSpeakingLayout.OnEngVoiceTestFinishListener() {
			
			@Override
			public void onAfter(int nTestTotalCount, int nTrueCount, int nFalseCount,int m_nTestCurrentProblemIndex_,Boolean[] is_successProblem_,ArrayList<WordData> m_arrAllWordData_) {
				// TODO Auto-generated method stub
				m_nCurrentTestProblemCount = nTestTotalCount;
				m_nTrueResCount = nTrueCount;
				m_nFalseResCount = nFalseCount;
				m_nTestCurrentProblemIndex = m_nTestCurrentProblemIndex_;
				is_successProblem = is_successProblem_;
				
//				ArrayList<WordData> m_arrAllWordData__ = new ArrayList<WordData>();
//				m_arrAllWordData__ = m_arrAllWordData_;
				m_arrAllWordData =m_arrAllWordData_;

				m_nTestPhase ++;
				performTest();
			}
		});
		
		m_layoutParent.addView(m_layoutSpeakingTest);

		//í•´ë‹¹í•œ ìž�ë£Œ ì„¤ì •
		initTestLayouts(m_layoutSpeakingTest);
		initWordData(m_layoutSpeakingTest);
		m_layoutSpeakingTest.setAllTestWordData(m_arrAllWordData);
		m_layoutSpeakingTest.performEngVoiceTest();
	}
	
	//ì˜�ì–´ì² ìž�ì‹œí—˜ ì§„í–‰
	private void performEngTypingTest() {
		m_layoutListeningTest = new TestListeningLayout(this);
		m_layoutListeningTest.setListener(new TestListeningLayout.OnEngKorTestFinishListener() {
			
			@Override
			public void onAfter(int nTestTotalCount, int nTrueCount, int nFalseCount,int m_nTestCurrentProblemIndex_,Boolean[] is_successProblem_,ArrayList<WordData> m_arrAllWordData_) {
				// TODO Auto-generated method stub
				m_nCurrentTestProblemCount = nTestTotalCount;
				m_nTrueResCount = nTrueCount;
				m_nFalseResCount = nFalseCount;
				m_nTestCurrentProblemIndex = m_nTestCurrentProblemIndex_;
				is_successProblem = is_successProblem_;
				m_nTestPhase ++;
				performTest();
			}
		});
		
		m_layoutParent.addView(m_layoutListeningTest);
	
		initTestLayouts(m_layoutListeningTest);
		initWordData(m_layoutListeningTest);
		m_layoutListeningTest.setAllTestWordData(m_arrAllWordData);
		m_layoutListeningTest.setTestTimeLimit(m_nTestLimitTime_typing);


		m_layoutListeningTest.performEngKorTest();
	}
	
	//ì‹œí—˜ê²°ê³¼ë¥¼ ë�„í‘œë¡œ ë³´ì—¬ì¤€ë‹¤
	private void displayChartResult() {
	}
	
	//ì‹œí—˜ê²°ê³¼ë¥¼ ë©”ë‹¬ë¡œ ë³´ì—¬ì¤€ë‹¤

	private void displayPopup(){

		if(m_bExercise == false){
			int nMarks = 100 * m_nTrueResCount / (m_nTrueResCount + m_nFalseResCount);
			int userLevel = m_progressDbMana.getLevel();
			int epiNum = m_nScheduleId/9;
			int stgNum = m_nScheduleId%9;
			m_progressData = new ProgressData(m_nUserId, userLevel, epiNum, stgNum, nMarks);
			m_progressDbMana.updateProgress(m_progressData);

			//update date marks
			long nCurTime = ESUtils.getNowDateMilliseconds();
			int nYear = ESUtils.getDateOfYear(nCurTime);
			int nMonth = ESUtils.getDateOfMonth(nCurTime);
			int nDate = ESUtils.getDateOfDay(nCurTime);

			long nRegTime = ESUtils.getDateMilliseconds(nYear, nMonth - 1, nDate);
			m_progressDbMana.updateProgressMarks(nRegTime, nMarks);
		} else {
			int userLevel = m_progressDbMana.getLevel();
			int epiNum = m_nScheduleId/9;
			int stgNum = m_nScheduleId%9;

			int nMarks = 100 ;
			ProgressData m_progressData = new ProgressData(userLevel, userLevel, epiNum, stgNum, nMarks);
			m_progressDbMana.updateProgress(m_progressData);
		}


		displayResultMedal();

		SekConvTestPopup popup;

		popup = new SekConvTestPopup(this);
		if(m_bExercise){
			popup.setTitle(getString(R.string.exercise_result));
		}
		popup.setInfo(m_nTrueResCount + m_nFalseResCount, m_nTrueResCount, m_nFalseResCount);
		popup.setListener(new SekConvTestPopup.OnSEKConvLaunchListener() {

			@Override
			public void onOK() {
				if(m_bExercise == false){

					int nPoint = m_nTrueResCount * 100 / (m_nTrueResCount + m_nFalseResCount);
					if(nPoint >= 1){
						sendScheduleInfo();
					}

					Intent intent = new Intent(StudyTestActivity.this, StudyPracticeActivity.class);
					intent.putExtra(Define.USER_ID, m_nUserId);
					intent.putExtra(Define.SCHEDULE_ID, m_nScheduleId);
					startActivity(intent);
				}

				finish();
			}
		});
		popup.show();

	}

	private void displayResultMedal() {
		m_layoutTestResultMedal = new TestResultMedalLayout(this);
		m_layoutTestResultMedal.setListener(new TestResultMedalLayout.OnTestResultMedalFinishListener() {
			
			@Override
			public void onAfter(int nOrder) {
				// TODO Auto-generated method stub
				//ì�¼ì •ë³´ê¸°í™”ë©´ìœ¼ë¡œ ì�´í–‰
				//sendScheduleInfo(false, nOrder);
				StudyTestActivity.this.finish();
			}
		});
		//m_layoutParent.addView(m_layoutTestResultMedal);
		
		initTestLayouts(m_layoutTestResultMedal);		
		m_layoutTestResultMedal.setTestResultInfo();

		bFinish = true;
	}
	
	private void initTestLayouts(TestLayout layout) {
		layout.setDbMana(m_studyDbMana, m_progressDbMana, m_wordpadDbMana);
		//layout.setScheduleData(m_scheduleData);
		layout.setProgressData(m_progressData);
		layout.setTestTimeLimit(m_nTestLimitTime);
		layout.setCurrentProblemCount(m_nCurrentTestProblemCount);
		layout.setTrueFalseWordCount(m_nTrueResCount, m_nFalseResCount);
		layout.setCurrentProblemIndex(m_nTestCurrentProblemIndex);
		layout.setIs_successProblem(is_successProblem);
		layout.setIsTestOrder(m_bIsTestOrder);
		layout.setTotalProblemCount(m_nTotalTestProblemCount);
		layout.setScheduleId(m_nScheduleId);
	}
	
	private void initWordData(TestLayout layout) {
		ArrayList<WordData> m_arrAllWordData_ = new ArrayList<WordData>();
		m_arrAllWordData_ = layout.initWordData();
		this.m_arrAllWordData=m_arrAllWordData_;
	}

	
	//ì‹œí—˜ì¤‘ì§€ëŒ€í™”ì°½ í˜„ì‹œ
	private void showTestStopDlg() {
		/*new AlertDialog.Builder(this)
		.setTitle(getString(R.string.test_stop))
		.setCancelable(false)
		.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which) {	
				setTestPause(false);
				disableAllTestLayout();
				
				m_nTestPhase = -1;
				m_layoutFirstStart.setVisibility(View.VISIBLE);
				
				m_nTrueResCount = 0;	//í•©ê²©í•œ ê°œìˆ˜
				m_nFalseResCount = 0;	//í‹€ë¦° ê°œìˆ˜					
				m_nCurrentTestProblemCount = 0;	//ë¬¸ì œì œì‹œìˆ˜
			}
		})
		.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which) {
				setTestPause(false);
				dialog.cancel();
			}
		})
		.show();*/
		SekWordPopup popup;

		popup = new SekWordPopup(StudyTestActivity.this);
		popup.setMessage(getString(R.string.study_back));
		popup.setListener(new OnSEKLaunchListener() {

			@Override
			public void onOK() {
				setTestPause(false);
				disableAllTestLayout();
				
				m_nTestPhase = -1;
				//m_layoutFirstStart.setVisibility(View.VISIBLE);
				
				m_nTrueResCount = 0;	//í•©ê²©í•œ ê°œìˆ˜
				m_nFalseResCount = 0;	//í‹€ë¦° ê°œìˆ˜					
				m_nCurrentTestProblemCount = 0;	//ë¬¸ì œì œì‹œìˆ˜
				finish();
			}

			@Override
			public void onCancel() {
				// TODO Auto-generated method stub
				setTestPause(false);
			}

		});
		
		popup.show();


	}
	
	private void setTestPause(boolean bPause) {
		switch(m_nTestPhase) {
		case 0:
			m_layoutVideoToTextTest.setTestPause(bPause);
			break;
		case 1:
			m_layoutTextToVideoTest.setTestPause(bPause);
			break;
		case 2:
			m_layoutSpeakingTest.setTestPause(bPause);
			break;
		case 3:
			m_layoutListeningTest.setTestPause(bPause);
			break;
		}
	}

	//ì�¼ì •ë³´ê¸°í™”ë©´ìœ¼ë¡œ ì�´í–‰
	private void sendScheduleInfo() {
		int epiNum = m_nScheduleId/9;
		int stgNum = m_nScheduleId%9;

		Intent intent = new Intent(Define.SCHEDULE_VIEW_ACTION);
		intent.putExtra(Define.STUDY_EPISODE, epiNum);
		intent.putExtra(Define.STUDY_STAGE, stgNum);

		sendBroadcast(intent);
	}
	
	private void sendStudyComplete() {
		Intent intent = new Intent(Define.USER_ORDER_ACTION);
		this.sendBroadcast(intent);
	}
}
