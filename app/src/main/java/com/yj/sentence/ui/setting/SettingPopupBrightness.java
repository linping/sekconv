package com.yj.sentence.ui.setting;

import com.yj.sentence.R;
import com.yj.sentence.database.SettingDBManager;
import com.yj.sentence.utils.ESUtils;

import android.app.Dialog;
import android.content.Context;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.SeekBar;


public class SettingPopupBrightness extends Dialog implements View.OnClickListener, View.OnKeyListener {
	
	private Context mContext;
	private Button mBtnOk;
	private Button mBtnCancel;
	private SeekBar mSeekbar;

	private SettingDBManager mDbMana = null;
	private OnOkListener mListener;

	int nBrightness;
	int nCurProgress;

	public SettingPopupBrightness(Context context) {
		super(context, R.style.MyDialog);
		
		// TODO Auto-generated constructor stub
		this.mContext = context;
		initViews();
	}
	
	public void initValues() {
		this.openDB();
		nBrightness = android.provider.Settings.System.getInt(mContext.getContentResolver(), android.provider.Settings.System.SCREEN_BRIGHTNESS,-1);
		mSeekbar.setProgress(nBrightness);
	}
	
	private void initViews() {
		View localView = LayoutInflater.from(this.mContext).inflate(R.layout.setting_popup_brightness, null);
		this.setContentView(localView);
		

		mBtnOk = (Button)localView.findViewById(R.id.btnSettingOk);
		mBtnCancel = (Button)localView.findViewById(R.id.btnSettingCancel);		

		mBtnOk.setOnClickListener(this);
		mBtnOk.setOnClickListener(this);
		mBtnCancel.setOnClickListener(this);

		mSeekbar = (SeekBar) localView.findViewById(R.id.seekBar);
		mSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				nCurProgress = progress;
				android.provider.Settings.System.putInt(mContext.getContentResolver(), android.provider.Settings.System.SCREEN_BRIGHTNESS,nCurProgress);
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {

			}

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {

			}
		});
	}

	
	@Override
	public void dismiss() {
		this.closeDB();		
	    super.dismiss();
	}

	@Override
	public boolean onKey(View arg0, int arg1, KeyEvent arg2) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

		ESUtils.playBtnSound(mContext);
		switch (v.getId()) {
			case R.id.btnSettingOk:
				
				mListener.onOK();
				dismiss();
				break;
			case R.id.btnSettingCancel:
				android.provider.Settings.System.putInt(mContext.getContentResolver(), android.provider.Settings.System.SCREEN_BRIGHTNESS,nBrightness);

				dismiss();
				break;
		}
	}
	
	private boolean openDB() {
		if (mDbMana == null)
			mDbMana = new SettingDBManager(mContext);
		
		if (mDbMana == null)
			return false;
		
		return true;
	}
	
	private void closeDB() {
		if (mDbMana != null) {
			mDbMana.close();
			mDbMana = null;
		}
	}
	
	public void setOkListener(OnOkListener listener) {
		mListener = listener;
	}
	
	public abstract interface OnOkListener {
		public abstract void onOK();
	}

}
