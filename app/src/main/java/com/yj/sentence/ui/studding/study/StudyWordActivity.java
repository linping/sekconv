package com.yj.sentence.ui.studding.study;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.media.SoundPool;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;

import com.afollestad.easyvideoplayer.EasyVideoCallback;
import com.afollestad.easyvideoplayer.EasyVideoPlayer;
import com.yj.sentence.R;
import com.yj.sentence.common.Define;
import com.yj.sentence.database.PAKDBManager;
import com.yj.sentence.database.ProgressDBManager;
import com.yj.sentence.database.SettingDBManager;
import com.yj.sentence.database.StudyDBManager;
import com.yj.sentence.database.WordpadDBManager;
import com.yj.sentence.structs.ProgressData;
import com.yj.sentence.structs.WordData;
import com.yj.sentence.ui.studding.SekWordPopup;
import com.yj.sentence.ui.studding.SekWordPopup.OnSEKLaunchListener;
import com.yj.sentence.utils.ESUtils;

import java.util.ArrayList;
import java.util.HashMap;

public class StudyWordActivity extends PageActivity {

//	private StudyPageCurlView m_vwStudyWord = null;
	private CurlView mCurlView = null;
	
	private FrameLayout FL_StudyWord;

	private Button m_btnPrev;
	private Button m_btnPause;
	private Button m_btnNext;
	
	private int m_nUserId = -1;	
	private int m_nScheduleId = -1;
	
	private int m_nOrder = 0;
	private int m_nTargetStudyWordCount = 0;
	
	private int m_nWordBgColorEn = 0;
	private int m_nWordBgColorKr = 0;
	
	private int[] m_nStudyRepeat = new int[2];
	private int m_nStudySpeed = 0;
	
	private long m_nStartStudyTime = 0;
	
	private StudyDBManager m_studyDbMana = null;
	private WordpadDBManager m_wordpadDbMana = null;
	private SettingDBManager mSettingDbMana = null;
	private ProgressDBManager mProgressDBMana = null;
	private PAKDBManager m_pakManager = null;

	private ArrayList<WordData> m_studyWordData = new ArrayList<WordData>();
	
	private EasyVideoPlayer easyPlayer;
	private FrameLayout frameMask;

	private ImageButton btnIndiator[] = new ImageButton[8];
	private int nTotalCount = 0;

    private static StudyWordActivity mThis;

    @Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_study);

        mThis = this;
		Intent intent = this.getIntent();
		m_nUserId = intent.getIntExtra(Define.USER_ID, -1);
		m_nScheduleId = intent.getIntExtra(Define.SCHEDULE_ID, -1);
		
		if(!isConnectDB()) {
			finish();
			return;
		}

		initView();

		getStudySettingInfo();
		setStudySettingInfo();
		setStudyWordData();
		setStudyListener();

		new Handler() {
			public void handleMessage(Message msg) {
				startStudy();
			}
		}.sendEmptyMessageDelayed(0, 200);

		FL_StudyWord = (FrameLayout) findViewById(R.id.FL_StudyWord);
		
		// This is something somewhat experimental. Before uncommenting next
		// line, please see method comments in CurlView.
		// mCurlView.setEnableTouchPressure(true);
	}

	public static Bitmap overlay(Bitmap bmp1, Bitmap bmp2) {
	    Bitmap bmOverlay = Bitmap.createBitmap(bmp1.getWidth(), bmp1.getHeight(), bmp1.getConfig());
	    Canvas canvas = new Canvas(bmOverlay);
	    canvas.drawBitmap(bmp1, new Matrix(), null);
	    canvas.drawBitmap(bmp2, 0, 0, null);
	    return bmOverlay;
	}

	@Override
	public void onPause() {
		super.onPause();
		mCurlView.onPause();
	}

	@Override
	public void onResume() {
		super.onResume();
		mCurlView.onResume();
	}

	@Override
	public Object onRetainNonConfigurationInstance() {
		return mCurlView.getCurrentIndex();
	}

	/**
	 * Bitmap provider.
	 */
	private class PageProvider implements CurlView.PageProvider {
		// Bitmap resources.

		private int[] mBitmapIds = { R.drawable.st_study_back_main, R.drawable.st_study_back_main,
				R.drawable.st_study_back_main, R.drawable.st_study_back_main };

		@Override
		public int getPageCount() {
			return 2*m_nTargetStudyWordCount*m_nStudyRepeat[0]*m_nStudyRepeat[1];
		}

		private Bitmap loadBitmap(int width, int height, int index) {
			Bitmap b = Bitmap.createBitmap(width, height,
					Bitmap.Config.ARGB_8888);
			b.eraseColor(0xFFFFFFFF);
			Canvas c = new Canvas(b);

			Drawable d = getResources().getDrawable(mBitmapIds[index]);

			int margin = 0;
			int border = 1;
			Rect r = new Rect(margin, margin, width - margin, height - margin);

			int imageWidth = r.width() - (border * 2);
			int imageHeight = r.height() - (border * 2);
			//int imageHeight = imageWidth * d.getIntrinsicHeight() / d.getIntrinsicWidth();
			/*if (imageHeight > r.height() - (border * 2)) {
				imageHeight = r.height() - (border * 2);
				imageWidth = imageHeight * d.getIntrinsicWidth()
						/ d.getIntrinsicHeight();
			}*/

			r.left += ((r.width() - imageWidth) / 2) - border;
			r.right = r.left + imageWidth + border + border;
			r.top += ((r.height() - imageHeight) / 2) - border;
			r.bottom = r.top + imageHeight + border + border;

			Paint p = new Paint();
			p.setColor(0xFFC0C0C0);
			c.drawRect(r, p);
			r.left += border;
			r.right -= border;
			r.top += border;
			r.bottom -= border;

			d.setBounds(r);
			d.draw(c);

			return b;
		}
		
		private Bitmap loadBitmap1(int width, int height, int index) {
			Bitmap b = Bitmap.createBitmap(width, height,
					Bitmap.Config.ARGB_8888);
			b.eraseColor(0xFFFFFFFF);
			Canvas c = new Canvas(b);

			Drawable d = getResources().getDrawable(mBitmapIds[index]);

			int margin = 0;
			int border = 1;
			Rect r = new Rect(margin, margin, width - margin, height - margin);

			int imageWidth = r.width() - (border * 2);
			int imageHeight = r.height() - (border * 2);
			//int imageHeight = imageWidth * d.getIntrinsicHeight() / d.getIntrinsicWidth();
			/*if (imageHeight > r.height() - (border * 2)) {
				imageHeight = r.height() - (border * 2);
				imageWidth = imageHeight * d.getIntrinsicWidth()
						/ d.getIntrinsicHeight();
			}*/

			r.left += ((r.width() - imageWidth) / 2) - border;
			r.right = r.left + imageWidth + border + border;
			r.top += ((r.height() - imageHeight) / 2) - border;
			r.bottom = r.top + imageHeight + border + border;

			Paint p = new Paint();
			p.setColor(0xFFC0C0C0);
			c.drawRect(r, p);
			r.left += border;
			r.right -= border;
			r.top += border;
			r.bottom -= border;

			d.setBounds(r);
			d.draw(c);
			
			Paint paint = new Paint();
			paint.setColor(Color.BLACK);
			paint.setTextSize(30);
			c.drawText("abcdefghi", 1020.0f, 670.0f, paint);

			return b;
		}


		@Override
		public void updatePage(CurlPage page, int width, int height, int index) {

//			Bitmap front = loadBitmap(width, height, 0);//			
//			FL_StudyWord.setDrawingCacheEnabled(true);
//			FL_StudyWord.buildDrawingCache();
//			Bitmap front2 = FL_StudyWord.getDrawingCache();
//			Bitmap front = overlay(front1, front2);			
			
			Bitmap front = loadBitmap(width, height, 0);

			/*if (index == 0)
			{
				front = loadBitmap(width, height, 0);
			}else
			{
				front = loadBitmapFromView(mCurlView);
			}*/
			//front = loadBitmapFromView(FL_StudyWord);
			
			page.setTexture(front, CurlPage.SIDE_BOTH);
			page.setColor(Color.argb(127, 255, 255, 255), CurlPage.SIDE_BACK);
		}
		
		@Override
		public void updatePage1(CurlPage page, int width, int height, int index) {

			Bitmap front = loadBitmap1(width, height, 0);

			page.setTexture(front, CurlPage.SIDE_BOTH);
			page.setColor(Color.argb(127, 255, 255, 255),
					CurlPage.SIDE_BACK);
			
		}

	}
	
	public static Bitmap loadBitmapFromView(View v) {
		if (v.getMeasuredHeight() <= 0) {
		    v.measure(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		    Bitmap b = Bitmap.createBitmap(v.getMeasuredWidth(), v.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
		    Canvas c = new Canvas(b);
		    v.layout(0, 0, v.getMeasuredWidth(), v.getMeasuredHeight());
		    v.draw(c);
		    return b;
		}else {
			Bitmap b = Bitmap.createBitmap( v.getLayoutParams().width, v.getLayoutParams().height, Bitmap.Config.ARGB_8888);
		    Canvas c = new Canvas(b);
		    v.layout(v.getLeft(), v.getTop(), v.getRight(), v.getBottom());
		    v.draw(c);
		    return b;
		}
	}
	
	/**
	 * CurlView size changed observer.
	 */
	private class SizeChangedObserver implements CurlView.SizeChangedObserver {
		@Override
		public void onSizeChanged(int w, int h) {
//			if (w > h) {
//				mCurlView.setViewMode(CurlView.SHOW_TWO_PAGES);
//				mCurlView.setMargins(.1f, .05f, .1f, .05f);
//			} else {
				mCurlView.setViewMode(CurlView.SHOW_ONE_PAGE);
				mCurlView.setMargins(0.0f, 0.0f, 0.0f, 0.0f);
//			}
		}
	}


	private void initView() {
//		m_vwStudyWord = (StudyPageCurlView)findViewById(R.id.vwStudyWord);
//		m_vwStudyWord.setPageBitmap(R.drawable.study_paper_bg1,R.drawable.study_paper_bg1);
//		m_vwStudyWord.SetCurlSpeed(180);
//		m_vwStudyWord.SetInitialEdgeOffset(0);
//		m_vwStudyWord.SetUpdateRate(5);
//		page_curl = (StudyCurlLayout) findViewById(R.id.vwStudyWord_curl);

		int index = 0;
		if (getLastNonConfigurationInstance() != null) {
			index = (Integer) getLastNonConfigurationInstance();
		}
		mCurlView = (CurlView) findViewById(R.id.curl_vwStudyWord);
		mCurlView.setPageProvider(new PageProvider());
		mCurlView.setSizeChangedObserver(new SizeChangedObserver());
		mCurlView.setCurrentIndex(index);
		mCurlView.setBackgroundColor(0xFFFFFFFF);
		mCurlView.setParent(this);

		//vView = (VideoView) findViewById(R.id.videoView);
		//vView.setVideoPath(Define.TEMP_PATH);
		//vView.start();

		easyPlayer = (EasyVideoPlayer) findViewById(R.id.easyPlayer);
		//easyPlayer.setSource(Uri.parse(Define.TEMP_PATH));
		easyPlayer.setCallback(callback);
		easyPlayer.disableControls();
		easyPlayer.setAutoPlay(true);


		frameMask = (FrameLayout) findViewById(R.id.frameMask);

		m_btnPrev = (Button)findViewById(R.id.btnPrevWord);
		m_btnPrev.setOnClickListener(m_clickListener);
		
		m_btnPause = (Button)findViewById(R.id.btnPause);
		m_btnPause.setOnClickListener(m_clickListener);
		
		m_btnNext = (Button)findViewById(R.id.btnNextWord);
		m_btnNext.setOnClickListener(m_clickListener);

		ImageButton ibtn0 = (ImageButton)findViewById(R.id.imageIndicate1);
		ImageButton ibtn1 = (ImageButton)findViewById(R.id.imageIndicate2);
		ImageButton ibtn2 = (ImageButton)findViewById(R.id.imageIndicate3);
		ImageButton ibtn3 = (ImageButton)findViewById(R.id.imageIndicate4);
		ImageButton ibtn4 = (ImageButton)findViewById(R.id.imageIndicate5);
		ImageButton ibtn5 = (ImageButton)findViewById(R.id.imageIndicate6);
		ImageButton ibtn6 = (ImageButton)findViewById(R.id.imageIndicate7);
		ImageButton ibtn7 = (ImageButton)findViewById(R.id.imageIndicate8);

		btnIndiator[0] = ibtn0;
		btnIndiator[1] = ibtn1;
		btnIndiator[2] = ibtn2;
		btnIndiator[3] = ibtn3;
		btnIndiator[4] = ibtn4;
		btnIndiator[5] = ibtn5;
		btnIndiator[6] = ibtn6;
		btnIndiator[7] = ibtn7;
	}
	
	private void showBackDlg(){
		
		mCurlView.doPause();

		SekWordPopup popup;

		popup = new SekWordPopup(StudyWordActivity.this);
		popup.setMessage(this.getString(R.string.study_back));
		popup.setListener(new OnSEKLaunchListener() {

			@Override
			public void onOK() {
				StudyWordActivity.this.finish();
			}

			@Override
			public void onCancel() {
				// TODO Auto-generated method stub
				mCurlView.doPause();
			}
		});
		
		popup.show();
	}
	private View.OnClickListener m_clickListener = new View.OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			ESUtils.playBtnSound(StudyWordActivity.this);
			switch(v.getId()) {
			case R.id.btnBack:
				//StudyWordActivity.this.finish();
				showBackDlg();
				break;
			case R.id.btnPrevWord:
				doPrevPerform();
				break;
			case R.id.btnNextWord:
				doNextPerform();
				break;
			case R.id.btnPause:
				doPausePeform();				
				break;
			}
		}
	};
	
	private void doPrevPerform() {
		
		/*if(mCurlView.getFlippingState())
			return;
		*/
		if(mCurlView.getCurrentWordCount() == 0) 
			return;
		
		sendMouseEventPrev();
		m_btnPause.setBackgroundResource(R.drawable.btn_study_play_pause);
	}
	
	private void doNextPerform() {
		/*
		if(mCurlView.getFlippingState())
			return;
		*/
		int nWordCount = mCurlView.getCurrentWordCount();
		int nRound = mCurlView.getCurrentRound();
		int nPhase = mCurlView.getCurrentPhase();
		
		if(nWordCount == m_studyWordData.size()-1 && nRound == m_nStudyRepeat[nPhase-1]-1 && nPhase == 2)
			return;

		sendMouseEventNext();
		m_btnPause.setBackgroundResource(R.drawable.btn_study_play_pause);
	}
	
	private void doPausePeform() {
		if(mCurlView.getFlippingState())
			return;
		
		mCurlView.doPause();
		pauseVideo();
		if(mCurlView.isPause()) 
			m_btnPause.setBackgroundResource(R.drawable.btn_study_play_play);
		else
			m_btnPause.setBackgroundResource(R.drawable.btn_study_play_pause);
	}
	
	//DB
	private boolean isConnectDB() {
		m_studyDbMana = new StudyDBManager(this, m_nUserId);
		if(m_studyDbMana == null)
			return false;
			
		mProgressDBMana = new ProgressDBManager(this, m_nUserId);
		if(mProgressDBMana == null)
			return false;
		
		m_wordpadDbMana = new WordpadDBManager(this, m_nUserId);
		if(m_wordpadDbMana == null)
			return false;
		
		mSettingDbMana = new SettingDBManager(this);
		if(mSettingDbMana == null)
			return false;

		m_pakManager = new PAKDBManager(this);
		if(m_pakManager == null)
			return false;

		return true;
	}

	private void closeDB() {
		if (m_studyDbMana != null) {
			m_studyDbMana.close();
			m_studyDbMana = null;
		}
	}
	
	public void saveStudyTimeToDB() {
		long nEndStudyTime = ESUtils.getNowTimeSeconds();
		int nDiff = (int)(nEndStudyTime - m_nStartStudyTime);
		m_nStartStudyTime = nEndStudyTime;

		long nCurTime = ESUtils.getNowDateMilliseconds();
		int nYear = ESUtils.getDateOfYear(nCurTime);
		int nMonth = ESUtils.getDateOfMonth(nCurTime);
		int nDate = ESUtils.getDateOfDay(nCurTime);
		long nRegTime = ESUtils.getDateMilliseconds(nYear, nMonth - 1 , nDate);

		mProgressDBMana.updateProgressTime(nRegTime, nDiff);

		
		/*if(m_scheduleData != null && m_studyDbMana != null) {
			m_scheduleData.setTimes(nDiff);
			m_studyDbMana.setTimes(m_scheduleData);
		}*/
	}
	
	private void saveStudyWord() {
		WordData wd = mCurlView.getCurrentWord();
		if(wd == null)
			return;
		
		m_wordpadDbMana.InsertWordpad(Define.KIND_IMPORT_WORD,	wd.getId());
	}
	
	private void getStudySettingInfo() {
		m_nStudyRepeat[0] = mSettingDbMana.getStudy1Value();
		m_nStudyRepeat[1] = mSettingDbMana.getStudy1Value();
		
		m_nWordBgColorEn = mSettingDbMana.getWordColorValue();
		m_nWordBgColorKr = mSettingDbMana.getWordColorValue();
		
		m_nStudySpeed = mSettingDbMana.getStudySpeedIndex();
	}
	
	private void setStudySettingInfo() {
		mCurlView.setStudyRepeatCount(m_nStudyRepeat);
		mCurlView.setStudySpeed(m_nStudySpeed);
		mCurlView.setWordBgColor(m_nWordBgColorEn, m_nWordBgColorKr);
	}
	
	private void setStudyWordData() {
		//m_scheduleData = m_progressDbMana.getScheduleDateData(m_nScheduleId);
		//m_nOrder = m_progressDbMana.getLevel(m_nScheduleId);
		//m_progressDbMana.createScheduleWord(m_scheduleData.getId());
		//m_studyWordData = m_studyDbMana.getWordList(m_scheduleData);

		m_nOrder = mProgressDBMana.getLevel();

		int epinum = m_nScheduleId / 9;
		int stgnum = m_nScheduleId % 9;

		ProgressData data = new ProgressData(m_nUserId, m_nOrder, epinum, stgnum);

		m_studyWordData = m_studyDbMana.getWordList( data);
		m_nTargetStudyWordCount = m_studyWordData.size();
		
		mCurlView.setWordData(m_studyWordData);
		mCurlView.setStudyOrderAndWordCount(m_nOrder, m_nTargetStudyWordCount);

		nTotalCount = m_studyWordData.size();
		for(int i = nTotalCount ; i < 8; i++){
			btnIndiator[i].setVisibility(View.GONE);
		}
	}
	
	private void startStudy() {
		getStudyStartTime();
		mCurlView.startStudy();
	}
	
	private void getStudyStartTime() {
		m_nStartStudyTime = ESUtils.getNowTimeSeconds();
	}
	
	private void setStudyListener() {
		mCurlView.setListener(new CurlView.StudyWordListener() {
			
			@Override
			public void onRember(WordData wd) {
				// TODO Auto-generated method stub
				/*if(m_studyDbMana != null)
					m_studyDbMana.setWordComplete(m_scheduleData.getId(), wd.getId());*/
			}
			
			@Override
			public void onFinishAfter() {
				// TODO Auto-generated method stub
				goStudySentenceActivity();
				StudyWordActivity.this.finish();
			}
		});
	}

	public static void playWordVoice(String strVoice) {
		if(strVoice.contains("page")){
			ESUtils.playPageSound(mThis);
		}
		/*if(m_soundPool != null)
			m_soundPool.play(m_soundMap.get(strVoice).intValue(), m_fVolume, m_fVolume, 1, 0, 1.0F);
*/
		/*
        if(strVoice.contains("page")){

            MediaPlayer mediaPlayer=MediaPlayer.create(mThis,R.raw.page);
            mediaPlayer.start();
        }
        */

		//MediaPlayer mp = MediaPlayer.create(StudyWordActivity.this, R.raw.page);
	}

	public String getImageFileName(int nWordID){
		return m_pakManager.getFileName(nWordID, 0);
	}

	public String getAudioFileName(int nWordID){
		return m_pakManager.getFileName(nWordID, 1);
	}

	public String getVideoFileName(int nWordID){
		return m_pakManager.getFileName(nWordID, 2);
	}

	private void finishStudy() {
		
		mCurlView.finishStudy();
		saveStudyTimeToDB();
		closeDB();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		finishStudy();
		super.onDestroy();
	}
	
	private void goStudySentenceActivity() {
		Intent intent = new Intent(this, StudySentenceActivity.class);
		intent.putExtra(Define.USER_ID, m_nUserId);
		intent.putExtra(Define.SCHEDULE_ID, m_nScheduleId);
		startActivity(intent);
	}
	
	private void sendMouseEventPrev() {	
		
		/*MotionEvent event = null;
			
		event = MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_DOWN, 10f, 660f, 1);
		mCurlView.dispatchTouchEvent(event);

		for (int i = 0; i < 19; i++) {
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			event = MotionEvent.obtain(SystemClock.uptimeMillis()+15*i, SystemClock.uptimeMillis(), MotionEvent.ACTION_MOVE, i*60, 700f-i*34f, 1);
			mCurlView.dispatchTouchEvent(event);
		}
		
		try {
			Thread.sleep(10);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}                                                                                                                    
		event = MotionEvent.obtain(SystemClock.uptimeMillis()+15*20, SystemClock.uptimeMillis(), MotionEvent.ACTION_UP,1250f, 10f, 1);
		mCurlView.dispatchTouchEvent(event);*/

		runOnUiThread(new Runnable() {
			public void run() {
				MotionEvent event = null;

				event = MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_DOWN, 10f, 660f, 1);
				mCurlView.dispatchTouchEvent(event);


				/*for (int i = 0; i < 19; i++) {
					try {
						Thread.sleep(10);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					event = MotionEvent.obtain(SystemClock.uptimeMillis()+15*i, SystemClock.uptimeMillis(), MotionEvent.ACTION_MOVE,(1230- i*60), 700-i*34f, 1);
					mCurlView.dispatchTouchEvent(event);
				}

				try {
					Thread.sleep(10);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				event = MotionEvent.obtain(SystemClock.uptimeMillis()+15*20, SystemClock.uptimeMillis(), MotionEvent.ACTION_UP,10, 10, 1);
				mCurlView.dispatchTouchEvent(event);*/
			}
		});
	}
	

	private void sendMouseEventNext() {

		
		runOnUiThread(new Runnable() {
		    public void run() {
		    	MotionEvent event = null;
				
				event = MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_DOWN, 1230, 700f, 1);
				mCurlView.dispatchTouchEvent(event);

				/*for (int i = 0; i < 19; i++) {
					try {
						Thread.sleep(10);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}                                                                                                                    
					event = MotionEvent.obtain(SystemClock.uptimeMillis()+15*i, SystemClock.uptimeMillis(), MotionEvent.ACTION_MOVE,(1230- i*60), 700-i*34f, 1);
					mCurlView.dispatchTouchEvent(event);
				}
				
				try {
					Thread.sleep(10);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}                                                                                                                    
				event = MotionEvent.obtain(SystemClock.uptimeMillis()+15*20, SystemClock.uptimeMillis(), MotionEvent.ACTION_UP,10, 10, 1);
				mCurlView.dispatchTouchEvent(event);*/
		    }
		});
	}

	public void playVideo(String url){
		/*vView.setVideoPath(url);
		vView.start();
		vView.setVisibility(View.VISIBLE);*/
		easyPlayer.setVisibility(View.VISIBLE);
		frameMask.setVisibility(View.VISIBLE);
		easyPlayer.stop();
		easyPlayer.setSource(Uri.parse(url));
		easyPlayer.setLoop(true);
	}

	public void pauseVideo(){
		//vView.pause();
		if(easyPlayer.isPlaying()){
			easyPlayer.pause();
		}else {
			easyPlayer.start();
		}
	}



	public void stopVideo(){
		/*vView.setVisibility(View.GONE);
		vView.stopPlayback();*/
		easyPlayer.stop();
		easyPlayer.setVisibility(View.INVISIBLE);
		frameMask.setVisibility(View.INVISIBLE);
	}

	EasyVideoCallback callback = new EasyVideoCallback() {
		@Override
		public void onStarted(EasyVideoPlayer player) {
			//easyPlayer.setVisibility(View.VISIBLE);

		}

		@Override
		public void onPaused(EasyVideoPlayer player) {

		}

		@Override
		public void onPreparing(EasyVideoPlayer player) {

		}

		@Override
		public void onPrepared(EasyVideoPlayer player) {
			//player.start();
			Handler mainHandler = new Handler(StudyWordActivity.this.getMainLooper());

			Runnable myRunnable = new Runnable() {
				@Override
				public void run() {
					frameMask.setVisibility(View.INVISIBLE);

				} // This is your code
			};
			mainHandler.postDelayed(myRunnable,200);

		}

		@Override
		public void onBuffering(int percent) {

		}

		@Override
		public void onError(EasyVideoPlayer player, Exception e) {

		}

		@Override
		public void onCompletion(EasyVideoPlayer player) {

		}

		@Override
		public void onRetry(EasyVideoPlayer player, Uri source) {

		}

		@Override
		public void onSubmit(EasyVideoPlayer player, Uri source) {

		}
	};

	public void setIndicator(int index){
		for(int i =0; i<nTotalCount; i++){
			btnIndiator[i].setImageResource(R.drawable.st_study_circle_gray);
		}

		btnIndiator[index].setImageResource(R.drawable.st_study_circle_red);
	}

	@Override
	public void onBackPressed() {
		showBackDlg();
	}
}
