package com.yj.sentence.ui.words;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.yj.sentence.R;
import com.yj.sentence.common.Define;
import com.yj.sentence.structs.WordData;
import com.yj.sentence.structs.WordpadData;
import com.yj.sentence.ui.studding.study.StudyPrepareActivity;
import com.yj.sentence.utils.ESUtils;

/**
 * Created by achilles on 12/29/16.
 */

public class SentenceView extends FrameLayout {

    private Context m_Context;
    private int m_nUserId;
    private int m_nWordId;

    private Handler m_handler;

    private Button btnDelete;
    private ImageView imageCheck;
    private TextView textEnglish;
    private TextView textKorean;
    private boolean bChecked = false;
    private boolean bCheckedAll = false;
    private int mWPID;

    public SentenceView(Context context) {
        super(context);
        m_Context = context;
        initViews();
    }

    public void setHandler(Handler handler){
        m_handler = handler;
    }

    private void initViews(){
        View v = LayoutInflater.from(m_Context).inflate(R.layout.sentence_item, null);

        textEnglish = (TextView) v.findViewById(R.id.textEnglish);
        textKorean = (TextView) v.findViewById(R.id.textKorean);

        ESUtils.setTextViewTypeFaceByRes(m_Context, textEnglish, Define.getEnglishFont());
        ESUtils.setTextViewTypeFaceByRes(m_Context, textKorean, Define.getMainFont());

        btnDelete = (Button) v.findViewById(R.id.btnDelete);
        //btnCheck = (Button) v.findViewById(R.id.btnCheck);
        imageCheck = (ImageView) v.findViewById(R.id.imageCheck);
        btnDelete.setVisibility(View.INVISIBLE);

        //btnDelete.setOnClickListener(m_clickListener);
        //btnCheck.setOnClickListener(m_clickListener);
        this.addView(v);

    }

    public void setUserId(int nUserId){
        m_nUserId = nUserId;
    }

    public void setWordId(int nWordId){
        m_nWordId = nWordId;
    }

    public void setWordpadData(WordpadData wpData){
        WordData wData = wpData.getWord();

        mWPID = wpData.getId();

        String strEnglish = wData.getMeanEn();
        String strKorean = wData.getMeanKr();
        textEnglish.setText(strEnglish);
        textKorean.setText(strKorean);
    }

    public void setSelected(boolean bSelect){
        if(bSelect){
            bChecked = true;
            bCheckedAll = true;
            imageCheck.setBackgroundResource(R.drawable.st_registry_item_checked);
            //btnDelete.setVisibility(View.INVISIBLE);
        } else {
            bChecked = false;
            bCheckedAll = false;
            imageCheck.setBackgroundResource(R.drawable.st_registry_item_unchecked);
            //btnDelete.setVisibility(View.INVISIBLE);
        }
    }

    private OnClickListener m_clickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            ESUtils.playBtnSound(m_Context);
        }
    };


}
