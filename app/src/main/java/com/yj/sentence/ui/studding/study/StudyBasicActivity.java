package com.yj.sentence.ui.studding.study;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;

import com.afollestad.easyvideoplayer.EasyVideoCallback;
import com.afollestad.easyvideoplayer.EasyVideoPlayer;
import com.yj.sentence.R;
import com.yj.sentence.common.Define;
import com.yj.sentence.database.PAKDBManager;
import com.yj.sentence.database.ProgressDBManager;
import com.yj.sentence.database.StudyDBManager;
import com.yj.sentence.structs.StageData;
import com.yj.sentence.structs.WordData;
import com.yj.sentence.ui.studding.SekWordPopup;
import com.yj.sentence.ui.studding.test.TestSelectActivity;
import com.yj.sentence.utils.ESUtils;

import java.io.File;
import java.util.Random;

public class StudyBasicActivity extends Activity implements View.OnClickListener {

    private ProgressDBManager mProgressManager;
    private StudyDBManager mStudyManager;
    private PAKDBManager m_pakManager = null;

    private EasyVideoPlayer easyPlayer;
    private FrameLayout framePlayerMask;

    private ImageButton btnBack;
    private ImageButton btnStart;

    private TextView textLevel;
    private TextView textEpisode;
    private TextView textStage;
    private TextView textTitle;
    private TextView textEnglish;
    private TextView textKorean;

    private int m_nUserID;
    private int m_nParam;

    private WordData wData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_study_basic);

        Intent intent = this.getIntent();
        m_nUserID = intent.getIntExtra(Define.USER_ID, -1);
        m_nParam = intent.getIntExtra(Define.SCHEDULE_ID, -1);

        CreateTemp();

        if(!openDB()){
            finish();
            return;
        }

        initViews();
    }

    private void CreateTemp(){
        File dir = new File(Define.TMP_PATH);

        if(dir.exists()){
            if (dir.isDirectory())
            {
                String[] children = dir.list();
                for (int i = 0; i < children.length; i++)
                {
                    new File(dir, children[i]).delete();
                }
            }
        }else{
            dir.mkdirs();
        }
    }

    private boolean openDB() {
        mProgressManager = new ProgressDBManager(this, m_nUserID);
        if(mProgressManager == null)
            return false;

        mStudyManager = new StudyDBManager(this, m_nUserID);
        if(mStudyManager == null)
            return false;

        m_pakManager = new PAKDBManager(this);
        if(m_pakManager == null)
            return false;

        return true;
    }

    private void initViews(){
        textLevel = (TextView) findViewById(R.id.textLevel);
        textEpisode = (TextView) findViewById(R.id.textEpisode);
        textStage = (TextView) findViewById(R.id.textStage);
        textTitle = (TextView) findViewById(R.id.textTitle);
        textEnglish = (TextView) findViewById(R.id.textEnglish);
        textKorean = (TextView) findViewById(R.id.textKorean);

        ESUtils.setTextViewTypeFaceByRes(this, textLevel, Define.getMainFont());
        ESUtils.setTextViewTypeFaceByRes(this, textEpisode, Define.getMainFont());
        ESUtils.setTextViewTypeFaceByRes(this, textStage, Define.getMainFont());
        ESUtils.setTextViewTypeFaceByRes(this, textTitle, Define.getMainFont());
        ESUtils.setTextViewTypeFaceByRes(this, textEnglish, Define.getEnglishFont());
        ESUtils.setTextViewTypeFaceByRes(this, textKorean, Define.getMainFont());
        ESUtils.setTextViewTypeFaceByRes(this, (TextView) findViewById(R.id.textSymbol), Define.getMainFont());

        easyPlayer = (EasyVideoPlayer) findViewById(R.id.easyPlayer);
        framePlayerMask = (FrameLayout) findViewById(R.id.framePlayerMask);

        btnBack = (ImageButton) findViewById(R.id.btnBack);
        btnStart = (ImageButton) findViewById(R.id.btnStart);
        btnBack.setOnClickListener(this);
        btnStart.setOnClickListener(this);

        //set information
        int userLevel = mProgressManager.getLevel();
        int epiNum = m_nParam/9;
        int stgNum = m_nParam%9;

        StageData stgData = mProgressManager.getStageData(userLevel, epiNum, stgNum);
        String stgname = stgData.getStgname();

        String strLevel = String.format(getString(R.string.string_format_level), userLevel);
        String strEpisode = String.format(getString(R.string.string_format_episode), epiNum);
        String strStage = String.format(getString(R.string.string_format_stage), stgNum);

        textLevel.setText(strLevel);
        textEpisode.setText(strEpisode);
        textStage.setText(strStage);
        textTitle.setText(stgname);

        //set mword
        int nWordId = stgData.getmWord();
        wData = mStudyManager.getWordData(nWordId);

        //setEnglishData();

        textEnglish.setText(wData.getMeanEn());
        textKorean.setText(wData.getMeanKr());

        //String strVideoPath = Define.VIDEO_PATH + String.format("%d.mp4", nWordId );
        String strVideoPath = getVideoFileName( nWordId );

        /*mPlayer.setCallback(this);
        mPlayer.setSource(Uri.parse(Define.TEMP_PATH));
        mPlayer.disableControls();
        mPlayer.start();*/

        //vView.start();
        //vView.setVideoPath(Define.TEMP_PATH1);
        //vView.setVideoPath(strVideoPath);
        //vView.start();

        easyPlayer.setCallback(callback);
        easyPlayer.setSource(Uri.parse(strVideoPath));
        easyPlayer.disableControls();
        easyPlayer.setAutoPlay(true);
        easyPlayer.setLoop(true);

        //easyPlayer.setVisibility(View.INVISIBLE);
        //easyPlayer.setAutoPlay(true);

    }

    private void setEnglishData(){
        WordData data = wData;
        String strEn = data.getMeanEn();

        String[] strWords = strEn.split(" ");

        int nNormalBlanks = 0;

        if(strWords.length > 0 && strWords.length <= 3){
            nNormalBlanks = 1;
        }else if(strWords.length > 3 && strWords.length <= 7){
            nNormalBlanks = 2;
        }else if(strWords.length > 7){
            nNormalBlanks = 3;
        }

        String strResult = "";
        for(int i = 0; i < nNormalBlanks; i++){
            Random r = new Random();
            int i1 = r.nextInt(strWords.length);

            int nCount = strWords[i1].length();
            strWords[i1] ="";
            for(int j = 0; j < nCount; j++){
                strWords[i1] += "_";
            }
        }

        for(int i = 0; i <strWords.length; i++){
            strResult += (strWords[i] + " ");
        }

        textEnglish.setText(strResult);

    }

    public String getVideoFileName(int nWordID){
        return m_pakManager.getFileName(nWordID, 2);
    }

    EasyVideoCallback callback = new EasyVideoCallback() {
        @Override
        public void onStarted(EasyVideoPlayer player) {
            //easyPlayer.setVisibility(View.VISIBLE);
        }

        @Override
        public void onPaused(EasyVideoPlayer player) {

        }

        @Override
        public void onPreparing(EasyVideoPlayer player) {

        }

        @Override
        public void onPrepared(EasyVideoPlayer player) {
            //player.start();
            Handler mainHandler = new Handler(StudyBasicActivity.this.getMainLooper());

            Runnable myRunnable = new Runnable() {
                @Override
                public void run() {
                    framePlayerMask.setVisibility(View.INVISIBLE);
                } // This is your code
            };
            mainHandler.post(myRunnable);

        }

        @Override
        public void onBuffering(int percent) {

        }

        @Override
        public void onError(EasyVideoPlayer player, Exception e) {

        }

        @Override
        public void onCompletion(EasyVideoPlayer player) {

        }

        @Override
        public void onRetry(EasyVideoPlayer player, Uri source) {

        }

        @Override
        public void onSubmit(EasyVideoPlayer player, Uri source) {

        }
    };

    @Override
    public void onClick(View v) {
        ESUtils.playBtnSound(this);

        switch (v.getId()){
            case R.id.btnBack:
                finishStudy();
                break;
            case R.id.btnStart:
                startStudy();
                break;
        }
    }

    private void startStudy(){
        /*Intent intent = new Intent(StudyPrepareActivity.this, StudyBasicActivity.class);
        intent.putExtra(Define.USER_ID, nUserId);
        intent.putExtra(Define.SCHEDULE_ID, nParam);
        startActivity(intent);*/

        Intent intent = new Intent(StudyBasicActivity.this, StudyWordActivity.class);
        intent.putExtra(Define.USER_ID, m_nUserID);
        intent.putExtra(Define.SCHEDULE_ID, m_nParam);
        startActivity(intent);
        finish();


/*
        Intent intent = new Intent(StudyBasicActivity.this, StudySentenceActivity.class);
        intent.putExtra(Define.USER_ID, m_nUserID);
        intent.putExtra(Define.SCHEDULE_ID, m_nParam);
        startActivity(intent);
        finish();
*/

/*
        Intent intent = new Intent(StudyBasicActivity.this, StudyPracticeResultActivity.class);
        intent.putExtra(Define.USER_ID, m_nUserID);
        intent.putExtra(Define.SCHEDULE_ID, m_nParam);
        startActivity(intent);
        finish();
*/

/*
        Intent intent = new Intent(StudyBasicActivity.this, StudyPracticeActivity.class);
        intent.putExtra(Define.USER_ID, m_nUserID);
        intent.putExtra(Define.SCHEDULE_ID, m_nParam);
        startActivity(intent);
        finish();
*/


/*
        Intent intent = new Intent(StudyBasicActivity.this, TestSelectActivity.class);
        intent.putExtra(Define.USER_ID, m_nUserID);
        intent.putExtra(Define.IS_ORDERTEST, false);
        intent.putExtra(Define.SCHEDULE_ID, m_nParam);
        startActivity(intent);
        finish();
*/

    }

    private void showBackDlg(){

        SekWordPopup popup;

        popup = new SekWordPopup(StudyBasicActivity.this);
        popup.setMessage(this.getString(R.string.study_back));
        popup.setListener(new SekWordPopup.OnSEKLaunchListener() {

            @Override
            public void onOK() {
                StudyBasicActivity.this.finish();
            }

            @Override
            public void onCancel() {
                // TODO Auto-generated method stub
            }
        });

        popup.show();
    }

    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        finishWorks();
        super.onDestroy();
    }


    private void finishWorks(){
        if(easyPlayer != null){
            easyPlayer.stop();
            easyPlayer.release();
            easyPlayer = null;
        }
    }

    private void finishStudy(){
        showBackDlg();
    }

    @Override
    public void onBackPressed() {
        // your code.
        finishStudy();

    }
}
