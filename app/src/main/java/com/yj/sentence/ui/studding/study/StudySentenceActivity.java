package com.yj.sentence.ui.studding.study;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.BounceInterpolator;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.easyvideoplayer.EasyVideoCallback;
import com.afollestad.easyvideoplayer.EasyVideoPlayer;

import com.yj.sentence.R;
import com.yj.sentence.common.Define;
import com.yj.sentence.database.PAKDBManager;
import com.yj.sentence.database.ProgressDBManager;
import com.yj.sentence.database.SettingDBManager;
import com.yj.sentence.database.StudyDBManager;
import com.yj.sentence.structs.ProgressData;
import com.yj.sentence.structs.StageData;
import com.yj.sentence.structs.WordData;
import com.yj.sentence.ui.studding.SekConvStudyPopup;
import com.yj.sentence.ui.studding.SekWordPopup;
import com.yj.sentence.ui.studding.audio.AudioDataReceivedListener;
import com.yj.sentence.ui.studding.audio.CheapSoundFile;
import com.yj.sentence.ui.studding.audio.FileWaveformView;
import com.yj.sentence.ui.studding.audio.RecognizeResultListener;
import com.yj.sentence.ui.studding.audio.RecordWaveformView;
import com.yj.sentence.ui.studding.audio.WavAudioRecorder;
import com.yj.sentence.ui.studding.test.TestSelectActivity;
import com.yj.sentence.ui.studding.util.EsStuddingRecognizer;
import com.yj.sentence.ui.studding.util.EsStuddingUtils;
import com.yj.sentence.utils.ESUtils;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Random;

import cafe.adriel.androidaudioconverter.AndroidAudioConverter;
import cafe.adriel.androidaudioconverter.callback.IConvertCallback;
import cafe.adriel.androidaudioconverter.callback.ILoadCallback;
import cafe.adriel.androidaudioconverter.model.AudioFormat;

public class StudySentenceActivity extends AppCompatActivity implements View.OnClickListener, MediaPlayer.OnCompletionListener, MediaPlayer.OnPreparedListener {

    private ProgressDBManager mProgressManager;
    private StudyDBManager mStudyManager;
    private SettingDBManager mSettingManager;
    private PAKDBManager m_pakManager = null;

    private FrameLayout frameMask;

    private EasyVideoPlayer easyPlayer;
    private FrameLayout framePlayerMask;

    private ImageButton btnBack;
    private ImageButton btnStart;
    private Button btnPrev;
    private Button btnPause;
    private Button btnNext;

    private TextView textEnglish;
    private TextView textKorean;

    private TextView textDescription;
    private ImageView imageStatus;
    private ImageView imageTalkDesc;
    private ImageView imageMark;


    private FrameLayout frameResult;
    private TextView textResultKior;
    private TextView textResultEng;
    private TextView textResult;

    private int m_nUserID;
    private int m_nParam;

    private ArrayList<WordData> arryWordData;

    private FrameLayout frameTeacher;
    private FrameLayout frameUser;
    private TextView textPercent;

    private FileWaveformView waveTeacher;
    private FileWaveformView waveUser;

    private ImageButton btnPlayTeacher;
    private ImageButton btnPlayUser;

    private RecordWaveformView mFormView;
    private ImageView imageMask;

    private Boolean m_bPlay = true;

    private ImageButton btnIndiator[] = new ImageButton[16];
    private int nTotalCount = 0;

    private Thread  m_threadStudy = null;
    private boolean m_bStudyStop = false;
    private boolean m_bStudyPause = false;
    private boolean m_bRecognitionResult = false;
    private boolean m_bArrowBtnPressed = false;

    private int[] m_result = new int[3];
    String strAudioPath;
    private WavAudioRecorder mRecorder = null;
    private EsStuddingRecognizer mRecognizer = null;
    private RandomAccessFile mRecordRawFile = null;

    private int[] mFontSizeList = new int[]{18, 20, 22};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_study_sentence);

        Intent intent = this.getIntent();
        m_nUserID = intent.getIntExtra(Define.USER_ID, -1);
        m_nParam = intent.getIntExtra(Define.SCHEDULE_ID, -1);

        if(!openDB()){
            finish();
            return;
        }

        mRecognizer = EsStuddingRecognizer.getInstance();
        mRecognizer.setListner(new AudioDataReceivedListener() {
            @Override
            public void onAudioDataReceived(short[] data) {
                if(m_bStudyStop) return;
                processBuffer(data);
            }
        });

        mRecognizer.setResultListner(new RecognizeResultListener() {
            @Override
            public void onResult(String strResult) {
                if(m_bStudyStop) return;
                processResult(strResult);
            }
        });

        AndroidAudioConverter.load(this, new ILoadCallback() {
            @Override
            public void onSuccess() {
                // Great!
                int a=1;

                //convertTeacher();
            }
            @Override
            public void onFailure(Exception error) {
                // FFmpeg is not supported by device
                int a=1;
            }
        });

        SekConvStudyPopup popup;

        popup = new SekConvStudyPopup(this);
        popup.setStep(Define.STUDY_STEP_SENTENCE);
        popup.setListener(new SekConvStudyPopup.OnSEKLaunchListener() {
            @Override
            public void onOK() {
                initViews();
            }
        });
        popup.show();

    }

    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        finishStudy();
        super.onDestroy();
    }

    public void finishStudy() {
        mRecognizer.stopSearch();

        m_bStudyPause = false;
        m_bArrowBtnPressed = true;
        m_bStudyStop = true;

        if(m_threadStudy != null && m_threadStudy.isAlive()) {
            try{
                m_threadStudy.join();
            }
            catch(Exception e) {}
        }

    }

    private boolean openDB() {
        mProgressManager = new ProgressDBManager(this, m_nUserID);
        if(mProgressManager == null)
            return false;

        mStudyManager = new StudyDBManager(this, m_nUserID);
        if(mStudyManager == null)
            return false;

        mSettingManager = new SettingDBManager(this);
        if(mSettingManager == null)
            return false;

        m_pakManager = new PAKDBManager(this);
        if(m_pakManager == null)
            return false;

        return true;
    }
    private void initViews(){

        frameMask = (FrameLayout) findViewById(R.id.frameMask);

        textDescription = (TextView) findViewById(R.id.textDescription);
        textEnglish = (TextView) findViewById(R.id.textEnglish);
        textKorean = (TextView) findViewById(R.id.textKorean);

        int fontSizeIndex = mSettingManager.getFontSizeIndex();
        textEnglish.setTextSize(mFontSizeList[fontSizeIndex]);
        textKorean.setTextSize(mFontSizeList[fontSizeIndex] + 2);

        btnBack = (ImageButton) findViewById(R.id.btnBack);
        btnStart = (ImageButton) findViewById(R.id.btnStart);
        btnPrev = (Button) findViewById(R.id.btnPrev);
        btnPause = (Button) findViewById(R.id.btnPause);
        btnNext = (Button) findViewById(R.id.btnNext);

        btnBack.setOnClickListener(this);
        btnStart.setOnClickListener(this);
        btnPrev.setOnClickListener(this);
        btnPause.setOnClickListener(this);
        btnNext.setOnClickListener(this);

        imageStatus = (ImageView) findViewById(R.id.imageStatus);
        imageTalkDesc = (ImageView) findViewById(R.id.imgTalkDesc);
        imageMark = (ImageView) findViewById(R.id.imageMark);
        imageMark.setAlpha(0.0f);


        imageTalkDesc.setAlpha(0.0f);
        imageTalkDesc.setVisibility(View.VISIBLE);

        easyPlayer = (EasyVideoPlayer) findViewById(R.id.easyPlayer);
        framePlayerMask = (FrameLayout) findViewById(R.id.framePlayerMask);

        frameResult = (FrameLayout) findViewById(R.id.frameResult);
        frameResult.setVisibility(View.INVISIBLE);

        textResultEng = (TextView) findViewById(R.id.textEng);
        textResultKior = (TextView) findViewById(R.id.textKor);
        textResult = (TextView) findViewById(R.id.textResult);

        frameTeacher = (FrameLayout) findViewById(R.id.frameTeacher);
        frameUser = (FrameLayout) findViewById(R.id.frameUser);

        textPercent = (TextView) findViewById(R.id.textPercent);
        //
        waveTeacher = (FileWaveformView) findViewById(R.id.waveTeacher);
        waveUser = (FileWaveformView) findViewById(R.id.waveUser);

        waveTeacher.setListener(listenerTeacher);
        waveUser.setListener(listenerTeacher);

        btnPlayTeacher = (ImageButton) findViewById(R.id.btnPlayTeacher);
        btnPlayUser = (ImageButton) findViewById(R.id.btnPlayUser);

        btnPlayTeacher.setOnClickListener(this);
        btnPlayUser.setOnClickListener(this);
        //

        mFormView = (RecordWaveformView) findViewById(R.id.viewRecordForm);
        imageMask = (ImageView) findViewById(R.id.imageMask);
        mFormView.setVisibility(View.INVISIBLE);
        mFormView.setColor(0);

        imageMask.setVisibility(View.INVISIBLE);

        //set information
        int userLevel = mProgressManager.getLevel();
        int epiNum = m_nParam/9;
        int stgNum = m_nParam%9;

        StageData stgData = mProgressManager.getStageData(userLevel, epiNum, stgNum);

        ImageButton ibtn0 = (ImageButton)findViewById(R.id.imageIndicate1);
        ImageButton ibtn1 = (ImageButton)findViewById(R.id.imageIndicate2);
        ImageButton ibtn2 = (ImageButton)findViewById(R.id.imageIndicate3);
        ImageButton ibtn3 = (ImageButton)findViewById(R.id.imageIndicate4);
        ImageButton ibtn4 = (ImageButton)findViewById(R.id.imageIndicate5);
        ImageButton ibtn5 = (ImageButton)findViewById(R.id.imageIndicate6);
        ImageButton ibtn6 = (ImageButton)findViewById(R.id.imageIndicate7);
        ImageButton ibtn7 = (ImageButton)findViewById(R.id.imageIndicate8);
        ImageButton ibtn8 = (ImageButton)findViewById(R.id.imageIndicate9);
        ImageButton ibtn9 = (ImageButton)findViewById(R.id.imageIndicate10);
        ImageButton ibtn10 = (ImageButton)findViewById(R.id.imageIndicate11);
        ImageButton ibtn11 = (ImageButton)findViewById(R.id.imageIndicate12);
        ImageButton ibtn12 = (ImageButton)findViewById(R.id.imageIndicate13);
        ImageButton ibtn13 = (ImageButton)findViewById(R.id.imageIndicate14);
        ImageButton ibtn14 = (ImageButton)findViewById(R.id.imageIndicate15);
        ImageButton ibtn15 = (ImageButton)findViewById(R.id.imageIndicate16);

        btnIndiator[0] = ibtn0;
        btnIndiator[1] = ibtn1;
        btnIndiator[2] = ibtn2;
        btnIndiator[3] = ibtn3;
        btnIndiator[4] = ibtn4;
        btnIndiator[5] = ibtn5;
        btnIndiator[6] = ibtn6;
        btnIndiator[7] = ibtn7;
        btnIndiator[8] = ibtn8;
        btnIndiator[9] = ibtn9;
        btnIndiator[10] = ibtn10;
        btnIndiator[11] = ibtn11;
        btnIndiator[12] = ibtn12;
        btnIndiator[13] = ibtn13;
        btnIndiator[14] = ibtn14;
        btnIndiator[15] = ibtn15;

        ProgressData data = new ProgressData(m_nUserID, userLevel, epiNum, stgNum);
        arryWordData = mStudyManager.getSentenceList(data);

        m_nCurrentWordNum = 0;

        nTotalCount = arryWordData.size();
        for(int i = nTotalCount; i < 16; i++){
            btnIndiator[i].setVisibility(View.GONE);
        }

        frameMask.setVisibility(View.INVISIBLE);
        //startWord();

        ArrayList<String> stringData = EsStuddingUtils.getWordList( arryWordData);
        String jsgfString = EsStuddingUtils.makeJSGFString(stringData);
        mRecognizer.setGrammarText(jsgfString);

        ESUtils.setTextViewTypeFaceByRes(this, textEnglish, Define.getEnglishFont());
        ESUtils.setTextViewTypeFaceByRes(this, textKorean, Define.getMainFont());
        ESUtils.setTextViewTypeFaceByRes(this, textDescription, Define.getMainFont());
        ESUtils.setTextViewTypeFaceByRes(this, textPercent, Define.getMainFont());
        ESUtils.setTextViewTypeFaceByRes(this, textResult, Define.getMainFont());
        ESUtils.setTextViewTypeFaceByRes(this, textResultEng, Define.getEnglishFont());
        ESUtils.setTextViewTypeFaceByRes(this, textResultKior, Define.getMainFont());
        ESUtils.setTextViewTypeFaceByRes(this, (TextView) findViewById(R.id.textResultDescription), Define.getMainFont());

        startStudy();
    }

    FileWaveformView.WaveformListener listenerTeacher = new FileWaveformView.WaveformListener() {
        @Override
        public void waveformTouchStart(float x) {

        }

        @Override
        public void waveformTouchMove(float x) {

        }

        @Override
        public void waveformTouchEnd() {

        }

        @Override
        public void waveformFling(float x) {

        }

        @Override
        public void waveformDraw() {

        }

        @Override
        public void waveformZoomIn() {

        }

        @Override
        public void waveformZoomOut() {

        }
    };

    private void goStudyTestActivity() {


        Intent intent = new Intent(this, TestSelectActivity.class);
        intent.putExtra(Define.USER_ID, m_nUserID);
        intent.putExtra(Define.IS_ORDERTEST, false);
        intent.putExtra(Define.SCHEDULE_ID, m_nParam);
        startActivity(intent);
    }
    int m_nCurrentWordNum;
    int nStep = 0;

    private void setIndicator(int nIndex){
        for(int i =0; i<nTotalCount; i++){
            btnIndiator[i].setImageResource(R.drawable.st_study_circle_gray);
        }

        btnIndiator[nIndex].setImageResource(R.drawable.st_study_circle_red);
    }


    private void pauseThread() {
        while(m_bStudyPause) {
            try{
                Thread.sleep(20);
            }
            catch(Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void waitRecognitionResult(){
        while(m_bRecognitionResult){
            try{
                if(m_bArrowBtnPressed)
                    break;

                Thread.sleep(20);
            }catch(Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void sleepThread(int nTime) {
        int nCount = 0;
        while(!m_bStudyStop) {
            pauseThread();



            if(nCount * 20 > nTime) {
                waitRecognitionResult();
                break;
            }

            if(m_bArrowBtnPressed)
                break;

            nCount ++;

            try{
                Thread.sleep(20);
            }
            catch(Exception e) {
                e.printStackTrace();
            }
        }
    }

    void startWord(){

        setIndicator(m_nCurrentWordNum);
        WordData data = arryWordData.get(m_nCurrentWordNum);
        m_result[0] = -1;
        m_result[1] = -1;
        m_result[2] = -1;

        String strVideoPath = getVideoFileName(Integer.parseInt(data.getImage()));
        //String strVideoPath = Define.VIDEO_PATH + String.format("%s.mp4", data.getImage() );

        textPercent.setText("");
        textResult.setText("");
        //waveUser.setSoundFile(null);

        easyPlayer.setCallback(callback);
        easyPlayer.setSource(Uri.parse(strVideoPath));
        easyPlayer.disableControls();
        easyPlayer.setAutoPlay(true);
        easyPlayer.setLoop(true);

        textDescription.setText(getString(R.string.string_sentence_description_think));
        //textEnglish.setTextColor(0x000000);
        textKorean.setText(data.getMeanKr() );
        textKorean.setVisibility(View.VISIBLE);
        textEnglish.setVisibility(View.INVISIBLE);
        textResultEng.setText(data.getMeanEn());
        textResultKior.setText(data.getMeanKr());

        imageStatus.setImageResource(R.drawable.st_study_thinking);
        imageTalkDesc.setAlpha(0.0f);
        imageMark.setAlpha(0.0f);

        imageMask.setVisibility(View.INVISIBLE);
        mFormView.setVisibility(View.INVISIBLE);

    }

    private void goListening()
    {
        WordData data = arryWordData.get(m_nCurrentWordNum);

        textDescription.setText(getString(R.string.string_sentence_description_pronunciation));
        imageStatus.setImageResource(R.drawable.st_study_listening);
        //imageMask.setVisibility(View.INVISIBLE);
        //mFormView.setVisibility(View.INVISIBLE);

        if(m_result[2] == 0){
            //textEnglish.setTextColor(this.getColor(R.color.app_color_red));
            //textEnglish.setTextColor(0xff0000);
        }
        textKorean.setVisibility(View.VISIBLE);
        textEnglish.setVisibility(View.VISIBLE);
        textKorean.setText(data.getMeanKr());
        textEnglish.setText(data.getMeanEn());

        strAudioPath = getAudioFileName(Integer.parseInt(data.getSound()));
        //strAudioPath = Define.SOUND_PATH + String.format("%s.mp3", data.getSound() );

        convertTeacher();


        MediaPlayer player = MediaPlayer.create(this, Uri.parse(strAudioPath));
               player.start();

    }

    private void setEnglishData(){
        WordData data = arryWordData.get(m_nCurrentWordNum);
        String strEn = data.getMeanEn();

        String[] strWords = strEn.split(" ");

        int nNormalBlanks = 0;

        if(strWords.length > 0 && strWords.length <= 3){
            nNormalBlanks = 1;
        }else if(strWords.length > 3 && strWords.length <= 7){
            nNormalBlanks = 2;
        }else if(strWords.length > 7){
            nNormalBlanks = 3;
        }

        int nDifficulty = 0;
        if((m_result[0] == 0 && m_result[1] == -1 ) || (m_result[0] == 0 && m_result[1] == 0) ) {
            nDifficulty = 0;
            textEnglish.setText(strEn);
        }else if((m_result[0] == -1 && m_result[1] == -1 ) || (m_result[0] == 1 && m_result[1] == 0 ) || (m_result[0] == 0 && m_result[1] == 1 )){
            nDifficulty = 1;
            String strResult = "";
            for(int i = 0; i < nNormalBlanks; i++){
                Random r = new Random();
                int i1 = r.nextInt(strWords.length);

                int nCount = strWords[i1].length();
                strWords[i1] ="";
                for(int j = 0; j < nCount; j++){
                    strWords[i1] += "_";
                }
            }

            for(int i = 0; i <strWords.length; i++){
                strResult += (strWords[i] + " ");
            }

            textEnglish.setText(strResult);

        }else if(m_result[0] == 1 && m_result[1] == -1) {
            nDifficulty = 2;
            //textEnglish.setText("");

            String strResult = "";
            for(int i = 0; i <strWords.length; i++){
                //strResult += (strWords[i] + " ");
                for(int j = 0; j < strWords[i].length(); j++){
                    strResult += "_";
                }

                strResult += " ";
            }

            textEnglish.setText(strResult);
        }


    }

    private void goRecording()
    {

        startAudioRecording();


        new Handler().postDelayed(new Runnable() {
            public void run() {

                imageStatus.setImageResource(R.drawable.st_study_speakin_red);
                imageTalkDesc.setVisibility(View.VISIBLE);
                imageMask.setVisibility(View.VISIBLE);
                mFormView.setVisibility(View.VISIBLE);

                textEnglish.setVisibility(View.VISIBLE);
                setEnglishData();

                ObjectAnimator fadeAnim = ObjectAnimator.ofFloat(imageTalkDesc, "alpha", 1.0f).setDuration(1000);
                fadeAnim.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        ObjectAnimator closeAnim = ObjectAnimator.ofFloat(imageTalkDesc, "alpha", 0.0f).setDuration(500);
                        closeAnim.setInterpolator(new AccelerateInterpolator());
                        closeAnim.start();
                    }
                });
                fadeAnim.setInterpolator(new BounceInterpolator());
                fadeAnim.start();
            }
        }, 400);
    }

    private long m_nStartStudyTime = 0;

    private void stopRecording(){
        m_bStudyPause = true;

        //stopAudioRecording();

        //update word date data
        WordData data = arryWordData.get(m_nCurrentWordNum);
        int wID = data.getId();
        long nCurTime = ESUtils.getNowDateMilliseconds();
        int nYear = ESUtils.getDateOfYear(nCurTime);
        int nMonth = ESUtils.getDateOfMonth(nCurTime);
        int nDate = ESUtils.getDateOfDay(nCurTime);
        long nRegTime = ESUtils.getDateMilliseconds(nYear, nMonth - 1, nDate);
        mProgressManager.updateProgressWords(nRegTime, wID);

        //update time date data
        long nEndStudyTime = ESUtils.getNowTimeSeconds();
        int nDiff = (int)(nEndStudyTime - m_nStartStudyTime);
        m_nStartStudyTime = nEndStudyTime;
        mProgressManager.updateProgressTime(nRegTime, nDiff);


        final File mUserFile = new File(Define.RECORD_PATH);
        new Thread() {
            public void run() {
                try {
                    final CheapSoundFile mUserSoundFile = CheapSoundFile.create(mUserFile.getAbsolutePath(), null);

                    if (mUserSoundFile == null) {

                    } else{
                        waveUser.setSoundFile(mUserSoundFile);

                        DisplayMetrics metrics = new DisplayMetrics();
                        getWindowManager().getDefaultDisplay().getMetrics(metrics);
                        float mDensity = metrics.density;
                        waveUser.recomputeHeights(mDensity);
                    }
                }catch (final Exception e){

                }
            }
        }.start();

        new Handler().postDelayed(new Runnable() {
            public void run() {
                frameResult.setVisibility(View.VISIBLE);
                imageMark.setAlpha(0.0f);
                imageStatus.setImageResource(R.drawable.st_study_thinking);
                textKorean.setText("");
                textEnglish.setText("");

            }
        }, 1500);
    }

    private void stopAudioRecording() {
        /*
        mRecorder.stop();
        mRecorder.release();
        mRecorder = null;
        */


        imageTalkDesc.setAlpha(0.0f);
        imageMask.setVisibility(View.INVISIBLE);
        mFormView.setVisibility(View.INVISIBLE);

        try {
            if (mRecordRawFile != null){
                mRecordRawFile.close();
                mRecordRawFile = null;
            }

            mRecognizer.stopSearch();
            String strResult = mRecognizer.getSearchResult();

            File mRawFile = new File(Define.RECORD_PATH_RAW);
            File mWavFile = new File(Define.RECORD_PATH);

            EsStuddingUtils.rawToWave(mRawFile, mWavFile, 16000);

        } catch (IOException e) {
            e.printStackTrace();
        }

        mFormView.setSamples(new short[20]);

    }



    private void startAudioRecording() {

        /*
        if(mRecorder != null){
            mRecorder.release();
        }
        mRecorder = WavAudioRecorder.getInstanse();
        mRecorder.setOutputFile(Define.RECORD_PATH);
        mRecorder.setAudioDataReceiveListener(new AudioDataReceivedListener() {
            @Override
            public void onAudioDataReceived(short[] data) {
                //mFormView.setSamfples(data);
            }
        });

        mRecorder.prepare();
        mRecorder.start();
        */



        returnBuffer= new short[20];

        try {

            File fdelete = new File(Define.RECORD_PATH_RAW);
            if (fdelete.exists()) {
                if (fdelete.delete()) {

                } else {

                }
            }

            if (mRecordRawFile != null){
                mRecordRawFile.close();
                mRecordRawFile = null;
            }

            mRecordRawFile = new RandomAccessFile(Define.RECORD_PATH_RAW, "rw");

        } catch (IOException e) {
            e.printStackTrace();
        }


        mRecognizer.startSearch();

    }


    short[] returnBuffer;

    float rms = 0f;
    float rms1 = 0f;
    float rms2 = 0f;
    float rms3 = 0f;
    float rms4 = 0f;

    float peak = 0f;

    private void processBuffer(short[] buffer){

        rms = 0f;
        rms1 = 0f;
        rms2 = 0f;
        rms3 = 0f;
        rms4 = 0f;

        peak = 0f;

        /*for(float sample : buffer) {

            float abs = Math.abs(sample);
            if(abs > peak) {
                peak = abs;
            }

            rms += sample * sample;
        }

        rms = (float)Math.sqrt(rms / buffer.length);

        */
        for(int i = 0; i < buffer.length / 4 ; i++){
            float sample = buffer[i];
            float abs = Math.abs(sample);
            if(abs > peak) {
                peak = abs;
            }

            rms1 += sample * sample;
        }

        for(int i = buffer.length / 4; i < buffer.length / 2; i++){
            float sample = buffer[i];
            float abs = Math.abs(sample);
            if(abs > peak) {
                peak = abs;
            }

            rms2 += sample * sample;
        }

        for(int i = buffer.length / 2; i < buffer.length / 4 * 3; i++){
            float sample = buffer[i];
            float abs = Math.abs(sample);
            if(abs > peak) {
                peak = abs;
            }

            rms3 += sample * sample;
        }

        for(int i = buffer.length / 4 * 3; i < buffer.length ; i++){
            float sample = buffer[i];
            float abs = Math.abs(sample);
            if(abs > peak) {
                peak = abs;
            }

            rms4 += sample * sample;
        }


        rms1 = (float)Math.sqrt(rms1 * 4 / buffer.length);
        rms2 = (float)Math.sqrt(rms2 * 4 / buffer.length);
        rms3 = (float)Math.sqrt(rms3 * 4 / buffer.length);
        rms4 = (float)Math.sqrt(rms4 * 4 / buffer.length);

        for(int i =0;i<19;i++){
            returnBuffer[i] = returnBuffer[i+1];
        }
        returnBuffer[19] = (short)rms1;

        mFormView.setSamples(returnBuffer);


        new Handler().postDelayed(new Runnable() {
            public void run() {
                for(int i =0;i<19;i++){
                    returnBuffer[i] = returnBuffer[i+1];
                }
                returnBuffer[19] = (short)rms2;

                mFormView.setSamples(returnBuffer);
            }
        }, 100);

        new Handler().postDelayed(new Runnable() {
            public void run() {
                for(int i =0;i<19;i++){
                    returnBuffer[i] = returnBuffer[i+1];
                }
                returnBuffer[19] = (short)rms3;

                mFormView.setSamples(returnBuffer);
            }
        }, 200);

        new Handler().postDelayed(new Runnable() {
            public void run() {
                for(int i =0;i<19;i++){
                    returnBuffer[i] = returnBuffer[i+1];
                }
                returnBuffer[19] = (short)rms4;

                mFormView.setSamples(returnBuffer);
            }
        }, 300);



        if(mRecordRawFile != null){
            try {
                mRecordRawFile.write(EsStuddingUtils.shortToBytes(buffer));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void processResult(String strResult){
        WordData data = arryWordData.get(m_nCurrentWordNum);
        String strEnglish = data.getMeanEn();

        int nPercent = (int) ( EsStuddingUtils.similarity(strEnglish.toLowerCase(), strResult.toLowerCase()) * 100);

        int nResultPercent = controlPercent(nPercent);

        if(nResultPercent >= 70)
            textPercent.setText(String.format("%d", nResultPercent) + "%");

        if(nResultPercent >= 76) {
            textResult.setText(getString(R.string.string_pronunciation_verygood));
            imageMark.setImageResource(R.drawable.st_study_excellent);
        }
        else if(nResultPercent >= 70) {
            textResult.setText(getString(R.string.string_pronunciation_normal));
            imageMark.setImageResource(R.drawable.st_study_good);

        }

        int nPass = 0;
        if(nResultPercent >= 70){
            nPass = 1;
            imageMark.setAlpha(1.0f);
            imageMark.setScaleX(0.7f);
            imageMark.setScaleY(0.7f);

            ObjectAnimator fadeAnim = ObjectAnimator.ofFloat(imageMark, "scaleX", 1.0f).setDuration(1000);
            AnimatorSet set = new AnimatorSet();
            set.playTogether(
                    ObjectAnimator.ofFloat(imageMark, "scaleX", 1.0f).setDuration(1000),
                    ObjectAnimator.ofFloat(imageMark, "scaleY", 1.0f).setDuration(1000)
            );
            set.setInterpolator(new BounceInterpolator());
            set.start();

            ESUtils.playSuccessSound(this);
        } else {
            ESUtils.playFailSound(this);
        }

        for(int i = 0; i < 3;i ++){
            if(m_result[i] == -1){
                m_result[i] = nPass;
                break;
            }
        }

        m_bRecognitionResult = false;

        //Toast.makeText(this, String.format("%d", nResultPercent), Toast.LENGTH_LONG).show();

    }

    private int controlPercent(int nPercent){

        int nResult = 0;
        if(nPercent >= 30){
            nResult = 80 + 20  * (nPercent - 70) / 70;
        }else if(nPercent >= 10){
            nResult = 50 + 30 * (nPercent - 10)/ 20;
        }else if(nPercent >=0){
            nResult = 0 + 50 * nPercent / 10;
        }

        return nResult;
    }

    private void convertTeacher(){

        File flacFile = new File(strAudioPath);

        IConvertCallback callback = new IConvertCallback() {
            @Override
            public void onSuccess(File convertedFile) {
                // So fast? Love it!
                //Toast.makeText(StudySentenceActivity.this, "Converted Successfully", Toast.LENGTH_LONG).show();

                final File mFile = convertedFile;

                new Thread() {
                    public void run() {
                        try {
                            CheapSoundFile mSoundFile = CheapSoundFile.create(mFile.getAbsolutePath(), null);
                            if (mSoundFile == null) {

                            } else{
                                waveTeacher.setSoundFile(mSoundFile);
                                DisplayMetrics metrics = new DisplayMetrics();
                                getWindowManager().getDefaultDisplay().getMetrics(metrics);
                                float mDensity = metrics.density;
                                waveTeacher.recomputeHeights(mDensity);
                            }
                        }catch (final Exception e){

                        }
                    }
                }.start();
            }
            @Override
            public void onFailure(Exception error) {
                // Oops! Something went wrong
                int bWrong = 1;
            }
        };
        AndroidAudioConverter.with(this)
                // Your current audio file
                .setFile(flacFile)

                // Your desired audio format
                .setFormat(AudioFormat.WAV)

                // An callback to know when conversion is finished
                .setCallback(callback)

                // Start conversion
                .convert();


    }

    private final int STEP_THINK = 0;
    private final int STEP_SPEAKING_FIRST = 1;
    private final int STEP_LISTENING_FIRST = 2;
    private final int STEP_SPEAKING_SECOND = 3;
    private final int STEP_LISTENING_SECOND = 4;

    private int m_nCurrentStep = 0;

    private Handler m_handlerStudy = new Handler() {
        public void handleMessage(Message msg) {
            imageMark.setAlpha(0.0f);

            switch (msg.what) {
                case 0:
                    startWord();
                    break;
                case 1:
                    goListening();
                    break;
                case 2:
                    goRecording();
                    break;
                case 3:
                    stopRecording();
                    break;
                case 4:
                    m_bRecognitionResult = true;
                    stopAudioRecording();
                    break;
                default:
                    break;
            }
        }
    };

    private void startStudy(){
        m_nStartStudyTime = ESUtils.getNowTimeSeconds();

        m_threadStudy = new Thread() {
            public void run() {
                while (!m_bStudyStop) {

                    sleepThread(0);

                    if(m_nCurrentWordNum == arryWordData.size()){

                        goStudyTestActivity();
                        finish();
                        return;
                    }

                    if(m_bArrowBtnPressed) {
                        m_bArrowBtnPressed = false;
                        continue;
                    }
                    m_handlerStudy.sendEmptyMessage(0);

                    // start - go first speaking
                    sleepThread(7000);

                    if(m_bArrowBtnPressed) {
                        m_bArrowBtnPressed = false;
                        continue;
                    }
                    m_handlerStudy.sendEmptyMessage(2);
                    // end

                    sleepThread(5000);
                    if(m_bArrowBtnPressed) {
                        m_bArrowBtnPressed = false;
                        continue;
                    }

                    m_handlerStudy.sendEmptyMessage(4);
                    sleepThread(1500);
                    if(m_bArrowBtnPressed) {
                        m_bArrowBtnPressed = false;
                        continue;
                    }

                    m_handlerStudy.sendEmptyMessage(1);

                    sleepThread(5000);
                    if(m_bArrowBtnPressed) {
                        m_bArrowBtnPressed = false;
                        continue;
                    }

                    m_handlerStudy.sendEmptyMessage(2);

                    sleepThread(5000);
                    if(m_bArrowBtnPressed) {
                        m_bArrowBtnPressed = false;
                        continue;
                    }

                    m_handlerStudy.sendEmptyMessage(4);
                    sleepThread(1500);
                    if(m_bArrowBtnPressed) {
                        m_bArrowBtnPressed = false;
                        continue;
                    }

                    if(m_result[0] == 1 && m_result[1] == 1){
                        m_handlerStudy.sendEmptyMessage(3);
                    }else{
                        //if(m_result[1] == 0){
                            m_handlerStudy.sendEmptyMessage(1);
                            sleepThread(5000);
                            if(m_bArrowBtnPressed) {
                                m_bArrowBtnPressed = false;
                                continue;
                            }
                        //}

                        m_handlerStudy.sendEmptyMessage(2);


                        sleepThread(5000);
                        if(m_bArrowBtnPressed) {
                            m_bArrowBtnPressed = false;
                            continue;
                        }

                        m_handlerStudy.sendEmptyMessage(4);
                        sleepThread(1500);
                        if(m_bArrowBtnPressed) {
                            m_bArrowBtnPressed = false;
                            continue;
                        }

                        if(m_result[2] == 1){
                            m_handlerStudy.sendEmptyMessage(3);
                        } else {
                            m_handlerStudy.sendEmptyMessage(1);
                            sleepThread(5000);
                            if(m_bArrowBtnPressed) {
                                m_bArrowBtnPressed = false;
                                continue;
                            }

                            m_handlerStudy.sendEmptyMessage(3);
                        }
                    }

                    try{
                        Thread.sleep(100);
                    }
                    catch(Exception e) {}
                }
            }
        };
        m_threadStudy.setDaemon(true);
        m_threadStudy.setName("StudyWordThread");
        m_threadStudy.start();
    }

    EasyVideoCallback callback = new EasyVideoCallback() {
        @Override
        public void onStarted(EasyVideoPlayer player) {
            //easyPlayer.setVisibility(View.VISIBLE);
        }

        @Override
        public void onPaused(EasyVideoPlayer player) {

        }

        @Override
        public void onPreparing(EasyVideoPlayer player) {

        }

        @Override
        public void onPrepared(EasyVideoPlayer player) {
            //player.start();
            Handler mainHandler = new Handler(StudySentenceActivity.this.getMainLooper());

            Runnable myRunnable = new Runnable() {
                @Override
                public void run() {
                    framePlayerMask.setVisibility(View.INVISIBLE);

                } // This is your code
            };
            mainHandler.post(myRunnable);

        }

        @Override
        public void onBuffering(int percent) {

        }

        @Override
        public void onError(EasyVideoPlayer player, Exception e) {

        }

        @Override
        public void onCompletion(EasyVideoPlayer player) {

        }

        @Override
        public void onRetry(EasyVideoPlayer player, Uri source) {

        }

        @Override
        public void onSubmit(EasyVideoPlayer player, Uri source) {

        }
    };
    @Override
    public void onClick(View v) {
        ESUtils.playBtnSound(this);
        switch (v.getId()){
            case R.id.btnBack:
                doAgain();
                break;
            case R.id.btnStart:
                doNextWord();
                break;
            case R.id.btnPlayTeacher:
                playTeacherVoice();
                break;
            case R.id.btnPlayUser:
                playUserVoice();
                break;
            case R.id.btnPrev:
                doPrev();
                break;
            case R.id.btnPause:
                doPause();
                break;
            case R.id.btnNext:
                doNext();
                break;
            default:
                break;
        }
    }

    private void doPrev(){
        if(m_nCurrentWordNum > 0){
            frameResult.setVisibility(View.INVISIBLE);
            m_nCurrentWordNum--;
            m_bArrowBtnPressed = true;
            m_bStudyPause = false;
        }
    }

    private void doPause(){
        if(m_bPlay){
            btnPause.setBackgroundResource(R.drawable.btn_study_play_play);
            m_bPlay = false;
            easyPlayer.pause();
            m_bStudyPause = true;

        }else {
            btnPause.setBackgroundResource(R.drawable.btn_study_play_pause);
            m_bPlay = true;
            easyPlayer.start();
            //doAgain();
            m_bStudyPause = false;
        }
    }

    private void doNext(){
        frameResult.setVisibility(View.INVISIBLE);
        m_nCurrentWordNum++;
        m_bArrowBtnPressed = true;
        m_bStudyPause = false;
    }

    private void playTeacherVoice(){
        MediaPlayer mp = MediaPlayer.create(this, Uri.parse(strAudioPath));
        mp.start();

    }

    private void playUserVoice(){
        MediaPlayer mp = MediaPlayer.create(this, Uri.parse(Define.RECORD_PATH));
        mp.start();

    }

    private void doNextWord(){
        frameResult.setVisibility(View.INVISIBLE);
        m_nCurrentWordNum++;
        m_bArrowBtnPressed = true;
        m_bStudyPause = false;
    }

    private void doAgain(){
        frameResult.setVisibility(View.INVISIBLE);
        m_bArrowBtnPressed = true;
        m_bStudyPause = false;
    }

    @Override
    public void onCompletion(MediaPlayer mp) {

    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        mp.setLooping(true);
    }


    private void showBackDlg(){
		/*new AlertDialog.Builder(this)
		.setTitle(this.getString(R.string.notice))
		.setCancelable(false)
		.setMessage(this.getString(R.string.study_back))
		.setPositiveButton(this.getString(R.string.yes), new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				dialog.cancel();
				StudyWordActivity.this.finish();

			}
		})
		.setNegativeButton(this.getString(R.string.cancel), new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				dialog.cancel();
			}
		})
		.show();*/



        SekWordPopup popup;

        popup = new SekWordPopup(StudySentenceActivity.this);
        popup.setMessage(this.getString(R.string.study_back));
        popup.setListener(new SekWordPopup.OnSEKLaunchListener() {

            @Override
            public void onOK() {
                StudySentenceActivity.this.finish();
            }

            @Override
            public void onCancel() {
                // TODO Auto-generated method stub
            }
        });

        popup.show();
    }


    @Override
    public void onBackPressed() {
        // your code.
        showBackDlg();

    }

    public String getImageFileName(int nWordID){
        return m_pakManager.getFileName(nWordID, 0);
    }

    public String getAudioFileName(int nWordID){
        return m_pakManager.getFileName(nWordID, 1);
    }

    public String getVideoFileName(int nWordID){
        return m_pakManager.getFileName(nWordID, 2);
    }
}
