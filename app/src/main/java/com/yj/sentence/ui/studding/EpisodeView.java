package com.yj.sentence.ui.studding;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.yj.sentence.R;
import com.yj.sentence.common.Define;
import com.yj.sentence.database.ProgressDBManager;
import com.yj.sentence.structs.ProgressData;
import com.yj.sentence.ui.studding.study.StudyPrepareActivity;
import com.yj.sentence.ui.studding.study.StudyPronunciationActivity;
import com.yj.sentence.ui.studding.test.StudyTestActivity;
import com.yj.sentence.ui.studding.test.TestSelectActivity;
import com.yj.sentence.utils.ESUtils;

/**
 * Created by achilles on 12/29/16.
 */

public class EpisodeView extends FrameLayout implements View.OnClickListener {

    private Context m_Context;
    private int m_nEpisodeNumber;

    private TextView textEpisode;
    private Button m_btnStage1;
    private Button m_btnStage2;
    private Button m_btnStage3;
    private Button m_btnStage4;
    private Button[] btnStages = new Button[4];

    private LinearLayout[] containerStages = new LinearLayout[4];
    private ImageView[] ivStages = new ImageView[4];

    private Button m_btnPronunciation;
    private Button m_btnExercise;
    private ImageView m_imgPronunciation;
    private ImageView m_imgExercise;

    private Handler m_stagehandler;
    private int m_nUserId = -1;

    private int nSelectedStage = -1;

    public EpisodeView(Context context) {
        super(context);
        m_Context = context;
        initViews();
    }

    public void setEpiNumber(int nEpisodeNumber){
        m_nEpisodeNumber = nEpisodeNumber;
        String strEpisode = String.format(getResources().getString(R.string.string_format_episode), m_nEpisodeNumber);
        textEpisode.setText(strEpisode);
    }

    public void setHandler(Handler handler){
        m_stagehandler = handler;
    }

    private void initViews(){
        View v = LayoutInflater.from(m_Context).inflate(R.layout.progress_item, null);
        this.addView(v);

        textEpisode = (TextView) this.findViewById(R.id.textEpisode);
        m_btnStage1 = (Button)this.findViewById(R.id.btnStage1);
        m_btnStage2 = (Button)this.findViewById(R.id.btnStage2);
        m_btnStage3 = (Button)this.findViewById(R.id.btnStage3);
        m_btnStage4 = (Button)this.findViewById(R.id.btnStage4);
        m_btnPronunciation = (Button) this.findViewById(R.id.btnPronunciation);
        m_btnExercise = (Button) this.findViewById(R.id.btnExercise);

        m_imgPronunciation = (ImageView) this.findViewById(R.id.imgChkPronunciation);
        m_imgExercise = (ImageView) this.findViewById(R.id.imgChkExercise);

        m_btnStage1.setOnClickListener(this);
        m_btnStage2.setOnClickListener(this);
        m_btnStage3.setOnClickListener(this);
        m_btnStage4.setOnClickListener(this);
        m_btnPronunciation.setOnClickListener(this);
        m_btnExercise.setOnClickListener(this);

        btnStages[0] = m_btnStage1;
        btnStages[1] = m_btnStage2;
        btnStages[2] = m_btnStage3;
        btnStages[3] = m_btnStage4;

        containerStages[0] = (LinearLayout) this.findViewById(R.id.containerStage1);
        containerStages[1] = (LinearLayout) this.findViewById(R.id.containerStage2);
        containerStages[2] = (LinearLayout) this.findViewById(R.id.containerStage3);
        containerStages[3] = (LinearLayout) this.findViewById(R.id.containerStage4);

        ivStages[0] = (ImageView) this.findViewById(R.id.imgStage1);
        ivStages[1] = (ImageView) this.findViewById(R.id.imgStage2);
        ivStages[2] = (ImageView) this.findViewById(R.id.imgStage3);
        ivStages[3] = (ImageView) this.findViewById(R.id.imgStage4);

        for(int i = 0; i < 4; i++){
            ivStages[i].setVisibility(View.GONE);
        }

        ESUtils.setTextViewTypeFaceByRes(m_Context, textEpisode, Define.getMainFont() );
        ESUtils.setTextViewTypeFaceByRes(m_Context, (TextView)findViewById(R.id.textStage1), Define.getMainFont() );
        ESUtils.setTextViewTypeFaceByRes(m_Context, (TextView)findViewById(R.id.textStage2), Define.getMainFont() );
        ESUtils.setTextViewTypeFaceByRes(m_Context, (TextView)findViewById(R.id.textStage3), Define.getMainFont() );
        ESUtils.setTextViewTypeFaceByRes(m_Context, (TextView)findViewById(R.id.textStage4), Define.getMainFont() );
        ESUtils.setButtonTypeFaceByRes(m_Context, (Button)findViewById(R.id.btnPronunciation), Define.getMainFont() );
        ESUtils.setButtonTypeFaceByRes(m_Context, (Button)findViewById(R.id.btnExercise), Define.getMainFont() );


    }

    public void setUserId(int nUserId){
        m_nUserId = nUserId;

        //update medal state;
        ProgressDBManager progressDBManager = new ProgressDBManager(m_Context, m_nUserId);
        int nLevel = progressDBManager.getLevel();

        for(int i = 0; i < 4; i++){
            ProgressData data = new ProgressData(m_nUserId, nLevel, m_nEpisodeNumber, i+1);
            int nMarks = progressDBManager.getMarks(data);

            if (nMarks >= 90){
                ivStages[i].setVisibility(View.VISIBLE);
                ivStages[i].setImageResource(R.drawable.medal5);
            }else if(nMarks >= 80){
                ivStages[i].setVisibility(View.VISIBLE);
                ivStages[i].setImageResource(R.drawable.medal4);
            }else if(nMarks >= 70){
                ivStages[i].setVisibility(View.VISIBLE);
                ivStages[i].setImageResource(R.drawable.medal3);
            }else if(nMarks > 0) {
                ivStages[i].setVisibility(View.VISIBLE);
                ivStages[i].setImageResource(R.drawable.medal2);
            }
        }
        {
            ProgressData data = new ProgressData(m_nUserId, nLevel, m_nEpisodeNumber, 5);
            int nMarks = progressDBManager.getMarks(data);
            if(nMarks > 0){
                //m_btnPronunciation.setBackgroundResource();
                m_imgPronunciation.setVisibility(View.VISIBLE);
                m_btnPronunciation.setText("");
            }else {
                m_imgPronunciation.setVisibility(View.INVISIBLE);
                m_btnPronunciation.setText(m_Context.getString(R.string.string_pronunciation));
            }

        }

        {
            ProgressData data = new ProgressData(m_nUserId, nLevel, m_nEpisodeNumber, 6);
            int nMarks = progressDBManager.getMarks(data);
            if(nMarks > 0){
                //m_btnPronunciation.setBackgroundResource();
                m_imgExercise.setVisibility(View.VISIBLE);
                m_btnExercise.setText("");
            }else {
                m_imgExercise.setVisibility(View.INVISIBLE);
                m_btnExercise.setText(m_Context.getString(R.string.string_exercise));
            }

        }


    }

    public void setSelectedIndex(int nIndex){
        
        if(nIndex == -1){
            nSelectedStage = nIndex;
        }else {
            nSelectedStage = nIndex;
            if(nIndex <= 4)
            {
                btnStages[nIndex - 1].setBackgroundResource(R.drawable.btn_stage_start);
                btnStages[nIndex - 1].setText("");

                containerStages[nIndex - 1].setVisibility(View.INVISIBLE);
            }

            if(nIndex == 5){
               // m_btnPronunciation.setBackgroundResource(R.drawable.btn_stage_start);
               // m_btnPronunciation.setText("");
            }

            if(nIndex == 6){
               // m_btnExercise.setImageResource(R.drawable.btn_stage_start);
            }

        }
    }

    @Override
    public void onClick(View v) {
        ESUtils.playBtnSound(m_Context);
        switch (v.getId()){
            case R.id.btnStage1:
                startStage(1);
                break;
            case R.id.btnStage2:
                startStage(2);
                break;
            case R.id.btnStage3:
                startStage(3);
                break;
            case R.id.btnStage4:
                startStage(4);
                break;
            case R.id.btnPronunciation:
                startStage(5);
                break;
            case R.id.btnExercise:
                startStage(6);
                break;
            default:
                break;
        }
    }

    private void startStage(int nStageNum){
        int param = m_nEpisodeNumber * 9 + nStageNum;

        if(nStageNum == 6){
            startExerciseActivity(param);
            return;
        }

        if(nStageNum == 5){
            startPronunciationActivity(param);
            return;
        }
        if(nStageNum == nSelectedStage){

            ProgressDBManager progressDBManager = new ProgressDBManager(m_Context, m_nUserId);
            int nLevel = progressDBManager.getLevel();

            ProgressData data = new ProgressData(m_nUserId, nLevel, m_nEpisodeNumber, nStageNum);
            int nMarks = progressDBManager.getMarks(data);

            if(nMarks > 0){
                param = 1000 + param;
                Message msg = m_stagehandler.obtainMessage(param);
                msg.sendToTarget();
            }else{
                startStudyPrepareActivity(param);
            }

        }else {
            ProgressDBManager progressDBManager = new ProgressDBManager(m_Context, m_nUserId);
            int nLevel = progressDBManager.getLevel();

            ProgressData data = new ProgressData(m_nUserId, nLevel, m_nEpisodeNumber, nStageNum);
            int nMarks = progressDBManager.getMarks(data);

           /* if(nMarks > 0){
                param = 1000 + param;
                Message msg = m_stagehandler.obtainMessage(param);
                msg.sendToTarget();
            }else*/
                {
                Message msg = m_stagehandler.obtainMessage(param);
                msg.sendToTarget();
            }
        }
    }


    private void startPronunciationActivity(int param){
        Intent intent = new Intent(m_Context, StudyPronunciationActivity.class);
        intent.putExtra(Define.USER_ID, m_nUserId);
        intent.putExtra(Define.SCHEDULE_ID, param);
        m_Context.startActivity(intent);
    }

    private void startStudyPrepareActivity(int param)
    {
        Intent intent = new Intent(m_Context, StudyPrepareActivity.class);
        intent.putExtra(Define.USER_ID, m_nUserId);
        intent.putExtra(Define.SCHEDULE_ID, param);
        m_Context.startActivity(intent);
    }

    private void startExerciseActivity(int param)
    {
        Intent intent = new Intent(m_Context, TestSelectActivity.class);
        intent.putExtra(Define.USER_ID, m_nUserId);
        intent.putExtra(Define.IS_ORDERTEST, false);
        intent.putExtra(Define.IS_EXERCISE, true);
        intent.putExtra(Define.SCHEDULE_ID, param);
        m_Context.startActivity(intent);
    }
}
