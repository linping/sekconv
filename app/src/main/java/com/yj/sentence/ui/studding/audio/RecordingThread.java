/*
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.yj.sentence.ui.studding.audio;

import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.util.Log;

public class RecordingThread {
    private static final String LOG_TAG = RecordingThread.class.getSimpleName();
    private static final int SAMPLE_RATE = 16000;

    public RecordingThread(AudioDataReceivedListener listener) {
        mListener = listener;
    }

    private boolean mShouldContinue;
    private AudioDataReceivedListener mListener;
    private AudioRealDataReceivedListener mRealListener;

    private Thread mThread;
    AudioRecord record;

    public boolean recording() {
        return mThread != null;
    }

    public void setRealDataListener(AudioRealDataReceivedListener listener){
        mRealListener = listener;
    }
    public void startRecording() {
        if (mThread != null)
            return;

        mShouldContinue = true;
        mThread = new Thread(new Runnable() {
            @Override
            public void run() {
                record();
            }
        });
        mThread.start();
    }

    public void stopRecording() {
        if (mThread == null)
            return;

        mShouldContinue = false;
        mThread = null;
    }

    /*
    public int getAmplitude(){
        if(record != null){

        }
    }
*/
    private void record() {
        Log.v(LOG_TAG, "Start");
        android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_AUDIO);

        // buffer size in bytes
        int bufferSize = AudioRecord.getMinBufferSize(SAMPLE_RATE,
                AudioFormat.CHANNEL_IN_MONO,
                AudioFormat.ENCODING_PCM_16BIT);

        if (bufferSize == AudioRecord.ERROR || bufferSize == AudioRecord.ERROR_BAD_VALUE) {
            bufferSize = SAMPLE_RATE * 2;
        }

        short[] audioBuffer = new short[bufferSize / 2];
        short[] allBuffer = new short[bufferSize * 10];

        byte[] byteBuffer= new byte[bufferSize];
        float[] samples = new float[bufferSize / 2];

        short[] returnBuffer= new short[20];
        record = new AudioRecord(MediaRecorder.AudioSource.DEFAULT,
                SAMPLE_RATE,
                AudioFormat.CHANNEL_IN_MONO,
                AudioFormat.ENCODING_PCM_16BIT,
                bufferSize);

        if (record.getState() != AudioRecord.STATE_INITIALIZED) {
            Log.e(LOG_TAG, "Audio Record can't initialize!");
            return;
        }
        record.startRecording();

        Log.v(LOG_TAG, "Start recording");

        long shortsRead = 0;

        while (mShouldContinue) {
            /*int numberOfShort = record.read(audioBuffer, 0, audioBuffer.length);
            shortsRead += numberOfShort;

            for(int i = 0; i < allBuffer.length - audioBuffer.length; i++){
                allBuffer[i] = allBuffer[i + audioBuffer.length];
            }

            for(int j = 0; j < audioBuffer.length; j++){
                allBuffer[allBuffer.length - audioBuffer.length + j] = audioBuffer[j];
            }

            // Notify waveform
            //mListener.onAudioDataReceived(audioBuffer);
            mListener.onAudioDataReceived(allBuffer);*/

            int b = record.read(byteBuffer, 0, byteBuffer.length);
            shortsRead += b;

            for(int i = 0, s = 0; i < b;) {
                int sample = 0;

                sample |= byteBuffer[i++] & 0xFF; // (reverse these two lines
                sample |= byteBuffer[i++] << 8;   //  if the format is big endian)

                // normalize to range of +/-1.0f
                samples[s++] = sample;// / 32768f;
            }

            float rms = 0f;
            float peak = 0f;
            for(float sample : samples) {

                float abs = Math.abs(sample);
                if(abs > peak) {
                    peak = abs;
                }

                rms += sample * sample;
            }

            rms = (float)Math.sqrt(rms / samples.length);

            for(int i =0;i<19;i++){
                returnBuffer[i] = returnBuffer[i+1];
            }
            returnBuffer[19] = (short)rms;

            mListener.onAudioDataReceived(returnBuffer);
            if(mRealListener != null) mRealListener.onAudioDataReceived(byteBuffer);
            /*
            if(lastPeak > peak) {
                peak = lastPeak * 0.875f;
            }

            lastPeak = peak; */



        }

        record.stop();
        record.release();
        record = null;
        Log.v(LOG_TAG, String.format("Recording stopped. Samples read: %d", shortsRead));
    }
}
