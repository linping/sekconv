package com.yj.sentence.ui.studding.study;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.widget.TextView;

public class StudyWordTextView extends TextView {
	private int m_nRight = 0;
	private int m_nWidth = 0;
	private boolean m_bRight = false;
	
	private Thread m_thread = null;
	private boolean m_bStop = false;
	private boolean m_bPause = false;
	
	private int m_nTime = 10;
	private int m_nSleep = 10;
	
	private Paint m_paint = new Paint();
	
	public StudyWordTextView(Context context) {
		super(context);
		// TODO Auto-generated constructor stub		
		initView();
	}

	public StudyWordTextView(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		initView();
	}

	public StudyWordTextView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		// TODO Auto-generated constructor stub
		initView();
	}
	
	private void initView() {
		m_paint.setColor(Color.TRANSPARENT);
		init();
	}
	
	public void setColorOfView(int nRed, int nGreen, int nBlue) {
		m_paint.setColor(Color.rgb(nRed, nGreen, nBlue));
	}

	@Override
	protected void onLayout(boolean changed, int left, int top, int right,
			int bottom) {
		// TODO Auto-generated method stub
		m_nWidth = right - left;
		super.onLayout(changed, left, top, right, bottom);
	}

	@Override
	protected void onDraw(Canvas canvas) {
		// TODO Auto-generated method stub
		int nHeight = getHeight();
		
		if(m_bRight) {
			m_nRight += m_nWidth/m_nTime;
			
			if(m_nRight > m_nWidth) {
				m_nRight = m_nWidth;
				finishThread();
			}
			
			canvas.drawRect(0, 0, m_nRight, nHeight, m_paint);
		}
		else {
			m_nRight += m_nWidth/m_nTime;
			
			if(m_nRight > m_nWidth) {
				m_nRight = m_nWidth;
				finishThread();
			}
			
			int left = m_nWidth-m_nRight;
			canvas.drawRect(left, 0, m_nWidth, nHeight, m_paint);
		}
		
		super.onDraw(canvas);
	}
	
	public void init() {
		m_nRight = 0;
		m_nWidth = 0;
		
		m_bStop = false;
		m_bPause = false;
		m_thread = null;
	}
	
	public void setDirect(boolean bRight) {
		m_bRight = bRight;
	}
	
	@SuppressLint("HandlerLeak")
	private Handler m_handler = new Handler() {
		public void handleMessage(Message msg) {
			if(!m_bPause)
				StudyWordTextView.this.invalidate();
		}
	};
	
	public void startAnimBg() {
		m_thread = new Thread() {
			public void run() {
				while(!m_bStop) {
					pauseThread();
					
					try{
						Thread.sleep(m_nSleep);
					}
					catch(Exception e) {}
				
					m_handler.sendEmptyMessage(0);
				}
			}
		};		
		m_thread.start();
	}
	
	public boolean getAnimState() {
		return m_bStop;
	}
	
	public void setPauseAnim() {
		m_bPause = true;
	}
	
	public void setResumeAnim() {
		m_bPause = (m_bPause == true) ? false:m_bPause;
	}
	
	public void setAnimSpeed(int nIndex) {
		switch(nIndex) {
		case 0:
			m_nTime = 60;
			m_nSleep = 50;
			break;
		case 1:
			m_nTime = 50;
			m_nSleep = 40;
			break;
		case 2:
			m_nTime = 35;
			m_nSleep = 25;
			break;
		case 3:
			m_nTime = 25;
			m_nSleep = 15;
			break;
		case 4:
			m_nTime = 10;
			m_nSleep = 10;
			break;
		}
	}
	
	private void pauseThread() {
		while(m_bPause) {
			try{
				Thread.sleep(m_nSleep);
			}
			catch(Exception e) {}
		}
	}
	
	public void finishThread() {
		m_bPause  = false;
		
		if(m_thread != null && m_thread.isAlive()) {
			m_bStop = true;
			try{
				m_thread.join();
			}
			catch(Exception e) {}
		}
	}
}
