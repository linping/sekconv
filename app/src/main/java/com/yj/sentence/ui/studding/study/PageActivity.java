package com.yj.sentence.ui.studding.study;

import android.app.Activity;
import android.graphics.Canvas;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

public class PageActivity extends Activity implements OnTouchListener {

	public	short		m_RowCurrent	=	0;
	public	short		m_RowBefore		=	0;
	public	short		m_RowNext		=	0;
	public	int			m_RowSize		=	0;
	
//	public	short		m_nPrevPage	=	0;
	public	short		m_nCurrPage	=	0;
//	public	short		m_nNextPage	=	0;
//	public	short		m_nPages	=	0;

	private MediaPlayer m_MediaPlayer	=	null;

	protected Thread m_soundThread = new Thread()
	{
		@Override
		public void run() {
			/*
			if (!m_MediaPlayer.isPlaying())
				m_MediaPlayer.start();
				*/
		}		
	};

	public void soundStart()
	{
		m_soundThread.run();
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		m_nCurrPage = 1;
	}

	public void setPageBorder(int w, int h)
	{
	}
	
	public void drawPage(Canvas canvas, int nBeginRow, int nAdd)
	{
	}

	public void restorePage(short nScrollDir)
	{
	}
	
//	@Override
	public void onScroll(float distanceX, float distanceY)
	{
	}
	
//	@Override
	public void onLongPress(MotionEvent e)
	{
	}
	
//	@Override
	public void onFling()
	{
	}
	
//	@Override
	public void onSingleTapUp(MotionEvent e)
	{
	}
	
//	@Override
	public void onDown(MotionEvent e)
	{
	}

//	@Override
	public boolean onTouch(View v, MotionEvent event) {
		return true;
	}
}
