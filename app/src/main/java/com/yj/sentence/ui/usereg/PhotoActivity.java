package com.yj.sentence.ui.usereg;

import java.io.File;

import com.yj.sentence.utils.ESUtils;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;

/**
 * 
 * @author HRH
 * 2015.10.04
 * 사진처리화면
 */

public class PhotoActivity extends Activity
{
	private final static int TAKE_PHOTO = 1;
	
	protected String m_strPhotoPath = "";
	protected String m_strPhotoTemp = "";
	
	protected Bitmap setPhotoImage(String strFile) {
		Bitmap bmpPhoto = BitmapFactory.decodeFile(strFile);
		if (bmpPhoto != null)
		{
			File fileOld = new File(m_strPhotoPath);
			if (fileOld.exists())
				fileOld.delete();
			
			m_strPhotoPath = strFile;
		}
		
		return bmpPhoto;
	}

	// Photo Processing
	protected void getPhoto() {
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		String strDirPhoto = getPhotoDir();
		
		m_strPhotoTemp = strDirPhoto + "_Temp.jpg";
		File file = new File(m_strPhotoTemp);
		Uri photoUri = Uri.fromFile(file);
		
		intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
		startActivityForResult(intent, TAKE_PHOTO);
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(requestCode == TAKE_PHOTO && resultCode < 0) {
			String	strNewPhoto = getPhotoDir() + String.valueOf(ESUtils.getNowDateMilliseconds()) + ".jpg";;
			File fileTemp = new File(m_strPhotoTemp);
			if (fileTemp.exists())
			{
				fileTemp.renameTo(new File(strNewPhoto));
				setPhotoImage(strNewPhoto);
			}
		}

		super.onActivityResult(requestCode, resultCode, data);
	}

	private String getPhotoDir() {
		String strDirPackage = Environment.getExternalStorageDirectory() + "/" + getPackageName();
		File dirPackage = new File(strDirPackage);
		if( !dirPackage.exists() )
			dirPackage.mkdir();

		String strDirPhoto = strDirPackage + "/photo/";
		File dirPhoto = new File(strDirPhoto);
		if( !dirPhoto.exists() )
			dirPhoto.mkdir();
		
		return strDirPhoto;
	}
}
