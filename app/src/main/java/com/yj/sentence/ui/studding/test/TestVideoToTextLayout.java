package com.yj.sentence.ui.studding.test;

import java.util.ArrayList;

import com.afollestad.easyvideoplayer.EasyVideoCallback;
import com.afollestad.easyvideoplayer.EasyVideoPlayer;
import com.yj.sentence.R;
import com.yj.sentence.common.Define;
import com.yj.sentence.database.PAKDBManager;
import com.yj.sentence.structs.WordData;
import com.yj.sentence.ui.studding.study.StudyBasicActivity;
import com.yj.sentence.utils.ESUtils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

public class TestVideoToTextLayout extends TestLayout {

private Context m_context;
	
	private TextView m_tvKorProblem;	//조선어문제
	private TextView m_tvTrueWords;		//맞힌 단어수
	private TextView m_tvFalseWords;	//틀린 단어수
	private TextView m_tvTotalWords;	//전체 단어수
	
	
	boolean is_enable_thread = false;
	
	
	private TextView tvkorengtest_title;	
	private TextView tvkorengtest_description;
	
	private ImageView[] m_arrIvTrueFalseMsg = new ImageView[4];		//합격 불합격
	//private ImageView[] m_arrIvWordNum = new ImageView[4];			//결과단어 순서번호
	private TextView[] m_arrTextWordNum = new TextView[4];
	private Button[] m_arrBtnResult = new Button[4];				//결과단어
	private TestProgressBar m_progressbar = null;					//ProgressBar
	
	public OnKorEngTestFinishListener m_listener = null;
	
	private String[] m_strEngRes = new String[4];
	private int m_nTrueIndex = -1;

	private EasyVideoPlayer easyPlayer;
	private FrameLayout framePlayerMask;

	public TestVideoToTextLayout(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		m_context = context;
		initLayout();
		initKorEngTestLayout();
	}

	private void initLayout() {
		View v = LayoutInflater.from(m_context).inflate(R.layout.test_vtt_layout, null);
		this.addView(v);
	}
	
	private void initKorEngTestLayout() {
		
		//Typeface textTypeFace_gulim = Typeface.createFromAsset(getContext().getAssets(), Define.TEXTFONT_GULIM_PATH);
		
		tvkorengtest_title = (TextView)findViewById(R.id.tvkorengtest_title);
		tvkorengtest_description = (TextView)findViewById(R.id.tvkorengtest_description);
		
		//tvkorengtest_title.setTypeface(textTypeFace_gulim);
		//tvkorengtest_description.setTypeface(textTypeFace_gulim);

		m_tvKorProblem = (TextView)findViewById(R.id.tvKorProblem);
		m_tvTrueWords = (TextView)findViewById(R.id.tvTrueWords);
		m_tvFalseWords = (TextView)findViewById(R.id.tvFalseWords);
		m_tvTotalWords = (TextView)findViewById(R.id.tvTotalWords);
		
		ImageView ivTrueFalseMsg1 = (ImageView)findViewById(R.id.ivTrueFalseMsg1);
		ivTrueFalseMsg1.setVisibility(View.GONE);
		ImageView ivTrueFalseMsg2 = (ImageView)findViewById(R.id.ivTrueFalseMsg2);
		ivTrueFalseMsg2.setVisibility(View.GONE);
		ImageView ivTrueFalseMsg3 = (ImageView)findViewById(R.id.ivTrueFalseMsg3);
		ivTrueFalseMsg3.setVisibility(View.GONE);
		ImageView ivTrueFalseMsg4 = (ImageView)findViewById(R.id.ivTrueFalseMsg4);
		ivTrueFalseMsg4.setVisibility(View.GONE);
		m_progressbar = (TestProgressBar)findViewById(R.id.progressbar);
		m_progressbar.setMax(1000);

		m_arrIvTrueFalseMsg[0] = ivTrueFalseMsg1;
		m_arrIvTrueFalseMsg[1] = ivTrueFalseMsg2;
		m_arrIvTrueFalseMsg[2] = ivTrueFalseMsg3;
		m_arrIvTrueFalseMsg[3] = ivTrueFalseMsg4;
		
		/*ImageView ivWordNum1 = (ImageView)findViewById(R.id.ivTestWordNum1);
		ImageView ivWordNum2 = (ImageView)findViewById(R.id.ivTestWordNum2);
		ImageView ivWordNum3 = (ImageView)findViewById(R.id.ivTestWordNum3);
		ImageView ivWordNum4 = (ImageView)findViewById(R.id.ivTestWordNum4);
		
		m_arrIvWordNum[0] = ivWordNum1;
		m_arrIvWordNum[1] = ivWordNum2;
		m_arrIvWordNum[2] = ivWordNum3;
		m_arrIvWordNum[3] = ivWordNum4;*/

		TextView tvWordNum1 = (TextView) findViewById(R.id.textWordNum1);
		TextView tvWordNum2 = (TextView) findViewById(R.id.textWordNum2);
		TextView tvWordNum3 = (TextView) findViewById(R.id.textWordNum3);
		TextView tvWordNum4 = (TextView) findViewById(R.id.textWordNum4);

		m_arrTextWordNum[0] = tvWordNum1;
		m_arrTextWordNum[1] = tvWordNum2;
		m_arrTextWordNum[2] = tvWordNum3;
		m_arrTextWordNum[3] = tvWordNum4;

		Button btnResultEngKor1 = (Button)findViewById(R.id.btnTestKorWord1);
		btnResultEngKor1.setOnClickListener(m_clickListener);
		
		Button btnResultEngKor2 = (Button)findViewById(R.id.btnTestKorWord2);
		btnResultEngKor2.setOnClickListener(m_clickListener);
		
		Button btnResultEngKor3 = (Button)findViewById(R.id.btnTestKorWord3);
		btnResultEngKor3.setOnClickListener(m_clickListener);
		
		Button btnResultEngKor4 = (Button)findViewById(R.id.btnTestKorWord4);
		btnResultEngKor4.setOnClickListener(m_clickListener);
		
		m_arrBtnResult[0] = btnResultEngKor1;
		m_arrBtnResult[1] = btnResultEngKor2;
		m_arrBtnResult[2] = btnResultEngKor3;
		m_arrBtnResult[3] = btnResultEngKor4;

		framePlayerMask = (FrameLayout) findViewById(R.id.frameMask);

		easyPlayer = (EasyVideoPlayer) findViewById(R.id.easyPlayer);
		easyPlayer.setCallback(callback);
		easyPlayer.disableControls();
		easyPlayer.setAutoPlay(true);
		easyPlayer.setLoop(true);

		ESUtils.setTextViewTypeFaceByRes(m_context, tvkorengtest_description, Define.getMainFont());
		for(int i = 0; i < 4; i++){
			ESUtils.setButtonTypeFaceByRes(m_context, m_arrBtnResult[i],Define.getEnglishFont());
		}

	}
	
	private View.OnClickListener m_clickListener = new View.OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			//ESUtils.playBtnSound(m_context);

			if(m_bResultPressed || !m_bProblemView) return;
			
			switch(v.getId()) {
			case R.id.btnTestKorWord1:
				checkResult(1);
				break;
			case R.id.btnTestKorWord2:
				checkResult(2);
				break;
			case R.id.btnTestKorWord3:
				checkResult(3);
				break;
			case R.id.btnTestKorWord4:
				checkResult(4);
				break;
			}
		}
	};

	int nCheckCount = 0;

	//시험문제에 대한 답 검사
	@SuppressLint("HandlerLeak")
	private void checkResult(int nIndex) {
		m_bResultPressed = true;
		
		m_arrIvTrueFalseMsg[nIndex-1].setVisibility(View.VISIBLE);
		m_arrTextWordNum[nIndex-1].setTextColor(getResources().getColor(R.color.Coral));
		m_arrBtnResult[nIndex-1].setBackgroundResource(R.drawable.st_test_item_select_bg);
		if(isTrueWord(nIndex-1)) {	//합격
			if(nCheckCount == 0) {
				is_successProblem[m_nCurrentTestProblemCount - 1] = true;
				m_nTrueResCount++;
			}else{
				is_successProblem[m_nCurrentTestProblemCount - 1] = false;
				m_nFalseResCount++;
			}


			nCheckCount = 0;
			m_arrIvTrueFalseMsg[nIndex-1].setImageResource(R.drawable.st_test_case_correct);
			//m_arrBtnResult[nIndex-1].setBackgroundResource(R.drawable.test_word_true);
			//m_arrIvTrueFalseMsg[nIndex-1].setSelected(true);
			//m_arrIvWordNum[nIndex-1].setImageResource(R.drawable.true_num1 + nIndex-1);

			StudyTestActivity.playWordVoice("success");

			setTestResult();


			new Handler() {
				public void handleMessage(Message msg) {
					m_nTestTime = 0;
					m_bResultPressed = false;
					m_bProblemView = false;
				}
			}.sendEmptyMessageDelayed(0, 1200);

		}
		else {	//불합격
			//setTestResult();
			if(m_nTestCount != 0)
				insertWrongWord(m_arrWordData.get(m_nTestCount-1).getId());

			m_nTestCount --;
			m_nCurrentTestProblemCount --;
			nCheckCount ++;

			new Handler() {
				public void handleMessage(Message msg) {
					m_nTestTime = 0;
					m_bResultPressed = false;
					m_bProblemView = false;
				}
			}.sendEmptyMessageDelayed(0, 1200);

			StudyTestActivity.playWordVoice("fail");

			/*is_successProblem[m_nCurrentTestProblemCount-1] = false;

			m_nFalseResCount ++;
			m_arrIvTrueFalseMsg[nIndex-1].setImageResource(R.drawable.st_test_case_wrong);
			m_arrIvTrueFalseMsg[m_nTrueIndex].setImageResource(R.drawable.st_test_case_correct);

			//m_arrBtnResult[nIndex-1].setBackgroundResource(R.drawable.test_word_false);
			//m_arrIvTrueFalseMsg[nIndex-1].setSelected(false);
			StudyTestActivity.playWordVoice("fail");
			animSelectTrueWord();*/
			
			//틀린 단어 단어장 추가

		}
		

		
	}
		
	//옳은 단어 지정 애니메이션
	private void animSelectTrueWord() {
		Thread thread = new Thread() {
			private boolean m_bVisible = false;
			private int m_nCount = 0;
			private boolean m_bStop = false;
			
			private Handler m_handler = new Handler() {
				public void handleMessage(Message msg) {
					if(m_nTrueIndex == -1) return;
					
					m_bVisible = !m_bVisible;
					if(m_bVisible) {
						m_arrIvTrueFalseMsg[m_nTrueIndex].setVisibility(View.VISIBLE);
						m_nCount ++;
					}
					else
						m_arrIvTrueFalseMsg[m_nTrueIndex].setVisibility(View.INVISIBLE);
					
					if(m_nCount > 4) {
						m_bStop = true;
						m_arrIvTrueFalseMsg[m_nTrueIndex].setVisibility(View.VISIBLE);
					}
				}
			};
			
			public void run() {
				while(!m_bStop && !m_bTestStop) {
					try{
						Thread.sleep(150);
					} catch(Exception e) {

					}
					
					m_nCount ++;
					m_handler.sendEmptyMessage(0);
				}
			}
		};
		
		//if(m_nTrueIndex != -1) m_arrIvTrueFalseMsg[m_nTrueIndex].setImageResource(R.drawable.true_msg2);
		thread.start();
	}
	
	//지정한 단어가 맞는가 검사
	private boolean isTrueWord(int nResIndex) {
		boolean bTrue = (nResIndex == m_nTrueIndex) ? true : false;
		return bTrue;
	}

	//시험결과 현시
	private void setTestResult() {
		Log.i("test result", "test result");

		m_nTestCurrentProblemIndex++;
		m_tvTrueWords.setText(Integer.toString(m_nTrueResCount));
		m_tvFalseWords.setText(Integer.toString(m_nFalseResCount));
		m_tvTotalWords.setText(Integer.toString(m_nCurrentTestProblemCount) + "/" 
									+ Integer.toString(m_nTotalTestProblemCount));
	}
	
	//시험전 초기화
	private void setTestInit() {
		for(int i=0; i<4; i++) {
			m_arrIvTrueFalseMsg[i].setVisibility(View.INVISIBLE);
			m_arrIvTrueFalseMsg[i].setImageResource(R.drawable.st_test_case_wrong);
			//m_arrIvWordNum[i].setImageResource(R.drawable.false_num1 + i);
			m_arrTextWordNum[i].setTextColor(getResources().getColor(R.color.app_color_green));
			m_arrBtnResult[i].setBackgroundResource(R.drawable.st_test_item_normal_bg);
		}
	}
	
	//답 구성
	protected void makeResWords() {
		////옳은 답의 단추순서
		ArrayList<Integer> arrResIndex = new ArrayList<Integer>();
		arrResIndex.add(-1);
		
		int nRes = ESUtils.getRandomValue(4, arrResIndex);	
		m_strEngRes[nRes] = m_arrWordData.get(m_nTestCount).getMeanEn();
		arrResIndex.add(nRes);
		
		m_nTrueIndex = nRes;
		
		ArrayList<Integer> arrIndex = new ArrayList<Integer>();
		arrIndex.add(m_nTestCount);
		
		for(int i=0; i<3; i++) {
			int nWrong = ESUtils.getRandomValue(m_arrWordData.size(), arrIndex);	//틀린 답의 인덱스
			nRes = ESUtils.getRandomValue(4, arrResIndex);	//틀린 답의 단추순서
			
			m_strEngRes[nRes] = m_arrWordData.get(nWrong).getMeanEn();
			
			arrResIndex.add(nRes);
			arrIndex.add(nWrong);
		}
		
		m_tvKorProblem.setText(m_arrWordData.get(m_nTestCount).getMeanKr());
		
		for(int i=0; i<4; i++){
			String text_ = getadjustedText(m_strEngRes[i]);
			m_arrBtnResult[i].setText(text_);
		}

		PAKDBManager pMangager = new PAKDBManager(m_context);
		String strVideoPath = pMangager.getFileName( m_arrWordData.get(m_nTestCount).getId(), 2);
		//String strVideoPath =String.format("%s%d.mp4", Define.VIDEO_PATH,m_arrWordData.get(m_nTestCount).getId());
		easyPlayer.setSource(Uri.parse(strVideoPath));
		easyPlayer.setLoop(true);

	}

	EasyVideoCallback callback = new EasyVideoCallback() {
		@Override
		public void onStarted(EasyVideoPlayer player) {
			//easyPlayer.setVisibility(View.VISIBLE);
		}

		@Override
		public void onPaused(EasyVideoPlayer player) {

		}

		@Override
		public void onPreparing(EasyVideoPlayer player) {

		}

		@Override
		public void onPrepared(EasyVideoPlayer player) {
			//player.start();
			Handler mainHandler = new Handler(m_context.getMainLooper());

			Runnable myRunnable = new Runnable() {
				@Override
				public void run() {
					framePlayerMask.setVisibility(View.INVISIBLE);
				} // This is your code
			};
			mainHandler.post(myRunnable);

		}

		@Override
		public void onBuffering(int percent) {

		}

		@Override
		public void onError(EasyVideoPlayer player, Exception e) {

		}

		@Override
		public void onCompletion(EasyVideoPlayer player) {

		}

		@Override
		public void onRetry(EasyVideoPlayer player, Uri source) {

		}

		@Override
		public void onSubmit(EasyVideoPlayer player, Uri source) {

		}
	};
	
	public String getadjustedText(String textstring){
		String temp_string1 = "";
		String temp_string2 = "";

		/*textstring =  "          " + textstring;
		if (textstring.length()>80) {
			temp_string2 = textstring.substring(0, 80);
			textstring    =textstring.replace(temp_string2, temp_string2+"\n" + "          ");
			temp_string1 = textstring.substring(0, 40);
			temp_string2 = textstring.replace(temp_string1, temp_string1+"\n" + "          ");
			textstring = temp_string2;
		}else if (textstring.length()>40) {
			temp_string1 = textstring.substring(0, 40);
			temp_string2 = textstring.replace(temp_string1, temp_string1+"\n" + "          ");
			textstring = temp_string2;
		}
		return textstring;*/

		textstring =  "" + textstring;
		if (textstring.length()>90) {
			temp_string2 = textstring.substring(0, 90);
			textstring    =textstring.replace(temp_string2, temp_string2+"\n" + "");
			temp_string1 = textstring.substring(0, 45);
			temp_string2 = textstring.replace(temp_string1, temp_string1+"\n" + "");
			textstring = temp_string2;
		}else if (textstring.length()>45) {
			temp_string1 = textstring.substring(0, 45);
			temp_string2 = textstring.replace(temp_string1, temp_string1+"\n" + "");
			textstring = temp_string2;
		}
		return textstring;
	}

	private long m_nStartStudyTime = 0;

	//조선어-영어 시험
	@SuppressLint({ "HandlerLeak", "NewApi" })
	public void performKorEngTest() {
		m_nStartStudyTime = ESUtils.getNowTimeSeconds();

		m_threadTest = new Thread() {
			private Handler m_handler = new Handler() {
				public void handleMessage(Message msg) {
					if(m_arrWordData == null) return;
					
					Log.i("message called", "message called");
					
					switch(msg.what) {
					case 0:
						if(m_nTestCount > m_tvTotalProblems-1) {	//모든 시험을 다 쳤다면
							m_bTestStop = true;		
							m_bResultPressed = true;
							
							new Handler() {
								public void handleMessage(Message msg) {
									m_bResultPressed = false;

									long nEndStudyTime = ESUtils.getNowTimeSeconds();
									int nDiff = (int)(nEndStudyTime - m_nStartStudyTime);
									long nCurTime = ESUtils.getNowDateMilliseconds();
									int nYear = ESUtils.getDateOfYear(nCurTime);
									int nMonth = ESUtils.getDateOfMonth(nCurTime);
									int nDate = ESUtils.getDateOfDay(nCurTime);
									long nRegTime = ESUtils.getDateMilliseconds(nYear, nMonth - 1, nDate);
									m_progressDbMana.updateProgressTime(nRegTime, nDiff);

									m_listener.onAfter(m_nCurrentTestProblemCount, m_nTrueResCount, m_nFalseResCount,m_nCurrentTestProblemCount,is_successProblem,m_arrAllWordData);
								}
							}.sendEmptyMessageDelayed(0, 300);
							break;
						}
						else {
							is_enable_thread = false;
							//문제 제시
							makeResWords();
							
							Log.i("Test Count", String.valueOf("Test Count:"+m_nTestCount));
							
							m_nTestCount ++;
							m_nCurrentTestProblemCount++;
							
							setTestInit();
							
							m_bProblemView = true;
						}
						
						setTestResult();
						
						break;
					case 1:
						
						if (!is_enable_thread) {
							is_enable_thread = true;
							m_nFalseResCount ++;						
							//틀린 문제 결과 설정
							is_successProblem[m_nCurrentTestProblemCount-1] = false;
							//틀린 단어 단어장 추가
							StudyTestActivity.playWordVoice("fail");
							if(m_nTestCount != 0) insertWrongWord(m_arrWordData.get(m_nTestCount-1).getId());
							//animSelectTrueWord();
							
							Log.i("repead", "repeat");
							
							m_bResultPressed = true;
							new Handler() {
								public void handleMessage(Message msg) {
									m_nTestTime = 0;	
									m_bResultPressed = false;
									m_bProblemView = false;
								}
							}.sendEmptyMessageDelayed(0, 1200);
						}

						break;
					}
					
				}
			};
			
			@SuppressLint("NewApi")
			public void run() {
				while(!m_bTestStop) {
					pauseThread();
					sleepResultPressed();
					
					if(m_nTestTime == 0) m_handler.sendEmptyMessage(0);	//시험문제 제시
					
					m_nTestTime += 20;
					
					/*if(m_nTestTime >= m_nTestLimitTime*1000) {
						m_nTestTime = m_nTestLimitTime*1000;
						
						m_handler.sendEmptyMessage(1);
					}
					
					m_progressbar.setProgress(100 * m_nTestTime / (m_nTestLimitTime *1000));
					*/
					try {
						Thread.sleep(20);
					}
					catch(Exception e) {
						e.printStackTrace();
					}
				}
			}
		};
		
		getTestProblems(Define.KIND_TEST_VTT);
		m_threadTest.setName("KorEngTestThread");
		m_threadTest.setDaemon(true);
		m_threadTest.start();
	}
	
	//답을 선택한후 1초있다가 다음 문제를 제시
	private void sleepResultPressed() {
		while(m_bResultPressed) {
			try {
				Thread.sleep(20);
			}
			catch(Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public void finishTest() {
		if(m_threadTest != null && m_threadTest.isAlive()) {
			m_bTestStop = true;
			try {
				m_threadTest.join();
			}
			catch(Exception e) {} 
		}
		
		m_arrBtnResult = null;
		m_arrIvTrueFalseMsg = null;
		//m_arrIvWordNum = null;
		m_arrTextWordNum = null;
		m_strEngRes = null;
		
		super.finishTest();
	}

	public void doNext(){
		is_successProblem[m_nCurrentTestProblemCount - 1] = false;
		m_nFalseResCount++;

		nCheckCount = 0;

		StudyTestActivity.playWordVoice("fail");

		setTestResult();

		new Handler() {
			public void handleMessage(Message msg) {
				m_nTestTime = 0;
				m_bResultPressed = false;
				m_bProblemView = false;
			}
		}.sendEmptyMessageDelayed(0, 100);
	}

	public void setListener(OnKorEngTestFinishListener listener) {
		m_listener = listener;
	}
	
	public abstract interface OnKorEngTestFinishListener {
		public abstract void onAfter(int nTestTotalCount, int nTrueCount, int nFalseCount,int m_nTestCurrentProblemIndex,Boolean[] is_successProblem_,ArrayList<WordData> m_arrAllWordData_);
	}
}
