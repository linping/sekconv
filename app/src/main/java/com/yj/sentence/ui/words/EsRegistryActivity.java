package com.yj.sentence.ui.words;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.yj.sentence.R;
import com.yj.sentence.common.Define;
import com.yj.sentence.database.UserDBManager;
import com.yj.sentence.database.WordpadDBManager;
import com.yj.sentence.structs.WordData;
import com.yj.sentence.structs.WordpadData;
import com.yj.sentence.ui.studding.study.StudyWordpadPlayActivity;
import com.yj.sentence.utils.ESUtils;


import org.json.JSONArray;

import java.util.ArrayList;

/**
 * 
 * @author RWI
 * 2015.6.28
 * 단어장
 */

public class EsRegistryActivity extends Activity implements View.OnClickListener {


	private int mUId = -1;     // 사용자ID

	private Button btnHome;
	private Button btnPrev;
	private Button btnNext;
	private Button btnDeleteAll;
	private Button btnCheckAll;

	private FrameLayout frameBottomBar;
	private ImageButton btnPlay;
	private ImageButton btnDelete;

	private ImageButton btnIndiator[] = new ImageButton[7];

	private TextView textLevel;
    private TextView textNoSentence;

	private ListView listSetences;
	private SentenceAdapter sAdapter;

	private UserDBManager mUserDbMana;
	private WordpadDBManager mDbMana;
	private ArrayList<WordpadData> mDataList;
	public static ArrayList<WordData> mPlayDataList;
	public ArrayList<WordpadData> mPlayList;
	private int TAG_UNCHECKED = 0;
	private int TAG_CHECKED = 1;

	int nCurrentLevel = 1;

	//private ArrayList<String> mDeleteList;

	private boolean m_bAllChecked = false;

	private static int NUMBER_EPISODES = 9;
	private Button[] btnEpisodes = new Button[NUMBER_EPISODES];

	private int nSelectedEpisode = 1;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_registry);
		
		Intent intent = this.getIntent();
		mUId = intent.getIntExtra(Define.USER_ID, -1);
		m_bAllChecked = false;

		if(!openDB()) {
			finish();
		}
		initViews();
		initData();

	}
	
	private void initViews() {
		listSetences = (ListView) findViewById(R.id.listSentences);

		btnHome = (Button) findViewById(R.id.btnHome);
		btnPrev = (Button) findViewById(R.id.btnPrevLevel);
		btnNext = (Button) findViewById(R.id.btnNextLevel);
		btnDeleteAll = (Button) findViewById(R.id.btnAllDelete);
		btnCheckAll = (Button) findViewById(R.id.btnAllCheck);

		btnPrev.setBackgroundResource(R.drawable.btn_registry_left_disable);
		btnNext.setBackgroundResource(R.drawable.btn_registry_right);

		btnHome.setOnClickListener(this);
		btnPrev.setOnClickListener(this);
		btnNext.setOnClickListener(this);
		btnDeleteAll.setOnClickListener(this);
		btnCheckAll.setOnClickListener(this);

		btnCheckAll.setTag(TAG_UNCHECKED);
		textLevel = (TextView) findViewById(R.id.textLevel);
		textNoSentence = (TextView) findViewById(R.id.textNoSentences);

		textNoSentence.setVisibility(View.GONE);
		ESUtils.setTextViewTypeFaceByRes(this, textLevel, Define.getMainFont());
		ESUtils.setTextViewTypeFaceByRes(this, textNoSentence, Define.getMainFont());
		ESUtils.setTextViewTypeFaceByRes(this, (TextView) findViewById(R.id.textAllSelect), Define.getMainFont());

		frameBottomBar = (FrameLayout) findViewById(R.id.frameBottomBar);
		btnPlay = (ImageButton) findViewById(R.id.btnRegistryPlay);
		btnDelete = (ImageButton) findViewById(R.id.btnRegistryDelete);
		btnPlay.setOnClickListener(this);
		btnDelete.setOnClickListener(this);

		btnEpisodes[0] = (Button) findViewById(R.id.btnEpisode1);
		btnEpisodes[1] = (Button) findViewById(R.id.btnEpisode2);
		btnEpisodes[2] = (Button) findViewById(R.id.btnEpisode3);
		btnEpisodes[3] = (Button) findViewById(R.id.btnEpisode4);
		btnEpisodes[4] = (Button) findViewById(R.id.btnEpisode5);
		btnEpisodes[5] = (Button) findViewById(R.id.btnEpisode6);
		btnEpisodes[6] = (Button) findViewById(R.id.btnEpisode7);
		btnEpisodes[7] = (Button) findViewById(R.id.btnEpisode8);
		btnEpisodes[8] = (Button) findViewById(R.id.btnEpisode9);

		for(int i = 0; i < NUMBER_EPISODES; i++){
			btnEpisodes[i].setOnClickListener(m_EpisodeListener);
			btnEpisodes[i].setTag(false);
			btnEpisodes[i].setBackgroundResource(R.drawable.st_registry_stage_back_unselected);
			btnEpisodes[i].setTextColor(getResources().getColor(R.color.Gray));

			ESUtils.setButtonTypeFaceByRes(this, btnEpisodes[i], Define.getMainFont());
		}

		btnEpisodes[0].setTag(true);
		btnEpisodes[0].setBackgroundResource(R.drawable.st_registry_stage_back_selected);
		btnEpisodes[0].setTextColor(getResources().getColor(R.color.White));

		btnIndiator[0] = (ImageButton) findViewById(R.id.imageIndicate1);
		btnIndiator[1] = (ImageButton) findViewById(R.id.imageIndicate2);
		btnIndiator[2] = (ImageButton) findViewById(R.id.imageIndicate3);
		btnIndiator[3] = (ImageButton) findViewById(R.id.imageIndicate4);
		btnIndiator[4] = (ImageButton) findViewById(R.id.imageIndicate5);
		btnIndiator[5] = (ImageButton) findViewById(R.id.imageIndicate6);
		btnIndiator[6] = (ImageButton) findViewById(R.id.imageIndicate7);
		setIndicator(0);

	}

	public void setIndicator(final int nIndex){
		for(int i =0; i<7; i++){
			btnIndiator[i].setImageResource(R.drawable.st_registry_circle_gray);
		}

		btnIndiator[nIndex].setImageResource(R.drawable.st_registry_circle_orange);
	}


	private void addPlayList(int nIndex){
		WordpadData wpData = mDataList.get(nIndex);
		mPlayList.add(wpData);
	}

	private void deletePlayList(int nIndex){
		WordpadData wpData = mDataList.get(nIndex);
		for(int i = 0; i < mPlayList.size(); i++){
			WordpadData iData = mPlayList.get(i);
			if(iData.getId() == wpData.getId()){
				mPlayList.remove(i);
				return;
			}
		}
	}

	private boolean isAlreadySeleted(int nIndex){
		WordpadData wpData = mDataList.get(nIndex);
		for(int i = 0; i < mPlayList.size(); i++){
			WordpadData iData = mPlayList.get(i);
			if(iData.getId() == wpData.getId()){
				return true;
			}
		}

		return false;
	}

	private void initData(){
		mPlayList = new ArrayList<WordpadData>();
		mPlayDataList = new ArrayList<WordData>();
		mDataList = mDbMana.getWordListBySort3(mUId, nCurrentLevel, nSelectedEpisode);

		if(mDataList.size() == 0){
			textNoSentence.setVisibility(View.VISIBLE);
		}else {
			textNoSentence.setVisibility(View.GONE);
		}

		sAdapter = new SentenceAdapter(this, m_Handler, mUId);
		listSetences.setAdapter(sAdapter);
		listSetences.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

				ESUtils.playBtnSound(EsRegistryActivity.this);
				if(isAlreadySeleted(position)){
					deletePlayList(position);
				}else{
					addPlayList(position);
				}

				if(mPlayList.size() == 0){
					frameBottomBar.setVisibility(View.INVISIBLE);
					btnCheckAll.setTag(TAG_UNCHECKED);
					btnCheckAll.setBackgroundResource(R.drawable.st_registry_item_unchecked);
				}else {
					frameBottomBar.setVisibility(View.VISIBLE);
				}

				if(mPlayList.size() == mDataList.size()){
					btnCheckAll.setTag(TAG_CHECKED);
					btnCheckAll.setBackgroundResource(R.drawable.st_registry_item_checked);
				}
				sAdapter.notifyDataSetChanged();

				/*mPlayList.clear();
				for(int i = position; i < mDataList.size(); i++){
					WordpadData wpData = mDataList.get(i);
					WordData wData = wpData.getWord();

					mPlayList.add(wData);
				}

				for(int i = 0; i < position; i++){
					WordpadData wpData = mDataList.get(i);
					WordData wData = wpData.getWord();

					mPlayList.add(wData);
				}

				Intent intent = new Intent(EsRegistryActivity.this, StudyWordpadPlayActivity.class);
				startActivity(intent);*/

			}
		});

	}

	private boolean openDB() {
		if (mDbMana == null) 
			mDbMana = new WordpadDBManager(this);
		
		if (mDbMana == null)
			return false;
		
		if (mUserDbMana == null)
			mUserDbMana = new UserDBManager(this);

		if (mUserDbMana == null)
			return false;
		
		return true;
	}
	
	private void closeDB() {
		if (mDbMana != null) {
			mDbMana.close();
			mDbMana = null;
		}
		
		if (mUserDbMana != null) {
			mUserDbMana.close();
			mUserDbMana = null;
		}
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		//initListData();
		
		super.onResume();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		closeDB();
		
		super.onDestroy();
	}

	private View.OnClickListener m_EpisodeListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			ESUtils.playBtnSound(EsRegistryActivity.this);
			switch(v.getId()){
				case R.id.btnEpisode1:
					nSelectedEpisode = 1;
					break;
				case R.id.btnEpisode2:
					nSelectedEpisode = 2;
					break;
				case R.id.btnEpisode3:
					nSelectedEpisode = 3;
					break;
				case R.id.btnEpisode4:
					nSelectedEpisode = 4;
					break;
				case R.id.btnEpisode5:
					nSelectedEpisode = 5;
					break;
				case R.id.btnEpisode6:
					nSelectedEpisode = 6;
					break;
				case R.id.btnEpisode7:
					nSelectedEpisode = 7;
					break;
				case R.id.btnEpisode8:
					nSelectedEpisode = 8;
					break;
				case R.id.btnEpisode9:
					nSelectedEpisode = 9;
					break;
				default:
					break;
			}

			/*if((Boolean) btnEpisodes[nSelectedLevel - 1].getTag() == true){
				btnEpisodes[nSelectedLevel - 1].setTag(false);
				btnEpisodes[nSelectedLevel - 1].setBackgroundResource(R.drawable.st_registry_stage_back_unselected);
			}else{
				btnEpisodes[nSelectedLevel - 1].setTag(true);
				btnEpisodes[nSelectedLevel - 1].setBackgroundResource(R.drawable.st_registry_stage_back_selected);
			}*/


			for(int i = 0; i < NUMBER_EPISODES; i++){
				btnEpisodes[i].setTag(false);
				btnEpisodes[i].setBackgroundResource(R.drawable.st_registry_stage_back_unselected);
				btnEpisodes[i].setTextColor(getResources().getColor(R.color.Gray));

			}

			btnEpisodes[nSelectedEpisode - 1].setTag(true);
			btnEpisodes[nSelectedEpisode - 1].setBackgroundResource(R.drawable.st_registry_stage_back_selected);
			btnEpisodes[nSelectedEpisode - 1].setTextColor(getResources().getColor(R.color.White));

			mDataList = mDbMana.getWordListBySort3(mUId, nCurrentLevel, nSelectedEpisode);
			if(mDataList.size() == 0){
				textNoSentence.setVisibility(View.VISIBLE);
			}else {
				textNoSentence.setVisibility(View.GONE);
			}

			mPlayList.clear();
			sAdapter.notifyDataSetChanged();
		}
	};


	@Override
	public void onClick(View v) {
		ESUtils.playBtnSound(this);

		switch (v.getId()){
			case R.id.btnHome:
				goHome();
				break;
			case R.id.btnPrevLevel:
				prevLevel();
				break;
			case R.id.btnNextLevel:
				nextLevel();
				break;
			case R.id.btnAllCheck:
				checkAll();
				break;
			case R.id.btnAllDelete:
				//deleteAll();
				break;
			case R.id.btnRegistryPlay:
				registryPlay();
				break;
			case R.id.btnRegistryDelete:
				registryDelete();
				break;
			default:
				break;
		}
	}

	private void makeWordPlayDataList(){

		mPlayDataList.clear();
		for(int i = 0; i<mPlayList.size(); i++){
			WordpadData data = mPlayList.get(i);
			mPlayDataList.add(data.getWord());
		}
	}
	private void registryPlay(){

		//mPlayDataList = new ArrayList<WordData>(mPlayList)  ;
		makeWordPlayDataList();

		Intent intent = new Intent(EsRegistryActivity.this, StudyWordpadPlayActivity.class);
		startActivity(intent);

		mPlayList.clear();
		frameBottomBar.setVisibility(View.INVISIBLE);
		btnCheckAll.setTag(TAG_UNCHECKED);
		btnCheckAll.setBackgroundResource(R.drawable.st_registry_item_unchecked);
		sAdapter.notifyDataSetChanged();


	}

	private void registryDelete(){
		for(int i = 0; i< mPlayList.size(); i++){
			WordpadData data = mPlayList.get(i);
			int wId = data.getId();
			mDbMana.Delete(wId);
		}

		mDataList = mDbMana.getWordListBySort2(mUId, nCurrentLevel);
		if(mDataList.size() == 0){
			textNoSentence.setVisibility(View.VISIBLE);
		}else {
			textNoSentence.setVisibility(View.GONE);
		}
		mPlayList.clear();
		frameBottomBar.setVisibility(View.VISIBLE);
		btnCheckAll.setTag(TAG_UNCHECKED);
		btnCheckAll.setBackgroundResource(R.drawable.st_registry_item_unchecked);
		sAdapter.notifyDataSetChanged();
	}

	private void goHome(){
		finish();
	}

	private void prevLevel(){
		if(nCurrentLevel > 1){
			nCurrentLevel --;

			setIndicator(nCurrentLevel - 1);

			m_bAllChecked = false;

			String strLevel = String.format(getResources().getString(R.string.string_format_level), nCurrentLevel);
			textLevel.setText(strLevel);

			mDataList = mDbMana.getWordListBySort3(mUId, nCurrentLevel, nSelectedEpisode);
			if(mDataList.size() == 0){
				textNoSentence.setVisibility(View.VISIBLE);
			}else {
				textNoSentence.setVisibility(View.GONE);
			}

			mPlayList.clear();

			sAdapter.notifyDataSetChanged();

			if(nCurrentLevel == 1){
				btnPrev.setBackgroundResource(R.drawable.btn_registry_left_disable);
			}

			if(nCurrentLevel == 6){
				btnNext.setBackgroundResource(R.drawable.btn_registry_right);
			}

			btnCheckAll.setTag(TAG_UNCHECKED);
			btnCheckAll.setBackgroundResource(R.drawable.st_registry_item_unchecked);
			btnDeleteAll.setVisibility(View.INVISIBLE);
			frameBottomBar.setVisibility(View.INVISIBLE);

		}
	}

	private void nextLevel(){
		if(nCurrentLevel < 7){
			nCurrentLevel ++;

			setIndicator(nCurrentLevel - 1);

			m_bAllChecked = false;
			String strLevel = String.format(getResources().getString(R.string.string_format_level), nCurrentLevel);
			textLevel.setText(strLevel);

			mDataList = mDbMana.getWordListBySort3(mUId, nCurrentLevel, nSelectedEpisode);
			if(mDataList.size() == 0){
				textNoSentence.setVisibility(View.VISIBLE);
			}else {
				textNoSentence.setVisibility(View.GONE);
			}

			mPlayList.clear();

			sAdapter.notifyDataSetChanged();

			if(nCurrentLevel == 2){
				btnPrev.setBackgroundResource(R.drawable.btn_registry_left);
			}

			if(nCurrentLevel == 7){
				btnNext.setBackgroundResource(R.drawable.btn_registry_right_disable);
			}

			btnCheckAll.setTag(TAG_UNCHECKED);
			btnCheckAll.setBackgroundResource(R.drawable.st_registry_item_unchecked);
			btnDeleteAll.setVisibility(View.INVISIBLE);
			frameBottomBar.setVisibility(View.INVISIBLE);
		}
	}

	/*private void deleteAll(){
		for ( int i = 0; i < mDeleteList.size(); i++){
			String strId = mDeleteList.get(i);
			mDbMana.Delete(Integer.parseInt(strId));
		}
		mDeleteList.clear();

		mDataList = mDbMana.getWordListBySort2(mUId, nCurrentLevel);
		sAdapter.notifyDataSetChanged();

		btnDeleteAll.setVisibility(View.INVISIBLE);
	}*/

	private void checkAll(){
		if((int)btnCheckAll.getTag() == TAG_UNCHECKED){
			btnCheckAll.setTag(TAG_CHECKED);
			btnCheckAll.setBackgroundResource(R.drawable.st_registry_item_checked);
			//btnDeleteAll.setVisibility(View.VISIBLE);
			frameBottomBar.setVisibility(View.VISIBLE);
			m_bAllChecked = true;

			//mDeleteList.clear();
			mPlayList.clear();
			for(int i = 0; i < mDataList.size(); i++){
				WordpadData wpData = mDataList.get(i);
				int nIndex = wpData.getId();
				String strIndex = String.format("%d", nIndex);
				mPlayList.add(wpData);
				//mDeleteList.add(strIndex);
			}

			sAdapter.notifyDataSetChanged();


		}else {
			btnCheckAll.setTag(TAG_UNCHECKED);
			btnCheckAll.setBackgroundResource(R.drawable.st_registry_item_unchecked);
			//btnDeleteAll.setVisibility(View.INVISIBLE);
			frameBottomBar.setVisibility(View.INVISIBLE);
			m_bAllChecked = false;
			//mDeleteList.clear();
			mPlayList.clear();
			sAdapter.notifyDataSetChanged();
		}

		if(mPlayList.size() == 0){
			frameBottomBar.setVisibility(View.INVISIBLE);
		}else {
			frameBottomBar.setVisibility(View.VISIBLE);
		}
	}

	Handler m_Handler = new Handler(Looper.getMainLooper()) {
		public void handleMessage(Message msg) {

			int nParam = msg.what;

			String strParam = String.format("%d", nParam);

			/*if(mDeleteList.contains(strParam)){
				mDeleteList.remove(strParam);
				if(mDeleteList.size() == 0){
					//btnDeleteAll.setVisibility(View.INVISIBLE);
					frameBottomBar.setVisibility(View.INVISIBLE);
				}
			} else {
				mDeleteList.add(strParam);
				//btnDeleteAll.setVisibility(View.VISIBLE);
				frameBottomBar.setVisibility(View.VISIBLE);
			}
*/
			/*mDbMana.Delete(nParam);
			mDataList = mDbMana.getWordListBySort2(mUId, nCurrentLevel);
			sAdapter.notifyDataSetChanged();*/

			/*int nData = msg.what;
			int episodeNum = nData / 9;
			int stageNum = nData % 9;
			setStageInfo(episodeNum, stageNum);*/
		}
	};


	public class SentenceAdapter extends BaseAdapter {

		private Context m_context;
		private LayoutInflater m_inflater;
		private Handler m_handler;
		private int m_nUserId = -1;

		public SentenceAdapter(Context ctx, Handler handler, int nUserId) {

			m_context = ctx;
			m_inflater = (LayoutInflater)m_context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			m_handler = handler;
			m_nUserId = nUserId;
		}

		@Override
		public int getCount() {
			return mDataList.size();
		}

		@Override
		public Object getItem(int position) {
			return null;
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View row = convertView;

			//if(row == null)
			{
				//row = m_inflater.inflate(R.layout.progress_item, parent, false);
				WordpadData wpData = mDataList.get(position);

				SentenceView viewSentence = new SentenceView(m_context);
				viewSentence.setHandler(m_handler);
				viewSentence.setUserId(m_nUserId);
				viewSentence.setWordpadData(wpData);
				//viewSentence.setSelected(m_bAllChecked);

				boolean bSelected = isAlreadySeleted(position);
				viewSentence.setSelected(bSelected);

				row = viewSentence;
			}

			return row;
		}
	}
}
