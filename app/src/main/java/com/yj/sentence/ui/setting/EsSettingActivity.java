package com.yj.sentence.ui.setting;

import com.yj.sentence.R;
import com.yj.sentence.common.Define;
import com.yj.sentence.database.SettingDBManager;
import com.yj.sentence.database.UserDBManager;
import com.yj.sentence.ui.studding.SekWordPopupOneButton;
import com.yj.sentence.ui.studding.SekWordPopupOneButton.OnSEKLaunchListener;
import com.yj.sentence.utils.ESUtils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

public class EsSettingActivity extends Activity {
	
	private String[] mStudy ;
	private String[] mColor ;
	private String[] mRecognition;
	private String[] mFontSize;
	private String[] mTestTime;
	private String[] mStudySpeed;
	
	private int mUId;
	
	private SettingPopupRepeat settingPopupRepeat = null;
	private SettingPopupRecognition settingPopupRecognition = null;
	private SettingPopupFontSize settingPopupFontSize = null;
	private SettingPopupBackground settingPopupBackground = null;
	private SettingPopupBrightness settingPopupBrightness = null;
	private SettingPopupVolume settingPopupVolume = null;
	private SettingPopupSpeed settingPopupSpeed = null;

	private ImageButton btnRepeat;
	private ImageButton btnBackground;
	private ImageButton btnRecognition;
	private ImageButton btnFontSize;
	private ImageButton btnBrightness;
	private ImageButton btnVolume;
	private ImageButton btnSpeed;

	private TextView textRepeat;
	private TextView textBackground;
	private TextView textRecognition;
	private TextView textFontSize;
	private TextView textBrightness;
	private TextView textVolume;
	private TextView textSpeed;


	private UserDBManager mUserDbMana = null;
	private SettingDBManager mDbMana = null;
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_setting);
		
		Intent intent = this.getIntent();
		mUId = intent.getIntExtra(Define.USER_ID, -1);
		
		if (openDB()) {
			int value = (mUId!=-1) ? mUserDbMana.getIsUseSettingHelp(mUId) : 1;
			if (value == 1) {
				/*new AlertDialog.Builder(this)
				.setTitle(R.string.msgTitle)
				.setMessage(R.string.settingMsg)
				.setCancelable(false)
				.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
				
					public void onClick(DialogInterface dialog, int which) {	
						if (mUId!=-1)
					}
				})
				.show();*/
				SekWordPopupOneButton popup;
				mUserDbMana.setNoUseSettingHelp(mUId);

				popup = new SekWordPopupOneButton(EsSettingActivity.this);
				popup.setMessage(this.getString(R.string.settingMsg));
				//popup.setMessageSize(15);
				popup.setListener(new OnSEKLaunchListener() {

					@Override
					public void onOK() {
						if (mUId!=-1)
							mUserDbMana.setNoUseSettingHelp(mUId);
					}
				});
				popup.show();
			}
		}
		
		
		initViews();
	} 
	
	private void initViews() {
		initVariables();
    	initButtons();
    	setButtonInfo();
    }
	
	private void initVariables() {
		mStudy = new String[]{ this.getString(R.string.setting_study1), 
				this.getString(R.string.setting_study2), 
				this.getString(R.string.setting_study3), 
				this.getString(R.string.setting_study4), 
				this.getString(R.string.setting_study5)};
		
		mColor = new String[]{ this.getString(R.string.setting_color1), 
				this.getString(R.string.setting_color2), 
				this.getString(R.string.setting_color3), 
				this.getString(R.string.setting_color4), 
				this.getString(R.string.setting_color5)};

		mRecognition = new String[] {
				this.getString(R.string.setting_recognition1),
				this.getString(R.string.setting_recognition2),
				this.getString(R.string.setting_recognition3)
		};

		mFontSize = new String[] {
				this.getString(R.string.setting_fontsize1),
				this.getString(R.string.setting_fontsize2),
				this.getString(R.string.setting_fontsize3)
		};

		mTestTime = new String[]{this.getString(R.string.setting_time1), 
				  this.getString(R.string.setting_time2), 
				  this.getString(R.string.setting_time3)};
		
		mStudySpeed = new String[]{this.getString(R.string.setting_speed1), 
					this.getString(R.string.setting_speed2), 
					this.getString(R.string.setting_speed3), 
					this.getString(R.string.setting_speed4), 
					this.getString(R.string.setting_speed5)};
	}
	
	private void initButtons() {

		textRepeat = (TextView) findViewById(R.id.textRepeat);
		textBackground = (TextView) findViewById(R.id.textBackground);
		textRecognition = (TextView) findViewById(R.id.textRecognition);
		textFontSize = (TextView) findViewById(R.id.textFontSize);
		textBrightness = (TextView) findViewById(R.id.textBrightness);
		textVolume = (TextView) findViewById(R.id.textVolume);
		textSpeed = (TextView) findViewById(R.id.textStudySpeed);

		TextView textRepeatDesc = (TextView) findViewById(R.id.textRepeatDesc);
		TextView textBackgroundDesc = (TextView) findViewById(R.id.textBackgroundDesc);
		TextView textRecognitionDesc = (TextView) findViewById(R.id.textRecognitionDesc);
		TextView textFontSizeDesc = (TextView) findViewById(R.id.textFontSizeDesc);
		TextView textBrightnessDesc = (TextView) findViewById(R.id.textBrightnessDesc);
		TextView textVolumeDesc = (TextView) findViewById(R.id.textVolumeDesc);
		TextView textSpeedDesc = (TextView) findViewById(R.id.textStudySpeedDesc);

		ESUtils.setTextViewTypeFaceByRes(this, textRepeat, Define.getMainFont());
		ESUtils.setTextViewTypeFaceByRes(this, textBackground, Define.getMainFont());
		ESUtils.setTextViewTypeFaceByRes(this, textRecognition, Define.getMainFont());
		ESUtils.setTextViewTypeFaceByRes(this, textFontSize, Define.getMainFont());
		ESUtils.setTextViewTypeFaceByRes(this, textBrightness, Define.getMainFont());
		ESUtils.setTextViewTypeFaceByRes(this, textVolume, Define.getMainFont());
		ESUtils.setTextViewTypeFaceByRes(this, textSpeed, Define.getMainFont());
		ESUtils.setTextViewTypeFaceByRes(this, textRepeatDesc, Define.getMainFont());
		ESUtils.setTextViewTypeFaceByRes(this, textBackgroundDesc, Define.getMainFont());
		ESUtils.setTextViewTypeFaceByRes(this, textRecognitionDesc, Define.getMainFont());
		ESUtils.setTextViewTypeFaceByRes(this, textFontSizeDesc, Define.getMainFont());
		ESUtils.setTextViewTypeFaceByRes(this, textBrightnessDesc, Define.getMainFont());
		ESUtils.setTextViewTypeFaceByRes(this, textVolumeDesc, Define.getMainFont());
		ESUtils.setTextViewTypeFaceByRes(this, textSpeedDesc, Define.getMainFont());

		btnRepeat = (ImageButton) findViewById(R.id.btnRepeat);
		btnBackground = (ImageButton) findViewById(R.id.btnBackground);
		btnRecognition = (ImageButton) findViewById(R.id.btnRecognition);
		btnFontSize = (ImageButton) findViewById(R.id.btnFontSize);
		btnBrightness = (ImageButton) findViewById(R.id.btnBrightness);
		btnVolume = (ImageButton) findViewById(R.id.btnVolume);
		btnSpeed = (ImageButton) findViewById(R.id.btnStudySpeed);

		btnRepeat.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				ESUtils.playBtnSound(EsSettingActivity.this);

				if (settingPopupRepeat == null)
					settingPopupRepeat = new SettingPopupRepeat(EsSettingActivity.this);
				settingPopupRepeat.initValues();
				settingPopupRepeat.setOkListener(new SettingPopupRepeat.OnOkListener() {
					@Override
					public void onOK() {
						setButtonInfo();
					}
				});
				settingPopupRepeat.show();
			}
		});

		btnBackground.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				ESUtils.playBtnSound(EsSettingActivity.this);

				if (settingPopupBackground == null)
					settingPopupBackground = new SettingPopupBackground(EsSettingActivity.this);
				settingPopupBackground.initValues();
				settingPopupBackground.setOkListener(new SettingPopupBackground.OnOkListener() {
					@Override
					public void onOK() {
						setButtonInfo();
					}
				});
				settingPopupBackground.show();
			}
		});

		btnRecognition.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				ESUtils.playBtnSound(EsSettingActivity.this);

				if (settingPopupRecognition == null)
					settingPopupRecognition = new SettingPopupRecognition(EsSettingActivity.this);
				settingPopupRecognition.initValues();
				settingPopupRecognition.setOkListener(new SettingPopupRecognition.OnOkListener() {
					@Override
					public void onOK() {
						setButtonInfo();
					}
				});
				settingPopupRecognition.show();
			}
		});

		btnFontSize.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				ESUtils.playBtnSound(EsSettingActivity.this);

				if (settingPopupFontSize == null)
					settingPopupFontSize = new SettingPopupFontSize(EsSettingActivity.this);
				settingPopupFontSize.initValues();
				settingPopupFontSize.setOkListener(new SettingPopupFontSize.OnOkListener() {
					@Override
					public void onOK() {
						setButtonInfo();
					}
				});
				settingPopupFontSize.show();
			}
		});

		btnSpeed.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				ESUtils.playBtnSound(EsSettingActivity.this);

				if (settingPopupSpeed == null)
					settingPopupSpeed = new SettingPopupSpeed(EsSettingActivity.this);
				settingPopupSpeed.initValues();
				settingPopupSpeed.setOkListener(new SettingPopupSpeed.OnOkListener() {
					@Override
					public void onOK() {
						setButtonInfo();
					}
				});
				settingPopupSpeed.show();
			}
		});

		btnBrightness.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				ESUtils.playBtnSound(EsSettingActivity.this);

				if (settingPopupBrightness == null)
					settingPopupBrightness = new SettingPopupBrightness(EsSettingActivity.this);
				settingPopupBrightness.initValues();
				settingPopupBrightness.setOkListener(new SettingPopupBrightness.OnOkListener() {
					@Override
					public void onOK() {
						setButtonInfo();
					}
				});
				settingPopupBrightness.show();
			}
		});

		btnVolume.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				ESUtils.playBtnSound(EsSettingActivity.this);

				if (settingPopupVolume == null)
					settingPopupVolume = new SettingPopupVolume(EsSettingActivity.this);
				settingPopupVolume.initValues();
				settingPopupVolume.setOkListener(new SettingPopupVolume.OnOkListener() {
					@Override
					public void onOK() {
						setButtonInfo();
					}
				});
				settingPopupVolume.show();
			}
		});

		ImageButton btnExit = (ImageButton)findViewById(R.id.btnBack);
		btnExit.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				ESUtils.playBtnSound(EsSettingActivity.this);

				EsSettingActivity.this.finish();
			}
		});

	}
	
	private void setButtonInfo() {
		if (openDB()) {
			textRepeat.setText(mStudy[mDbMana.getStudy1Index()]);
			textBackground.setText(mColor[mDbMana.getWordColorIndex()]);
			textRecognition.setText(mRecognition[mDbMana.getRecognitionIndex()]);
			textFontSize.setText(mFontSize[mDbMana.getFontSizeIndex()]);
			textSpeed.setText(mStudySpeed[mDbMana.getStudySpeedIndex()]);

			int curBrightnessValue = android.provider.Settings.System.getInt(getContentResolver(), android.provider.Settings.System.SCREEN_BRIGHTNESS,-1);
			int nPercent = curBrightnessValue * 100 /255;

			textBrightness.setText(String.format("%d", nPercent) + "%");

			AudioManager audio = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
			int currentVolume = audio.getStreamVolume(AudioManager.STREAM_MUSIC);
			int nVolumePercent = currentVolume * 100 /15;
			textVolume.setText(String.format("%d", nVolumePercent) + "%");


			/*btnSetting1.setText(this.getString(R.string.btnsetting_pref1)+mStudy[mDbMana.getStudy1Index()]);
			btnSetting2.setText(this.getString(R.string.btnsetting_pref2)+mStudy[mDbMana.getRecognitionIndex()]);
			btnSetting3.setText(this.getString(R.string.btnsetting_pref3)+mColor[mDbMana.getWordColorIndex()]);
			btnSetting4.setText(this.getString(R.string.btnsetting_pref4)+mColor[mDbMana.getFontSizeIndex()]);
			btnSetting5.setText(this.getString(R.string.btnsetting_pref5)+mTestTime[mDbMana.getTestTimeIndex()]);
			btnSetting7.setText(this.getString(R.string.btnsetting_pref7)+mStudySpeed[mDbMana.getStudySpeedIndex()]);*/
		}
	}
	
	private boolean openDB() {
		if (mDbMana == null)
			mDbMana = new SettingDBManager(this);
		
		if (mDbMana == null)
			return false;
		
		if (mUserDbMana == null)
			mUserDbMana = new UserDBManager(this);
		
		if (mUserDbMana == null)
			return false;
		
		
		return true;
	}
	
	private void closeDB() {
		if (mDbMana != null) {
			mDbMana.close();
			mDbMana = null;
		}
		
		if (mUserDbMana != null) {
			mUserDbMana.close();
			mUserDbMana = null;
		}
	}
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		this.closeDB();		
		super.onDestroy();
	}
}
