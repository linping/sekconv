package com.yj.sentence.ui.studding;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.BounceInterpolator;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.yj.sentence.R;
import com.yj.sentence.common.Define;
import com.yj.sentence.database.PAKDBManager;
import com.yj.sentence.database.ProgressDBManager;
import com.yj.sentence.structs.EpisodeData;
import com.yj.sentence.structs.ProgressData;
import com.yj.sentence.structs.StageData;
import com.yj.sentence.ui.studding.SekWordPopup.OnSEKLaunchListener;
import com.yj.sentence.ui.studding.adapter.ListAdapter;
import com.yj.sentence.ui.studding.study.StudyBasicActivity;
import com.yj.sentence.ui.studding.study.StudyPracticeActivity;
import com.yj.sentence.ui.studding.study.StudyWordActivity;
import com.yj.sentence.ui.studding.test.TestSelectActivity;
import com.yj.sentence.ui.studyresult.EsStudyResultActivity;
import com.yj.sentence.utils.ESUtils;

import java.io.File;
import java.util.ArrayList;

@SuppressLint("HandlerLeak")
public class EsStuddingActivity extends Activity implements View.OnClickListener {

	private final int TAB_SELECT_ORDER = 3;
	private final int TAB_VIEW_SCHEDULE = 2;
	private final int TAB_RESULT_VIEW = 1;
	private final int TAB_BACK = 0;

	private int m_nTabIndex = TAB_SELECT_ORDER;

	private FrameLayout m_layoutTab;

	private Button[] m_btnSelectTabs = new Button[4];
	private Button[] m_btnDefaultTabs = new Button[4];

	private FrameLayout m_layoutSelectOrder;
	private FrameLayout m_layoutSchedule;

	private int m_nOrder = 0;
	private int m_nUserId = -1;

	private ProgressDBManager m_progressDbMana = null;
	private PAKDBManager m_pakManager = null;

	private ScheduleViewReceiver m_receiver = null;


	private ImageView[] imgViews = new ImageView[7];
	private FrameLayout[] frameViews = new FrameLayout[7];
	private ImageView[] imgTriViews = new ImageView[7];

	private ImageView[] imgStart = new ImageView[7];
	private ImageButton[] btnLStart = new ImageButton[7];
	private int m_nLevel = -1;
	private TextView textLevel;

	private TextView textEpisode;
	private TextView textStage;
	private TextView textInfo1;
	private TextView textInfo2;
	private TextView textDesc1;
	private TextView textDesc2;

	private ImageView imginfo1;
	private ImageView imginfo2;
	private ImageView imgDesc1;
	private ImageView imgDesc2;

	private ImageView imageStage;
	ListView listItems;

	private FrameLayout m_frameLoading;

	int m_nSelectedEpisode = 1;
	int m_nSelectedStage = 1;

	ListAdapter mAdpater;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_studding);

		Intent intent = this.getIntent();
		m_nUserId = intent.getIntExtra(Define.USER_ID, -1);

		m_frameLoading = (FrameLayout) findViewById(R.id.frameLoading);
		m_frameLoading.setVisibility(View.VISIBLE);

		new BackgroundTask().execute();
	}

	@Override
	protected void onResume() {
		super.onResume();

		if(mAdpater != null){
			mAdpater.notifyDataSetChanged();
		}
	}

	private void initViews(){

		imgViews[0] = (ImageView)	findViewById(R.id.imgLevel1);
		imgViews[1] = (ImageView)	findViewById(R.id.imgLevel2);
		imgViews[2] = (ImageView)	findViewById(R.id.imgLevel3);
		imgViews[3] = (ImageView)	findViewById(R.id.imgLevel4);
		imgViews[4] = (ImageView)	findViewById(R.id.imgLevel5);
		imgViews[5] = (ImageView)	findViewById(R.id.imgLevel6);
		imgViews[6] = (ImageView)	findViewById(R.id.imgLevel7);

		frameViews[0] = (FrameLayout) findViewById(R.id.frameLevel1);
		frameViews[1] = (FrameLayout) findViewById(R.id.frameLevel2);
		frameViews[2] = (FrameLayout) findViewById(R.id.frameLevel3);
		frameViews[3] = (FrameLayout) findViewById(R.id.frameLevel4);
		frameViews[4] = (FrameLayout) findViewById(R.id.frameLevel5);
		frameViews[5] = (FrameLayout) findViewById(R.id.frameLevel6);
		frameViews[6] = (FrameLayout) findViewById(R.id.frameLevel7);

		imgTriViews[0] = (ImageView) findViewById(R.id.imgTriangle1);
		imgTriViews[1] = (ImageView) findViewById(R.id.imgTriangle2);
		imgTriViews[2] = (ImageView) findViewById(R.id.imgTriangle3);
		imgTriViews[3] = (ImageView) findViewById(R.id.imgTriangle4);
		imgTriViews[4] = (ImageView) findViewById(R.id.imgTriangle5);
		imgTriViews[5] = (ImageView) findViewById(R.id.imgTriangle6);
		imgTriViews[6] = (ImageView) findViewById(R.id.imgTriangle7);

		imgStart[0] = (ImageView) findViewById(R.id.imgStart1);
		imgStart[1] = (ImageView) findViewById(R.id.imgStart2);
		imgStart[2] = (ImageView) findViewById(R.id.imgStart3);
		imgStart[3] = (ImageView) findViewById(R.id.imgStart4);
		imgStart[4] = (ImageView) findViewById(R.id.imgStart5);
		imgStart[5] = (ImageView) findViewById(R.id.imgStart6);
		imgStart[6] = (ImageView) findViewById(R.id.imgStart7);

		btnLStart[0] = (ImageButton) findViewById(R.id.btnLStart1);
		btnLStart[1] = (ImageButton) findViewById(R.id.btnLStart2);
		btnLStart[2] = (ImageButton) findViewById(R.id.btnLStart3);
		btnLStart[3] = (ImageButton) findViewById(R.id.btnLStart4);
		btnLStart[4] = (ImageButton) findViewById(R.id.btnLStart5);
		btnLStart[5] = (ImageButton) findViewById(R.id.btnLStart6);
		btnLStart[6] = (ImageButton) findViewById(R.id.btnLStart7);

		for(int i = 0; i < 7 ; i++){
			imgViews[i].setOnClickListener(m_LevelViewClickListener);
			btnLStart[i].setOnClickListener(m_orederBtnClickListener);
		}

		ProgressData firstData = getNext(1, 0);
		m_nSelectedEpisode = firstData.getEpisode();
		m_nSelectedStage = firstData.getStage();

		listItems = (ListView) findViewById(R.id.listItems);
		mAdpater = new ListAdapter(this,m_stageStartHandler,m_nUserId);
		mAdpater.setInfo(m_nSelectedEpisode, m_nSelectedStage);
		listItems.setAdapter(mAdpater);

		textLevel = (TextView) findViewById(R.id.textLevel);
		textEpisode = (TextView) findViewById(R.id.textEpisode);
		textStage = (TextView) findViewById(R.id.textStage);
		textInfo1 = (TextView) findViewById(R.id.textInfo1);
		textInfo2 = (TextView) findViewById(R.id.textInfo2);
		textDesc1 = (TextView) findViewById(R.id.textDesc1);
		textDesc2 = (TextView) findViewById(R.id.textDesc2);

		textDesc1.setLetterSpacing(-0.05f);
		textDesc2.setLetterSpacing(-0.05f);
		textInfo1.setLetterSpacing(-0.05f);
		textInfo2.setLetterSpacing(-0.05f);

		ESUtils.setTextViewTypeFaceByRes(this, textLevel, Define.getMainFont());
		ESUtils.setTextViewTypeFaceByRes(this, textEpisode, Define.getMainFont());
		ESUtils.setTextViewTypeFaceByRes(this, textStage, Define.getMainFont());
		ESUtils.setTextViewTypeFaceByRes(this, textInfo1, Define.getMainFont());
		ESUtils.setTextViewTypeFaceByRes(this, textInfo2, Define.getMainFont());
		ESUtils.setTextViewTypeFaceByRes(this, textDesc1, Define.getMainFont());
		ESUtils.setTextViewTypeFaceByRes(this, textDesc2, Define.getMainFont());
		ESUtils.setTextViewTypeFaceByRes(this, (TextView) findViewById(R.id.textSelect), Define.getMainFont());
		ESUtils.setTextViewTypeFaceByRes(this,  (TextView) findViewById(R.id.textSelectDesc), Define.getMainFont());
		ESUtils.setTextViewTypeFaceByRes(this,  (TextView) findViewById(R.id.textLevelDesc), Define.getMainFont());

		ESUtils.setTextViewTypeFaceByRes(this,  (TextView) findViewById(R.id.textLevel1), Define.getMainFont());
		ESUtils.setTextViewTypeFaceByRes(this,  (TextView) findViewById(R.id.textLevel2), Define.getMainFont());
		ESUtils.setTextViewTypeFaceByRes(this,  (TextView) findViewById(R.id.textLevel3), Define.getMainFont());
		ESUtils.setTextViewTypeFaceByRes(this,  (TextView) findViewById(R.id.textLevel4), Define.getMainFont());
		ESUtils.setTextViewTypeFaceByRes(this,  (TextView) findViewById(R.id.textLevel5), Define.getMainFont());
		ESUtils.setTextViewTypeFaceByRes(this,  (TextView) findViewById(R.id.textLevel6), Define.getMainFont());
		ESUtils.setTextViewTypeFaceByRes(this,  (TextView) findViewById(R.id.textLevel7), Define.getMainFont());

		imginfo1 = (ImageView) findViewById(R.id.imgInfo1);
		imginfo2 = (ImageView) findViewById(R.id.imgInfo2);
		imgDesc1 = (ImageView) findViewById(R.id.imgDesc1);
		imgDesc2 = (ImageView) findViewById(R.id.imgDesc2);

		imageStage = (ImageView) findViewById(R.id.imageStage);
	}

	private ProgressData getNext(int nEpisode, int nStage){
		for (int i = nEpisode; i <= 9; i++){
			for(int j = 1; j <= 4; j++){
				if(i == nEpisode && j <= nStage) continue;
				ProgressData pData = new ProgressData(m_nUserId, m_progressDbMana.getLevel(), i, j);
				boolean b = m_progressDbMana.isAvailableData(pData);
				if(b == false){
					ProgressData rData = new ProgressData(m_nUserId, m_progressDbMana.getLevel(), i, j);
					return rData;
				}
			}
		}

		for(int i = 1; i < nEpisode; i++){
			for(int j = 1; j <= 4; j++){
				ProgressData pData = new ProgressData(m_nUserId, m_progressDbMana.getLevel(), i, j);
				boolean b = m_progressDbMana.isAvailableData(pData);
				if(b == false){
					ProgressData rData = new ProgressData(m_nUserId, m_progressDbMana.getLevel(), i, j);
					return rData;
				}
			}
		}

		return new ProgressData(m_nUserId, m_progressDbMana.getLevel(), 1, 1);
	}

	private void initTabBar() {
		m_layoutTab = (FrameLayout)findViewById(R.id.TabLayout);

		m_btnSelectTabs[3] = (Button)findViewById(R.id.ivTabSelectOrderOff);
		m_btnSelectTabs[2] = (Button)findViewById(R.id.ivTabScheduleOff);
		m_btnSelectTabs[1] = (Button)findViewById(R.id.ivTabResultOff);
		m_btnSelectTabs[0] = (Button)findViewById(R.id.ivTabBackOff);

		m_btnDefaultTabs[3] = (Button)findViewById(R.id.ivTabSelectOrderOn);
		m_btnDefaultTabs[2] = (Button)findViewById(R.id.ivTabScheduleOn);
		m_btnDefaultTabs[1] = (Button)findViewById(R.id.ivTabResultOn);
		m_btnDefaultTabs[0] = (Button)findViewById(R.id.ivTabBackOn);

		for(int i=0; i<4; i++) {
			m_btnSelectTabs[i].setVisibility(View.GONE);
			m_btnDefaultTabs[i].setOnClickListener(this);
		}

		Handler handler = new Handler() {
			public void handleMessage(Message msg) {
				showOnFocusAnimation(m_nTabIndex, 10);
			}
		};
		handler.sendEmptyMessageDelayed(0, 10);
	}

	private void initTabLayouts() {
		m_layoutSelectOrder = (FrameLayout)findViewById(R.id.select_order_layout);
		m_layoutSchedule = (FrameLayout)findViewById(R.id.SchduleViewLayout);

		setTabLayout(m_nTabIndex);
	}

	private void showLooseFocusAinimation(final int index) {
		m_btnSelectTabs[index].setVisibility(View.GONE);
		m_btnDefaultTabs[index].setVisibility(View.VISIBLE);
	}

	private void showOnFocusAnimation(final int index, int duration) {
		m_btnDefaultTabs[index].setVisibility(View.GONE);
		m_btnSelectTabs[index].setVisibility(View.VISIBLE);
	}

	private View.OnClickListener m_testStartListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			/*Intent intent = new Intent(EsStuddingActivity.this, TestSelectActivity.class);
			startActivity(intent);*/
			ESUtils.playBtnSound(EsStuddingActivity.this);
			goStudyTestActivity();
		}
	};

	private void startLevelPrepare(int pLevel){

		int nSelected = m_nLevel;

		for (int i = 0 ; i < 7 ; i++){
			if(nSelected == i){
				int imgWidth = imgViews[i].getWidth();
				int frameWidth = frameViews[i].getWidth();

				float scale = (float) (frameWidth - imgWidth / 2) * 2/ (float) imgWidth;

				AnimatorSet set = new AnimatorSet();
				set.playTogether(
						ObjectAnimator.ofFloat(imgViews[i], "scaleX", 1.0f).setDuration(1500),
						ObjectAnimator.ofFloat(imgTriViews[i], "X", imgTriViews[i].getLeft()).setDuration(1500),
						ObjectAnimator.ofObject(imgViews[i], "backgroundColor", new ArgbEvaluator(),/*Red*/0xfffaa602, /*Blue*/0xFFFBC00B).setDuration(1500),
						ObjectAnimator.ofObject(imgTriViews[i], "backgroundColor", new ArgbEvaluator(),/*Red*/0xfffaa602, /*Blue*/0xFFFBC00B).setDuration(1500)
				);
				set.start();

				ObjectAnimator fadeAnim = ObjectAnimator.ofFloat(btnLStart[nSelected], "alpha", 0.0f).setDuration(1500);

				fadeAnim.start();

			} else 	if ( i == pLevel) {
				m_nLevel = pLevel;
				int imgWidth = imgViews[i].getWidth();
				int frameWidth = frameViews[i].getWidth();
				int triWidth = imgTriViews[i].getWidth();

				float scale = (float) (frameWidth - triWidth - imgWidth / 2) * 2 / (float) imgWidth;

				AnimatorSet set = new AnimatorSet();

				ObjectAnimator anim = ObjectAnimator.ofFloat(imgViews[i], "scaleX", scale).setDuration(1500);
				anim.addListener(new AnimatorListenerAdapter() {
					@Override
					public void onAnimationEnd(Animator animation) {
						super.onAnimationEnd(animation);

						btnLStart[m_nLevel].setAlpha(0.0f);
						btnLStart[m_nLevel].setVisibility(View.VISIBLE);
						ObjectAnimator fadeAnim = ObjectAnimator.ofFloat(btnLStart[m_nLevel], "alpha", 1.0f).setDuration(1500);
						fadeAnim.setInterpolator(new BounceInterpolator());
						fadeAnim.start();
					}
				});
				anim.setInterpolator(new AccelerateInterpolator());


				ObjectAnimator trimAnim = ObjectAnimator.ofFloat(imgTriViews[i], "X", frameWidth - triWidth).setDuration(1500);
				trimAnim.setInterpolator(new AccelerateInterpolator());
				set.playTogether(
						anim,
						trimAnim,
						ObjectAnimator.ofObject(imgViews[i], "backgroundColor", new ArgbEvaluator(),/*Red*/0xFFFBC00B, /* Blue*/0xfffaa602).setDuration(1500),
						ObjectAnimator.ofObject(imgTriViews[i], "backgroundColor", new ArgbEvaluator(),/*Red*/0xFFFBC00B, /*Blue*/0xfffaa602).setDuration(1500)
				);
				set.start();
			}
		}
	}
	int nLevel = 0;

	private View.OnClickListener m_LevelViewClickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			ESUtils.playBtnSound(EsStuddingActivity.this);

			nLevel = 0;
			switch (v.getId()){
				case R.id.imgLevel1:
					nLevel = 0;
					break;
				case R.id.imgLevel2:
					nLevel = 1;
					break;
				case R.id.imgLevel3:
					nLevel = 2;
					break;
				case R.id.imgLevel4:
					nLevel = 3;
					break;
				case R.id.imgLevel5:
					nLevel = 4;
					break;
				case R.id.imgLevel6:
					nLevel = 5;
					break;
				case R.id.imgLevel7:
					nLevel = 6;
					break;
			}

			if(m_nLevel == nLevel) return;

			startLevelPrepare(nLevel);

			/*if(m_nLevel >=  0){
				String strDescription = String.format("%d%s", m_nLevel, getString(R.string.string_level_start));
				SekConvTwoButtonPopup popup;
				popup = new SekConvTwoButtonPopup(EsStuddingActivity.this);
				popup.setInfo(strDescription);
				popup.setListener(new SekConvTwoButtonPopup.OnSEKLaunchListener() {
					@Override
					public void onOK() {
						startLevelPrepare(nLevel);
					}
				});
				popup.show();
			} else{
				startLevelPrepare(nLevel);
			}*/

		}
	};


	private class BackgroundTask extends AsyncTask<Void, Void, Void>
	{

		private boolean isConnected = true;
		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			try {
				Thread.sleep(1);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if(!isConnectDB()){
				isConnected = false;
				finish();
			}

			return null;
		}

	    @Override
	    protected void onPostExecute(Void result) {
	        super.onPostExecute(result);


	        if(isConnected == false)
	        	return;
	        //this method will be running on UI thread

			initTabBar();
			initTabLayouts();
			initViews();
			/*
			initTestButton();
			initScheduleMakeDlg();
			initScheduleLayout();
*/

			if(m_progressDbMana.getLevel() > 0)
			{
				m_nTabIndex = TAB_VIEW_SCHEDULE;
				setTabLayout(m_nTabIndex);

				setLevelInfo();
			}

			registerReceiver();
			m_frameLoading.setVisibility(View.INVISIBLE);

	    }


	}

	private void setLevelInfo(){
		int nLevel =  m_progressDbMana.getLevel();
		String strLevel = String.format("%d급", nLevel);
		textLevel.setText(strLevel);

		imginfo1.setVisibility(View.VISIBLE);
		imginfo2.setVisibility(View.INVISIBLE);
		imgDesc1.setVisibility(View.VISIBLE);
		imgDesc2.setVisibility(View.VISIBLE);

		textInfo2.setVisibility(View.INVISIBLE);
		//set episode info
		EpisodeData epiData = m_progressDbMana.getEpisodeData(nLevel, 1);
		if(epiData != null){
			String strDesc1 = epiData.getStrDesc1();
			String strDesc2 = epiData.getStrDesc2();

			textDesc1.setText(strDesc1);
			textDesc2.setText(strDesc2);

		}

		//set stage info
		StageData stgData = m_progressDbMana.getStageData(nLevel, 1, 1);
		if(stgData != null){
			String strInfo1 = stgData.getStrDesc1();
			String strInfo2 = stgData.getStrDesc2();

			textInfo1.setText(strInfo1);
			textInfo2.setText(strInfo2);

			PAKDBManager pakManager = new PAKDBManager(this);
			String strImagePath = pakManager.getFileName(stgData.getStageImage(), 0);

			//String strImagePath = String.format("%s%d.png", Define.IMAGE_PATH, stgData.getStageImage());
			File imgFile = new  File(strImagePath);

			if(imgFile.exists()){
				Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
				imageStage.setImageBitmap(myBitmap);
			}

		}

		setCurrentProgress(1,0);
	}
	private void finishActivity() {
		for(int i=0; i<4; i++) {
			//m_layoutTabMenus[i] = null;
			m_btnSelectTabs[i] = null;
			m_btnDefaultTabs[i] = null;
		}

		//m_layoutTabMenus = null;
		m_btnSelectTabs = null;
		m_btnDefaultTabs = null;

		unregisterReceiver();

		closeDB();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		finishActivity();
		super.onDestroy();
		System.gc();
	}



	@Override
	public void onClick(View v) {
		boolean is_enabled_animation = true;
		ESUtils.playBtnSound(this);

		switch(v.getId()) {
		case R.id.ivTabSelectOrderOn:
			break;
		case R.id.ivTabScheduleOn:
			break;
		case R.id.ivTabResultOn:
			is_enabled_animation = false;
			goResultActivity();
			break;
		case R.id.ivTabBackOn:
			break;
		}

		if (is_enabled_animation) {
			// TODO Auto-generated method stub
			showLooseFocusAinimation(m_nTabIndex);

			switch(v.getId()) {
			case R.id.ivTabSelectOrderOn:
				m_nTabIndex = TAB_SELECT_ORDER;
				break;
			case R.id.ivTabScheduleOn:
				m_nTabIndex = TAB_VIEW_SCHEDULE;
				break;
			case R.id.ivTabResultOn:
				m_nTabIndex = TAB_RESULT_VIEW;
				goResultActivity();
				break;
			case R.id.ivTabBackOn:
				m_nTabIndex = TAB_BACK;
				this.finish();
				break;
			}

			showOnFocusAnimation(m_nTabIndex, 150);
			setTabLayout(m_nTabIndex);
		}
	}

	public void setTabLayout(int nIndex) {
		if(nIndex == TAB_SELECT_ORDER) {
			m_layoutSelectOrder.setVisibility(View.VISIBLE);
			m_layoutSchedule.setVisibility(View.GONE);
		}
		else if(nIndex == TAB_VIEW_SCHEDULE){

			if(m_progressDbMana.getLevel() > 0){
				m_layoutSelectOrder.setVisibility(View.GONE);
				m_layoutSchedule.setVisibility(View.VISIBLE);
			} else {
				showDlgNoSchedule();
				return;
			}
		}
	}


	private void showDlgNoSchedule() {
		SekWordPopupOneButton popup;
		popup = new SekWordPopupOneButton(this);
		popup.setMessage(this.getString(R.string.noscheduledata));
		popup.setListener(new SekWordPopupOneButton.OnSEKLaunchListener() {
			
			@Override
			public void onOK() {
				// TODO Auto-generated method stub
				showLooseFocusAinimation(m_nTabIndex);
				m_nTabIndex = TAB_SELECT_ORDER;
				showOnFocusAnimation(m_nTabIndex, 150);
			}
		});
		
		popup.show();
	}
	

	
	private View.OnClickListener m_orederBtnClickListener = new View.OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub

			ESUtils.playBtnSound(EsStuddingActivity.this);

			for(int i = 0; i < 9; i++){
				if(v == btnLStart[i]){
					showSelectOrderDlg(i+1);
					break;
				}
			}
		}
	};
	
	private void showSelectOrderDlg(final int nOrder) {
		String strMsg = String.valueOf(nOrder) + this.getString(R.string.start_study_order);
		
		SekWordPopup popup;
		popup = new SekWordPopup(EsStuddingActivity.this);
		popup.setMessage(strMsg);
		popup.setListener(new OnSEKLaunchListener() {
			
			@Override
			public void onOK() {
				// TODO Auto-generated method stub
				setStudyOrderOfUser(nOrder);				
			}

			@Override
			public void onCancel() {
				// TODO Auto-generated method stub
				
			}
		});

		popup.show();
		
	}
	
	private void setStudyOrderOfUser(int nOrder) {
		m_nOrder = nOrder;
		
		/*if(m_progressDbMana.IsExistProgressData()){
			ProgressData data = new ProgressData(m_nUserId, m_nOrder, 1, 1);
			m_progressDbMana.updateProgress(data);
		} else {
			ProgressData data = new ProgressData(m_nUserId, m_nOrder, 1, 1);
			m_progressDbMana.createProgress(data);
		}*/
		m_progressDbMana.updateLevel(m_nOrder);

		//m_nLevel = m_nOrder + 1;

		setLevelInfo();

		showLooseFocusAinimation(m_nTabIndex);
		m_nTabIndex = TAB_VIEW_SCHEDULE;
		showOnFocusAnimation(m_nTabIndex, 150);
		setTabLayout(m_nTabIndex);

		mAdpater.notifyDataSetChanged();

	}
	


	private boolean isConnectDB() {
		m_progressDbMana = new ProgressDBManager(this, m_nUserId);
		if(m_progressDbMana == null)
			return false;
		
		return true;
	}
		
	private void closeDB() {
		if (m_progressDbMana != null){
			m_progressDbMana.close();
			m_progressDbMana = null;
		}
	}
	

	private void goStudyTestActivity() {
		/*
		Intent intent = new Intent(this, TestSelectActivity.class);
		intent.putExtra(Define.USER_ID, m_nUserId);
		intent.putExtra(Define.IS_ORDERTEST, true);	
		intent.putExtra(Define.SCHEDULE_ID, -1);
		startActivity(intent);
		*/

		Intent intent = new Intent(this, TestSelectActivity.class);
		intent.putExtra(Define.USER_ID, m_nUserId);
		intent.putExtra(Define.IS_ORDERTEST, false);
		intent.putExtra(Define.SCHEDULE_ID, 12);
		startActivity(intent);
	}
	
	private void goResultActivity() {
		Intent intent = new Intent(this, EsStudyResultActivity.class);
		intent.putExtra(Define.USER_ID, m_nUserId);
		startActivity(intent);
	}

	private void registerReceiver() {
		IntentFilter filter = new IntentFilter(Define.SCHEDULE_VIEW_ACTION);
		
		m_receiver = new ScheduleViewReceiver();
		registerReceiver(m_receiver, filter);
	}
	
	private void unregisterReceiver() {
		if(m_receiver != null)
			unregisterReceiver(m_receiver);
	}
	
	private class ScheduleViewReceiver extends BroadcastReceiver {
	   public void onReceive(Context context, Intent intent) {
	    	if (intent.getAction().equals(Define.SCHEDULE_VIEW_ACTION)) {
	    		int nEpisode = intent.getIntExtra(Define.STUDY_EPISODE, 1);
				int nStage = intent.getIntExtra(Define.STUDY_STAGE, 0);
	    		//setStudyOrderOfUser(m_nOrder);
				setCurrentProgress(nEpisode, nStage);
	    	}
	    }
	}

	private void setCurrentProgress(int nEpisode, int nStage){

		ProgressData nextData = getNext(nEpisode, nStage);

		m_nSelectedEpisode = nextData.getEpisode();
		m_nSelectedStage = nextData.getStage();
		mAdpater.setInfo(m_nSelectedEpisode, m_nSelectedStage);
		mAdpater.notifyDataSetChanged();
	}
	Handler m_stageStartHandler = new Handler(Looper.getMainLooper()) {
		public void handleMessage(Message msg) {
			int nData = msg.what;

			if(nData > 1000){

				nData = nData - 1000;
				int episodeNum = nData / 9;
				int stageNum = nData % 9;

				SekConvExercisePopup popup = new SekConvExercisePopup(EsStuddingActivity.this);
				popup.setInfo(m_nUserId, nData);

				final int finalNData = nData;
				popup.setListener(new SekConvExercisePopup.OnSEKLaunchListener(){

					@Override
					public void onOK(int nStep) {
						if(nStep == 0){
							Intent intent = new Intent(EsStuddingActivity.this, StudyWordActivity.class);
							intent.putExtra(Define.USER_ID, m_nUserId);
							intent.putExtra(Define.SCHEDULE_ID, finalNData);
							startActivity(intent);
						}else if(nStep == 1){
							Intent intent = new Intent(EsStuddingActivity.this, TestSelectActivity.class);
							intent.putExtra(Define.USER_ID, m_nUserId);
							intent.putExtra(Define.IS_ORDERTEST, false);
							intent.putExtra(Define.SCHEDULE_ID, finalNData);
							startActivity(intent);
						}else if(nStep == 2){
							Intent intent = new Intent(EsStuddingActivity.this, StudyPracticeActivity.class);
							intent.putExtra(Define.USER_ID, m_nUserId);
							intent.putExtra(Define.SCHEDULE_ID, finalNData);
							startActivity(intent);
							finish();
						}
					}
				});

				popup.show();
			} else {
				int episodeNum = nData / 9;
				int stageNum = nData % 9;

				setStageInfo(episodeNum, stageNum);

				m_nSelectedEpisode = episodeNum;
				m_nSelectedStage = stageNum;

				mAdpater.setInfo(m_nSelectedEpisode, m_nSelectedStage);
				mAdpater.notifyDataSetChanged();
			}

		}
	};

	private void setStageInfo(int nEpisodeNum, int nStageNum){

		imginfo1.setVisibility(View.VISIBLE);
		imginfo2.setVisibility(View.INVISIBLE);
		imgDesc1.setVisibility(View.VISIBLE);
		imgDesc2.setVisibility(View.VISIBLE);

		textInfo2.setVisibility(View.INVISIBLE);

		if(nStageNum > 4) {
			if(nStageNum == 5){
				String strEpisode = String.format("제%d장", nEpisodeNum);
				String strStage = String.format("발음", nStageNum);

				imginfo2.setVisibility(View.INVISIBLE);
				imgDesc2.setVisibility(View.INVISIBLE);

				textEpisode.setText(strEpisode);
				textStage.setText(strStage);
				textDesc2.setText("");
				textInfo2.setText("");

				textDesc1.setText("발음학습");
				int nLevel =  m_progressDbMana.getLevel();
				StageData stgData = m_progressDbMana.getStageData(nLevel, nEpisodeNum, nStageNum);
				if(stgData != null){
					String strInfo1 = stgData.getStgname();
					textInfo1.setText(strInfo1);
				}

			}else if(nStageNum == 5){
				String strEpisode = String.format("제%d장", nEpisodeNum);
				String strStage = String.format("복습", nStageNum);

				textEpisode.setText(strEpisode);
				textStage.setText(strStage);
				textDesc1.setText("");
				textDesc2.setText("");
				textInfo1.setText("");
				textInfo2.setText("");
			}
			return;
		}

		int nLevel =  m_progressDbMana.getLevel();
		String strLevel = String.format("%d급", nLevel);
		textLevel.setText(strLevel);

		String strEpisode = String.format("제%d장", nEpisodeNum);
		String strStage = String.format("제%d과", nStageNum);

		textEpisode.setText(strEpisode);
		textStage.setText(strStage);

		//set episode info
		EpisodeData epiData = m_progressDbMana.getEpisodeData(nLevel, nEpisodeNum);
		if(epiData != null){
			String strDesc1 = epiData.getStrDesc1();
			String strDesc2 = epiData.getStrDesc2();

			textDesc1.setText(strDesc1);
			textDesc2.setText(strDesc2);

		}

		//set stage info
		StageData stgData = m_progressDbMana.getStageData(nLevel, nEpisodeNum, nStageNum);
		if(stgData != null){
			String strInfo1 = stgData.getStrDesc1();
			String strInfo2 = stgData.getStrDesc2();

			textInfo1.setText(strInfo1);
			textInfo2.setText(strInfo2);

			PAKDBManager pakManager = new PAKDBManager(this);
			String strImagePath = pakManager.getFileName(stgData.getStageImage(), 0);
			//String strImagePath = String.format("%s%d.png", Define.IMAGE_PATH, stgData.getStageImage());
			File imgFile = new  File(strImagePath);

			if(imgFile.exists()){
				Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
				imageStage.setImageBitmap(myBitmap);
			}
		}
	}


}