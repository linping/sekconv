/*
   Copyright 2012 Harri Smatt

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

package com.yj.sentence.ui.studding.study;

import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.HashMap;

import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.opengles.GL10;

import com.yj.sentence.R;
import com.yj.sentence.common.Define;
import com.yj.sentence.structs.WordData;
import com.yj.sentence.ui.studding.SekConvStudyPopup;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.Paint.Align;
import android.graphics.Paint.Style;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.net.Uri;
import android.opengl.GLException;
import android.opengl.GLSurfaceView;
import android.os.Debug.MemoryInfo;
import android.os.Handler;
import android.os.Message;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

/**
 * OpenGL ES View.
 * 
 * @author harism
 */
public class CurlView extends GLSurfaceView implements View.OnTouchListener,
		CurlRenderer.Observer {

	/** Our Log tag */
	private final static String TAG = "PageCurlView";
	
	// Debug text paint stuff
	private Paint mTextPaint;
	private TextPaint mTextPaintShadow;
	
	/** Px / Draw call */
	private int mCurlSpeed;		//ì�´ë�™ê±°ë¦¬
	
	/** Fixed update time used to create a smooth curl animation */
	private int mUpdateRate;	//ì• ë‹ˆë©”ì�´ì…˜ íƒ€ìž„ì‹œê°„
	
	/** The initial offset for x and y axis movements */
	private int mInitialEdgeOffset;	//ì´ˆê¸° ì¢…ì�´ë��ë³€ë‘�ë¦¬í�¬ê¸°
	
	/** The mode we will use */
	private int mCurlMode;
	
	/** Simple curl mode. Curl target will move only in one axis. */
	public static final int CURLMODE_SIMPLE = 0;
	
	/** Dynamic curl mode. Curl target will move on both X and Y axis. */
	public static final int CURLMODE_DYNAMIC = 1;
	
	/** Enable/Disable debug mode */
	private boolean bEnableDebugMode = false;
	
	/** The context which owns us */
	private WeakReference<Context> mContext;
	
	/** Handler used to auto flip time based */
	private FlipAnimationHandler mAnimationHandler;
	
	/** Maximum radius a page can be flipped, by default it's the width of the view */
	private float mFlipRadius;
	
	/** Point used to move */
	private Vector2D mMovement;
	
	/** The finger position */
	private Vector2D mFinger;
	
	/** Movement point form the last frame */
	private Vector2D mOldMovement;
	
	/** Page curl edge */
	private Paint mCurlEdgePaint;
	
	/** Our points used to define the current clipping paths in our draw call */
	private Vector2D mA, mB, mC, mD, mE, mF, mOldF, mOrigin;
	
	/** Left and top offset to be applied when drawing */
	private int mCurrentLeft, mCurrentTop;
	
	/** If false no draw call has been done */
	private boolean bViewDrawn;
	
	/** Defines the flip direction that is currently considered */
	private boolean bFlipRight;		//íŽ¼ì¹˜ëŠ” ë°©í–¥ì�´ ì˜¤ë¥¸ìª½ì�´ë�¼ë©´ true, ì™¼ìª½ì�´ë�¼ë©´ false
	
	/** If TRUE we are currently auto-flipping */
	private boolean bFlipping;

	
	
	/** TRUE if the user moves the pages */
	private boolean bUserMoves;

	/** Used to control touch input blocking */
	private boolean bBlockTouchInput = false;	//í„°ì¹˜ì‚¬ê±´ì�„ ë°›ì�„ìˆ˜ ìžˆë‹¤ë©´ true, ì¦‰ í•œë²ˆì—� í•˜ë‚˜ì�˜ ì¢…ì�´ìž¥ë§Œ ë„˜ê¸¸ìˆ˜ ìžˆê²Œ í•˜ì˜€ë‹¤.
												//í˜„ìž¬ëŠ” í•œë²ˆì—� í•˜ë‚˜ì�˜ ì¢…ì�´ë§Œ ë„˜ê¸¸ìˆ˜ ìžˆê²Œ í•˜ì˜€ë‹¤.
	
	/** Enable input after the next draw event */
	private boolean bEnableInputAfterDraw = false;
	
	/** LAGACY The current foreground */
	private Bitmap mForeground;
	
	/** LAGACY The current background */
	private Bitmap mBackground;
	
	/** LAGACY List of pages, this is just temporal */
	private ArrayList<Bitmap> mPages;
	
	/** LAGACY Current selected page */
	private int mIndex = 0;
	
	////////////////////LJG////////////////////////
	private final Point EN_WORD_POINT = new Point(160, 140);
	private final Point KR_WORD_POINT = new Point(910 - 1024 + 1280 - 20, 140);
	private final Point REMEMER_IMG_POINT = new Point(910 - 1024 + 1280, 420);
	
	private Context m_context = null;
	private AssetManager m_assetMgr = null;
	private int m_nTargetWordCount = 0;
	private int m_nStudyOrder = 0;
	
	private int m_nWordBgColorEn = 0;
	private int m_nWordBgColorKr = 0;
	
	private int[] m_nStudyRepeat = null;
	private int m_nCurrentWordCount = 0;
	private int m_nCurrentRound = 0;
	private int m_nCurrentPhase = 1;
	private int m_nStudySpeed = 0;
	
	private Paint m_paintWord;
	private Paint m_paintLeftText, m_paintRightText;
	
	private ArrayList<WordData> m_arrStudyWords = null;
	
	private SoundPool m_soundPool = null;
	private HashMap<String, Integer> m_soundMap = new HashMap<String, Integer>();
	private float m_fVolume = 0.0f;
	
	private Paint m_paintPic = null;
	private ArrayList<Bitmap> m_arrStudyBmps = new ArrayList<Bitmap>();
	
	private Bitmap m_bmpRemberOn, m_bmpRemberOff;
	private RectF m_rcRemember;
	private boolean m_bRemberBtnPress = false;
	
	private Thread m_threadStudy = null;
	private boolean m_bStudyStop = false;
	private boolean m_bWordEnDraw = false;
	private boolean m_bWordKrDraw = false;
	private boolean m_bWordPicDraw = false;
	private boolean m_bShowNoticeDlg = false;
	
	private Thread m_threadTextBg = null;
	private boolean m_bDrawBgStopEn = false, m_bDrawBgStopKr = false;
	private int m_nTextBgRightEn = 0;
	private int m_nTextBgRightKr = 0;
	
	private boolean m_bStudyPause = false;
	private boolean m_bArrowBtnPressed = false;
	
	private int[] m_studyTimeList = {};
	private int m_nBgTimeSleep = 0;
	private int m_nBgWidthCount = 2;
	
	private StudyWordListener m_listener;

	// Curl state. We are flipping none, left or right page.
	private int Curl_Direction = 0;
	
	// Curl state. We are flipping none, left or right page.
	private static final int CURL_LEFT = 1;
	private static final int CURL_NONE = 0;
	private static final int CURL_RIGHT = 2;

	// Constants for mAnimationTargetEvent.
	private static final int SET_CURL_TO_LEFT = 1;
	private static final int SET_CURL_TO_RIGHT = 2;
	private static final int SET_CURL_TO_NONE = 3;

	// Shows one page at the center of view.
	public static final int SHOW_ONE_PAGE = 1;
	// Shows two pages side by side.
	public static final int SHOW_TWO_PAGES = 2;

	private boolean mAllowLastPageCurl = true;

	private boolean mAnimate = false;
	private long mAnimationDurationTime = 1000;
	private PointF mAnimationSource = new PointF();
	private long mAnimationStartTime;
	private PointF mAnimationTarget = new PointF();
	private int mAnimationTargetEvent;
	
	private int mAnimationTargetDirection;

	private PointF mCurlDir = new PointF();

	private PointF mCurlPos = new PointF();
	private int mCurlState = CURL_NONE;
	// Current bitmap index. This is always showed as front of right page.
	private int mCurrentIndex = 0;

	// Start position for dragging.
	private PointF mDragStartPos = new PointF();

	private boolean mEnableTouchPressure = false;
	// Bitmap size. These are updated from renderer once it's initialized.
	private int mPageBitmapHeight = -1;

	private int mPageBitmapWidth = -1;
	// Page meshes. Left and right meshes are 'static' while curl is used to
	// show page flipping.
	private CurlMesh mPageCurl;

	private CurlMesh mPageLeft;
	private PageProvider mPageProvider;
	private CurlMesh mPageRight;

	private PointerPosition mPointerPos = new PointerPosition();

	private CurlRenderer mRenderer;
	private boolean mRenderLeftPage = true;
	private SizeChangedObserver mSizeChangedObserver;

	// One page is the default.
	private int mViewMode = SHOW_ONE_PAGE;

	private StudyWordActivity m_Parent;

	
	Canvas canvasCurl;
	Bitmap bitmapCurl;
	
	/**
	 * Default constructor.
	 */
	public CurlView(Context ctx) {
		super(ctx);
		init(ctx);
		initialize(ctx);
		initStudyView(ctx);
		
		
	}
	////////////////////////////////////////////Study Curl View Functions
	private class Vector2D
	{
		public float x,y;
		public Vector2D(float x, float y)
		{
			this.x = x;
			this.y = y;
		}
		
		@Override
		public String toString() {
			// TODO Auto-generated method stub
			return "("+this.x+","+this.y+")";
		}
		
		 public float length() {
             return (float) Math.sqrt(x * x + y * y);
	     }
	
	     public float lengthSquared() {
	             return (x * x) + (y * y);
	     }
		
		public boolean equals(Object o) {
			if (o instanceof Vector2D) {
				Vector2D p = (Vector2D) o;
				return p.x == x && p.y == y;
	        }
	        return false;
		}
		
		public Vector2D reverse() {
			return new Vector2D(-x,-y);
		}
		
		public Vector2D sum(Vector2D b) {
            return new Vector2D(x+b.x,y+b.y);
		}
		
		public Vector2D sub(Vector2D b) {
            return new Vector2D(x-b.x,y-b.y);
		}		

		public float dot(Vector2D vec) {
            return (x * vec.x) + (y * vec.y);
		}

	    public float cross(Vector2D a, Vector2D b) {
	            return a.cross(b);
	    }
	
	    public float cross(Vector2D vec) {
	            return x * vec.y - y * vec.x;
	    }
	    
	    public float distanceSquared(Vector2D other) {
	    	float dx = other.x - x;
	    	float dy = other.y - y;

            return (dx * dx) + (dy * dy);
	    }
	
	    public float distance(Vector2D other) {
	            return (float) Math.sqrt(distanceSquared(other));
	    }
	    
	    public float dotProduct(Vector2D other) {
            return other.x * x + other.y * y;
	    }
		
		public Vector2D normalize() {
			float magnitude = (float) Math.sqrt(dotProduct(this));
            return new Vector2D(x / magnitude, y / magnitude);
		}
		
		public Vector2D mult(float scalar) {
	            return new Vector2D(x*scalar,y*scalar);
	    }
	}

	/**
	 * Inner class used to make a fixed timed animation of the curl effect.
	 */
	class FlipAnimationHandler extends Handler {
		@Override
		public void handleMessage(Message msg) {
			CurlView.this.FlipAnimationStep();
		}

		public void sleep(long millis) {
			this.removeMessages(0);
			sendMessageDelayed(obtainMessage(0), millis);
		}
	}
    
	/**
	 * Base
	 * @param context
	 */


	public void setParent(StudyWordActivity activity_conv){
		m_Parent = activity_conv;
	}
	private Handler m_handlerStudy = new Handler() {
		public void handleMessage(Message msg) {
			switch(msg.what) {
			case 0:		
				if(m_nCurrentPhase == 1) {
					m_bWordEnDraw = true;
					
					if(m_nWordBgColorEn != 0)
						startTextBgDrawEn();					
				}
				else if(m_nCurrentPhase == 2) {
					m_bWordKrDraw = true;
					
					if(m_nWordBgColorKr != 0)
						startTextBgDrawKr();
				}
				
				playWordVoice(m_arrStudyWords.get(m_nCurrentWordCount).getSound());
				m_Parent.setIndicator(m_nCurrentWordCount);
				break;
			case 1:		
				m_bWordPicDraw = true;
				break;
			case 2:		
				if(m_nCurrentPhase == 1) {
					m_bWordKrDraw = true;
					
					if(m_nWordBgColorKr != 0)
						startTextBgDrawKr();
				}
				else if(m_nCurrentPhase == 2) {
					m_bWordEnDraw = true;
					
					if(m_nWordBgColorEn != 0)
						startTextBgDrawEn();
				}
				break;
			case 3:		
				m_nCurrentWordCount ++;	
				mCurrentIndex++;
				initStudyDraw();
				
				break;
			case 4:		
				m_bShowNoticeDlg = true;
				showStartStudyDialog();
				break;
			case 5:
				initStudyDraw();
				if(m_listener != null)
					m_listener.onFinishAfter();
				
				return;
			}
			
			CurlView.this.invalidate();
		}
	};
	
	/**
	 * Construct the object from an XML file. Valid Attributes:
	 * 
	 * @see android.view.View#View(android.content.Context, android.util.AttributeSet)
	 */

	
	/**
	 * Initialize the view
	 */
	private final void initialize(Context context) {
		// Foreground text paint
		mTextPaint = new Paint();
		mTextPaint.setAntiAlias(true);
		mTextPaint.setTextSize(32);
		mTextPaint.setColor(0xFF000000);
		
		// The shadow
		mTextPaintShadow = new TextPaint();
		mTextPaintShadow.setAntiAlias(true);
		mTextPaintShadow.setTextSize(32);
		mTextPaintShadow.setColor(0x10000000);
		
		// Cache the context
		mContext = new WeakReference<Context>(context);
		
		// Base padding
		setPadding(3, 3, 3, 3);
		
		// The focus flags are needed
		setFocusable(true);
		setFocusableInTouchMode(true);
		
		mMovement =  new Vector2D(0,0);
		mFinger = new Vector2D(0,0);
		mOldMovement = new Vector2D(0,0);
		
		// Create our curl animation handler
		mAnimationHandler = new FlipAnimationHandler();
		
		// Create our edge paint
		mCurlEdgePaint = new Paint();
		mCurlEdgePaint.setColor(Color.WHITE);
		mCurlEdgePaint.setAntiAlias(true);
		mCurlEdgePaint.setStyle(Paint.Style.FILL);
		mCurlEdgePaint.setShadowLayer(10, -5, 5, 0x99000000);
		
		// Set the default props, those come from an XML :D
		mCurlSpeed = 30;
		mUpdateRate = 33;
		mInitialEdgeOffset = 20;
		mCurlMode = 1;
		
		// LEGACY PAGE HANDLING!
		
		// Create pages
		mPages = new ArrayList<Bitmap>();
		
		bitmapCurl = Bitmap.createBitmap(1280, 800,
				Bitmap.Config.ARGB_8888);
		bitmapCurl.eraseColor(0x00FFFFFF);
		
	}
	
	public void setPageBitmap(int nResId,int nRedId2) {
		mPages.add(BitmapFactory.decodeResource(getResources(), nResId));
		mPages.add(BitmapFactory.decodeResource(getResources(), nResId));
		
		// Create some sample images
		mForeground = mPages.get(0);
		mBackground = mPages.get(1);
	}
	public void setPageBitmap(Bitmap bitmap1,int nResId) {
		mPages.add(bitmap1);
		mPages.add(BitmapFactory.decodeResource(getResources(), nResId));
		
		// Create some sample images
		mForeground = mPages.get(0);
		mBackground = mPages.get(1);
	}
	
	/**
	 * Reset points to it's initial clip edge state
	 */
	public void ResetClipEdge()
	{
		// Set our base movement
		mMovement.x = mInitialEdgeOffset;
		mMovement.y = mInitialEdgeOffset;
		mOldMovement.x = 0;
		mOldMovement.y = 0;
		
		// Now set the points
		// TODO: OK, those points MUST come from our measures and
		// the actual bounds of the view!
		mA = new Vector2D(mInitialEdgeOffset, 0);
		mB = new Vector2D(this.getWidth(), this.getHeight());
		mC = new Vector2D(this.getWidth(), 0);
		mD = new Vector2D(0, 0);
		mE = new Vector2D(0, 0);
		mF = new Vector2D(0, 0);		
		mOldF = new Vector2D(0, 0);
		
		// The movement origin point
		mOrigin = new Vector2D(this.getWidth(), 0);
	}
	
	/**
	 * Return the context which created use. Can return null if the
	 * context has been erased.
	 */
	private Context GetContext() {
		return mContext.get();
	}
	
	/**
	 * See if the current curl mode is dynamic
	 * @return TRUE if the mode is CURLMODE_DYNAMIC, FALSE otherwise
	 */
	public boolean IsCurlModeDynamic()
	{
		return mCurlMode == CURLMODE_DYNAMIC;
	}
	
	/**
	 * Set the curl speed.
	 * @param curlSpeed - New speed in px/frame
	 * @throws IllegalArgumentException if curlspeed < 1
	 */
	public void SetCurlSpeed(int curlSpeed)
	{
		if ( curlSpeed < 1 )
			throw new IllegalArgumentException("curlSpeed must be greated than 0");
		mCurlSpeed = curlSpeed;
	}
	
	/**
	 * Get the current curl speed
	 * @return int - Curl speed in px/frame
	 */
	public int GetCurlSpeed()
	{
		return mCurlSpeed;
	}
	
	/**
	 * Set the update rate for the curl animation
	 * @param updateRate - Fixed animation update rate in fps
	 * @throws IllegalArgumentException if updateRate < 1
	 */
	public void SetUpdateRate(int updateRate)
	{
		if ( updateRate < 1 )
			throw new IllegalArgumentException("updateRate must be greated than 0");
		mUpdateRate = updateRate;
	}
	
	/**
	 * Get the current animation update rate
	 * @return int - Fixed animation update rate in fps
	 */
	public int GetUpdateRate()
	{
		return mUpdateRate;
	}
	
	/**
	 * Set the initial pixel offset for the curl edge
	 * @param initialEdgeOffset - px offset for curl edge
	 * @throws IllegalArgumentException if initialEdgeOffset < 0
	 */
	public void SetInitialEdgeOffset(int initialEdgeOffset)
	{
		if ( initialEdgeOffset < 0 )
			throw new IllegalArgumentException("initialEdgeOffset can not negative");
		mInitialEdgeOffset = initialEdgeOffset;
	}
	
	/**
	 * Get the initial pixel offset for the curl edge
	 * @return int - px
	 */
	public int GetInitialEdgeOffset()
	{
		return mInitialEdgeOffset;
	}
	
	/**
	 * Set the curl mode.
	 * <p>Can be one of the following values:</p>
     * <table>
     * <colgroup align="left" />
     * <colgroup align="left" />
     * <tr><th>Value</th><th>Description</th></tr>
     * <tr><td><code>{@link #CURLMODE_SIMPLE com.dcg.pagecurl:CURLMODE_SIMPLE}</code></td><td>Curl target will move only in one axis.</td></tr>
     * <tr><td><code>{@link #CURLMODE_DYNAMIC com.dcg.pagecurl:CURLMODE_DYNAMIC}</code></td><td>Curl target will move on both X and Y axis.</td></tr>
     * </table>
     * @see #CURLMODE_SIMPLE
     * @see #CURLMODE_DYNAMIC
	 * @param curlMode
	 * @throws IllegalArgumentException if curlMode is invalid
	 */
	public void SetCurlMode(int curlMode)
	{
		if ( curlMode != CURLMODE_SIMPLE &&
			 curlMode != CURLMODE_DYNAMIC )
			throw new IllegalArgumentException("Invalid curlMode");
		mCurlMode = curlMode;
	}
	
	/**
	 * Return an integer that represents the current curl mode.
	 * <p>Can be one of the following values:</p>
     * <table>
     * <colgroup align="left" />
     * <colgroup align="left" />
     * <tr><th>Value</th><th>Description</th></tr>
     * <tr><td><code>{@link #CURLMODE_SIMPLE com.dcg.pagecurl:CURLMODE_SIMPLE}</code></td><td>Curl target will move only in one axis.</td></tr>
     * <tr><td><code>{@link #CURLMODE_DYNAMIC com.dcg.pagecurl:CURLMODE_DYNAMIC}</code></td><td>Curl target will move on both X and Y axis.</td></tr>
     * </table>
     * @see #CURLMODE_SIMPLE
     * @see #CURLMODE_DYNAMIC
     * @return int - current curl mode
	 */
	public int GetCurlMode()
	{
		return mCurlMode;
	}
	
	/**
	 * Enable debug mode. This will draw a lot of data in the view so you can track what is happening
	 * @param bFlag - boolean flag
	 */
	public void SetEnableDebugMode(boolean bFlag)
	{
		bEnableDebugMode = bFlag;
	}
	
	/**
	 * Check if we are currently in debug mode.
	 * @return boolean - If TRUE debug mode is on, FALSE otherwise.
	 */
	public boolean IsDebugModeEnabled()
	{
		return bEnableDebugMode;
	}

	/**
	 * @see android.view.View#measure(int, int)
	 */
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int finalWidth, finalHeight;
		finalWidth = measureWidth(widthMeasureSpec);
		finalHeight = measureHeight(heightMeasureSpec);
		setMeasuredDimension(finalWidth, finalHeight);
	}

	/**
	 * Determines the width of this view
	 * @param measureSpec A measureSpec packed into an int
	 * @return The width of the view, honoring constraints from measureSpec
	 */
	private int measureWidth(int measureSpec) {
		int result = 0;
		int specMode = MeasureSpec.getMode(measureSpec);
		int specSize = MeasureSpec.getSize(measureSpec);
		
		if (specMode == MeasureSpec.EXACTLY) {
			// We were told how big to be
			result = specSize;
		} else {
			// Measure the text
			result = specSize;
		}
		
		return result;
	}

	/**
	 * Determines the height of this view
	 * @param measureSpec A measureSpec packed into an int
	 * @return The height of the view, honoring constraints from measureSpec
	 */
	private int measureHeight(int measureSpec) {
		int result = 0;
		int specMode = MeasureSpec.getMode(measureSpec);
		int specSize = MeasureSpec.getSize(measureSpec);
		
		if (specMode == MeasureSpec.EXACTLY) {
			// We were told how big to be
			result = specSize;
		} else {
			// Measure the text (beware: ascent is a negative number)
			result = specSize;
		}
		
		return result;
	}

	/**
	 * Render the text
	 * 
	 * @see android.view.View#onDraw(android.graphics.Canvas)
	 */
	//@Override
	//protected void onDraw(Canvas canvas) {
	//	super.onDraw(canvas);
	//	canvas.drawText(mText, getPaddingLeft(), getPaddingTop() - mAscent, mTextPaint);
	//}
	
	//---------------------------------------------------------------
	// Curling. This handles touch events, the actual curling
	// implementations and so on.
	//---------------------------------------------------------------
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if (!bBlockTouchInput) {
			
			// Get our finger position
			mFinger.x = event.getX();
			mFinger.y = event.getY();
			int width = getWidth();
			
			// Depending on the action do what we need to
			switch (event.getAction()) {
			case MotionEvent.ACTION_DOWN:				
				mOldMovement.x = mFinger.x;
				mOldMovement.y = mFinger.y;
				
				// If we moved over the half of the display flip to next
				if (mOldMovement.x > (width >> 1)) {
					mMovement.x = mInitialEdgeOffset;
					mMovement.y = mInitialEdgeOffset;
					
					// Set the right movement flag
					bFlipRight = true;
				} else {
					// Set the left movement flag
					bFlipRight = false;
					
					// go to next previous page
					previousView();
					
					// Set new movement
					mMovement.x = IsCurlModeDynamic()?width<<1:width;
					mMovement.y = mInitialEdgeOffset;
				}
				
				/*if(m_rcRemember.contains(mFinger.x, mFinger.y)) {
					m_bRemberBtnPress = true;
				}*/
				
				break;
			case MotionEvent.ACTION_UP:	
				/*if(m_bRemberBtnPress && m_rcRemember.contains(mFinger.x, mFinger.y)) {
					m_bRemberBtnPress = false;
					setCurrentWordRember();
					StudyPageCurlView.this.invalidate();
					break;
				}*/
				
				bUserMoves = false;
				m_bStudyPause = false;
				
				
				if(m_nCurrentWordCount <= m_arrStudyWords.size()-1)
					pauseWordVoice(m_arrStudyWords.get(m_nCurrentWordCount).getSound());
				
				StudyWordActivity.playWordVoice("page");
				
				if(m_threadTextBg != null && m_threadTextBg.isAlive()) {
					m_bDrawBgStopEn = true;
					m_bDrawBgStopKr = true;
					try{
						m_threadTextBg.join();
					}
					catch(Exception e) {}
				}
				
				bFlipping=true;
				if(!bFlipRight){
					initStudyDraw();
				}
				
				FlipAnimationStep();
				break;
			case MotionEvent.ACTION_MOVE:
				bUserMoves=true;
				
				if((m_nCurrentWordCount == 0 && !bFlipRight) /*|| 
						(m_nCurrentWordCount == m_arrStudyWords.size()-1 && m_nCurrentRound == m_nStudyRepeat[m_nCurrentPhase-1]-1 &&
						m_nCurrentPhase == 2 && bFlipRight)*/ ) {
					bUserMoves = false;
					break;
				}
				
				// Get movement
				mMovement.x -= mFinger.x - mOldMovement.x;
				mMovement.y -= mFinger.y - mOldMovement.y;
				mMovement = CapMovement(mMovement, true);
				
				// Make sure the y value get's locked at a nice level
				if ( mMovement.y  <= 1 )
					mMovement.y = 1;
				
				// Get movement direction
				if (mFinger.x < mOldMovement.x ) {
					bFlipRight = true;
				} else {
					bFlipRight = false;
				}
				
				// Save old movement values
				mOldMovement.x  = mFinger.x;
				mOldMovement.y  = mFinger.y;
				
				// Force a new draw call
				DoPageCurl();
				this.invalidate();
				break;
			}

			if(bFlipping || bUserMoves) {	//LJG
				m_bStudyPause = true;		
			}
		}
		
		// TODO: Only consume event if we need to.
		return true;
	}
	
	/**
	 * Make sure we never move too much, and make sure that if we 
	 * move too much to add a displacement so that the movement will 
	 * be still in our radius.
	 * @param radius - radius form the flip origin
	 * @param bMaintainMoveDir - Cap movement but do not change the
	 * current movement direction
	 * @return Corrected point
	 */
	private Vector2D CapMovement(Vector2D point, boolean bMaintainMoveDir)
	{
		// Make sure we never ever move too much
		if (point.distance(mOrigin) > mFlipRadius)
		{
			if ( bMaintainMoveDir )
			{
				// Maintain the direction
				point = mOrigin.sum(point.sub(mOrigin).normalize().mult(mFlipRadius));
			}
			else
			{
				// Change direction
				if ( point.x > (mOrigin.x+mFlipRadius))
					point.x = (mOrigin.x+mFlipRadius);
				else if ( point.x < (mOrigin.x-mFlipRadius) )
					point.x = (mOrigin.x-mFlipRadius);
				point.y = (float) (Math.sin(Math.acos(Math.abs(point.x-mOrigin.x)/mFlipRadius))*mFlipRadius);
			}
		}
		return point;
	}
	
	/**
	 * Execute a step of the flip animation
	 */
	public void FlipAnimationStep() {
		if ( !bFlipping )
			return;
		
		int width = getWidth();
			
		// No input when flipping
		bBlockTouchInput = true;
		
		// Handle speed
		float curlSpeed = mCurlSpeed;
		if ( !bFlipRight )
			curlSpeed *= -1;
		
		// Move us
		mMovement.x += curlSpeed;
		mMovement = CapMovement(mMovement, false);
		
		// Create values
		DoPageCurl();
		
		// Check for endings :D
		if (mA.x < 1 || mA.x > width - 1) {
			
			bFlipping = false;
			if (bFlipRight) {
				//SwapViews();
				nextView();
			}
			ResetClipEdge();
			
			// Create values
			DoPageCurl();

			// Enable touch input after the next draw event
			bEnableInputAfterDraw = true;
			
			//LJG	íŽ˜ì§€ê°€ ë‹¤ ë„˜ì–´ê°€ë©´ ë‹¤ì‹œ í•™ìŠµ
			if(bFlipRight)
				doNext();
			else
				doPrev();
		}
		else
		{
			mAnimationHandler.sleep(mUpdateRate);
		}
		
		// Force a new draw call
		this.invalidate();
	}
	
	/**
	 * Do the page curl depending on the methods we are using
	 */
	private void DoPageCurl()
	{
//		if(bFlipping){
//			if ( IsCurlModeDynamic() )
//				doDynamicCurl();
//			else
//				doSimpleCurl();
//			
//		} else {
//			if ( IsCurlModeDynamic() )
//				doDynamicCurl();
//			else
//				doSimpleCurl();
//		}
	}
	
	/**
	 * Do a simple page curl effect
	 */
	private void doSimpleCurl() {
		int width = getWidth();
		int height = getHeight();
		
		// Calculate point A
		mA.x = width - mMovement.x;
		mA.y = height;

		// Calculate point D
		mD.x = 0;
		mD.y = 0;
		if (mA.x > width / 2) {
			mD.x = width;
			mD.y = height - (width - mA.x) * height / mA.x;
		} else {
			mD.x = 2 * mA.x;
			mD.y = 0;
		}
		
		// Now calculate E and F taking into account that the line
		// AD is perpendicular to FB and EC. B and C are fixed points.
		double angle = Math.atan((height - mD.y) / (mD.x + mMovement.x - width));
		double _cos = Math.cos(2 * angle);
		double _sin = Math.sin(2 * angle);

		// And get F
		mF.x = (float) (width - mMovement.x + _cos * mMovement.x);
		mF.y = (float) (height - _sin * mMovement.x);
		
		// If the x position of A is above half of the page we are still not
		// folding the upper-right edge and so E and D are equal.
		if (mA.x > width / 2) {
			mE.x = mD.x;
			mE.y = mD.y;
		}
		else
		{
			// So get E
			mE.x = (float) (mD.x + _cos * (width - mD.x));
			mE.y = (float) -(_sin * (width - mD.x));
		}
	}

	/**
	 * Calculate the dynamic effect, that one that follows the users finger
	 */
	private void doDynamicCurl() {
		int width = getWidth();
		int height = getHeight();

		// F will follow the finger, we add a small displacement
		// So that we can see the edge
		mF.x = width - mMovement.x+0.1f;
		mF.y = height - mMovement.y+0.1f;
		
		// Set min points
		if(mA.x==0) {
			mF.x= Math.min(mF.x, mOldF.x);
			mF.y= Math.max(mF.y, mOldF.y);
		}
		
		// Get diffs
		float deltaX = width-mF.x;
		float deltaY = height-mF.y;

		float BH = (float) (Math.sqrt(deltaX * deltaX + deltaY * deltaY) / 2);
		double tangAlpha = deltaY / deltaX;
		double alpha = Math.atan(deltaY / deltaX);
		double _cos = Math.cos(alpha);
		double _sin = Math.sin(alpha);
		
		mA.x = (float) (width - (BH / _cos));
		mA.y = height;
		
		mD.y = (float) (height - (BH / _sin));
		mD.x = width;

		mA.x = Math.max(0,mA.x);
		if(mA.x==0) {
			mOldF.x = mF.x;
			mOldF.y = mF.y;
		}
		
		// Get W
		mE.x = mD.x;
		mE.y = mD.y;
		
		// Correct
		if (mD.y < 0) {
			mD.x = width + (float) (tangAlpha * mD.y);
			mE.y = 0;
			mE.x = width + (float) (Math.tan(2 * alpha) * mD.y);
		}
	}

	/**
	 * Swap between the fore and back-ground.
	 */
	@Deprecated
	private void SwapViews() {
		Bitmap temp = mForeground;
		mForeground = mBackground;
		mBackground = temp;
	}
	
	/**
	 * Swap to next view
	 */
	private void nextView() {
		int foreIndex = mIndex + 1;
		if(foreIndex >= mPages.size()) {
			foreIndex = 0;
		}
		int backIndex = foreIndex + 1;
		if(backIndex >= mPages.size()) {
			backIndex = 0;
		}
		mIndex = foreIndex;
		setViews(foreIndex, backIndex);
	}
	
	/**
	 * Swap to previous view
	 */
	private void previousView() {
		if(mIndex == 0) return;

		int backIndex = mIndex;
		int foreIndex = backIndex - 1;
		if(foreIndex < 0) {
			foreIndex = mPages.size()-1;
		}
		mIndex = foreIndex;
		setViews(foreIndex, backIndex);
	}
	
	/**
	 * Set current fore and background
	 * @param foreground - Foreground view index
	 * @param background - Background view index
	 */
	private void setViews(int foreground, int background) {
		mForeground = mPages.get(foreground);
		mBackground = mPages.get(background);
	}
	
	//---------------------------------------------------------------
	// Drawing methods
	//---------------------------------------------------------------

	boolean bDrawn = false;
	Canvas tempCanvas;
	
	@Override
	protected void onDraw(Canvas canvas) {
		// Always refresh offsets
		mCurrentLeft = getLeft();
		mCurrentTop = getTop();
		
		// Translate the whole canvas
		//canvas.translate(mCurrentLeft, mCurrentTop);
		
		// We need to initialize all size data when we first draw the view
		if ( !bViewDrawn ) {
			bViewDrawn = true;
			onFirstDrawEvent(canvas);
		}
		
		canvas.drawColor(Color.parseColor("#00ffffff"));
		
		// Curl pages
		//DoPageCurl();
		
		// TODO: This just scales the views to the current
		// width and height. We should add some logic for:
		//  1) Maintain aspect ratio
		//  2) Uniform scale
		//  3) ...
		Rect rect = new Rect();
		rect.left = 0;
		rect.top = 0;
		rect.bottom = getHeight();
		rect.right = getWidth();
		
		// First Page render
		Paint paint = new Paint();
		
		// Draw our elements
		drawForeground(canvas, rect, paint);
		drawBackground(canvas, rect, paint);	
		drawCurlEdge(canvas);
		
		// Draw any debug info once we are done
		if ( bEnableDebugMode )
			drawDebug(canvas);
		/*if (m_bStudyPause == false){

			mPageCurl.getTexturePage().reset();
			mPageCurl.getTexturePage().setTexture(this.getDrawingCache(), CurlPage.SIDE_BOTH);
			mPageCurl.getTexturePage().setColor(Color.argb(127, 255, 255, 255),
					CurlPage.SIDE_BACK);
			
		}*/
		// Check if we can re-enable input
		if ( bEnableInputAfterDraw )
		{
			bBlockTouchInput = false;
			bEnableInputAfterDraw = false;
		}
		
		bDrawn = true;
		tempCanvas = canvas;
			
		// Restore canvas
		//canvas.restore();
	}
/*	
	protected void onDraw(Canvas canvas) {
		// Always refresh offsets
		mCurrentLeft = getLeft();
		mCurrentTop = getTop();
		
		// Translate the whole canvas
		//canvas.translate(mCurrentLeft, mCurrentTop);
		
		// We need to initialize all size data when we first draw the view
		if ( !bViewDrawn ) {
			bViewDrawn = true;
			onFirstDrawEvent(canvas);
		}
		
		canvas.drawColor(Color.parseColor("#70ffffff"));
		
		// Curl pages
		//DoPageCurl();
		
		// TODO: This just scales the views to the current
		// width and height. We should add some logic for:
		//  1) Maintain aspect ratio
		//  2) Uniform scale
		//  3) ...
		Rect rect = new Rect();
		rect.left = 0;
		rect.top = 0;
		rect.bottom = getHeight();
		rect.right = getWidth();
		
		// First Page render
		Paint paint = new Paint();
		
		// Draw our elements
		drawForeground(canvas, rect, paint);
		drawBackground(canvas, rect, paint);	
		drawCurlEdge(canvas);
		
		// Draw any debug info once we are done
		if ( bEnableDebugMode )
			drawDebug(canvasCurl);

		Bitmap bitmap = Bitmap.createBitmap(1280, 800,
				Bitmap.Config.ARGB_8888);
		canvas.setBitmap(bitmap);
		
		mPageCurl.getTexturePage().setTexture(bitmap, CurlPage.SIDE_BOTH);
		mPageCurl.getTexturePage().setColor(Color.argb(127, 255, 255, 255), CurlPage.SIDE_BACK);
		//canvas.drawBitmap(bitmapCurl, new Matrix(), null);
		//updatePage1(mPageCurl.getTexturePage(), 0);
		// Check if we can re-enable input
		if ( bEnableInputAfterDraw )
		{
			bBlockTouchInput = false;
			bEnableInputAfterDraw = false;
		}
		
		

		// Restore canvas
		//canvas.restore();
	}*/
	
	/**
	 * Called on the first draw event of the view
	 * @param canvas
	 */
	protected void onFirstDrawEvent(Canvas canvas) {
		
		mFlipRadius = getWidth();
		
		ResetClipEdge();
		DoPageCurl();
	}
	
	/**
	 * Draw the foreground
	 * @param canvas
	 * @param rect
	 * @param paint
	 */
	private void drawForeground( Canvas canvas, Rect rect, Paint paint ) {
		canvas.save();
		
//		canvas.drawBitmap(mForeground, null, rect, paint);
		
		// Draw the page number (first page is 1 in real life :D 
		// there is no page number 0 hehe)
	//	drawPageNum(canvas, mIndex);
		drawStudyWords(canvas);
		canvas.restore();
	}
	
	/**
	 * Create a Path used as a mask to draw the background page
	 * @return
	 */
	private Path createBackgroundPath() {
		Path path = new Path();
		path.moveTo(mA.x, mA.y);
		path.lineTo(mB.x, mB.y);
		path.lineTo(mC.x, mC.y);
		path.lineTo(mD.x, mD.y);
		path.lineTo(mA.x, mA.y);
		return path;
	}
	
	/**
	 * Draw the background image.
	 * @param canvas
	 * @param rect
	 * @param paint
	 */
	private void drawBackground( Canvas canvas, Rect rect, Paint paint ) {
		Path mask = createBackgroundPath();
		
		// Save current canvas so we do not mess it up
		canvas.save();
		canvas.clipPath(mask);
//		canvas.drawBitmap(mBackground, null, rect, paint);
		
		// Draw the page number (first page is 1 in real life :D 
		// there is no page number 0 hehe)
	//	drawPageNum(canvas, mIndex);
		
		drawBackgroundImage(canvas);
		drawStudyInfo(canvas);
		
		//updatePage1(mPageCurl.getTexturePage(), 0);
		canvas.restore();
	}
	
	/**
	 * Creates a path used to draw the curl edge in.
	 * @return
	 */
	private Path createCurlEdgePath() {
		Path path = new Path();
		path.moveTo(mA.x, mA.y);
		path.lineTo(mD.x, mD.y);
		path.lineTo(mE.x, mE.y);
		path.lineTo(mF.x, mF.y);
		path.lineTo(mA.x, mA.y);
		return path;
	}
	
	/**
	 * Draw the curl page edge
	 * @param canvas
	 */
	private void drawCurlEdge( Canvas canvas )
	{
		Path path = createCurlEdgePath();
		canvas.drawPath(path, mCurlEdgePaint);
	}
	
	/**
	 * Draw page num (let this be a bit more custom)
	 * @param canvas
	 * @param pageNum
	 */
	private void drawPageNum(Canvas canvas, int pageNum)
	{
		mTextPaint.setColor(Color.WHITE);
		String pageNumText = "- "+pageNum+" -";
		drawCentered(canvas, pageNumText,canvas.getHeight()-mTextPaint.getTextSize()-5,mTextPaint,mTextPaintShadow);
	}
	
	//---------------------------------------------------------------
	// Debug draw methods
	//---------------------------------------------------------------
	
	/**
	 * Draw a text with a nice shadow
	 */
	public static void drawTextShadowed(Canvas canvas, String text, float x, float y, Paint textPain, Paint shadowPaint) {
    	canvas.drawText(text, (x-1)*2, 2*y, shadowPaint);
    	canvas.drawText(text, 2*x, 2*(y+1), shadowPaint);
    	canvas.drawText(text, 2*(x+1), 2*y, shadowPaint);
    	canvas.drawText(text, 2*x, 2*(y-1), shadowPaint);    	
    	canvas.drawText(text, 2*x, 2*y, textPain);
    }
	
	/**
	 * Draw a text with a nice shadow centered in the X axis
	 * @param canvas
	 * @param text
	 * @param y
	 * @param textPain
	 * @param shadowPaint
	 */
	public static void drawCentered(Canvas canvas, String text, float y, Paint textPain, Paint shadowPaint)
	{
		float posx = (canvas.getWidth() - textPain.measureText(text))/2;
		drawTextShadowed(canvas, text, posx, y, textPain, shadowPaint);
	}
	
	/**
	 * Draw debug info
	 * @param canvas
	 */
	private void drawDebug(Canvas canvas)
	{
		float posX = 10;
		float posY = 20;
		
		Paint paint = new Paint();
		paint.setStrokeWidth(5);
		paint.setStyle(Style.STROKE);
		
		paint.setColor(Color.BLACK);		
		canvas.drawCircle(mOrigin.x, mOrigin.y, getWidth(), paint);
		
		paint.setStrokeWidth(3);
		paint.setColor(Color.RED);		
		canvas.drawCircle(mOrigin.x, mOrigin.y, getWidth(), paint);
		
		paint.setStrokeWidth(5);
		paint.setColor(Color.BLACK);
		canvas.drawLine(mOrigin.x, mOrigin.y, mMovement.x, mMovement.y, paint);
		
		paint.setStrokeWidth(3);
		paint.setColor(Color.RED);
		canvas.drawLine(mOrigin.x, mOrigin.y, mMovement.x, mMovement.y, paint);
		
		posY = debugDrawPoint(canvas,"A",mA,Color.RED,posX,posY);
		posY = debugDrawPoint(canvas,"B",mB,Color.GREEN,posX,posY);
		posY = debugDrawPoint(canvas,"C",mC,Color.BLUE,posX,posY);
		posY = debugDrawPoint(canvas,"D",mD,Color.CYAN,posX,posY);
		posY = debugDrawPoint(canvas,"E",mE,Color.YELLOW,posX,posY);
		posY = debugDrawPoint(canvas,"F",mF,Color.LTGRAY,posX,posY);
		posY = debugDrawPoint(canvas,"Mov",mMovement,Color.DKGRAY,posX,posY);
		posY = debugDrawPoint(canvas,"Origin",mOrigin,Color.MAGENTA,posX,posY);
		posY = debugDrawPoint(canvas,"Finger",mFinger,Color.GREEN,posX,posY);
		
		// Draw some curl stuff (Just some test)
		/*
		canvas.save();
		Vector2D center = new Vector2D(getWidth()/2,getHeight()/2);
	    //canvas.rotate(315,center.x,center.y);
	    
	    // Test each lines
		//float radius = mA.distance(mD)/2.f;
	    //float radius = mA.distance(mE)/2.f;
	    float radius = mA.distance(mF)/2.f;
		//float radius = 10;
	    float reduction = 4.f;
		RectF oval = new RectF();
		oval.top = center.y-radius/reduction;
		oval.bottom = center.y+radius/reduction;
		oval.left = center.x-radius;
		oval.right = center.x+radius;
		canvas.drawArc(oval, 0, 360, false, paint);
		canvas.restore();
		/**/
	}
	
	private float debugDrawPoint(Canvas canvas, String name, Vector2D point, int color, float posX, float posY) {	
		return debugDrawPoint(canvas,name+" "+point.toString(),point.x, point.y, color, posX, posY);
	}
	
	private float debugDrawPoint(Canvas canvas, String name, float X, float Y, int color, float posX, float posY) {
		mTextPaint.setColor(color);
		drawTextShadowed(canvas,name,posX , posY, mTextPaint,mTextPaintShadow);
		Paint paint = new Paint();
		paint.setStrokeWidth(5);
		paint.setColor(color);	
		canvas.drawPoint(X, Y, paint);
		return posY+15;
	}

	Typeface tfKorean;
	Typeface tfEnglish;

	///////////LJG//////////////////////////
	public void initStudyView(Context context) {
		m_context = context;
		m_assetMgr = this.getResources().getAssets();
		m_bmpRemberOn = BitmapFactory.decodeResource(this.getResources(), R.drawable.studding_remember_off);
		m_bmpRemberOff = BitmapFactory.decodeResource(this.getResources(), R.drawable.studding_remember_on);
		m_rcRemember = new RectF(910, 420, 910+m_bmpRemberOn.getWidth(), 420+m_bmpRemberOn.getHeight());
		
		m_paintPic = new Paint();
		m_paintPic.setAlpha(255);
		
		m_paintWord = new Paint();
		m_paintWord.setAntiAlias(true);
		m_paintWord.setAlpha(255);

		tfKorean = /*Typeface.create(Typeface.SANS_SERIF, Typeface.NORMAL);*/Typeface.createFromAsset(context.getAssets(), Define.getMainFont());
		tfEnglish = Typeface.create(Typeface.SANS_SERIF, Typeface.NORMAL);

		m_paintLeftText = new Paint();
		m_paintLeftText.setAntiAlias(true);
		m_paintLeftText.setTextAlign(Align.CENTER);
		m_paintLeftText.setTextSize(30);
		m_paintLeftText.setTypeface(tfEnglish);
		
		m_paintRightText = new Paint();
		m_paintRightText.setAntiAlias(true);
		m_paintRightText.setTextAlign(Align.LEFT);
		m_paintRightText.setTextSize(30);
		m_paintRightText.setTypeface(tfKorean);
		
	}
	
	private void initSound() {
		/*AudioManager audioManager = (AudioManager)m_context.getSystemService(Context.AUDIO_SERVICE);
		m_soundPool = new SoundPool(audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC), AudioManager.STREAM_MUSIC, 0);
		m_fVolume =0.99f;
		
		m_soundMap = new HashMap<String, Integer>();
		
		for(int i=0; i < m_arrStudyWords.size(); i++)
			loadSound(m_arrStudyWords.get(i).getSound());*/
	}
	
	private void loadSound(String strWord) {
	/*	AssetFileDescriptor fd = null;
		try {
			String soundPath = m_Parent.getAudioFileName(Integer.parseInt(strWord));
			int nSoundId = m_soundPool.load(soundPath , 1);
			m_soundMap.put(strWord, nSoundId);
		}
		catch(Exception e) {
			e.printStackTrace();
		}*/
	}
	
	private void playWordVoice(String strWord) {
		/*if(m_soundPool != null)
			m_soundPool.play(m_soundMap.get(strWord).intValue(), m_fVolume, m_fVolume, 1, 0, 1.0F);*/
		String soundPath = m_Parent.getAudioFileName(Integer.parseInt(strWord));
		MediaPlayer mp = MediaPlayer.create(m_context, Uri.parse(soundPath));
		mp.start();
	}
	
	private void pauseWordVoice(String strWord) {
		/*
		if(m_soundPool != null)
			m_soundPool.pause(m_soundMap.get(strWord).intValue());
			*/
	}
	
	private void resumeWordVoice(String strWord) {
		/*
		if(m_soundPool != null)
			m_soundPool.resume(m_soundMap.get(strWord).intValue());
			*/
	}
	
	private void drawBackgroundImage(Canvas canvas) {

	}
	private void drawStudyInfo(Canvas canvas) {
		if(m_nCurrentWordCount > m_arrStudyWords.size()-1)
			return;
		
		drawTargetStudyInfo(canvas);
		drawStudyCurInfo(canvas);		
	//	drawRememberBitmap(canvas, true);
	}
	
	private void drawStudyWords(Canvas canvas) {
		if(m_nCurrentWordCount > m_arrStudyWords.size()-1)
			return;
		
		drawTargetStudyInfo(canvas);
		drawStudyCurInfo(canvas);		
	//	drawRememberBitmap(canvas, false);
		
		drawStudyWordEn(canvas);
		drawStudyImage(canvas);
		drawStudyWordKr(canvas);
	}
	
	private void drawTargetStudyInfo(Canvas canvas) {
		/*Paint paint = new Paint();
		paint.setColor(Color.BLACK);
		paint.setTypeface(Typeface.create(Typeface.SANS_SERIF, Typeface.NORMAL));
		paint.setTextAlign(Align.LEFT);
		paint.setTextSize(42);
		
		String strOrder = String.valueOf(m_nStudyOrder) + this.getResources().getString(R.string.order);
		canvas.drawText(strOrder, 2*70.0f, 2*60.0f, paint);
		
		paint.setTextSize(38);
		paint.setTextAlign(Align.CENTER);
		String strWordCount = String.valueOf(m_nTargetWordCount);
		canvas.drawText(strWordCount, 2*950.0f, 2*70.0f, paint);
		
		paint.setTextSize(28);
		paint.setTextAlign(Align.CENTER);
		float w = ESUtils.getTextWidth(paint, strWordCount);
		canvas.drawText(this.getResources().getString(R.string.sum), 2*(945.0f + w + 3), 2*68.0f, paint);*/

		/*
		Paint paint = new Paint();
		paint.setColor(Color.BLACK);
		paint.setTypeface(Typeface.create(Typeface.SANS_SERIF, Typeface.NORMAL));
		paint.setTextAlign(Align.LEFT);
		paint.setTextSize(30);
		
		String strOrder = String.valueOf(m_nStudyOrder) + this.getResources().getString(R.string.order);
		canvas.drawText(strOrder, 90.0f, 90.0f, paint);
		
		paint.setTextSize(28);
		paint.setTextAlign(Align.CENTER);
		String strWordCount = String.valueOf(m_nTargetWordCount);
		canvas.drawText(strWordCount, getWidth() - 110.0f, 105.0f, paint);
		
		paint.setTextSize(21);
		paint.setTextAlign(Align.CENTER);
		float w = ESUtils.getTextWidth(paint, strWordCount);
		canvas.drawText(this.getResources().getString(R.string.sum), (getWidth() - 110.0f + w + 3), 105.0f, paint);
		*/
	}
	
	private void drawStudyCurInfo(Canvas canvas) {
		/*Paint paint = new Paint();
		paint.setColor(Color.BLACK);
		paint.setTextSize(36);
		
		String strInfo = Integer.toString(m_nCurrentPhase) + this.getResources().getString(R.string.phase) + ":";
		strInfo += " " + Integer.toString(m_nCurrentWordCount+1) + "/" + Integer.toString(m_nTargetWordCount);
		strInfo += "  " + m_context.getString(R.string.round) + " : " + Integer.toString(m_nCurrentRound+1);
		canvas.drawText(strInfo, 2*780.0f, 2*610.0f, paint);*/

		Paint paint = new Paint();
		paint.setAntiAlias(true);
		paint.setColor(Color.BLACK);
		paint.setTextSize(20);
		paint.setTypeface(tfKorean);

		String strInfo = Integer.toString(m_nCurrentPhase) + this.getResources().getString(R.string.phase) + ":";
		strInfo += " " + Integer.toString(m_nCurrentWordCount+1) + "/" + Integer.toString(m_nTargetWordCount);
		strInfo += "  " + m_context.getString(R.string.round) + " : " + Integer.toString(m_nCurrentRound+1);
		canvas.drawText(strInfo, 970.0f, 670.0f, paint);
	}
	
	private void drawRememberBitmap(Canvas canvas, boolean bBackground) {
		if(bBackground) {
			canvas.drawBitmap(m_bmpRemberOff, REMEMER_IMG_POINT.x, REMEMER_IMG_POINT.y, new Paint());
			return;
		}
		
		if(m_arrStudyWords.get(m_nCurrentWordCount).getComplete() == 0)
			canvas.drawBitmap(m_bmpRemberOff, REMEMER_IMG_POINT.x, REMEMER_IMG_POINT.y, new Paint());
		else
			canvas.drawBitmap(m_bmpRemberOn, REMEMER_IMG_POINT.x, REMEMER_IMG_POINT.y, new Paint());
	}
	
	private void drawStudyWordEn(Canvas canvas) {
		/*if(m_bWordEnDraw) {
			if(m_nWordBgColorEn != 0) {
				m_paintWord.setColor(m_nWordBgColorEn);
				m_paintWord.setAlpha(255);
				Rect rc = new Rect(2*EN_WORD_POINT.x-5, 2*(EN_WORD_POINT.y-23), (2*m_nTextBgRightEn), 2*(EN_WORD_POINT.y+10));
				canvas.drawRect(rc, m_paintWord);
			}
			canvas.drawText(m_arrStudyWords.get(m_nCurrentWordCount).getMeanEn(), 2*EN_WORD_POINT.x, 2*EN_WORD_POINT.y, m_paintLeftText);
		}*/

		int txtWidthBig = (int)m_paintLeftText.measureText(m_arrStudyWords.get(m_nCurrentWordCount).getMeanEn());
		final int txtWidth =txtWidthBig > 300 ? txtWidthBig : 300;

		if(m_bWordEnDraw) {
			if(m_nWordBgColorEn != 0) {
				m_paintWord.setColor(m_nWordBgColorEn);
				m_paintWord.setAlpha(255);
				Rect rc = new Rect(EN_WORD_POINT.x-10, (EN_WORD_POINT.y-50), (m_nTextBgRightEn), (EN_WORD_POINT.y+30));
				canvas.drawRect(rc, m_paintWord);
			}
			//canvas.drawText(m_arrStudyWords.get(m_nCurrentWordCount).getMeanEn(), EN_WORD_POINT.x, EN_WORD_POINT.y, m_paintLeftText);
			canvas.drawText(m_arrStudyWords.get(m_nCurrentWordCount).getMeanEn(), EN_WORD_POINT.x + (txtWidth) / 2, EN_WORD_POINT.y, m_paintLeftText);

		}
	}
	
	private void drawStudyWordKr(Canvas canvas) {
		/*if(m_bWordKrDraw) {
			if(m_nWordBgColorKr != 0) {
				m_paintWord.setColor(m_nWordBgColorKr);
				m_paintWord.setAlpha(255);
				int txtWidth = (int)m_paintLeftText.measureText(m_arrStudyWords.get(m_nCurrentWordCount).getMeanKr());
				Rect rc = new Rect(2*KR_WORD_POINT.x-txtWidth+5, 2*(KR_WORD_POINT.y-22), 2*(m_nTextBgRightKr), 2*(KR_WORD_POINT.y+10));
				canvas.drawRect(rc, m_paintWord);
			}
			
			canvas.drawText(m_arrStudyWords.get(m_nCurrentWordCount).getMeanKr(), 2*KR_WORD_POINT.x, 2*KR_WORD_POINT.y, m_paintRightText);
		}*/

		int txtWidthBig = (int)m_paintRightText.measureText(m_arrStudyWords.get(m_nCurrentWordCount).getMeanKr());
		final int txtWidth =txtWidthBig > 300 ? txtWidthBig : 300;

		if(m_bWordKrDraw) {
			if(m_nWordBgColorKr != 0) {
				m_paintWord.setColor(m_nWordBgColorKr);
				m_paintWord.setAlpha(255);
				//int txtWidth = (int)m_paintRightText.measureText(m_arrStudyWords.get(m_nCurrentWordCount).getMeanKr()) + 10;
				Rect rc = new Rect(KR_WORD_POINT.x-txtWidth+5, (KR_WORD_POINT.y-50), (m_nTextBgRightKr), (KR_WORD_POINT.y+30));
				canvas.drawRect(rc, m_paintWord);
			}

			canvas.drawText(m_arrStudyWords.get(m_nCurrentWordCount).getMeanKr(), KR_WORD_POINT.x -txtWidth + (txtWidth - txtWidthBig) / 2, KR_WORD_POINT.y, m_paintRightText);
		}
	}

	private int nTempCount  = -1;

	private void drawStudyImage(Canvas canvas) {
		if(!m_bWordPicDraw ) {
			m_Parent.stopVideo();
			return;
		}
		if(nTempCount != m_nCurrentWordCount){
			try {
				nTempCount = m_nCurrentWordCount;
				//String strVideoPath = Define.VIDEO_PATH + m_arrStudyWords.get(m_nCurrentWordCount).getImage() + ".mp4";
				String strVideoPath = m_Parent.getVideoFileName(Integer.parseInt(m_arrStudyWords.get(m_nCurrentWordCount).getImage()));
				m_Parent.playVideo(strVideoPath);
				m_Parent.saveStudyTimeToDB();
			}
			catch(Exception e) {
				e.printStackTrace();
			}
		}

//	    Bitmap img_bitmap = Bitmap.createScaledBitmap( m_arrStudyBmps.get(m_nCurrentWordCount), 500, 320, true );
//
//
//	    float left = (getWidth()-img_bitmap.getWidth())/2 + 30;
//	    float top = (getHeight()-img_bitmap.getHeight())/2 + 30;
//
//	    canvas.drawBitmap(img_bitmap, left, top, m_paintPic);

//		float left = (getWidth()-m_arrStudyBmps.get(m_nCurrentWordCount).getWidth())/2 + 30;
//		float top = (getHeight()-m_arrStudyBmps.get(m_nCurrentWordCount).getHeight())/2 + 30;
//		
//		canvas.drawBitmap(m_arrStudyBmps.get(m_nCurrentWordCount), left, top, m_paintPic);
	}
	
	public void setWordBgColor(int nColor1, int nColor2) {
		m_nWordBgColorEn = nColor1;
		m_nWordBgColorKr = nColor2;
	}
	
	public void setStudyRepeatCount(int[] repeat) {
		m_nStudyRepeat = repeat;
	}
	
	public void setStudySpeed(int nSpeed) {
		m_nStudySpeed = nSpeed;

		m_studyTimeList = new int[] {2500, 2500, 2500};
		m_nBgTimeSleep = 40;


		switch(m_nStudySpeed) {
		case 0:
			m_studyTimeList = new int[] {5500, 3000, 10000};
			m_nBgTimeSleep = 80;
			break;
		case 1:
			m_studyTimeList = new int[] {4500, 2000, 7000};
			m_nBgTimeSleep = 60;
			break;
		case 2:
			m_studyTimeList = new int[] {2500, 1500, 5000};
			m_nBgTimeSleep = 40;
			break;
		case 3:
			m_studyTimeList = new int[] {2000, 1000, 2000};
			m_nBgTimeSleep = 20;
			break;
		case 4:
			m_studyTimeList = new int[] {800, 1000, 1000};
			m_nBgTimeSleep = 5;
			break;
		}
	}
	
	//í•™ìŠµê¸‰ìˆ˜, í•™ìŠµëª©í‘œë‹¨ì–´ê°œìˆ˜ ì„¤ì •
	public void setStudyOrderAndWordCount(int nOrder, int nWordCount) {
		m_nStudyOrder = nOrder;
		m_nTargetWordCount = nWordCount;
	}
	
	public void setWordData(ArrayList<WordData> wordData) {
		m_arrStudyWords = wordData;
		if(m_arrStudyWords == null)
			return;
		
		initSound();

		/*
		try {
			for(int i=0; i<m_arrStudyWords.size(); i++) {
				String imgPath = Define.IMAGE_PATH + m_arrStudyWords.get(i).getImage() + ".png";
				FileInputStream fis = new FileInputStream(imgPath);
				BitmapFactory.Options options = new BitmapFactory.Options();
				options.inPurgeable = true;
				Bitmap wordPic = BitmapFactory.decodeStream(fis,null,options);
				m_arrStudyBmps.add(wordPic);
				fis.close();
			}
		}
		catch(Exception e) {
			e.printStackTrace();
		}*/

	}

	public Bitmap decodeSampledBitmapFromResourceMemOpt(
            InputStream inputStream, int reqWidth, int reqHeight) {

        byte[] byteArr = new byte[0];
        byte[] buffer = new byte[1024];
        int len;
        int count = 0;

        try {
            while ((len = inputStream.read(buffer)) > -1) {
                if (len != 0) {
                    if (count + len > byteArr.length) {
                        byte[] newbuf = new byte[(count + len) * 2];
                        System.arraycopy(byteArr, 0, newbuf, 0, count);
                        byteArr = newbuf;
                    }

                    System.arraycopy(buffer, 0, byteArr, count, len);
                    count += len;
                }
            }

            final BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeByteArray(byteArr, 0, count, options);

            options.inSampleSize = calculateInSampleSize(options, reqWidth,
                    reqHeight);
            options.inPurgeable = true;
            options.inInputShareable = true;
            options.inJustDecodeBounds = false;
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;

            int[] pids = { android.os.Process.myPid() };
            
            ActivityManager mAM = (ActivityManager)getContext().getSystemService(Context.ACTIVITY_SERVICE);
            MemoryInfo myMemInfo = mAM.getProcessMemoryInfo(pids)[0];
            Log.e(TAG, "dalvikPss (decoding) = " + myMemInfo.dalvikPss);

            return BitmapFactory.decodeByteArray(byteArr, 0, count, options);

        } catch (Exception e) {
            e.printStackTrace();

            return null;
        }
    }
	public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
	    // Raw height and width of image
	    final int height = options.outHeight;
	    final int width = options.outWidth;
	    int inSampleSize = 1;

	    if (height > reqHeight || width > reqWidth) {

	        // Calculate ratios of height and width to requested height and width
	        final int heightRatio = Math.round((float) height / (float) reqHeight);
	        final int widthRatio = Math.round((float) width / (float) reqWidth);

	        // Choose the smallest ratio as inSampleSize value, this will guarantee
	        // a final image with both dimensions larger than or equal to the
	        // requested height and width.
	        inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
	    }


	    return inSampleSize;
	}
	public WordData getCurrentWord() {
		return m_arrStudyWords.get(m_nCurrentWordCount);
	}
	
	public boolean getFlippingState() {
		return bFlipping;
	}
	
	public int getCurrentPhase() {
		return m_nCurrentPhase;
	}
	
	public void setCurrentPhase(int nPhase) {
		m_nCurrentPhase = nPhase;
	}
	
	public int getCurrentWordCount() {
		return m_nCurrentWordCount;
	}
	
	public void setCurrentWordCount(int nCount) {
		m_nCurrentWordCount = nCount;
		mCurrentIndex = nCount;
	}
	
	public int getCurrentRound() {
		return m_nCurrentRound;
	}
	
	public void setCurrentRound(int nRound) {
		m_nCurrentRound = nRound;
	}
	
	public void finishStudy() {
		m_bStudyPause = false;
		m_bArrowBtnPressed = true;
		m_bStudyStop = true;
		m_bDrawBgStopEn = true;
		m_bDrawBgStopKr = true;
		
		if(m_threadTextBg != null && m_threadTextBg.isAlive()) {
			try{
				m_threadTextBg.join();
			}
			catch(Exception e) {}
		}
		
		if(m_threadStudy != null && m_threadStudy.isAlive()) {
			try{
				m_threadStudy.join();
			}
			catch(Exception e) {}
		}
		
		if(m_soundPool != null) {
			m_soundPool.release();
			m_soundPool = null;
		}
		
		if(m_soundMap != null) {
			m_soundMap.clear();
			m_soundMap = null;
		}
	}
	
	private void setCurrentWordRember() {
		WordData wd = m_arrStudyWords.get(m_nCurrentWordCount);
		
		if(wd.getComplete() == 0) {
			wd.setComplete(1);
		}
		else {
			wd.setComplete(0);
		}
		
		if(m_listener != null)
			m_listener.onRember(wd);
	}
	
	private void showStartStudyDialog () {
		if(m_nCurrentPhase == 2) {
			m_bStudyPause = false;
			return;
		}

		String strMsg = this.getResources().getString(R.string.start_study_word);

		/*
		SekWordPopupOneButton popup;

		popup = new SekWordPopupOneButton(m_context);
		popup.setMessage(strMsg);
		popup.setListener(new OnSEKLaunchListener() {

			@Override
			public void onOK() {
				m_bStudyPause = false;
			}
		});
		popup.show();
*/

		SekConvStudyPopup popup;

		popup = new SekConvStudyPopup(m_context);
		popup.setStep(Define.STUDY_STEP_WORD);
		popup.setListener(new SekConvStudyPopup.OnSEKLaunchListener() {

			@Override
			public void onOK() {
				m_bStudyPause = false;
			}
		});
		popup.show();

		/*String strMsg = this.getResources().getString(R.string.start_study_phase1);
		if(m_nCurrentPhase == 2)
			strMsg = this.getResources().getString(R.string.start_study_phase2);
		
		SekWordPopupOneButton popup;
		
		popup = new SekWordPopupOneButton(m_context);
		popup.setMessage(strMsg);
		popup.setListener(new OnSEKLaunchListener() {

			@Override
			public void onOK() {
				m_bStudyPause = false;
			}
		});
		popup.show();*/

		/*new AlertDialog.Builder(m_context)
		.setTitle(this.getResources().getString(R.string.notice))
		.setCancelable(false)
		.setMessage(strMsg)
		.setPositiveButton(this.getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				m_bStudyPause = false;
				dialog.dismiss();
			}
		})
		.show();*/
		
	}
	
	private void startTextBgDrawEn() {

		int txtWidthBig = (int)(m_paintLeftText.measureText(m_arrStudyWords.get(m_nCurrentWordCount).getMeanEn()));

		final int txtWidth = txtWidthBig > 300 ? txtWidthBig : 300;
		
		if(txtWidth >= 250)
			m_nBgWidthCount = 6;
		else if(txtWidth >= 130 && txtWidth < 250)
			m_nBgWidthCount = 4;
		else
			m_nBgWidthCount = 2;
		
		m_nTextBgRightEn = EN_WORD_POINT.x - 5;
		m_bDrawBgStopEn = false;
		
		m_threadTextBg = new Thread() {
			private Handler m_handler = new Handler() {
				public void handleMessage(Message msg) {
					CurlView.this.invalidate();
				}
			};
			
			public void run() {
				while(!m_bDrawBgStopEn) {
					pauseThread();
					m_nTextBgRightEn += m_nBgWidthCount;
					if(m_nTextBgRightEn >= EN_WORD_POINT.x - 5 + txtWidth + 10) {
						m_nTextBgRightEn = EN_WORD_POINT.x - 5 + txtWidth + 10;
						m_handler.sendEmptyMessage(0);
						m_bDrawBgStopEn = true;
						break;
					}
					
					if(m_bArrowBtnPressed) continue;
					m_handler.sendEmptyMessage(0);
					
					try{
						Thread.sleep(m_nBgTimeSleep);
					}
					catch(Exception e) {}
				}
			}
		};
		m_threadTextBg.setName("TextBgThread");
		m_threadTextBg.start();
	}
	
	private void startTextBgDrawKr() {
		int txtWidthBig = (int)m_paintRightText.measureText(m_arrStudyWords.get(m_nCurrentWordCount).getMeanKr());

		final int txtWidth =txtWidthBig > 300 ? txtWidthBig : 300;

		if(txtWidth >= 250)
			m_nBgWidthCount = 6;
		else if(txtWidth >= 130 && txtWidth < 250)
			m_nBgWidthCount = 4;
		else
			m_nBgWidthCount = 2;
		
		m_nTextBgRightKr = KR_WORD_POINT.x - txtWidth + 5;		
		m_bDrawBgStopKr = false;
		
		m_threadTextBg = new Thread() {
			private Handler m_handler = new Handler() {
				public void handleMessage(Message msg) {
					CurlView.this.invalidate();
				}
			};
			
			public void run() {
				while(!m_bDrawBgStopKr) {
					pauseThread();
					
					m_nTextBgRightKr += m_nBgWidthCount;
					if(m_nTextBgRightKr >= KR_WORD_POINT.x  + 5) {
						m_nTextBgRightKr = KR_WORD_POINT.x  + 5;
						m_handler.sendEmptyMessage(0);
						m_bDrawBgStopKr = true;
						break;
					}
					
					if(m_bArrowBtnPressed) continue;
					m_handler.sendEmptyMessage(0);
					
					try{
						Thread.sleep(m_nBgTimeSleep);
					}
					catch(Exception e) {}
				}
			}
		};
		m_threadTextBg.setName("TextBgThread");
		m_threadTextBg.start();
	}
	
	private void pauseThread() {
		while(m_bStudyPause) {
			try{
				Thread.sleep(20);
			}
			catch(Exception e) {
				e.printStackTrace();
			}
		}
	}
	private void sleepThread(int nTime) {
		int nCount = 0;
		while(!m_bStudyStop) {
			pauseThread();
			
			if(m_bArrowBtnPressed)
				break;
			
			if(nCount * 20 > nTime) 
				break;
			
			nCount ++;
			
			try{
				Thread.sleep(20);
			}
			catch(Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	private void initStudyDraw() {
		m_bWordEnDraw = false;
		m_bWordKrDraw = false;
		m_bWordPicDraw = false;
	}
	
	public void startStudy() {
		m_threadStudy = new Thread() {
			public void run() {
				while(!m_bStudyStop) {
					if(m_nCurrentWordCount > m_arrStudyWords.size()-1) { 
						m_nCurrentRound ++;			
						m_nCurrentWordCount = 0;		
						mCurrentIndex = 0; 
						if(m_nCurrentRound > m_nStudyRepeat[m_nCurrentPhase-1]-1) {	
							m_nCurrentPhase ++;	
							m_nCurrentRound = 0;	
							m_bShowNoticeDlg = false;
							
							if(m_nCurrentPhase > 2) {		
								//m_nCurrentPhase  = 3;
							//	if(isCompleteAllRember()) {
									m_nCurrentPhase = 2;
									m_bStudyStop = true;
									m_handlerStudy.sendEmptyMessage(5);	
									break;
							//	}
							//	else	
							//		m_nCurrentCase = 1;
							}
						}
					}
					
					if((m_nCurrentPhase == 1 || m_nCurrentPhase == 2) &&
							m_nCurrentWordCount == 0 && m_nCurrentRound == 0) {	
						
						/*if(isCompleteAllRember()) {	
							m_bStudyStop = true;
							m_handlerStudy.sendEmptyMessage(6);	
							break;
						}*/
						if(!m_bShowNoticeDlg) {
							m_bStudyPause = true;
							m_handlerStudy.sendEmptyMessage(4);
						}
					}
					
					sleepThread(0);
					if(m_bArrowBtnPressed) {
						m_bArrowBtnPressed = false;
						continue;
					}
					m_handlerStudy.sendEmptyMessage(0);
					
					sleepThread(m_studyTimeList[0]);
					if(m_bArrowBtnPressed) {
						m_bArrowBtnPressed = false;
						continue;
					}
					m_handlerStudy.sendEmptyMessage(1);		
					
					sleepThread(m_studyTimeList[1]);
					if(m_bArrowBtnPressed) {
						m_bArrowBtnPressed = false;
						continue;
					}
					m_handlerStudy.sendEmptyMessage(2);		
					
					sleepThread(m_studyTimeList[2]);
					if(m_bArrowBtnPressed) {
						m_bArrowBtnPressed = false;
						continue;
					}
					m_handlerStudy.sendEmptyMessage(3);		
					
					try{
						Thread.sleep(100);
					}
					catch(Exception e) {}
				}
			}
		};
		m_threadStudy.setDaemon(true);
		m_threadStudy.setName("StudyWordThread");
		m_threadStudy.start();
	}
	
	@SuppressLint("NewApi")
	public void doNext() {
		m_bArrowBtnPressed = true;
		m_bStudyPause = false;
		m_bDrawBgStopEn = true;
		m_bDrawBgStopKr = true;
		
	//	m_soundPool.autoPause();
		initStudyDraw();
		
		m_nCurrentWordCount ++;
		mCurrentIndex++;
	}
	
	@SuppressLint("NewApi")
	public void doPrev() {
		m_bArrowBtnPressed = true;
		m_bStudyPause = false;
		m_bDrawBgStopEn = true;
		m_bDrawBgStopKr = true;
		
	//	m_soundPool.autoPause();
		initStudyDraw();
		
		m_nCurrentWordCount --;
		mCurrentIndex--;
		if(m_nCurrentWordCount < 0){
			m_nCurrentWordCount = 0;
			mCurrentIndex = 0;
		}
	}
	
	public void doPause() {
		m_bStudyPause = !m_bStudyPause;
		
		if(m_bStudyPause)
			pauseWordVoice(m_arrStudyWords.get(m_nCurrentWordCount).getSound());
		else
			resumeWordVoice(m_arrStudyWords.get(m_nCurrentWordCount).getSound());
	}
	
	public boolean isPause() {
		return m_bStudyPause;
	}
	
	public void setListener(StudyWordListener listener) {
		m_listener = listener;
	}
	
	public abstract interface StudyWordListener {
		public abstract void onRember(WordData wd);
		public abstract void onFinishAfter();
	}
	
	
	/////////////////////////////////////////////Curl Origin Functions
	
	
	
	
	
	/**
	 * Default constructor.
	 */
	public CurlView(Context ctx, AttributeSet attrs) {
		super(ctx, attrs);
		init(ctx);
		initialize(ctx);
		initStudyView(ctx);
		
		// Get the data from the XML AttributeSet
		{
			TypedArray a = ctx.obtainStyledAttributes(attrs, R.styleable.StudyPageCurlView);
	
			// Get data
			bEnableDebugMode = a.getBoolean(R.styleable.StudyPageCurlView_enableDebugMode, bEnableDebugMode);
			mCurlSpeed = a.getInt(R.styleable.StudyPageCurlView_curlSpeed, mCurlSpeed);
			mUpdateRate = a.getInt(R.styleable.StudyPageCurlView_updateRate, mUpdateRate);
			mInitialEdgeOffset = a.getInt(R.styleable.StudyPageCurlView_initialEdgeOffset, mInitialEdgeOffset);
			mCurlMode = a.getInt(R.styleable.StudyPageCurlView_curlMode, mCurlMode);
			
			Log.i(TAG, "mCurlSpeed: " + mCurlSpeed);
			Log.i(TAG, "mUpdateRate: " + mUpdateRate);
			Log.i(TAG, "mInitialEdgeOffset: " + mInitialEdgeOffset);
			Log.i(TAG, "mCurlMode: " + mCurlMode);
			// recycle object (so it can be used by others)
			a.recycle();
		}
		
		ResetClipEdge();
	
	}

	/**
	 * Default constructor.
	 */
	public CurlView(Context ctx, AttributeSet attrs, int defStyle) {
		this(ctx, attrs);
	}

	/**
	 * Get current page index. Page indices are zero based values presenting
	 * page being shown on right side of the book.
	 */
	public int getCurrentIndex() {
		return mCurrentIndex;
	}

	/**
	 * Initialize method.
	 */
	private void init(Context ctx) {
		mRenderer = new CurlRenderer(this);
		setRenderer(mRenderer);
		setWillNotDraw(false);
		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
		//setRenderMode(GLSurfaceView.RENDERMODE_CONTINUOUSLY);
		setOnTouchListener(this);

		// Even though left and right pages are static we have to allocate room
		// for curl on them too as we are switching meshes. Another way would be
		// to swap texture ids only.
		mPageLeft = new CurlMesh(10);
		mPageRight = new CurlMesh(10);
		mPageCurl = new CurlMesh(10);
		mPageLeft.setFlipTexture(true);
		mPageRight.setFlipTexture(false);
		
		this.setDrawingCacheEnabled(true);

	}

	@Override
	public void onDrawFrame() {
		// We are not animating.
		if (mAnimate == false) {
			return;
		}

		long currentTime = System.currentTimeMillis();
		// If animation is done.
		if (currentTime >= mAnimationStartTime + mAnimationDurationTime) {
			if (mAnimationTargetEvent == SET_CURL_TO_RIGHT) {
				// Switch curled page to right.
				CurlMesh right = mPageCurl;
				CurlMesh curl = mPageRight;
				right.setRect(mRenderer.getPageRect(CurlRenderer.PAGE_RIGHT));
				right.setFlipTexture(false);
				right.reset();
				mRenderer.removeCurlMesh(curl);
				mPageCurl = curl;
				mPageRight = right;
				// If we were curling left page update current index.
//				if (mCurlState == CURL_LEFT) {
//					--mCurrentIndex;
//				}
				
				if (mCurrentIndex > 0) {
					if (mCurlState == CURL_LEFT) {
						doPrev();
						--mCurrentIndex;
					}
				}

			} else if (mAnimationTargetEvent == SET_CURL_TO_LEFT) {
				// Switch curled page to left.
				CurlMesh left = mPageCurl;
				CurlMesh curl = mPageLeft;
				left.setRect(mRenderer.getPageRect(CurlRenderer.PAGE_LEFT));
				left.setFlipTexture(true);
				left.reset();
				mRenderer.removeCurlMesh(curl);
				if (!mRenderLeftPage) {
					mRenderer.removeCurlMesh(left);
				}
				mPageCurl = curl;
				mPageLeft = left;
				// If we were curling right page update current index.
//				if (mCurlState == CURL_RIGHT) {
//				}
				
				if (mCurrentIndex < mPageProvider.getPageCount()) {
					if (mCurlState == CURL_RIGHT) {
						doNext();
						++mCurrentIndex;

					}
				}
				
			}
			mCurlState = CURL_NONE;
			mAnimate = false;
			requestRender();
		} else {
			mPointerPos.mPos.set(mAnimationSource);
			float t = 1f - ((float) (currentTime - mAnimationStartTime) / mAnimationDurationTime);
			t = 1f - (t * t * t * (3 - 2 * t));
			mPointerPos.mPos.x += (mAnimationTarget.x - mAnimationSource.x) * t;
			mPointerPos.mPos.y += (mAnimationTarget.y - mAnimationSource.y) * t;
			updateCurlPos(mPointerPos);
		}
	}

	@Override
	public void onPageSizeChanged(int width, int height) {
		mPageBitmapWidth = width;
		mPageBitmapHeight = height;
		updatePages();
		requestRender();
	}

	@Override
	public void onSizeChanged(int w, int h, int ow, int oh) {
		super.onSizeChanged(w, h, ow, oh);
		requestRender();
		if (mSizeChangedObserver != null) {
			mSizeChangedObserver.onSizeChanged(w, h);
		}
	}

	@Override
	public void onSurfaceCreated() {
		// In case surface is recreated, let page meshes drop allocated texture
		// ids and ask for new ones. There's no need to set textures here as
		// onPageSizeChanged should be called later on.
		mPageLeft.resetTexture();
		mPageRight.resetTexture();
		mPageCurl.resetTexture();
	}

	@Override
	public boolean onTouch(View view, MotionEvent me) {
		
//		if (!bBlockTouchInput) {
				// No dragging during animation at the moment.
				// TODO: Stop animation on touch event and return to drag mode.
				if (mAnimate || mPageProvider == null) {
					return false;
				}
		
				// We need page rects quite extensively so get them for later use.
				RectF rightRect = mRenderer.getPageRect(CurlRenderer.PAGE_RIGHT);
				RectF leftRect = mRenderer.getPageRect(CurlRenderer.PAGE_LEFT);
		
				// Store pointer position.
				mPointerPos.mPos.set(me.getX(), me.getY());
				mRenderer.translate(mPointerPos.mPos);
				if (mEnableTouchPressure) {
					mPointerPos.mPressure = me.getPressure();
				} else {
					mPointerPos.mPressure = 0.8f;
				}

		
		
				// Get our finger position
							mFinger.x = me.getX();
							mFinger.y = me.getY();
							int width = getWidth();
				
				
				switch (me.getAction()) {
				case MotionEvent.ACTION_DOWN: {

					
					
                    Log.i("Action_Down: ", "Action_down : x: "+mFinger.x+" y: "+mFinger.y );
//					mOldMovement.x = mFinger.x;
//					mOldMovement.y = mFinger.y;
//					
//					// If we moved over the half of the display flip to next
//					if (mOldMovement.x > (width >> 1)) {
//						mMovement.x = mInitialEdgeOffset;
//						mMovement.y = mInitialEdgeOffset;
//						
//						// Set the right movement flag
//						bFlipRight = true;
//					} else {
//						// Set the left movement flag
//						bFlipRight = false;
//						
//						// go to next previous page
////						previousView();
//						
//						// Set new movement
//						mMovement.x = IsCurlModeDynamic()?width<<1:width;
//						mMovement.y = mInitialEdgeOffset;
//					}
//					
					
					// Once we receive pointer down event its position is mapped to
					// right or left edge of page and that'll be the position from where
					// user is holding the paper to make curl happen.
					mDragStartPos.set(mPointerPos.mPos);
		
					// First we make sure it's not over or below page. Pages are
					// supposed to be same height so it really doesn't matter do we use
					// left or right one.
					if (mDragStartPos.y > rightRect.top) {
						mDragStartPos.y = rightRect.top;
					} else if (mDragStartPos.y < rightRect.bottom) {
						mDragStartPos.y = rightRect.bottom;
					}
		
					// Then we have to make decisions for the user whether curl is going
					// to happen from left or right, and on which page.
					if (mViewMode == SHOW_TWO_PAGES) {
						// If we have an open book and pointer is on the left from right
						// page we'll mark drag position to left edge of left page.
						// Additionally checking mCurrentIndex is higher than zero tells
						// us there is a visible page at all.
						if (mDragStartPos.x < rightRect.left && mCurrentIndex > 0) {
							mDragStartPos.x = leftRect.left;
							startCurl(CURL_LEFT);
							
						}
						// Otherwise check pointer is on right page's side.
						else if (mDragStartPos.x >= rightRect.left
								&& mCurrentIndex < mPageProvider.getPageCount()) {
							mDragStartPos.x = rightRect.right;
							if (!mAllowLastPageCurl
									&& mCurrentIndex >= mPageProvider.getPageCount() - 1) {
								return false;
							}
							startCurl(CURL_RIGHT);
						}
					} else if (mViewMode == SHOW_ONE_PAGE) {
						float halfX = (rightRect.right + rightRect.left) / 2;
						if (mDragStartPos.x < halfX && mCurrentIndex > 0) {
							mDragStartPos.x = rightRect.left;
							startCurl(CURL_LEFT);
						} else if (mDragStartPos.x >= halfX
								&& mCurrentIndex < mPageProvider.getPageCount()) {
							mDragStartPos.x = rightRect.right;
							if (!mAllowLastPageCurl
									&& mCurrentIndex >= mPageProvider.getPageCount() - 1) {
								return false;
							}
							startCurl(CURL_RIGHT);
						}else{

						}
					}
					// If we have are in curl state, let this case clause flow through
					// to next one. We have pointer position and drag position defined
					// and this will create first render request given these points.
					if (mCurlState == CURL_NONE) {
						return false;
					} 
					
					if (mCurlState == CURL_LEFT || mCurlState == CURL_RIGHT) {
						// Animation source is the point from where animation starts.
						// Also it's handled in a way we actually simulate touch events
						// meaning the output is exactly the same as if user drags the
						// page to other side. While not producing the best looking
						// result (which is easier done by altering curl position and/or
						// direction directly), this is done in a hope it made code a
						// bit more readable and easier to maintain.
						mAnimationSource.set(mPointerPos.mPos);
						mAnimationStartTime = System.currentTimeMillis();
		
						// Given the explanation, here we decide whether to simulate
						// drag to left or right end.

						
						if (mCurlState == CURL_LEFT ){
					
							
							bFlipRight = false;
							bFlipping=false;
							// flipping = true
							// On right side target is always right page's right border.
							mAnimationTarget.set(mDragStartPos);
							mAnimationTarget.x = mRenderer
									.getPageRect(CurlRenderer.PAGE_RIGHT).right;
							
							
							mAnimationTargetEvent = SET_CURL_TO_RIGHT;
							
						} else {
							

							// flipping = false;
							// On left side target depends on visible pages.
							mAnimationTarget.set(mDragStartPos);
							if (mCurlState == CURL_RIGHT || mViewMode == SHOW_TWO_PAGES) {
								mAnimationTarget.x = leftRect.left;
							} else {
								mAnimationTarget.x = rightRect.left;
							}
							mAnimationTargetEvent = SET_CURL_TO_LEFT;
						}
						mAnimate = true;
						requestRender();
					}
					
					m_bStudyPause = false;
					
					if((m_nCurrentWordCount == 0 && !bFlipRight)/* ||
							(m_nCurrentWordCount == m_arrStudyWords.size()-1 && m_nCurrentRound == m_nStudyRepeat[m_nCurrentPhase-1]-1 &&
							m_nCurrentPhase == 2 && bFlipRight)*/ ){
						break;
					}
					
					if(m_nCurrentWordCount <= m_arrStudyWords.size()-1)
						pauseWordVoice(m_arrStudyWords.get(m_nCurrentWordCount).getSound());
					
					StudyWordActivity.playWordVoice("page");
					
					if(m_threadTextBg != null && m_threadTextBg.isAlive()) {
						m_bDrawBgStopEn = true;
						m_bDrawBgStopKr = true;
						try{
							m_threadTextBg.join();
						}
						catch(Exception e) {
							
						}
					}
					
					break;
				}
				case MotionEvent.ACTION_MOVE: {
						//updateCurlPos(mPointerPos);

					break;
				}
				case MotionEvent.ACTION_CANCEL:
				case MotionEvent.ACTION_UP: {
                    Log.i("Action_Up: ", "Action_Up : x: "+mFinger.x+" y: "+mFinger.y );
	
					}				

					break;
				}
				if(bFlipping || bUserMoves) {	//LJG
					m_bStudyPause = true;		
				}
		return true;
	}

	/**
	 * Allow the last page to curl.
	 */
	public void setAllowLastPageCurl(boolean allowLastPageCurl) {
		mAllowLastPageCurl = allowLastPageCurl;
	}

	/**
	 * Sets background color - or OpenGL clear color to be more precise. Color
	 * is a 32bit value consisting of 0xAARRGGBB and is extracted using
	 * android.graphics.Color eventually.
	 */
	@Override
	public void setBackgroundColor(int color) {
		mRenderer.setBackgroundColor(color);
		requestRender();
	}

	/**
	 * Sets mPageCurl curl position.
	 */
	private void setCurlPos(PointF curlPos, PointF curlDir, double radius) {

		// First reposition curl so that page doesn't 'rip off' from book.
		if (mCurlState == CURL_RIGHT
				|| (mCurlState == CURL_LEFT && mViewMode == SHOW_ONE_PAGE)) {
			RectF pageRect = mRenderer.getPageRect(CurlRenderer.PAGE_RIGHT);
			if (curlPos.x >= pageRect.right) {
				mPageCurl.reset();
				requestRender();
				return;
			}
			if (curlPos.x < pageRect.left) {
				curlPos.x = pageRect.left;
			}
			if (curlDir.y != 0) {
				float diffX = curlPos.x - pageRect.left;
				float leftY = curlPos.y + (diffX * curlDir.x / curlDir.y);
				if (curlDir.y < 0 && leftY < pageRect.top) {
					curlDir.x = curlPos.y - pageRect.top;
					curlDir.y = pageRect.left - curlPos.x;
				} else if (curlDir.y > 0 && leftY > pageRect.bottom) {
					curlDir.x = pageRect.bottom - curlPos.y;
					curlDir.y = curlPos.x - pageRect.left;
				}
			}
		} else if (mCurlState == CURL_LEFT) {
			RectF pageRect = mRenderer.getPageRect(CurlRenderer.PAGE_LEFT);
			if (curlPos.x <= pageRect.left) {
				mPageCurl.reset();
				requestRender();
				return;
			}
			if (curlPos.x > pageRect.right) {
				curlPos.x = pageRect.right;
			}
			if (curlDir.y != 0) {
				float diffX = curlPos.x - pageRect.right;
				float rightY = curlPos.y + (diffX * curlDir.x / curlDir.y);
				if (curlDir.y < 0 && rightY < pageRect.top) {
					curlDir.x = pageRect.top - curlPos.y;
					curlDir.y = curlPos.x - pageRect.right;
				} else if (curlDir.y > 0 && rightY > pageRect.bottom) {
					curlDir.x = curlPos.y - pageRect.bottom;
					curlDir.y = pageRect.right - curlPos.x;
				}
			}
		}

		// Finally normalize direction vector and do rendering.
		double dist = Math.sqrt(curlDir.x * curlDir.x + curlDir.y * curlDir.y);
		if (dist != 0) {
			curlDir.x /= dist;
			curlDir.y /= dist;
			mPageCurl.curl(curlPos, curlDir, radius);
		} else {
			mPageCurl.reset();
		}

		requestRender();
	}

	/**
	 * Set current page index. Page indices are zero based values presenting
	 * page being shown on right side of the book. E.g if you set value to 4;
	 * right side front facing bitmap will be with index 4, back facing 5 and
	 * for left side page index 3 is front facing, and index 2 back facing (once
	 * page is on left side it's flipped over).
	 * 
	 * Current index is rounded to closest value divisible with 2.
	 */
	public void setCurrentIndex(int index) {
		if (mPageProvider == null || index < 0) {
			mCurrentIndex = 0;
		} else {
			if (mAllowLastPageCurl) {
				mCurrentIndex = Math.min(index, mPageProvider.getPageCount());
			} else {
				mCurrentIndex = Math.min(index,
						mPageProvider.getPageCount() - 1);
			}
		}
		updatePages();
		requestRender();
	}

	/**
	 * If set to true, touch event pressure information is used to adjust curl
	 * radius. The more you press, the flatter the curl becomes. This is
	 * somewhat experimental and results may vary significantly between devices.
	 * On emulator pressure information seems to be flat 1.0f which is maximum
	 * value and therefore not very much of use.
	 */
	public void setEnableTouchPressure(boolean enableTouchPressure) {
		mEnableTouchPressure = enableTouchPressure;
	}

	/**
	 * Set margins (or padding). Note: margins are proportional. Meaning a value
	 * of .1f will produce a 10% margin.
	 */
	public void setMargins(float left, float top, float right, float bottom) {
		mRenderer.setMargins(left, top, right, bottom);
	}

	/**
	 * Update/set page provider.
	 */
	public void setPageProvider(PageProvider pageProvider) {
		mPageProvider = pageProvider;
		mCurrentIndex = 0;
		updatePages();
		requestRender();
	}

	/**
	 * Setter for whether left side page is rendered. This is useful mostly for
	 * situations where right (main) page is aligned to left side of screen and
	 * left page is not visible anyway.
	 */
	public void setRenderLeftPage(boolean renderLeftPage) {
		mRenderLeftPage = renderLeftPage;
	}

	/**
	 * Sets SizeChangedObserver for this View. Call back method is called from
	 * this View's onSizeChanged method.
	 */
	public void setSizeChangedObserver(SizeChangedObserver observer) {
		mSizeChangedObserver = observer;
	}

	/**
	 * Sets view mode. Value can be either SHOW_ONE_PAGE or SHOW_TWO_PAGES. In
	 * former case right page is made size of display, and in latter case two
	 * pages are laid on visible area.
	 */
	public void setViewMode(int viewMode) {
		switch (viewMode) {
		case SHOW_ONE_PAGE:
			mViewMode = viewMode;
			mPageLeft.setFlipTexture(true);
			mRenderer.setViewMode(CurlRenderer.SHOW_ONE_PAGE);
			break;
		case SHOW_TWO_PAGES:
			mViewMode = viewMode;
			mPageLeft.setFlipTexture(false);
			mRenderer.setViewMode(CurlRenderer.SHOW_TWO_PAGES);
			break;
		}
	}

	/**
	 * Switches meshes and loads new bitmaps if available. Updated to support 2
	 * pages in landscape
	 */
	private void startCurl(int page) {
		//updatePages();
		
		switch (page) {

		// Once right side page is curled, first right page is assigned into
		// curled page. And if there are more bitmaps available new bitmap is
		// loaded into right side mesh.
		case CURL_RIGHT: {
			// Remove meshes from renderer.
			mRenderer.removeCurlMesh(mPageLeft);
			mRenderer.removeCurlMesh(mPageRight);
			mRenderer.removeCurlMesh(mPageCurl);

			// We are curling right page.
			CurlMesh curl = mPageRight;
			mPageRight = mPageCurl;
			mPageCurl = curl;

			if (mCurrentIndex > 0) {
				mPageLeft.setFlipTexture(true);
				mPageLeft
						.setRect(mRenderer.getPageRect(CurlRenderer.PAGE_LEFT));
				mPageLeft.reset();
				if (mRenderLeftPage) {
					mRenderer.addCurlMesh(mPageLeft);
				}
			}
			if (mCurrentIndex < mPageProvider.getPageCount() - 1) {
				updatePage(mPageRight.getTexturePage(), mCurrentIndex + 1);
				mPageRight.setRect(mRenderer
						.getPageRect(CurlRenderer.PAGE_RIGHT));
				mPageRight.setFlipTexture(false);
				mPageRight.reset();
				mRenderer.addCurlMesh(mPageRight);
			}

			doPause();
			initStudyDraw();
			this.invalidate();
			//refreshDrawableState();

			// Add curled page to renderer.
			//updatePage1(mPageCurl.getTexturePage(), 0);
			
			//this.invalidate();
			
			mPageCurl.setRect(mRenderer.getPageRect(CurlRenderer.PAGE_RIGHT));
			mPageCurl.setFlipTexture(false);
			mPageCurl.reset();
			mRenderer.addCurlMesh(mPageCurl);

			mCurlState = CURL_RIGHT;
			break;
		}

		// On left side curl, left page is assigned to curled page. And if
		// there are more bitmaps available before currentIndex, new bitmap
		// is loaded into left page.
		case CURL_LEFT: {
			// Remove meshes from renderer.
			mRenderer.removeCurlMesh(mPageLeft);
			mRenderer.removeCurlMesh(mPageRight);
			mRenderer.removeCurlMesh(mPageCurl);

			// We are curling left page.
			CurlMesh curl = mPageLeft;
			mPageLeft = mPageCurl;
			mPageCurl = curl;

			if (mCurrentIndex > 1) {
				updatePage(mPageLeft.getTexturePage(), mCurrentIndex - 2);
				mPageLeft.setFlipTexture(true);
				mPageLeft
						.setRect(mRenderer.getPageRect(CurlRenderer.PAGE_LEFT));
				mPageLeft.reset();
				if (mRenderLeftPage) {
					mRenderer.addCurlMesh(mPageLeft);
				}
			}

			// If there is something to show on right page add it to renderer.
			if (mCurrentIndex < mPageProvider.getPageCount()) {
				mPageRight.setFlipTexture(false);
				mPageRight.setRect(mRenderer
						.getPageRect(CurlRenderer.PAGE_RIGHT));
				mPageRight.reset();
				mRenderer.addCurlMesh(mPageRight);
			}

			doPause();
			initStudyDraw();
			this.invalidate();
			
			//updatePage1(mPageRight.getTexturePage(), 0);
			//updatePage(mPageCurl.getTexturePage(), 0);

			
			// How dragging previous page happens depends on view mode.
			if (mViewMode == SHOW_ONE_PAGE
					|| (mCurlState == CURL_LEFT && mViewMode == SHOW_TWO_PAGES)) {
				mPageCurl.setRect(mRenderer
						.getPageRect(CurlRenderer.PAGE_RIGHT));
				mPageCurl.setFlipTexture(false);
			} else {
				mPageCurl
						.setRect(mRenderer.getPageRect(CurlRenderer.PAGE_LEFT));
				mPageCurl.setFlipTexture(true);
			}
			mPageCurl.reset();
			mRenderer.addCurlMesh(mPageCurl);

			mCurlState = CURL_LEFT;
			break;
		}

		}
		
	}

	/**
	 * Updates curl position.
	 */
	private void updateCurlPos(PointerPosition pointerPos) {

		// Default curl radius.
		double radius = mRenderer.getPageRect(CURL_RIGHT).width() / 3;
		// TODO: This is not an optimal solution. Based on feedback received so
		// far; pressure is not very accurate, it may be better not to map
		// coefficient to range [0f, 1f] but something like [.2f, 1f] instead.
		// Leaving it as is until get my hands on a real device. On emulator
		// this doesn't work anyway.
		radius *= Math.max(1f - pointerPos.mPressure, 0f);
		// NOTE: Here we set pointerPos to mCurlPos. It might be a bit confusing
		// later to see e.g "mCurlPos.x - mDragStartPos.x" used. But it's
		// actually pointerPos we are doing calculations against. Why? Simply to
		// optimize code a bit with the cost of making it unreadable. Otherwise
		// we had to this in both of the next if-else branches.
		mCurlPos.set(pointerPos.mPos);

		// If curl happens on right page, or on left page on two page mode,
		// we'll calculate curl position from pointerPos.
		if (mCurlState == CURL_RIGHT
				|| (mCurlState == CURL_LEFT && mViewMode == SHOW_TWO_PAGES)) {

			mCurlDir.x = mCurlPos.x - mDragStartPos.x;
			mCurlDir.y = mCurlPos.y - mDragStartPos.y;
			float dist = (float) Math.sqrt(mCurlDir.x * mCurlDir.x + mCurlDir.y
					* mCurlDir.y);

			// Adjust curl radius so that if page is dragged far enough on
			// opposite side, radius gets closer to zero.
			float pageWidth = mRenderer.getPageRect(CurlRenderer.PAGE_RIGHT)
					.width();
			double curlLen = radius * Math.PI;
			if (dist > (pageWidth * 2) - curlLen) {
				curlLen = Math.max((pageWidth * 2) - dist, 0f);
				radius = curlLen / Math.PI;
			}

			// Actual curl position calculation.
			if (dist >= curlLen) {
				double translate = (dist - curlLen) / 2;
				if (mViewMode == SHOW_TWO_PAGES) {
					mCurlPos.x -= mCurlDir.x * translate / dist;
				} else {
					float pageLeftX = mRenderer
							.getPageRect(CurlRenderer.PAGE_RIGHT).left;
					radius = Math.max(Math.min(mCurlPos.x - pageLeftX, radius),
							0f);
				}
				mCurlPos.y -= mCurlDir.y * translate / dist;
			} else {
				double angle = Math.PI * Math.sqrt(dist / curlLen);
				double translate = radius * Math.sin(angle);
				mCurlPos.x += mCurlDir.x * translate / dist;
				mCurlPos.y += mCurlDir.y * translate / dist;
			}
		}
		// Otherwise we'll let curl follow pointer position.
		else if (mCurlState == CURL_LEFT) {

			// Adjust radius regarding how close to page edge we are.
			float pageLeftX = mRenderer.getPageRect(CurlRenderer.PAGE_RIGHT).left;
			radius = Math.max(Math.min(mCurlPos.x - pageLeftX, radius), 0f);

			float pageRightX = mRenderer.getPageRect(CurlRenderer.PAGE_RIGHT).right;
			mCurlPos.x -= Math.min(pageRightX - mCurlPos.x, radius);
			mCurlDir.x = mCurlPos.x + mDragStartPos.x;
			mCurlDir.y = mCurlPos.y - mDragStartPos.y;
		}

		setCurlPos(mCurlPos, mCurlDir, radius);
	}

	/**
	 * Updates given CurlPage via PageProvider for page located at index.
	 */
	private void updatePage(CurlPage page, int index) {
		// First reset page to initial state.
		page.reset();
		// Ask page provider to fill it up with bitmaps and colors.
		mPageProvider.updatePage(page, mPageBitmapWidth, mPageBitmapHeight,
				index);
	}
	
	private void updatePage1(CurlPage page, int index) {
		Log.e("TEST PAGE", "!234");
		// First reset page to initial state.
		//page.reset();
		// Ask page provider to fill it up with bitmaps and colors.
		//mPageProvider.updatePage1(page, mPageBitmapWidth, mPageBitmapHeight, index);
		EGL10 egl = (EGL10)EGLContext.getEGL();
		GL10 gl = (GL10) egl.eglGetCurrentContext().getGL();
		
		
		Bitmap bitmap = createBitmapFromGLSurface(0, 0, mPageBitmapWidth, mPageBitmapHeight+68, gl);
		
		Bitmap bitmapBack = Bitmap.createBitmap(mPageBitmapWidth, mPageBitmapHeight,
				Bitmap.Config.ARGB_8888);
		bitmapBack.eraseColor(0xFFFFFFFF);
		Canvas c = new Canvas(bitmapBack);
		
		Drawable d = getResources().getDrawable( R.drawable.study_paper_bg3);

		int margin = 0;
		int border = 1;
		Rect r = new Rect(margin, margin, mPageBitmapWidth - margin, mPageBitmapHeight - margin);

		int imageWidth = r.width() - (border * 2);
		int imageHeight = r.height() - (border * 2);
		
		r.left += ((r.width() - imageWidth) / 2) - border;
		r.right = r.left + imageWidth + border + border;
		r.top += ((r.height() - imageHeight) / 2) - border;
		r.bottom = r.top + imageHeight + border + border;

		Paint p = new Paint();
		p.setColor(0xFFC0C0C0);
		c.drawRect(r, p);
		r.left += border;
		r.right -= border;
		r.top += border;
		r.bottom -= border;

		d.setBounds(r);
		d.draw(c);
		
		c.drawBitmap(bitmap, new Matrix(), null);
		
		page.setTexture(bitmapBack, CurlPage.SIDE_BOTH);
		page.setColor(Color.argb(127, 255, 255, 255),
				CurlPage.SIDE_BACK);
		
		bDrawn = false;
		
		this.invalidate();
		
		/*while(bDrawn == false){
			try {
				Thread.sleep(30);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}*/
		
		/*page.reset();
		page.setTexture(bitmapCurl, CurlPage.SIDE_BOTH);
		page.setColor(Color.argb(127, 255, 255, 255),
				CurlPage.SIDE_BACK);
		*/
		//requestRender();
		
		
//		gl.glClearColor(0,0,0,1);
//		gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		//onPause();
		//requestRender();
		//page.setColor(Color.argb(127, 255, 255, 255), CurlPage.SIDE_BACK);
		
	}


	private Bitmap createBitmapFromGLSurface(int x, int y, int w, int h, GL10 gl)
	        throws OutOfMemoryError {
	    int bitmapBuffer[] = new int[w * h];
	    int bitmapSource[] = new int[w * h];
	    IntBuffer intBuffer = IntBuffer.wrap(bitmapBuffer);
	    intBuffer.position(0);

	    try {
	        gl.glReadPixels(x, y, w, h, GL10.GL_RGBA, GL10.GL_UNSIGNED_BYTE, intBuffer);
	        int offset1, offset2;
	        for (int i = 0; i < h; i++) {
	            offset1 = i * w;
	            offset2 = (h - i - 1) * w;
	            for (int j = 0; j < w; j++) {
	                int texturePixel = bitmapBuffer[offset1 + j];
	                int blue = (texturePixel >> 16) & 0xff;
	                int red = (texturePixel << 16) & 0x00ff0000;
	                int pixel = (texturePixel & 0xff00ff00) | red | blue;
	                bitmapSource[offset2 + j] = pixel;
	            }
	        }
	    } catch (GLException e) {
	        return null;
	    }

	    return Bitmap.createBitmap(bitmapSource, w, h, Bitmap.Config.ARGB_8888);
	}

	/**
	 * Updates bitmaps for page meshes.
	 */
	private void updatePages() {
		if (mPageProvider == null || mPageBitmapWidth <= 0
				|| mPageBitmapHeight <= 0) {
			return;
		}

		// Remove meshes from renderer.
		mRenderer.removeCurlMesh(mPageLeft);
		mRenderer.removeCurlMesh(mPageRight);
		mRenderer.removeCurlMesh(mPageCurl);

		int leftIdx = mCurrentIndex - 1;
		int rightIdx = mCurrentIndex;
		int curlIdx = -1;
		if (mCurlState == CURL_LEFT) {
			curlIdx = leftIdx;
			--leftIdx;
		} else if (mCurlState == CURL_RIGHT) {
			curlIdx = rightIdx;
			++rightIdx;
		}

		if (rightIdx >= 0 && rightIdx < mPageProvider.getPageCount()) {
			updatePage(mPageRight.getTexturePage(), rightIdx);
			mPageRight.setFlipTexture(false);
			mPageRight.setRect(mRenderer.getPageRect(CurlRenderer.PAGE_RIGHT));
			mPageRight.reset();
			mRenderer.addCurlMesh(mPageRight);
		}

		//if (leftIdx >= 0 && leftIdx < mPageProvider.getPageCount())
		{
			updatePage(mPageLeft.getTexturePage(), leftIdx);
			mPageLeft.setFlipTexture(true);
			mPageLeft.setRect(mRenderer.getPageRect(CurlRenderer.PAGE_LEFT));
			mPageLeft.reset();
			if (mRenderLeftPage) {
				mRenderer.addCurlMesh(mPageLeft);
			}
		}
		if (curlIdx == 0 && curlIdx < mPageProvider.getPageCount()) {
			updatePage(mPageCurl.getTexturePage(), curlIdx);

			if (mCurlState == CURL_RIGHT) {
				mPageCurl.setFlipTexture(true);
				mPageCurl.setRect(mRenderer
						.getPageRect(CurlRenderer.PAGE_RIGHT));
			} else {
				mPageCurl.setFlipTexture(false);
				mPageCurl
						.setRect(mRenderer.getPageRect(CurlRenderer.PAGE_LEFT));
			}

			mPageCurl.reset();
			mRenderer.addCurlMesh(mPageCurl);
		}
		if (curlIdx > 0 && curlIdx < mPageProvider.getPageCount()) {
			updatePage(mPageCurl.getTexturePage(), curlIdx);

			if (mCurlState == CURL_RIGHT) {
				mPageCurl.setFlipTexture(true);
				mPageCurl.setRect(mRenderer
						.getPageRect(CurlRenderer.PAGE_RIGHT));
			} else {
				mPageCurl.setFlipTexture(false);
				mPageCurl
						.setRect(mRenderer.getPageRect(CurlRenderer.PAGE_LEFT));
			}

			mPageCurl.reset();
			mRenderer.addCurlMesh(mPageCurl);
		}
	}

	/**
	 * Provider for feeding 'book' with bitmaps which are used for rendering
	 * pages.
	 */
	public interface PageProvider {

		/**
		 * Return number of pages available.
		 */
		public int getPageCount();

		/**
		 * Called once new bitmaps/textures are needed. Width and height are in
		 * pixels telling the size it will be drawn on screen and following them
		 * ensures that aspect ratio remains. But it's possible to return bitmap
		 * of any size though. You should use provided CurlPage for storing page
		 * information for requested page number.<br/>
		 * <br/>
		 * Index is a number between 0 and getBitmapCount() - 1.
		 */
		public void updatePage(CurlPage page, int width, int height, int index);
		public void updatePage1(CurlPage page, int width, int height, int index);

	}

	/**
	 * Simple holder for pointer position.
	 */
	private class PointerPosition {
		PointF mPos = new PointF();
		float mPressure;
	}

	/**
	 * Observer interface for handling CurlView size changes.
	 */
	public interface SizeChangedObserver {

		/**
		 * Called once CurlView size changes.
		 */
		public void onSizeChanged(int width, int height);
	}

}
