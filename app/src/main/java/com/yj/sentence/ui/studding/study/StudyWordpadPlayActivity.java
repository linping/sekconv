package com.yj.sentence.ui.studding.study;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.animation.BounceInterpolator;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.yj.sentence.R;
import com.yj.sentence.common.Define;
import com.yj.sentence.database.PAKDBManager;
import com.yj.sentence.database.ProgressDBManager;
import com.yj.sentence.database.StudyDBManager;
import com.yj.sentence.structs.WordData;
import com.yj.sentence.ui.studding.audio.AudioDataReceivedListener;
import com.yj.sentence.ui.studding.audio.RecognizeResultListener;
import com.yj.sentence.ui.studding.audio.RecordWaveformView;
import com.yj.sentence.ui.studding.audio.RecordingThread;
import com.yj.sentence.ui.studding.util.EsStuddingRecognizer;
import com.yj.sentence.ui.studding.util.EsStuddingUtils;
import com.yj.sentence.ui.words.EsRegistryActivity;
import com.yj.sentence.utils.ESUtils;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;

public class StudyWordpadPlayActivity extends Activity implements View.OnClickListener {


    private ImageButton btnBack;
    private ImageButton btnNext;

    private ImageView imageStatus;
    private TextView textPrev1;
    private TextView textPrev2;
    private TextView textNext1;
    private TextView textNext2;
    private TextView textEnglish;
    private TextView textKorean;
    private TextView textCurNum;
    private TextView textAllNum;

    private ColorView viewColor;

    private ProgressDBManager mProgressManager;
    private StudyDBManager mStudyManager;
    private PAKDBManager m_pakManager = null;

    ArrayList<WordData> m_PracticeData;

    private Thread  m_threadStudy = null;
    private boolean m_bStudyStop = false;

    private int m_nCurrentWordCount = 0;

    private boolean m_bStudyPause = false;
    private boolean m_bArrowBtnPressed = false;

    private String strAudioPath;
    private MediaPlayer mp;

    private RecordWaveformView mRealtimeWaveformView;
    private EsStuddingRecognizer mRecognizer = null;

    private ImageView imageMark;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_study_wordpadplay);

        Intent intent = this.getIntent();

        mRecognizer = EsStuddingRecognizer.getInstance();
        mRecognizer.setListner(new AudioDataReceivedListener() {
            @Override
            public void onAudioDataReceived(short[] data) {
                processBuffer(data);
            }
        });

        mRecognizer.setResultListner(new RecognizeResultListener() {
            @Override
            public void onResult(String strResult) {
                processResult(strResult);
            }
        });

        initViews();
        initData();
        startStudy();
    }

    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        finishStudy();
        super.onDestroy();
    }


    private void initViews(){

        imageStatus = (ImageView) findViewById(R.id.imgStatus);
        viewColor = (ColorView) findViewById(R.id.viewColor);
        imageMark = (ImageView) findViewById(R.id.imageMark);
        imageMark.setAlpha(0.0f);

        btnBack = (ImageButton) findViewById(R.id.btnBack);
        btnNext = (ImageButton) findViewById(R.id.btnNext);

        textPrev1 = (TextView) findViewById(R.id.textPrev1);
        textPrev2 = (TextView) findViewById(R.id.textPrev2);
        textNext1 = (TextView) findViewById(R.id.textNext1);
        textNext2 = (TextView) findViewById(R.id.textNext2);
        textEnglish= (TextView) findViewById(R.id.textEnglish);
        textKorean = (TextView) findViewById(R.id.textKorean);
        textCurNum = (TextView) findViewById(R.id.textCurWordNum);
        textAllNum = (TextView) findViewById(R.id.textAllWordNum);

        ESUtils.setTextViewTypeFaceByRes(this, textPrev1, Define.getMainFont());
        ESUtils.setTextViewTypeFaceByRes(this, textPrev2, Define.getMainFont());
        ESUtils.setTextViewTypeFaceByRes(this, textNext1, Define.getMainFont());
        ESUtils.setTextViewTypeFaceByRes(this, textNext2, Define.getMainFont());
        ESUtils.setTextViewTypeFaceByRes(this, textEnglish, Define.getEnglishFont());
        ESUtils.setTextViewTypeFaceByRes(this, textKorean, Define.getMainFont());
        ESUtils.setTextViewTypeFaceByRes(this, textCurNum, Define.getMainFont());
        ESUtils.setTextViewTypeFaceByRes(this, textAllNum, Define.getMainFont());
        ESUtils.setTextViewTypeFaceByRes(this, (TextView) findViewById(R.id.textSymbol), Define.getMainFont());

        textPrev1.setText("");
        textPrev2.setText("");
        textNext1.setText("");
        textNext2.setText("");
        textKorean.setText("");
        textEnglish.setText("");

        btnBack.setOnClickListener(this);
        btnNext.setOnClickListener(this);


        mRealtimeWaveformView = (RecordWaveformView) findViewById(R.id.viewRecordForm);
        mRealtimeWaveformView.setColor(0);


    }

    private void initData(){

        m_PracticeData = EsRegistryActivity.mPlayDataList;

        ArrayList<String> stringData = EsStuddingUtils.getWordList( m_PracticeData);
        String jsgfString = EsStuddingUtils.makeJSGFString(stringData);
        mRecognizer.setGrammarText(jsgfString);

        String strLen = String.format("%d", m_PracticeData.size());
        textAllNum.setText(strLen);

    }

    public void setData(ArrayList<WordData> arrWords){

    }

    @Override
    public void onClick(View v) {
        ESUtils.playBtnSound(this);
        switch (v.getId()){
            case R.id.btnBack:
                finish();
                break;
            case R.id.btnNext:
                doNext();
                break;
        }
    }

    private void finishStudy(){

        mRecognizer.stopSearch();

        viewColor.finishView();
        m_bStudyStop = true;
        m_bArrowBtnPressed = true;

        if(m_threadStudy != null && m_threadStudy.isAlive()) {
            try{
                m_threadStudy.join();
            }
            catch(Exception e) {}
        }
    }

    private void doNext(){
        m_bArrowBtnPressed = true;
        viewColor.setDraw(false);
        m_nCurrentWordCount ++;

    }

    private void pauseThread() {
        while(m_bStudyPause) {
            try{
                Thread.sleep(20);
            }
            catch(Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void sleepThread(int nTime) {
        int nCount = 0;
        while(!m_bStudyStop) {
            pauseThread();

            if(m_bArrowBtnPressed)
                break;

            if(nCount * 20 > nTime)
                break;

            nCount ++;

            try{
                Thread.sleep(20);
            }
            catch(Exception e) {
                e.printStackTrace();
            }
        }
    }

    private Handler m_handlerStudy = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0:
                    setStudyData();
                    break;
                case 1:
                    startListening();
                    startFirstDraw();

                    break;
                case 2:
                    startSpeaking();
                    startSecondDraw();
                    break;
                case 3:
                    clearDraw();
                    break;
                case 4:
                    m_nCurrentWordCount ++;
                    break;
                default:
                    break;
            }
        }
    };

    private void startStudy(){

        m_threadStudy = new Thread() {
            public void run() {
                while(!m_bStudyStop) {
                    if(m_nCurrentWordCount > m_PracticeData.size() - 1) {
                        finish();
                    }

                    sleepThread(0);
                    if(m_bArrowBtnPressed) {
                        m_bArrowBtnPressed = false;
                        continue;
                    }

                    m_handlerStudy.sendEmptyMessage(0);

                    sleepThread(3000);
                    if(m_bArrowBtnPressed) {
                        m_bArrowBtnPressed = false;
                        continue;
                    }
                    m_handlerStudy.sendEmptyMessage(1);

                    sleepThread(5000);
                    if(m_bArrowBtnPressed) {
                        m_bArrowBtnPressed = false;
                        continue;
                    }
                    m_handlerStudy.sendEmptyMessage(2);

                    sleepThread(5000);
                    if(m_bArrowBtnPressed) {
                        m_bArrowBtnPressed = false;
                        continue;
                    }

                    m_handlerStudy.sendEmptyMessage(3);

                    sleepThread(1500);
                    if(m_bArrowBtnPressed) {
                        m_bArrowBtnPressed = false;
                        continue;
                    }
                    m_handlerStudy.sendEmptyMessage(4);

                    try{
                        Thread.sleep(100);
                    }
                    catch(Exception e) {}

                }
            }
        };
        m_threadStudy.setDaemon(true);
        m_threadStudy.setName("StudyWordThread");
        m_threadStudy.start();
    }

    private void clearDraw(){
        viewColor.setDraw(false);
        mRealtimeWaveformView.setVisibility(View.INVISIBLE);

        try {
            if (mRecordRawFile != null){
                mRecordRawFile.close();
                mRecordRawFile = null;
            }

            mRecognizer.stopSearch();
            String strResult = mRecognizer.getSearchResult();

            File mRawFile = new File(Define.RECORD_PATH_RAW);
            File mWavFile = new File(Define.RECORD_PATH);

            EsStuddingUtils.rawToWave(mRawFile, mWavFile, 16000);

        } catch (IOException e) {
            e.printStackTrace();
        }

        mRealtimeWaveformView.setSamples(new short[20]);

    }

    private void startFirstDraw(){
        viewColor.setColor(true);
        viewColor.setDraw(true);
        viewColor.startDraw();
    }

    private void startSecondDraw(){
        viewColor.setColor(false);
        viewColor.setDraw(true);
        viewColor.startDraw();
    }
    private void setStudyData(){

        if(m_nCurrentWordCount >= 0 && m_nCurrentWordCount < m_PracticeData.size()){
            WordData wData = m_PracticeData.get(m_nCurrentWordCount );
            //String strEnglish = wData.getMeanEn();
            String strKorean = wData.getMeanKr();
            String strEnglish = wData.getMeanEn();

            textKorean.setText(strKorean);
            textEnglish.setText(strEnglish);
            textEnglish.setVisibility(View.INVISIBLE);

            String strLen = String.format("%d", m_nCurrentWordCount + 1);
            textCurNum.setText(strLen);

            imageStatus.setImageResource(R.drawable.st_practice_icon_thinking);
        }

        if(m_nCurrentWordCount > 1){
            WordData wData = m_PracticeData.get(m_nCurrentWordCount - 2);
            //String strEnglish = wData.getMeanEn();
            String strKorean = wData.getMeanKr();

            textPrev1.setText(strKorean);
        }
        if(m_nCurrentWordCount > 0){
            WordData wData = m_PracticeData.get(m_nCurrentWordCount - 1);
            //String strEnglish = wData.getMeanEn();
            String strKorean = wData.getMeanKr();
            textPrev2.setText(strKorean);
        }

        if(m_PracticeData.size() > m_nCurrentWordCount + 1){
            WordData wData = m_PracticeData.get(m_nCurrentWordCount + 1);
            String strKorean = wData.getMeanKr();
            textNext1.setText(strKorean);
        }else {
            textNext1.setText("");
        }

        if(m_PracticeData.size() > m_nCurrentWordCount + 2){
            WordData wData = m_PracticeData.get(m_nCurrentWordCount + 2);
            String strKorean = wData.getMeanKr();
            textNext2.setText(strKorean);
        }else {
            textNext2.setText("");
        }

        mRealtimeWaveformView.setVisibility(View.INVISIBLE);
        imageMark.setAlpha(0.0f);

    }

    private void startListening(){
        imageStatus.setImageResource(R.drawable.st_practice_icon_listening);
        textEnglish.setVisibility(View.VISIBLE);

        playSound();
    }

    private void startSpeaking(){
        imageStatus.setImageResource(R.drawable.st_practice_icon_speaking);
        mRealtimeWaveformView.setVisibility(View.VISIBLE);

        returnBuffer= new short[20];

        try {

            File fdelete = new File(Define.RECORD_PATH_RAW);
            if (fdelete.exists()) {
                if (fdelete.delete()) {

                } else {

                }
            }

            if (mRecordRawFile != null){
                mRecordRawFile.close();
                mRecordRawFile = null;
            }

            mRecordRawFile = new RandomAccessFile(Define.RECORD_PATH_RAW, "rw");

        } catch (IOException e) {
            e.printStackTrace();
        }


        mRecognizer.startSearch();
    }


    private void playSound(){
        PAKDBManager pManager = new PAKDBManager(this);

        strAudioPath = pManager.getFileName( Integer.parseInt(m_PracticeData.get(m_nCurrentWordCount).getSound()), 1);
        //strAudioPath = Define.SOUND_PATH + String.format("%s.mp3", m_PracticeData.get(m_nCurrentWordCount).getSound() );
        new Handler().postDelayed(new Runnable() {
            public void run() {
                try {
                    if(mp != null){
                        mp.stop();
                        mp = null;
                    }
                    mp = new MediaPlayer();
                    mp.setDataSource(StudyWordpadPlayActivity.this, Uri.parse(strAudioPath));
                    mp.prepare();
                    mp.start();

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }, 300);

    }


    short[] returnBuffer;
    private RandomAccessFile mRecordRawFile = null;

    float rms = 0f;
    float rms1 = 0f;
    float rms2 = 0f;
    float rms3 = 0f;
    float rms4 = 0f;

    float peak = 0f;

    private void processBuffer(short[] buffer){

        rms = 0f;
        rms1 = 0f;
        rms2 = 0f;
        rms3 = 0f;
        rms4 = 0f;

        peak = 0f;

        /*for(float sample : buffer) {

            float abs = Math.abs(sample);
            if(abs > peak) {
                peak = abs;
            }

            rms += sample * sample;
        }

        rms = (float)Math.sqrt(rms / buffer.length);

        */
        for(int i = 0; i < buffer.length / 4 ; i++){
            float sample = buffer[i];
            float abs = Math.abs(sample);
            if(abs > peak) {
                peak = abs;
            }

            rms1 += sample * sample;
        }

        for(int i = buffer.length / 4; i < buffer.length / 2; i++){
            float sample = buffer[i];
            float abs = Math.abs(sample);
            if(abs > peak) {
                peak = abs;
            }

            rms2 += sample * sample;
        }

        for(int i = buffer.length / 2; i < buffer.length / 4 * 3; i++){
            float sample = buffer[i];
            float abs = Math.abs(sample);
            if(abs > peak) {
                peak = abs;
            }

            rms3 += sample * sample;
        }

        for(int i = buffer.length / 4 * 3; i < buffer.length ; i++){
            float sample = buffer[i];
            float abs = Math.abs(sample);
            if(abs > peak) {
                peak = abs;
            }

            rms4 += sample * sample;
        }


        rms1 = (float)Math.sqrt(rms1 * 4 / buffer.length);
        rms2 = (float)Math.sqrt(rms2 * 4 / buffer.length);
        rms3 = (float)Math.sqrt(rms3 * 4 / buffer.length);
        rms4 = (float)Math.sqrt(rms4 * 4 / buffer.length);

        for(int i =0;i<19;i++){
            returnBuffer[i] = returnBuffer[i+1];
        }
        returnBuffer[19] = (short)rms1;

        mRealtimeWaveformView.setSamples(returnBuffer);


        new Handler().postDelayed(new Runnable() {
            public void run() {
                for(int i =0;i<19;i++){
                    returnBuffer[i] = returnBuffer[i+1];
                }
                returnBuffer[19] = (short)rms2;

                mRealtimeWaveformView.setSamples(returnBuffer);
            }
        }, 100);

        new Handler().postDelayed(new Runnable() {
            public void run() {
                for(int i =0;i<19;i++){
                    returnBuffer[i] = returnBuffer[i+1];
                }
                returnBuffer[19] = (short)rms3;

                mRealtimeWaveformView.setSamples(returnBuffer);
            }
        }, 200);

        new Handler().postDelayed(new Runnable() {
            public void run() {
                for(int i =0;i<19;i++){
                    returnBuffer[i] = returnBuffer[i+1];
                }
                returnBuffer[19] = (short)rms4;

                mRealtimeWaveformView.setSamples(returnBuffer);
            }
        }, 300);



        if(mRecordRawFile != null){
            try {
                mRecordRawFile.write(EsStuddingUtils.shortToBytes(buffer));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private int controlPercent(int nPercent){

        int nResult = 0;
        if(nPercent >= 30){
            nResult = 80 + 20  * (nPercent - 70) / 70;
        }else if(nPercent >= 10){
            nResult = 50 + 30 * (nPercent - 10)/ 20;
        }else if(nPercent >=0){
            nResult = 0 + 50 * nPercent / 10;
        }

        return nResult;
    }

    private void processResult(String strResult){
        WordData data = m_PracticeData.get(m_nCurrentWordCount);
        String strEnglish = data.getMeanEn();

        int nPercent = (int) ( EsStuddingUtils.similarity(strEnglish.toLowerCase(), strResult.toLowerCase()) * 100);

        int nResultPercent = controlPercent(nPercent);

        if(nResultPercent >= 76) {
            imageMark.setImageResource(R.drawable.st_study_excellent);
        }
        else if(nResultPercent >= 70) {
            imageMark.setImageResource(R.drawable.st_study_good);
        }

        int nPass = 0;
        if(nResultPercent >= 70){
            nPass = 1;
            imageMark.setAlpha(1.0f);
            imageMark.setScaleX(0.7f);
            imageMark.setScaleY(0.7f);

            ObjectAnimator fadeAnim = ObjectAnimator.ofFloat(imageMark, "scaleX", 1.0f).setDuration(1000);
            AnimatorSet set = new AnimatorSet();
            set.playTogether(
                    ObjectAnimator.ofFloat(imageMark, "scaleX", 1.0f).setDuration(1000),
                    ObjectAnimator.ofFloat(imageMark, "scaleY", 1.0f).setDuration(1000)
            );
            set.setInterpolator(new BounceInterpolator());
            set.start();

            ESUtils.playSuccessSound(this);
        } else {
            ESUtils.playFailSound(this);
        }

        //Toast.makeText(this, String.format("%d", nResultPercent), Toast.LENGTH_LONG).show();

    }

    public String getImageFileName(int nWordID){
        return m_pakManager.getFileName(nWordID, 0);
    }

    public String getAudioFileName(int nWordID){
        return m_pakManager.getFileName(nWordID, 1);
    }

    public String getVideoFileName(int nWordID){
        return m_pakManager.getFileName(nWordID, 2);
    }
}
