package com.yj.sentence.ui.studding.test;


import com.yj.sentence.R;
import com.yj.sentence.common.Define;
import com.yj.sentence.database.UserDBManager;
import com.yj.sentence.structs.UserData;
import com.yj.sentence.ui.studding.ScoreStar;
import com.yj.sentence.utils.ESUtils;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class TestResultMedalLayout extends TestLayout {

	private Context m_context = null;
	
	private TextView m_tvStudyWordCount = null;
	private TextView m_tvScore = null;
	private TextView m_tvFalseWords = null;
	private TextView m_tvResult = null;
	
	private ImageView m_ivMedal = null;
	private ScoreStar m_scoreStar = null;
	
	private int m_nStudyWordCount = 24;
	
	private UserDBManager m_userDbMana = null;
	
	private OnTestResultMedalFinishListener m_listener;
	
	public TestResultMedalLayout(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		m_context = context;
		
		initLayout();
	}

	private void initLayout() {
		View v = LayoutInflater.from(m_context).inflate(R.layout.testresult_medal_layout, null);
		this.addView(v);
		
		m_tvStudyWordCount = (TextView)findViewById(R.id.tvStudyWordCount);
		m_tvScore = (TextView)findViewById(R.id.tvScore);
		m_tvFalseWords = (TextView)findViewById(R.id.tvFalseWordCount);
		m_tvResult = (TextView)findViewById(R.id.tvTestResult);

		ESUtils.setTextViewTypeFaceByRes(m_context, m_tvStudyWordCount, Define.getMainFont());
		ESUtils.setTextViewTypeFaceByRes(m_context, m_tvScore, Define.getMainFont());
		ESUtils.setTextViewTypeFaceByRes(m_context, m_tvFalseWords, Define.getMainFont());
		ESUtils.setTextViewTypeFaceByRes(m_context, m_tvResult, Define.getMainFont());

		m_ivMedal = (ImageView)findViewById(R.id.ivResultMedal);
		m_scoreStar = (ScoreStar)findViewById(R.id.scorestar);
		
		/*Button btnOk = (Button)findViewById(R.id.btnResultOk);
		btnOk.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// 일정보기로 넘어간다.
				m_listener.onAfter(m_nOrder);
			}
		});*/
	}
	
	//급수에 해당한 모든 일정을 완료했는가 검사 즉 급수에 해당한 모든 단어를 다 학습하였는가 검사
	private boolean isStudyScheduleComplete() {
		/*boolean bComplete = false;
		
		ScheduleData scheduleData = m_progressDbMana.getScheduleData(m_nOrder);
		
		int nTotalTargetWordCount = m_studyDbMana.getWordCount(m_nOrder);
		int nTotalStudyCount = m_progressDbMana.getWordCount(scheduleData);
		
		if(ESUtils.isSameDate(ESUtils.getNowDateMilliseconds(), m_progressDbMana.getLastDate(scheduleData)) &&
				(nTotalTargetWordCount == nTotalStudyCount))
			bComplete = true;
		
		return bComplete;*/
		return true;

	}
	
	public void setTestResultInfo() {
		
		int nScore = (int)((float)m_nTrueResCount * (100.0f / m_nTotalTestProblemCount));	//맞힌 문제당 2.5점씩 할당
		//nScore = 85;

		int	nMedal = ESUtils.getMedalType(nScore);
		int	idImg = R.drawable.medal2;
		int	idTxt = R.string.result_msg2;
		switch(nMedal)
		{
		case Define.MEDAL_BAD:
			idImg = R.drawable.medal2;
			idTxt = R.string.result_msg2;
			break;
		case Define.MEDAL_NORMAL:
			idImg = R.drawable.medal3;
			idTxt = R.string.result_msg3;
			break;
		case Define.MEDAL_GOOD:
			idImg = R.drawable.medal4;
			idTxt = R.string.result_msg4;
			break;
		case Define.MEDAL_EXCELLENT:
			idImg = R.drawable.medal5;
			idTxt = R.string.result_msg5;
			break;
		}
		m_ivMedal.setImageResource(idImg);
		m_tvResult.setText(idTxt);
		/*if(nScore >= 70) {
			//80점이상인 경우에 메달 설정
			m_studyDbMana.setWordpadComplete();
			
			if(isStudyScheduleComplete()) {
				saveUserOrder();
				sendStudyComplete();
			}
		}*/
		
		//m_scheduleData.setMarks(nScore);
		//m_studyDbMana.setMarks(m_scheduleData);
		
		//m_scheduleData.setDate(ESUtils.getNowDateMilliseconds());
		//m_studyDbMana.setDate(m_scheduleData);
		
		//m_nStudyWordCount = m_progressDbMana.getWordCountOfDay(m_scheduleData);
		
		m_tvStudyWordCount.setText(Integer.toString(m_nTotalTestProblemCount));
		m_tvScore.setText(Integer.toString(nScore));
		m_tvFalseWords.setText(Integer.toString(m_nFalseResCount));

		m_scoreStar.setScore(nScore);
	}
	
	private void saveUserOrder() {
		UserData data = m_userDbMana.getActiveUser();
		data.setLevel(m_nOrder);
		m_userDbMana.setLevel(data);
	}
	
	private void sendStudyComplete() {
		Intent intent = new Intent(Define.USER_ORDER_ACTION);
		m_context.sendBroadcast(intent);
	}
	
	public void setUserDbMana(UserDBManager dbMana) {
		m_userDbMana = dbMana;
	}
	
	public void setListener(OnTestResultMedalFinishListener listener) {
		m_listener = listener;
	}
	
	public abstract interface OnTestResultMedalFinishListener {
		public abstract void onAfter(int nOrder);
	}
}
