package com.yj.sentence.ui.studding.util;

import com.yj.sentence.structs.WordData;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Array;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;

/**
 * Created by achilles on 3/15/17.
 */

public class EsStuddingUtils {
    public static ArrayList<String> getWordList(ArrayList<WordData> wordDataList){
        ArrayList<String> result = new ArrayList<String>();
        EsStuddingRecognizer mRecognizer = EsStuddingRecognizer.getInstance();

        for ( int i = 0; i< wordDataList.size(); i++){
            WordData wData = wordDataList.get(i);
            String strWord = wData.getMeanEn();

            strWord = strWord.toLowerCase();
            strWord = strWord.replace(".", "");
            strWord = strWord.replace("?", "");
            strWord = strWord.replace(",", "");
            strWord = strWord.replace("!", "");
            strWord = strWord.replace("-", " ");
            strWord = strWord.replace("’", "'");
            strWord = strWord.replace("1", " one ");
            strWord = strWord.replace("2", " two ");
            strWord = strWord.replace("3", " three ");
            strWord = strWord.replace("4", " four ");
            strWord = strWord.replace("5", " five ");
            strWord = strWord.replace("6", " six ");
            strWord = strWord.replace("7", " seven ");
            strWord = strWord.replace("8", " eight ");
            strWord = strWord.replace("9", " nine ");
            strWord = strWord.replace("0", " oh ");
            strWord = strWord.replace("recook", "re cook");
            strWord = strWord.replace("washrooms", "wash rooms");
            strWord = strWord.replace("polishes", "polish");
            strWord = strWord.replace("smartphone", "smart phone");
            strWord = strWord.replace("eyeliners", "eye liners");

            strWord = strWord.replace(":", " ");

            String[] strArrays = strWord.split(" ");
            for(int j = 0; j < strArrays.length; j++){
                strArrays[j].replace("   ", "");
                strArrays[j].replace("  ", "");
                strArrays[j].replace(" ", "");
                if(!isContainWord(result, strArrays[j]) && strArrays[j].length() > 0){

                    if(mRecognizer.getSpeechRecognizer() != null && mRecognizer.getSpeechRecognizer().getDecoder().lookupWord(strArrays[j]) == null)
                        continue;
                    result.add(strArrays[j]);
                }
            }
        }
        return result;
    };

    public static boolean isContainWord(ArrayList<String> arrayString, String strWord){
        for(int i = 0; i < arrayString.size(); i++){
            String strIndex = arrayString.get(i);
            if(strIndex.compareToIgnoreCase(strWord) == 0){
                return true;
            }
        }
        return false;
    }

    public static String makeJSGFString(ArrayList<String> wordList){
        String str = null;

        //header
        str = "#JSGF V1.0;";
        str += "\n";
        str += "\n";
        str += "grammar digits;";
        str += "\n";
        str += "\n";
        str += "<digit> = ";

        //body
        for(int i = 0; i < wordList.size(); i++){
            String strWord = wordList.get(i);
            str += strWord;

            if(i == wordList.size() - 1){
                str += " ;";
                str += "\n";
                str += "\n";
            }else{
                str += " | ";
            }
        }

        //footer

        str += "public <digits> = <digit>+;";

        return str;
    }

    public static double similarity(String s1, String s2) {
        String longer = s1, shorter = s2;
        if (s1.length() < s2.length()) { // longer should always have greater length
            longer = s2; shorter = s1;
        }
        int longerLength = longer.length();
        if (longerLength == 0) { return 1.0; /* both strings are zero length */ }
    /* // If you have StringUtils, you can use it to calculate the edit distance:
    return (longerLength - StringUtils.getLevenshteinDistance(longer, shorter)) /
                               (double) longerLength; */
        return (longerLength - editDistance(longer, shorter)) / (double) longerLength;

    }

    // Example implementation of the Levenshtein Edit Distance
    // See http://rosettacode.org/wiki/Levenshtein_distance#Java
    public static int editDistance(String s1, String s2) {
        s1 = s1.toLowerCase();
        s2 = s2.toLowerCase();

        int[] costs = new int[s2.length() + 1];
        for (int i = 0; i <= s1.length(); i++) {
            int lastValue = i;
            for (int j = 0; j <= s2.length(); j++) {
                if (i == 0)
                    costs[j] = j;
                else {
                    if (j > 0) {
                        int newValue = costs[j - 1];
                        if (s1.charAt(i - 1) != s2.charAt(j - 1))
                            newValue = Math.min(Math.min(newValue, lastValue),
                                    costs[j]) + 1;
                        costs[j - 1] = lastValue;
                        lastValue = newValue;
                    }
                }
            }
            if (i > 0)
                costs[s2.length()] = lastValue;
        }
        return costs[s2.length()];
    }

    public static byte[] shortToBytes(short[] buffer) {
        /*byte[] bytes2 = new byte[buffer.length * 2];
        ByteBuffer.wrap(bytes2).order(ByteOrder.LITTLE_ENDIAN).asShortBuffer().put(buffer);
        return bytes2;*/

        ByteBuffer bytes = ByteBuffer.allocate(buffer.length * 2);
        for (short s : buffer) {
            bytes.putShort(s);
        }

        return bytes.array();
    }

    public static void rawToWave(final File rawFile, final File waveFile, int SAMPLE_RATE) throws IOException {

        byte[] rawData = new byte[(int) rawFile.length()];
        DataInputStream input = null;
        try {
            input = new DataInputStream(new FileInputStream(rawFile));
            input.read(rawData);
        } finally {
            if (input != null) {
                input.close();
            }
        }

        DataOutputStream output = null;
        try {
            output = new DataOutputStream(new FileOutputStream(waveFile));
            // WAVE header
            // see http://ccrma.stanford.edu/courses/422/projects/WaveFormat/
            writeString(output, "RIFF"); // chunk id
            writeInt(output, 36 + rawData.length); // chunk size
            writeString(output, "WAVE"); // format
            writeString(output, "fmt "); // subchunk 1 id
            writeInt(output, 16); // subchunk 1 size
            writeShort(output, (short) 1); // audio format (1 = PCM)
            writeShort(output, (short) 1); // number of channels
            writeInt(output, SAMPLE_RATE); // sample rate
            writeInt(output, SAMPLE_RATE * 2); // byte rate
            writeShort(output, (short) 2); // block align
            writeShort(output, (short) 16); // bits per sample
            writeString(output, "data"); // subchunk 2 id
            writeInt(output, rawData.length); // subchunk 2 size
            // Audio data (conversion big endian -> little endian)
            short[] shorts = new short[rawData.length / 2];
            ByteBuffer.wrap(rawData).order(ByteOrder.LITTLE_ENDIAN).asShortBuffer().get(shorts);
            ByteBuffer bytes = ByteBuffer.allocate(shorts.length * 2);
            for (short s : shorts) {
                bytes.putShort(s);
            }
            output.write(bytes.array());
            //output.write(rawData);
        } finally {
            if (output != null) {
                output.close();
            }
        }
    }

    private static void writeInt(final DataOutputStream output, final int value) throws IOException {
        output.write(value >> 0);
        output.write(value >> 8);
        output.write(value >> 16);
        output.write(value >> 24);
    }

    private static void writeShort(final DataOutputStream output, final short value) throws IOException {
        output.write(value >> 0);
        output.write(value >> 8);
    }

    private static void writeString(final DataOutputStream output, final String value) throws IOException {
        for (int i = 0; i < value.length(); i++) {
            output.write(value.charAt(i));
        }
    }

}
