package com.yj.sentence.ui.studding.study;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by achilles on 2/14/17.
 */

public class ColorView extends View {

    private Thread m_threadTextBg;
    private boolean bDrawColor = false;
    private int m_nTextBgRightEn = 0;
    private int m_nBgWidthCount = 15;
    private int m_nBgTimeSleep = 40;
    private Paint m_paintWord;

    private int colorBg1 = 0xa4d467;
    private int colorBg2 = 0xffce81;
    private int colorMain;

    private int m_nTime = 5000;
    public ColorView(Context context, AttributeSet attrs) {
        super(context, attrs);

        m_paintWord = new Paint();
        m_paintWord.setAntiAlias(true);
        m_paintWord.setAlpha(255);
    }

    public void setColor(boolean bListening){
        if(bListening){
            colorMain = colorBg1;
        }else {
            colorMain = colorBg2;
        }
    }

    public void setDraw(boolean bDraw){
        bDrawColor = bDraw;
    }

    public void setTime(int nTime){
        m_nTime = nTime;
    }
    public void startDraw(){
        final int measuredWidth = getMeasuredWidth();
        int measuredHeight = getMeasuredHeight();

        m_nTextBgRightEn = 0;
        m_nBgWidthCount = measuredWidth / (m_nTime / m_nBgTimeSleep) + 1;

        m_threadTextBg = new Thread() {
            private Handler m_handler = new Handler() {
                public void handleMessage(Message msg) {
                    ColorView.this.invalidate();
                }
            };

            public void run() {
                while(bDrawColor) {
                    m_nTextBgRightEn += m_nBgWidthCount;
                    if(m_nTextBgRightEn >= measuredWidth ) {
                        //m_nTextBgRightEn = measuredWidth;
                        m_handler.sendEmptyMessage(0);
                        //bDrawColor = false;
                        break;
                    }
                    m_handler.sendEmptyMessage(0);

                    try{
                        Thread.sleep(m_nBgTimeSleep);
                    } catch(Exception e) {

                    }
                }
            }
        };

        m_threadTextBg.setName("TextBgThread");
        m_threadTextBg.start();
    }

    public void finishView(){
        bDrawColor = false;
        if(m_threadTextBg != null && m_threadTextBg.isAlive()) {
            try{
                m_threadTextBg.join();
            }
            catch(Exception e) {}
        }
    }
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        drawBack(canvas);
    }

    private void drawBack(Canvas canvas){
        final int measuredWidth = getMeasuredWidth();
        int measuredHeight = getMeasuredHeight();

        if(bDrawColor){
            m_paintWord.setColor(colorMain);
            m_paintWord.setAlpha(255);
            Rect rc = new Rect(0, 0, (m_nTextBgRightEn), measuredHeight);
            canvas.drawRect(rc, m_paintWord);
        }
    }

}
