package com.yj.sentence.ui.studding.test;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.yj.sentence.R;
import com.yj.sentence.common.Define;
import com.yj.sentence.ui.studding.SekConvStudyPopup;
import com.yj.sentence.ui.studding.SekWordPopup;
import com.yj.sentence.ui.studding.SekWordPopupOneButton;
import com.yj.sentence.ui.studding.study.StudyBasicActivity;
import com.yj.sentence.utils.ESUtils;

public class TestSelectActivity extends Activity implements View.OnClickListener {

    FrameLayout fLayouts[];
    ImageView imgView[];

    private int m_nUserId = -1;
    private boolean m_bOrdered = false;
    private boolean m_bExercise = false;
    private int m_nScheduleId = -1;

    private ImageButton btnStart;
    private ImageButton btnBack;

    private ImageView imgMask;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_select);

        /*intent.putExtra(Define.USER_ID, m_nUserId);
        intent.putExtra(Define.IS_ORDERTEST, true);
        intent.putExtra(Define.SCHEDULE_ID, -1);*/

        Intent intent = getIntent();
        m_nUserId = intent.getIntExtra(Define.USER_ID, -1);
        m_bOrdered = intent.getBooleanExtra(Define.IS_ORDERTEST, false);
        m_bExercise = intent.getBooleanExtra(Define.IS_EXERCISE, false);
        m_nScheduleId = intent.getIntExtra(Define.SCHEDULE_ID, -1);

        fLayouts = new FrameLayout[4];
        imgView = new ImageView[4];

        fLayouts[0] = (FrameLayout) findViewById(R.id.frameVTT);
        fLayouts[1] = (FrameLayout) findViewById(R.id.frameTTV);
        fLayouts[2] = (FrameLayout) findViewById(R.id.frameVTS);
        fLayouts[3] = (FrameLayout) findViewById(R.id.frameLTV);

        imgView[0] = (ImageView) findViewById(R.id.imgVTT);
        imgView[1] = (ImageView) findViewById(R.id.imgTTV);
        imgView[2] = (ImageView) findViewById(R.id.imgVTS);
        imgView[3] = (ImageView) findViewById(R.id.imgLTV);

        for(int i = 0; i < 4; i++){
            fLayouts[i].setOnClickListener(this);
            imgView[i].setTag(true);
        }

        btnStart = (ImageButton) findViewById(R.id.btnStart);
        btnBack = (ImageButton) findViewById(R.id.btnBack);
        btnStart.setOnClickListener(m_btnClickListener);
        btnBack.setOnClickListener(m_btnClickListener);

        imgMask = (ImageView) findViewById(R.id.imageMask);
        imgMask.setVisibility(View.VISIBLE);

        if(m_bExercise){
            imgMask.setVisibility(View.INVISIBLE);
        }else {
            SekConvStudyPopup popup;

            popup = new SekConvStudyPopup(this);
            popup.setStep(Define.STUDY_STEP_TEST);
            popup.setListener(new SekConvStudyPopup.OnSEKLaunchListener() {

                @Override
                public void onOK() {
                    imgMask.setVisibility(View.INVISIBLE);
                }
            });

            popup.show();
        }

        ESUtils.setTextViewTypeFaceByRes(this, (TextView)findViewById(R.id.textTitle1), Define.getMainFont());
        ESUtils.setTextViewTypeFaceByRes(this, (TextView)findViewById(R.id.textTitle2), Define.getMainFont());
        ESUtils.setTextViewTypeFaceByRes(this, (TextView)findViewById(R.id.textTitle3), Define.getMainFont());
        ESUtils.setTextViewTypeFaceByRes(this, (TextView)findViewById(R.id.textItem1), Define.getMainFont());
        ESUtils.setTextViewTypeFaceByRes(this, (TextView)findViewById(R.id.textItem2), Define.getMainFont());
        ESUtils.setTextViewTypeFaceByRes(this, (TextView)findViewById(R.id.textItem3), Define.getMainFont());
        ESUtils.setTextViewTypeFaceByRes(this, (TextView)findViewById(R.id.textItem4), Define.getMainFont());

    }

    private View.OnClickListener m_btnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            ESUtils.playBtnSound(TestSelectActivity.this);

            switch (v.getId()){
                case R.id.btnStart:
                    doTest();
                    break;
                case R.id.btnBack:
                    doBack();
                    break;
            }
        }
    };

    private void showBackDlg(){

        SekWordPopup popup;

        popup = new SekWordPopup(TestSelectActivity.this);
        popup.setMessage(this.getString(R.string.study_back));
        popup.setListener(new SekWordPopup.OnSEKLaunchListener() {

            @Override
            public void onOK() {
                TestSelectActivity.this.finish();
            }

            @Override
            public void onCancel() {
                // TODO Auto-generated method stub
            }
        });

        popup.show();
    }


    private void doBack(){
        //finish();
        showBackDlg();

    }


    @Override
    public void onBackPressed() {
        // your code.
        doBack();

    }

    private void doTest(){

        int nTestCount =0;
        for(int i=0; i< 4; i++){
            if((boolean)(imgView[i].getTag()) == true){
                nTestCount ++;
            }
        }

        if(nTestCount < 2){
            SekWordPopupOneButton popup;

            popup = new SekWordPopupOneButton(TestSelectActivity.this);
            popup.setMessage(this.getString(R.string.app_test_count));
            //popup.setMessageSize(15);
            popup.setListener(new SekWordPopupOneButton.OnSEKLaunchListener() {

                @Override
                public void onOK() {

                }
            });
            popup.show();

        }else {
            Intent intent = new Intent(TestSelectActivity.this, StudyTestActivity.class);
            intent.putExtra(Define.USER_ID, m_nUserId);
            intent.putExtra(Define.IS_ORDERTEST, m_bOrdered);
            intent.putExtra(Define.SCHEDULE_ID, m_nScheduleId);
            intent.putExtra(Define.IS_VTT, (boolean) imgView[0].getTag());
            intent.putExtra(Define.IS_TTV, (boolean) imgView[1].getTag());
            intent.putExtra(Define.IS_VTS, (boolean) imgView[2].getTag());
            intent.putExtra(Define.IS_LTV, (boolean) imgView[3].getTag());
            intent.putExtra(Define.IS_EXERCISE, m_bExercise);
            startActivity(intent);
            finish();
        }

    }

    @Override
    public void onClick(View v) {
        ESUtils.playBtnSound(this);

        int nSelectedIndex = -1;

        switch(v.getId()){
            case R.id.frameVTT:
                nSelectedIndex = 0;
                break;
            case R.id.frameTTV:
                nSelectedIndex = 1;
                break;
            case R.id.frameVTS:
                nSelectedIndex = 2;
                break;
            case R.id.frameLTV:
                nSelectedIndex = 3;
                break;
        }

        if(nSelectedIndex != -1){
            itemClicked(nSelectedIndex);
        }
    }

    private void itemClicked(int nIndex){
        if((boolean)imgView[nIndex].getTag() == true){
            imgView[nIndex].setImageResource(R.drawable.st_test_method_unchecked);
            imgView[nIndex].setTag(false);
        }else {
            imgView[nIndex].setImageResource(R.drawable.st_test_method_checked);
            imgView[nIndex].setTag(true);
        }
    }



}
