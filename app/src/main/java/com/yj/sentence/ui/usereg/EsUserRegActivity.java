package com.yj.sentence.ui.usereg;

import java.io.File;

import com.yj.sentence.R;
import com.yj.sentence.database.UserDBManager;
import com.yj.sentence.structs.UserData;
import com.yj.sentence.utils.ESUtils;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

/**
 * 
 * @author RWI
 * 2015.6.28
 * 사용자등록화면
 */

@SuppressLint({ "ShowToast", "NewApi" })
public class EsUserRegActivity extends PhotoActivity implements View.OnClickListener
{
	
	private final static int TAKE_PHOTO = 1;
	
	private Button btnPhoto;
	private Button btnOk;
	private Button btnCancel;
	private EditText txtName;
	private EditText txtJob;
	private ImageView m_ivPhoto;
	private ImageView m_ivPhotoMask;
	
	private UserDBManager mDbMana = null;
	UserData mData = null;

	private String m_oldPhotoPath = "";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_user_reg);
		
		openDB();
		
		Intent intent = getIntent();
		if (intent.getStringExtra("uName") != null) {
			mData = new UserData(intent.getIntExtra("uId", -1), 
								 intent.getStringExtra("uName"), 
								 intent.getStringExtra("uJob"), 
								 intent.getStringExtra("uPhoto"), 
								 intent.getIntExtra("uLevel", 0), 
								 intent.getIntExtra("uActive", 0));
			
			m_oldPhotoPath = mData.getPhoto();
		}
		
		initButtons();
		initEditTexts();
	}
	
	@Override
	protected void onDestroy() {
		closeDB();
		super.onDestroy();
	}

	@Override
	public void onClick(View arg0) {
		ESUtils.playBtnSound(this);
		switch(arg0.getId()) {
			case R.id.btnUserPhoto:
				getPhoto();
				break;
			case R.id.btnUserRegOk:
				onOK();
				break;
			case R.id.btnUserRegCancel:
				onCancel();
				break;
		}
	} 

	private void initButtons() {
		btnPhoto  = (Button)findViewById(R.id.btnUserPhoto);
		btnOk     = (Button)findViewById(R.id.btnUserRegOk);
		btnCancel = (Button)findViewById(R.id.btnUserRegCancel);
		
		btnPhoto.setOnClickListener(this);
		btnOk.setOnClickListener(this);
		btnCancel.setOnClickListener(this);
	}
	
	private void initEditTexts() {
		txtName = (EditText)findViewById(R.id.txtUserName);
		txtJob = (EditText)findViewById(R.id.txtUserJob);
		m_ivPhoto = (ImageView)findViewById(R.id.ivPhoto);
		m_ivPhotoMask = (ImageView)findViewById(R.id.ivPhotoMask);
		
		if (mData != null) {
			txtName.setText(mData.getName());
			txtJob.setText(mData.getJob());
		}

		if ( !m_oldPhotoPath.isEmpty() && setPhotoImage(m_oldPhotoPath) != null )
		{
			m_strPhotoPath = m_oldPhotoPath;
		}
		else
		{
			m_ivPhoto.setVisibility(View.INVISIBLE);
			m_ivPhotoMask.setVisibility(View.INVISIBLE);
		}
	}
	
	@Override
	protected Bitmap setPhotoImage(String strFile) {
		Bitmap bmpPhoto = super.setPhotoImage(strFile);
		if (bmpPhoto != null)
		{
			m_ivPhoto.setImageBitmap(bmpPhoto);
			
			m_ivPhoto.setVisibility(View.VISIBLE);
			m_ivPhotoMask.setVisibility(View.VISIBLE);
		}
		
		return bmpPhoto;
	}

	private void openDB() {
		if (mDbMana == null) 
			mDbMana = new UserDBManager(this);
		
		if (mDbMana == null)
			return;
	}
	
	private void closeDB() {
		if (mDbMana != null) {
			mDbMana.close();
			mDbMana = null;
		}
	}
	
	private void onOK() {
		if (txtName.getText().toString().isEmpty()) {
			Toast.makeText(this, this.getString(R.string.user_msg1), Toast.LENGTH_SHORT).show();
			return;
		}
		
		if ( !m_oldPhotoPath.equals(m_strPhotoPath) )
		{
			File fileOld = new File(m_oldPhotoPath);
			if (fileOld.exists())
				fileOld.delete();
		}

		String	strName = txtName.getText().toString();
		String	strJob = txtJob.getText().toString();
		String	strPhotoPath = m_strPhotoPath;
		
		if (mData == null) {
			mData = new UserData();
			mData.setName(strName);
			mData.setJob(strJob);
			mData.setPhoto(strPhotoPath);
			mDbMana.Insert(mData);
		} else {
			mData.setName(strName);
			mData.setJob(strJob);
			mData.setPhoto(strPhotoPath);
			mDbMana.Update(mData);
		}
		
		this.finish();
	}
	
	private void onCancel() {
		File fileOld = new File(m_strPhotoPath);
		if (fileOld.exists())
			fileOld.delete();

		this.finish();
	}
}
