package com.yj.sentence.ui.studding;

import com.yj.sentence.R;
import com.yj.sentence.common.Define;
import com.yj.sentence.utils.ESUtils;

import android.app.Dialog;
import android.content.Context;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class SekWordPopupOneButton extends Dialog implements View.OnClickListener {
	
	private Context mContext;
	private Button mBtnOK;
	private TextView textMessage;
	
	private OnSEKLaunchListener mListener;

	public SekWordPopupOneButton(Context context) {
		super(context, R.style.MyDialog);
		// TODO Auto-generated constructor stub
		this.mContext = context;
		this.setCancelable(false);
		initViews();
		ViewPager pager;

	}
	
	private void initViews() {
		View localView = LayoutInflater.from(this.mContext).inflate(R.layout.sekword_popup_onebutton, null);
		this.setContentView(localView);
		
		mBtnOK = (Button)localView.findViewById(R.id.btnOk);
		mBtnOK.setOnClickListener(this);
		
		textMessage = (TextView) localView.findViewById(R.id.textMessage);
		ESUtils.setTextViewTypeFaceByRes(mContext, textMessage, Define.getMainFont());

	}
	
	public void setListener(OnSEKLaunchListener listener) {
		mListener = listener;
	}
	
	public void setMessage(String text){
		textMessage.setText(text);
	}
	
	public void setMessageSize(int size){
		textMessage.setTextSize(size);
	}
	
	@Override
	public void dismiss() {
	    super.dismiss();
	}

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		ESUtils.playBtnSound(mContext);
		switch (arg0.getId()) {
			case R.id.btnOk:
				mListener.onOK();
				this.dismiss();
				break;
				
		}
	}
	
	public abstract interface OnSEKLaunchListener {
		public abstract void onOK();
	}

}
