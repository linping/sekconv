package com.yj.sentence.ui.studding;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.yj.sentence.R;
import com.yj.sentence.common.Define;
import com.yj.sentence.utils.ESUtils;


public class SekConvStudyPopup extends Dialog implements View.OnClickListener {

	private Context mContext;
	private Button mBtnOK;

	private TextView textStep;
	private TextView textDescription;
	private ImageView imgWord;
	private ImageView imgSentence;
	private ImageView imgTest;
	private ImageView imgPractice;

	private OnSEKLaunchListener mListener;

	public SekConvStudyPopup(Context context) {
		super(context, R.style.MyDialog);
		// TODO Auto-generated constructor stub
		this.mContext = context;
		this.setCancelable(false);
		initViews();
	}
	
	private void initViews() {
		View localView = LayoutInflater.from(this.mContext).inflate(R.layout.sekconv_study_popup, null);
		this.setContentView(localView);
		
		mBtnOK = (Button)localView.findViewById(R.id.btnOk);
		mBtnOK.setOnClickListener(this);

		textStep = (TextView) findViewById(R.id.textStep);
		textDescription = (TextView) findViewById(R.id.textDescription);
		imgWord = (ImageView) findViewById(R.id.imgWord);
		imgSentence = (ImageView) findViewById(R.id.imgSentence);
		imgTest = (ImageView) findViewById(R.id.imgTest);
		imgPractice = (ImageView) findViewById(R.id.imgPractice);

		ESUtils.setTextViewTypeFaceByRes(mContext, textStep, Define.getMainFont());
		ESUtils.setTextViewTypeFaceByRes(mContext, textDescription, Define.getMainFont());
		ESUtils.setTextViewTypeFaceByRes(mContext, (TextView) findViewById(R.id.textStepSub), Define.getMainFont());


	}
	
	public void setListener(OnSEKLaunchListener listener) {
		mListener = listener;
	}

	public void setStep(int nStep){
		switch(nStep){
			case Define.STUDY_STEP_WORD:
				textStep.setText(R.string.string_study_popup_step_word);
				textDescription.setText(R.string.string_study_popup_desc_word);
				imgWord.setImageResource(R.drawable.st_study_popup_icon_word_on);
				break;
			case Define.STUDY_STEP_SENTENCE:
				textStep.setText(R.string.string_study_popup_step_sentence);
				textDescription.setText(R.string.string_study_popup_desc_sentence);
				imgSentence.setImageResource(R.drawable.st_study_popup_icon_sentence_on);
				break;
			case Define.STUDY_STEP_TEST:
				textStep.setText(R.string.string_study_popup_step_test);
				textDescription.setText(R.string.string_study_popup_desc_test);
				imgTest.setImageResource(R.drawable.st_study_popup_icon_test_on);
				break;
			case Define.STUDY_STEP_PRACTICE:
				textStep.setText(R.string.string_study_popup_step_practice);
				textDescription.setText(R.string.string_study_popup_desc_practice);
				imgPractice.setImageResource(R.drawable.st_study_popup_icon_practice_on);
				break;
			default:
				break;
		}
	}
	@Override
	public void dismiss() {
	    super.dismiss();
	}


	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		ESUtils.playBtnSound(mContext);
		switch (arg0.getId()) {
			case R.id.btnOk:
				mListener.onOK();
				this.dismiss();
				break;
				
		}
	}
	
	public abstract interface OnSEKLaunchListener {
		public abstract void onOK();
	}

}
