package com.yj.sentence.ui.studding;

import com.yj.sentence.R;
import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

@SuppressLint("NewApi") public class ScoreStar extends LinearLayout {

	private Button[] m_arrStar = new Button[5];
	private int m_nScore = 0;
	
	public ScoreStar(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		init(context);
	}

	public ScoreStar(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		init(context);
	}

	public ScoreStar(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		
		init(context);
	}
	
	private void init(Context context) {
		View v = LayoutInflater.from(context).inflate(R.layout.scorestarbar, null);
		this.addView(v);
		
		m_arrStar[0] = (Button)findViewById(R.id.star1);
		m_arrStar[1] = (Button)findViewById(R.id.star2);
		m_arrStar[2] = (Button)findViewById(R.id.star3);
		m_arrStar[3] = (Button)findViewById(R.id.star4);
		m_arrStar[4] = (Button)findViewById(R.id.star5);
		
		initStar();
	}

	public void initStar() {
		for(int i=0;  i<5; i++) {
			m_arrStar[i].setSelected(false);
		}
	}
	
	public void setScore(int nScore) {
		m_nScore = 1;

		if(nScore >=70 && nScore < 80)
			m_nScore = 3;
		else if(nScore >= 80 && nScore < 90)
			m_nScore = 4;
		else if(nScore >= 90 && nScore <= 100)
			m_nScore = 5;
		
		setScoreStar();
	}
	
	private void setScoreStar() {
		for(int i=0;  i<m_nScore; i++) {
			m_arrStar[i].setSelected(true);
		}
	}
}
