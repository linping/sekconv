package com.yj.sentence.ui.studding;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.yj.sentence.R;
import com.yj.sentence.common.Define;
import com.yj.sentence.database.ProgressDBManager;
import com.yj.sentence.structs.ProgressData;
import com.yj.sentence.utils.ESUtils;


public class SekConvExercisePopup extends Dialog implements View.OnClickListener {

	private Context mContext;
	private Button mBtnCancel;

	private TextView textTitle;
	private TextView textDescription;
	private ImageView imgMedal;

	private Button btnStudy;
	private Button btnTest;
	private Button btnApp;

	private int m_nUserID;
	private int m_nParam;

	private OnSEKLaunchListener mListener;

	public SekConvExercisePopup(Context context) {
		super(context, R.style.MyDialog);
		// TODO Auto-generated constructor stub
		this.mContext = context;
		this.setCancelable(false);
		initViews();
	}
	
	private void initViews() {
		View localView = LayoutInflater.from(this.mContext).inflate(R.layout.sekconv_study_exercise_popup, null);
		this.setContentView(localView);

		imgMedal = (ImageView) localView.findViewById(R.id.imgMedal);

		mBtnCancel = (Button)localView.findViewById(R.id.btnCancel);
		mBtnCancel.setOnClickListener(this);

		btnStudy = (Button) localView.findViewById(R.id.btnStudy);
		btnApp = (Button) localView.findViewById(R.id.btnApp);
		btnTest = (Button) localView.findViewById(R.id.btnTest);

		btnStudy.setOnClickListener(this);
		btnApp.setOnClickListener(this);
		btnTest.setOnClickListener(this);

		textTitle = (TextView) findViewById(R.id.textTitle);
		textDescription = (TextView) findViewById(R.id.textDescription);

		ESUtils.setTextViewTypeFaceByRes(mContext, textTitle, Define.getMainFont());
		ESUtils.setTextViewTypeFaceByRes(mContext, textDescription, Define.getMainFont());

	}
	
	public void setListener(OnSEKLaunchListener listener) {
		mListener = listener;
	}

	public void setInfo(int nUserId, int nParam){
		m_nUserID = nUserId;
		m_nParam = nParam;

		int episodeNum = m_nParam / 9;
		int stageNum = m_nParam % 9;

		ProgressDBManager progressDBManager = new ProgressDBManager(mContext, m_nUserID);
		int nLevel = progressDBManager.getLevel();

		ProgressData data = new ProgressData(m_nUserID, nLevel, episodeNum, stageNum);
		int nMarks = progressDBManager.getMarks(data);

		if (nMarks >= 90){
			imgMedal.setImageResource(R.drawable.medal5);
		}else if(nMarks >= 80){
			imgMedal.setImageResource(R.drawable.medal4);
		}else if(nMarks >= 70){
			imgMedal.setImageResource(R.drawable.medal3);
		}else if(nMarks > 0) {
			imgMedal.setImageResource(R.drawable.medal2);
		}
	}

	@Override
	public void dismiss() {
	    super.dismiss();
	}


	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		ESUtils.playBtnSound(mContext);
		switch (arg0.getId()) {
			case R.id.btnCancel:
				//mListener.onOK();
				this.dismiss();
				break;
			case R.id.btnStudy:
				mListener.onOK(0);
				this.dismiss();

				break;
			case R.id.btnTest:
				mListener.onOK(1);
				this.dismiss();

				break;
			case R.id.btnApp:
				mListener.onOK(2);
				this.dismiss();

				break;
			default:
				break;
				
		}
	}
	
	public abstract interface OnSEKLaunchListener {
		public abstract void onOK(int nStep);
	}

}
