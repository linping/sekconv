package com.yj.sentence.ui.studding.study;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.yj.sentence.R;
import com.yj.sentence.common.Define;
import com.yj.sentence.database.ProgressDBManager;
import com.yj.sentence.structs.StageData;
import com.yj.sentence.ui.studding.SekWordPopup;
import com.yj.sentence.utils.ESUtils;

public class StudyPrepareActivity extends Activity implements View.OnClickListener {


    private ImageButton btnBack;
    private ImageButton btnStart;

    private TextView textLevel;
    private TextView textEpisode;
    private TextView textStage;
    private TextView textTitle;

    private int nUserId;
    private int nParam;

    private ProgressDBManager mProgressManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_study_prepare);

        Intent intent = this.getIntent();
        nUserId = intent.getIntExtra(Define.USER_ID, -1);
        nParam = intent.getIntExtra(Define.SCHEDULE_ID, -1);

        if(!openDB())
        {
            finish();
            return;
        }

        initViews();
    }

    private boolean openDB(){
        mProgressManager = new ProgressDBManager(this, nUserId);
        if(mProgressManager == null)
            return false;

        return  true;
    }
    private void initViews(){
        btnBack = (ImageButton) findViewById(R.id.btnBack);
        btnStart = (ImageButton) findViewById(R.id.btnStart);
        textLevel = (TextView) findViewById(R.id.textLevel);
        textEpisode = (TextView) findViewById(R.id.textEpisode);
        textStage = (TextView) findViewById(R.id.textStage);
        textTitle = (TextView) findViewById(R.id.textTitle);

        ESUtils.setTextViewTypeFaceByRes(this, textLevel, Define.getMainFont());
        ESUtils.setTextViewTypeFaceByRes(this, textEpisode, Define.getMainFont());
        ESUtils.setTextViewTypeFaceByRes(this, textStage, Define.getMainFont());
        ESUtils.setTextViewTypeFaceByRes(this, textTitle, Define.getMainFont());
        ESUtils.setTextViewTypeFaceByRes(this, (TextView) findViewById(R.id.textSymbol), Define.getMainFont());
        ESUtils.setTextViewTypeFaceByRes(this, (TextView) findViewById(R.id.textDesc1), Define.getMainFont());
        ESUtils.setTextViewTypeFaceByRes(this, (TextView) findViewById(R.id.textDesc2), Define.getMainFont());
        ESUtils.setTextViewTypeFaceByRes(this, (TextView) findViewById(R.id.textDesc3), Define.getMainFont());

        btnBack.setOnClickListener(this);
        btnStart.setOnClickListener(this);

        //set information
        int userLevel = mProgressManager.getLevel();
        int epiNum = nParam/9;
        int stgNum = nParam%9;

        StageData stgData = mProgressManager.getStageData(userLevel, epiNum, stgNum);
        String stgname = stgData.getStgname();

        String strLevel = String.format(getString(R.string.string_format_level), userLevel);
        String strEpisode = String.format(getString(R.string.string_format_episode), epiNum);
        String strStage = String.format(getString(R.string.string_format_stage), stgNum);

        textLevel.setText(strLevel);
        textEpisode.setText(strEpisode);
        textStage.setText(strStage);
        textTitle.setText(stgname);

    }


    @Override
    public void onClick(View v) {
        ESUtils.playBtnSound(this);
        switch (v.getId()){
            case R.id.btnBack:
                finishStudy();
                break;
            case R.id.btnStart:
                startStudy();
                break;
        }
    }

    private void startStudy(){
        Intent intent = new Intent(StudyPrepareActivity.this, StudyBasicActivity.class);
        intent.putExtra(Define.USER_ID, nUserId);
        intent.putExtra(Define.SCHEDULE_ID, nParam);
        startActivity(intent);

        finish();
    }

    private void showBackDlg(){
		/*new AlertDialog.Builder(this)
		.setTitle(this.getString(R.string.notice))
		.setCancelable(false)
		.setMessage(this.getString(R.string.study_back))
		.setPositiveButton(this.getString(R.string.yes), new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				dialog.cancel();
				StudyWordActivity.this.finish();

			}
		})
		.setNegativeButton(this.getString(R.string.cancel), new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				dialog.cancel();
			}
		})
		.show();*/



        SekWordPopup popup;

        popup = new SekWordPopup(StudyPrepareActivity.this);
        popup.setMessage(this.getString(R.string.study_back));
        popup.setListener(new SekWordPopup.OnSEKLaunchListener() {

            @Override
            public void onOK() {
                StudyPrepareActivity.this.finish();
            }

            @Override
            public void onCancel() {
                // TODO Auto-generated method stub
            }
        });

        popup.show();
    }

    private void finishStudy(){
        showBackDlg();
    }

    @Override
    public void onBackPressed() {
        // your code.
        finishStudy();

    }
}
