package com.yj.sentence.ui.studding.study;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.yj.sentence.R;
import com.yj.sentence.common.Define;
import com.yj.sentence.database.PAKDBManager;
import com.yj.sentence.database.ProgressDBManager;
import com.yj.sentence.database.StudyDBManager;
import com.yj.sentence.structs.ProgressData;
import com.yj.sentence.structs.WordData;
import com.yj.sentence.ui.studding.PracticeResultItemView;
import com.yj.sentence.ui.studding.test.StudyTestActivity;
import com.yj.sentence.utils.ESUtils;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by achilles on 3/18/17.
 */

public class StudyPracticeResultActivity extends Activity implements MediaPlayer.OnCompletionListener , View.OnClickListener{


    private ArrayList<WordData> m_arrWordData;
    private int nUserId;
    private int nParam;

    private StudyDBManager mStudyManager;
    private ProgressDBManager mProgressManager;
    private PAKDBManager m_pakManager = null;

    private FrameLayout frameContainer;
    private ListView listSentences;
    private PRItemAdapter listAdapter;

    private int m_nRunningIndex = -1;
    private MediaPlayer mMediaPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_study_practice_result);

        Intent intent = this.getIntent();
        nUserId = intent.getIntExtra(Define.USER_ID, -1);
        nParam = intent.getIntExtra(Define.SCHEDULE_ID, -1);

        if(!openDB()) {
            finish();
            return;
        }

        initData();
        initViews();
    }

    private void initViews(){
        listSentences = (ListView) findViewById(R.id.listSentences);
        listSentences.setFocusable(false);
        listAdapter = new PRItemAdapter();
        listSentences.setAdapter(listAdapter);

        ESUtils.setTextViewTypeFaceByRes(this, (TextView) findViewById(R.id.textTitle1), Define.getMainFont());
        ESUtils.setTextViewTypeFaceByRes(this, (TextView) findViewById(R.id.textTitle2), Define.getMainFont());
        ESUtils.setTextViewTypeFaceByRes(this, (TextView) findViewById(R.id.textTitle3), Define.getMainFont());

        frameContainer = (FrameLayout) findViewById(R.id.frameContainer);
        frameContainer.setVisibility(View.INVISIBLE);


    }

    private void initData(){
        int userLevel = mProgressManager.getLevel();
        int epiNum = nParam/9;
        int stgNum = nParam%9;

        ProgressData data = new ProgressData(nUserId, userLevel, epiNum, stgNum);
        m_arrWordData = mStudyManager.getPracticeList(data);

        //mMediaPlayer = new MediaPlayer();
        //mMediaPlayer.setOnCompletionListener(this);
    }

    private boolean openDB(){
        mProgressManager = new ProgressDBManager(this, nUserId);
        if(mProgressManager == null)
            return false;

        mStudyManager = new StudyDBManager(this, nUserId);
        if(mStudyManager == null)
            return false;

        m_pakManager = new PAKDBManager(this);
        if(m_pakManager == null)
            return false;
        return  true;
    }

    private void startPlay(int nIndex){

        WordData data = m_arrWordData.get(nIndex);

        final String strAudioPath = getAudioFileName(Integer.parseInt(data.getSound()));
        //final String strAudioPath = Define.SOUND_PATH + String.format("%s.mp3", data.getSound());


        new Handler().postDelayed(new Runnable() {
            public void run() {
                try {
                    if(mMediaPlayer != null){
                        mMediaPlayer.stop();
                        mMediaPlayer = null;
                    }
                    mMediaPlayer = new MediaPlayer();
                    mMediaPlayer.setDataSource(StudyPracticeResultActivity.this, Uri.parse(strAudioPath));
                    mMediaPlayer.setOnCompletionListener(StudyPracticeResultActivity.this);
                    mMediaPlayer.prepare();
                    mMediaPlayer.start();

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }, 10);


//        mMediaPlayer.stop();
//
//        WordData data = m_arrWordData.get(nIndex);
//        String strAudioPath = Define.SOUND_PATH + String.format("%s.mp3", data.getSound());
//
//        try {
//            mMediaPlayer.setDataSource(this, Uri.parse(strAudioPath));
//            mMediaPlayer.start();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

        m_nRunningIndex = nIndex;
    }

    private void stopPlay(){

/*        mMediaPlayer.stop();*/
        m_nRunningIndex = -1;
    }

    Handler m_playHandler = new Handler(Looper.getMainLooper()) {
        public void handleMessage(Message msg) {
            int nData = msg.what;
            int nPrefix = nData / 1000;
            int nID = nData % 1000;

            boolean bStart = false;
            if(nPrefix == 2)
                bStart = true;
            else
                bStart = false;

            if(bStart){
                startPlay(nID);
            } else {
                stopPlay();
            }

            listAdapter.notifyDataSetChanged();
        }
    };

    @Override
    public void onCompletion(MediaPlayer mp) {
        m_nRunningIndex = -1;
        listAdapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View v) {
        ESUtils.playBtnSound(this);

        switch(v.getId()){
            case R.id.btnBack:
                doBack();
                break;
            case R.id.btnNext:
                doNext();
                break;
        }
    }

    boolean bFinish = false;
    private void doNext(){
        if(bFinish ==false && StudyTestActivity.m_layoutTestResultMedal != null){
            frameContainer.addView(StudyTestActivity.m_layoutTestResultMedal);
            frameContainer.setVisibility(View.VISIBLE);
            bFinish = true;
        }else{
            finish();

        }
    }

    private void doBack(){
        finish();
    }
    private class PRItemAdapter extends BaseAdapter{

        @Override
        public int getCount() {
            return m_arrWordData.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            PracticeResultItemView viewItem = new PracticeResultItemView(StudyPracticeResultActivity.this);
            WordData itemData = m_arrWordData.get(position);
            String strEnglish = itemData.getMeanEn();
            String strKorean = itemData.getMeanKr();
            viewItem.setInfo(strEnglish, strKorean);
            viewItem.setHandler(m_playHandler);
            viewItem.setIndex(position);
            viewItem.setMarks(StudyPracticeActivity.arrayMarks[position]);

            if(position == m_nRunningIndex){
                viewItem.setRunning(true);
            }else{
                viewItem.setRunning(false);
            }

            return viewItem;
        }
    }

    public String getImageFileName(int nWordID){
        return m_pakManager.getFileName(nWordID, 0);
    }

    public String getAudioFileName(int nWordID){
        return m_pakManager.getFileName(nWordID, 1);
    }

    public String getVideoFileName(int nWordID){
        return m_pakManager.getFileName(nWordID, 2);
    }
}
