package com.yj.sentence.ui.usereg;

import com.yj.sentence.R;
import com.yj.sentence.common.Define;
import com.yj.sentence.utils.ESUtils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

public class UserListItem extends FrameLayout {
	
	private Context m_context;
	private OnUserItemListener m_listener;
	
	private TextView txtName;
	private TextView txtJob;
	private ImageView ivPhoto;
	private ImageView ivPhotoMask;
	private ImageButton btnEdit;
	private ImageButton btnDel;
	private Button btnActive;
	private Button btnSelect;
	private ImageView[] ivStar;
	
	private int mIndex;

	public UserListItem(Context context) {
		super(context);

		m_context = context;
		
		initLayout();
		initial();
	}
	
	public UserListItem(Context context, AttributeSet attrs) {
		super(context, attrs);

		m_context = context;
		
		initLayout();
		initial();
	}

	public UserListItem(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		
		m_context = context;
		
		initLayout();
		initial();
	}
	
	private void initLayout() {
		View v = LayoutInflater.from(m_context).inflate(R.layout.user_data, null);
		this.addView(v);
	}
	
	private void initial() {
		initVariables();
		initView();
		initEvent();
	}
	
	private void initVariables() {
		m_listener = null;
		mIndex = -1;
	}
	
	private void initView() {
		txtName = (TextView)findViewById(R.id.user_name);
		txtJob = (TextView)findViewById(R.id.user_job);

		ESUtils.setTextViewTypeFaceByRes(m_context, txtName, Define.getMainFont());
		ESUtils.setTextViewTypeFaceByRes(m_context, txtJob, Define.getMainFont());

		ivPhoto = (ImageView)findViewById(R.id.user_photo);
		ivPhotoMask = (ImageView)findViewById(R.id.user_photoMask);
		btnEdit = (ImageButton)findViewById(R.id.btnEdit);
		btnDel = (ImageButton)findViewById(R.id.btnDel);
		btnActive = (Button)findViewById(R.id.btnActive);
		btnSelect = (Button)findViewById(R.id.btnSelect);
		
		ivStar = new ImageView[5];
		ivStar[0] = (ImageView)findViewById(R.id.user_star1);
		ivStar[1] = (ImageView)findViewById(R.id.user_star2);
		ivStar[2] = (ImageView)findViewById(R.id.user_star3);
		ivStar[3] = (ImageView)findViewById(R.id.user_star4);
		ivStar[4] = (ImageView)findViewById(R.id.user_star5);
		
		ivPhoto.setVisibility(View.INVISIBLE);
		ivPhotoMask.setVisibility(View.INVISIBLE);
		
		for (int i=0; i<ivStar.length; i++) {
			ivStar[i].setVisibility(View.INVISIBLE);
		}
	}
	
	private void initEvent() {
		txtName.setOnClickListener(new View.OnClickListener() {			
			@Override
			public void onClick(View v) {
				ESUtils.playBtnSound(m_context);
				m_listener.onActive(mIndex);
			}
		});

		txtJob.setOnClickListener(new View.OnClickListener() {			
			@Override
			public void onClick(View v) {
				ESUtils.playBtnSound(m_context);

				m_listener.onActive(mIndex);
			}
		});
		
		btnSelect.setOnClickListener(new View.OnClickListener() {			
			@Override
			public void onClick(View v) {
				ESUtils.playBtnSound(m_context);

				m_listener.onActive(mIndex);
			}
		});

		btnEdit.setOnClickListener(new View.OnClickListener() {			
			@Override
			public void onClick(View v) {
				ESUtils.playBtnSound(m_context);

				m_listener.onEdit(mIndex);
			}
		});
		
		btnDel.setOnClickListener(new View.OnClickListener() {			
			@Override
			public void onClick(View v) {
				ESUtils.playBtnSound(m_context);

				m_listener.onDelete(mIndex);
			}
		});
		
		btnActive.setOnClickListener(new View.OnClickListener() {			
			@Override
			public void onClick(View v) {
				ESUtils.playBtnSound(m_context);

				m_listener.onPhoto(mIndex);
			}
		});
	}
	
	public void setName(String name) {
		txtName.setText(name);
	}
	
	public void setJob(String job) {
		txtJob.setText(job);
	}
	
	public void setSelect() {
		ivPhotoMask.setImageResource(R.drawable.user_photo_mask_1);
	}
	
	@SuppressLint("NewApi")
	public void setPhoto(String path) {
		if (path != null && !path.isEmpty()) {
			Bitmap bmpPhoto = BitmapFactory.decodeFile(path);
			if (bmpPhoto != null)
			{
				ivPhoto.setImageBitmap(bmpPhoto);
				ivPhoto.setVisibility(View.VISIBLE);
			}
			else
			{
				ivPhoto.setVisibility(View.INVISIBLE);
			}
		} else {
			ivPhoto.setVisibility(View.INVISIBLE);
		}
		ivPhotoMask.setVisibility(View.VISIBLE);
	}
	
	public void setLevel(int value) {
		if (value <= ivStar.length) {
			for (int i=0; i<value; i++) {
				ivStar[i].setVisibility(View.VISIBLE);
			}
		}
	}
	
	public void setListener(OnUserItemListener m_listener) {
		this.m_listener = m_listener;
	}
	
	public void setIndex(int mIndex) {
		this.mIndex = mIndex;
	}

	public abstract interface OnUserItemListener {
		public abstract void onActive(int index);
		public abstract void onPhoto(int index);
		public abstract void onEdit(int index);
		public abstract void onDelete(int index);
	}

}
