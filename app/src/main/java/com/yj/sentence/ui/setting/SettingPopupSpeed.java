package com.yj.sentence.ui.setting;

import com.yj.sentence.R;
import com.yj.sentence.common.Define;
import com.yj.sentence.database.SettingDBManager;
import com.yj.sentence.utils.ESUtils;

import android.app.Dialog;
import android.content.Context;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;


public class SettingPopupSpeed extends Dialog implements View.OnClickListener, View.OnKeyListener {
	
	private Context mContext;
	private Button mBtnOk;
	private Button mBtnCancel;
	private Button[] mItems;
	
	private SettingDBManager mDbMana = null;
	private OnOkListener mListener;
	
	public SettingPopupSpeed(Context context) {
		super(context, R.style.MyDialog);
		
		// TODO Auto-generated constructor stub
		this.mContext = context;
		initViews();
	}
	
	public void initValues() {
		this.openDB();
		this.setSelectedItem(mDbMana.getStudySpeedIndex());
	}
	
	private void initViews() {
		View localView = LayoutInflater.from(this.mContext).inflate(R.layout.setting_popup_speed, null);
		this.setContentView(localView);
		
		mItems = new Button[5];
		mItems[0] = (Button)localView.findViewById(R.id.btnItem1);
		mItems[1] = (Button)localView.findViewById(R.id.btnItem2);
		mItems[2] = (Button)localView.findViewById(R.id.btnItem3);
		mItems[3] = (Button)localView.findViewById(R.id.btnItem4);
		mItems[4] = (Button)localView.findViewById(R.id.btnItem5);
		mBtnOk = (Button)localView.findViewById(R.id.btnSettingOk);
		mBtnCancel = (Button)localView.findViewById(R.id.btnSettingCancel);		
		
		for (int i=0; i<mItems.length; i++) {
			mItems[i].setOnClickListener(this);
			ESUtils.setButtonTypeFaceByRes(mContext, mItems[i], Define.getMainFont());
		}

		mBtnOk.setOnClickListener(this);
		mBtnOk.setOnClickListener(this);
		mBtnCancel.setOnClickListener(this);
	}
	
	public void setSelectedItem(int pos) {
		for (int i=0; i<mItems.length; i++)
			mItems[i].setSelected(false);
		
		if (pos < 0) return;
		
		mItems[pos].setSelected(true);
	}
	
	@Override
	public void dismiss() {
		this.closeDB();		
	    super.dismiss();
	}

	@Override
	public boolean onKey(View arg0, int arg1, KeyEvent arg2) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		ESUtils.playBtnSound(mContext);
		switch (v.getId()) {
			case R.id.btnItem1:
				setSelectedItem(0);
				break;
			case R.id.btnItem2:
				setSelectedItem(1);
				break;
			case R.id.btnItem3:
				setSelectedItem(2);
				break;
			case R.id.btnItem4:
				setSelectedItem(3);
				break;
			case R.id.btnItem5:
				setSelectedItem(4);
				break;
			case R.id.btnSettingOk:
				if (this.openDB()) {
					for (int i=0; i<mItems.length; i++) {
						if (mItems[i].isSelected())
							mDbMana.setStudySpeedValue(i);
					}
				}
				
				mListener.onOK();
				dismiss();
				break;
			case R.id.btnSettingCancel:
				dismiss();
				break;
		}
	}
	
	private boolean openDB() {
		if (mDbMana == null)
			mDbMana = new SettingDBManager(mContext);
		
		if (mDbMana == null)
			return false;
		
		return true;
	}
	
	private void closeDB() {
		if (mDbMana != null) {
			mDbMana.close();
			mDbMana = null;
		}
	}
	
	public void setOkListener(OnOkListener listener) {
		mListener = listener;
	}
	
	public abstract interface OnOkListener {
		public abstract void onOK();
	}

}
