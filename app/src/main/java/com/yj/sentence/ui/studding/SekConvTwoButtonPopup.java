package com.yj.sentence.ui.studding;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.yj.sentence.R;
import com.yj.sentence.common.Define;
import com.yj.sentence.utils.ESUtils;


public class SekConvTwoButtonPopup extends Dialog implements View.OnClickListener {

	private Context mContext;
	private Button mBtnOK;
	private Button mBtnCancel;

	private TextView textDescription;

	private OnSEKLaunchListener mListener;

	public SekConvTwoButtonPopup(Context context) {
		super(context, R.style.MyDialog);
		// TODO Auto-generated constructor stub
		this.mContext = context;
		this.setCancelable(false);
		initViews();
	}
	
	private void initViews() {
		View localView = LayoutInflater.from(this.mContext).inflate(R.layout.sekconv_popup_twobutton, null);
		this.setContentView(localView);
		
		mBtnOK = (Button)localView.findViewById(R.id.btnOk);
		mBtnOK.setOnClickListener(this);

		mBtnCancel = (Button) localView.findViewById(R.id.btnCancel);
		mBtnCancel.setOnClickListener(this);

		textDescription = (TextView) findViewById(R.id.textDescription);
		ESUtils.setTextViewTypeFaceByRes(mContext, textDescription, Define.getMainFont());

	}
	
	public void setListener(OnSEKLaunchListener listener) {
		mListener = listener;
	}

	public void setInfo(String strDescription)
	{
		textDescription.setText(strDescription);
	}

	@Override
	public void dismiss() {
	    super.dismiss();
	}


	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		ESUtils.playBtnSound(mContext);
		switch (arg0.getId()) {
			case R.id.btnOk:
				mListener.onOK();
				this.dismiss();
				break;
			case R.id.btnCancel:
				this.dismiss();
				break;
		}
	}
	
	public abstract interface OnSEKLaunchListener {
		public abstract void onOK();
	}

}
